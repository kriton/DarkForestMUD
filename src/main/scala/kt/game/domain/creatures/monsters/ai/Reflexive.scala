package kt.game.domain.creatures.monsters.ai

import scala.concurrent.duration.DurationInt
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.InControl
import kt.game.domain.creatures.monsters.Behaviour
import kt.game.domain.creatures.monsters.Monster
import kt.game.events.LoggedEvent
import kt.game.events.MonsterEvent
import kt.game.domain.creatures.MonsterAction
import kt.game.domain.creatures.AttackType

trait Reflexive extends Monster with Behaviour {
  import context._
  import Reflexive._

  def updateCStrikeSchedule() = system.scheduler.scheduleOnce(actionFrequency, self, CounterStrike())
  allSchedules += updateCStrikeSchedule

  override def detailedDescription = "Looks to be minding its own business." + (if (super.detailedDescription != "") " " + super.detailedDescription else "")

  override def attackedReaction(attackRoll: Int, from: ActorRef) = spot()

  override def presence(result: Int, location: ActorRef) = {
    log(LoggedEvent(MonsterEvent("You are testing " + sender.path.name + "' s presence with a conviction of " + result)))
    if (result >= 0) {
      addAggro(sender, Monster.AGGRO_MIN * result)
    }
    if (priorityTarget().orNull == sender) {
      log(LoggedEvent(MonsterEvent("New priority target is " + sender.path.name + " with a convition of " + aggro.get(sender).get._1)))
    }
  }

  def counterStrike() = {
    val target = targetFrom(spottedCache.values.toSet)
    if (target != None) target.get ! Creature.CanMeleeAttackFrom(self, currentLocation, AttackType.Melee)
  }

  override def receive = ({
    case CounterStrike() => {
      updateCStrikeSchedule()
      if (actionCheck(MonsterAction.CounterStrike)) counterStrike()
    }
  }: Receive) orElse super.receive

}

object Reflexive {
  case class CounterStrike()
}