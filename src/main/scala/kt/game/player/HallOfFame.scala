package kt.game.player

import scala.collection.mutable.ListBuffer

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.items.AllObjects
import kt.game.domain.items.SuitItem
import kt.game.domain.items.TrinketItem
import kt.game.domain.items.WeaponItem
import kt.game.persistence.DBManager

class HallOfFame private (db: ActorRef) extends Actor {
  import HallOfFame._

  db ! DBManager.LoadHallOfFame()

  implicit val listingordering = orderingByRankFirstThenTime
  val listings = ListBuffer[Listing]()

  def receive = {
    case Print(player)                => player ! Player.HallOfFameListing(listings.sorted.toSeq)
    case x: Listing if (sender == db) => listings += x
    case x: Listing => {
      val old = listings.find { _.name == x.name }
      if (old != None) listings -= old.get
      listings += x; db ! DBManager.HallOfFameEntry(x)
    }
  }
}

object HallOfFame {
  def props(db: ActorRef) = Props(new HallOfFame(db))
  
  val NAME = "hallOfFame"
  
  case class Listing(name: String, password: String, level: Int, skillset: String, receipesKnown: Int, timePlayed: Long, weapon: String, suit: String, trinket: String, timeRetired: Long, talentsKnown: Int, alive: Boolean) {
    def rank = (level * 2) + (receipesKnown * 3) + (talentsKnown * 12) +
      (if (weapon == null) -10 else AllObjects.map.get(weapon).get.asInstanceOf[WeaponItem].bonuses.bonusSum) +
      (if (suit == null) -5 else AllObjects.map.get(suit).get.asInstanceOf[SuitItem].bonuses.bonusSum) +
      (if (trinket == null) -5 else AllObjects.map.get(trinket).get.asInstanceOf[TrinketItem].bonuses.bonusSum) +
      (if (alive) 75 else 0)
  }
  case class Print(player: ActorRef)

  val orderingByTime: Ordering[Listing] = Ordering.by { e => e.timePlayed }
  val orderingByRank: Ordering[Listing] = Ordering.by { e => -e.rank }
  val orderingByRankFirstThenTime: Ordering[Listing] = Ordering.by { e: Listing => (e, e) }(Ordering.Tuple2(orderingByRank, orderingByTime))

}