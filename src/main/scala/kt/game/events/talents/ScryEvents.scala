package kt.game.events.talents

import kt.game.events.NonCombatEvent
import akka.actor.ActorRef
import kt.game.events.talents.ScryEvents.FailureReason._
import kt.game.events.ActionFailedEvent

object ScryEvents {

  case object ScryExpiredEvent extends NonCombatEvent {
    override def toString = "You are no longer scrying your target."
  }

  case class ScrySucceededEvent(targetRef: ActorRef) extends NonCombatEvent {
    override def toString = "You are now scrying your target!"
  }

  case class ScryFailedEvent(reason: FailureReason, targetRef: Option[ActorRef]) extends NonCombatEvent with ActionFailedEvent {
    override def toString = reason match {
      case NoLongerSpotted => "Scry Failed. Target creature is no longer visible."
      case CheckFailure    => "Your scry attempt failed!"
      case OneScryAtAAtime => "You can only scry at one creature at a time."
    }
  }

  sealed trait FailureReason
  object FailureReason {
    case object NoLongerSpotted extends FailureReason
    case object CheckFailure extends FailureReason
    case object OneScryAtAAtime extends FailureReason
  }

}