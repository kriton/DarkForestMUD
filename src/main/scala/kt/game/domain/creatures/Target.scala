package kt.game.domain.creatures

import akka.actor.ActorRef

case class Target(key: Option[String], name: Option[String], ref: Option[ActorRef]) {
  override def toString = (key, name) match {
    case (Some(x), Some(n)) => s"<$x> $n"
    case (None, Some(n))    => n
    case (Some(x), None)    => s"target <$x>"
    case _                  => "unknown target"
  }
} //TODO: move to domain creatures package

object NoneTarget extends Target(None, None, None)