package kt.game.requests.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.ControlState
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.Stunned
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.characters.talents.Revive
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import akka.actor.ReceiveTimeout
import akka.actor.PoisonPill
import kt.game.events.ActionFailedReason._
import kt.game.events.talents.ReviveEvents.CleanseFailedEvent
import kt.game.events.ActionOnCoolDownEvent

class CleanseRequestHandler(character: ActorRef, target: String) extends Actor {
  import context._

  setReceiveTimeout(500 milliseconds)

  implicit val timeout = Timeout(200 milliseconds)
  val cooldown = Await.result(character ? Character.GetActionCooldown(Revive.action), timeout.duration).asInstanceOf[Option[Deadline]]
  if (cooldown == None || cooldown.get.isOverdue) {
    val creature = Await.result(character ? Creature.GetSpottedTarget(target), timeout.duration).asInstanceOf[Option[ActorRef]]
    if (creature != None) {
      val targetLocation = creature.get ? Creature.GetCurrentLocation()
      val charLocation = character ? Creature.GetCurrentLocation()
      (for {
        a <- targetLocation.mapTo[Option[ActorRef]]
        b <- charLocation.mapTo[Option[ActorRef]]
      } yield (a != None && a.get == b.get)).onSuccess {
        case true => {
          val targetState = Await.result(creature.get ? Creature.GetControlState(), timeout.duration).asInstanceOf[ControlState]
          if (targetState == Stunned) {
            val roll = Await.result(character ? Creature.Roll(SkillName.CRAF), timeout.duration).asInstanceOf[Option[Int]].get
            parent ! Revive.SetCleanseCD()
            if (roll - Revive.cleanseDC >= 0) creature.get ! Creature.StunnedConditionEnd()
            else character ! Creature.Message(CleanseFailedEvent(CheckFailure))
          } else {
            character ! Creature.Message(CleanseFailedEvent(InvalidState))
          }
        }
        case false => {
          character ! Creature.Message(CleanseFailedEvent(NotInSameLocation))
        }
      }
    } else {
      character ! Creature.Message(CleanseFailedEvent(CanNoLongerSpot))
    }
  } else {
    character ! Creature.Message(ActionOnCoolDownEvent(Some(Revive.action), cooldown.get.timeLeft.toSeconds))
  }

  def receive = {
    case ReceiveTimeout => self ! PoisonPill
  }
}

object CleanseRequestHandler {
  def props(character: ActorRef, target: String) = Props(new CleanseRequestHandler(character, target))
}