package kt.game.domain.locations

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import Location.Reset
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.Creature
import kt.game.domain.items.Object
import kt.game.domain.locations.Features.Heal
import kt.game.domain.locations.Features.Learn
import kt.game.domain.locations.Features.Train
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.tombstones.Tombstone
import kt.game.events.ActionFailedEvent
import kt.game.events.CurrentLocationDescriptionEvent
import kt.game.utility.Result
import kt.game.domain.locations.Features.Discover
import kt.game.domain.worldbuilder.WorldBuilder
import kt.game.events.MovementFailedEvent
import kt.game.events.MovementFailedReason
import kt.game.events.FeatureUseFailedEvent
import kt.game.events.CanNoLongerFindPathEvent
import kt.game.events.ItemNoLongerAvailableToPickUpEvent

class Location(val descriptors: Descriptor, option: Feature) extends Actor {
  import Location._
  import context._

  var creatures: Set[ActorRef] = Set()
  var tombstones: Set[ActorRef] = Set()

  val itemsCache: ListBuffer[Tuple3[Object, Integer, Deadline]] = ListBuffer()

  var nearbyLocations: Set[(ActorRef, ActorRef)] = Set() // link and location

  var optionNotRuined = true

  def check(roll: Int) = {
    checkLinks(roll)
    spotCurrentLocation(sender, descriptors ! roll, roll)
  }

  def checkLinks(roll: Int) = {
    nearbyLocations foreach { _._1 ! Link.SpotAttept(sender, roll) }
  }

  def enhanchedDescriptionCondition(result: Int, visibility: Boolean) = Result.criticalSteps(result) >= 1 && visibility

  def spotLinkedLocation(creature: ActorRef, fromLink: ActorRef, description: Descriptor, spotRoll: Int, DCtoSpot: Int, DCtoMoveTo: Int, visibleCreatures: Boolean) = {
    val result = spotRoll - DCtoSpot;
    if (enhanchedDescriptionCondition(result, visibleCreatures)) {
      creature ! Creature.Observed(self, fromLink, description, Some(option, optionNotRuined), DCtoMoveTo)
    } else {
      creature ! Creature.Observed(self, fromLink, description, None, DCtoMoveTo)
    }
    if (visibleCreatures) spotCreatures(creature, spotRoll)
  }

  def spotCurrentLocation(creature: ActorRef, description: Descriptor, spotRoll: Int) = {
    creature ! Creature.Message(CurrentLocationDescriptionEvent(description, option, optionNotRuined))
    spotCreatures(creature, spotRoll)
    if (Character.is(creature))
      itemsCache.foreach {
        case (item, dc, deadline) if (deadline.hasTimeLeft) => creature ! Character.Found(item)
        case x                                              => itemsCache -= x
      }
  }

  def spotCreatures(creature: ActorRef, spotRoll: Int) = creatures.foreach(c => if (c != creature) c ! Creature.SpottedBy(creature, spotRoll, self))

  def spotSpecificCreature(creatureToSpot: ActorRef, locationOfCreature: ActorRef, spotRoll: Int) = {
    val location = nearbyLocations.collectFirst { case e if locationOfCreature == e._2 => e }
    if (location != None) {
      location.get._1 ! Link.TargettedContinueSpotAttept(sender, spotRoll, creatureToSpot)
    } else {
      sender ! Creature.Message(CanNoLongerFindPathEvent(locationOfCreature))
    }
  }

  def descriptionWithFeature(startPoint: String) = {
    startPoint + (if (!option.isInstanceOf[Features.Discover]) ". " + (if (optionNotRuined) option.descriptionWhenAvaliable else option.descriptionWhenNotAvaliable) else "")
  }

  def moveAttempt(creature: ActorRef, roll: Int, to: ActorRef) = {
    val destination = nearbyLocations.filter(_._2 == to).headOption
    if (destination != None) {
      destination.get._1 ! Link.MoveAttempt(creature, roll)
    } else {
      creature ! Creature.Message(MovementFailedEvent(self, MovementFailedReason.CanNoLongerObserve, Some(to)))
      creature ! Creature.MoveFailed()
    }
  }

  def moveIn(creature: ActorRef, from: Option[ActorRef]) = {
    if (from != None) from.get ! TriggerLinkItems(creature)
    creatures foreach { _ ! Creature.MovedInYourArea(creature) }
    creatures += creature
    creature ! Creature.Moved(self, descriptors >)
  }

  def moveOut() = {
    creatures -= sender()
    creatures foreach { _ ! Creature.MovedOutOfYourArea(sender) }
  }

  def use(creature: ActorRef, useRoll: Int) = {
    val result = useRoll - option.dc()
    if (optionNotRuined && result >= 0) {
      creature ! option.messageOnUse
    } else {
      creature ! Creature.Message(FeatureUseFailedEvent(option.onError))
    }
    creatures foreach { c => if (c != creature) c ! Creature.UsedInYourArea(creature) }
  }

  def pickUp(item: Object) = {
    if (!itemsCache.filter(_._1 == item).isEmpty) {
      itemsCache -= itemsCache.filter(_._1 == item).head
      sender ! Character.PickedUp(item)
    } else {
      sender ! Creature.Message(ItemNoLongerAvailableToPickUpEvent)
    }
  }

  def addLinkItemToThisAndOptionallyToLinkedLocation(locationTo: ActorRef, itemDetails: LinkItem.Value, addToOtherDirection: Boolean) = {
    val newLinkedItem = context.actorOf(LinkItem.props(itemDetails))
    val link = nearbyLocations.filter(_._2 == locationTo).headOption
    if (link != None) {
      link.get._1 ! Link.AddItem(newLinkedItem)
      if (addToOtherDirection) locationTo ! Location.AddLinkItemFromLinkedLocation(self, newLinkedItem)
    }
  }

  def addLinkItemFromLocation(locationFrom: ActorRef, linkItem: ActorRef) = {
    val link = nearbyLocations.filter(_._2 == locationFrom).headOption
    if (link != None) {
      link.get._1 ! Link.AddItem(linkItem)
    }
  }

  def triggerLinkItemsTo(toLocation: ActorRef, creature: ActorRef) = {
    val link = nearbyLocations.filter(_._2 == toLocation).headOption
    if (link != None) link.get._1 ! Link.TriggerFor(creature, self, toLocation)

  }

  def receive = {
    case Check(roll)                                                             => check(roll)
    case SpotFrom(location, creature, description, roll, dcToSpot, dcToMove, vc) => spotLinkedLocation(creature, sender, description, roll, dcToSpot, dcToMove, vc)
    case MoveAttempt(creature, roll, to)                                         => moveAttempt(creature, roll, to)
    case MoveSuccess(creature, from)                                             => moveIn(creature, Some(from))
    case Position(creature)                                                      => moveIn(creature, None)
    case ContinueSpottingSpecificCreature(creature, location, roll)              => spotSpecificCreature(creature, location, roll)
    case MoveOut()                                                               => moveOut()
    case UseNonTravelOptionIfAvailable(roll)                                     => use(sender(), roll)
    case AddItem(item)                                                           => itemsCache += Tuple3(item, 0, (300 seconds).fromNow)
    case PickUp(item)                                                            => pickUp(item)
    case Tombstones(roll)                                                        => tombstones foreach { _ ! Tombstone.Spotted(sender, roll) }
    case AddTombstone(tombstone)                                                 => { tombstones += tombstone; tombstone ! Tombstone.AddedTombstone() }
    case RemoveTombstone(tombstone)                                              => tombstones -= tombstone
    case AddLink(to, description, spotDC, moveDC, willDC, fullVisibility)        => nearbyLocations += Tuple2(context.actorOf(Link.props(description, to, spotDC, moveDC, willDC, fullVisibility), to.path.name), to)
    case Reset()                                                                 => optionNotRuined = true
    case FeatureUsed()                                                           => { optionNotRuined = false; system.scheduler.scheduleOnce(option.frequency, self, Reset()) }
    case AddLinkItemToThis(location, item)                                       => addLinkItemToThisAndOptionallyToLinkedLocation(location, item, false)
    case AddLinkItemToThisAndToLinkedLocation(location, item)                    => addLinkItemToThisAndOptionallyToLinkedLocation(location, item, true)
    case AddLinkItemFromLinkedLocation(location, item)                           => addLinkItemFromLocation(location, item)
    case TriggerLinkItems(creature)                                              => triggerLinkItemsTo(sender, creature)
  }
}

object Location {
  def props(descriptors: Descriptor, option: Feature) = Props(new Location(descriptors, option))

  case class MoveAttempt(creature: ActorRef, moveRoll: Int, to: ActorRef)
  case class MoveSuccess(creature: ActorRef, from: ActorRef)
  case class Position(creature: ActorRef)
  case class SpotFrom(location: ActorRef, creature: ActorRef, description: Descriptor, spotRoll: Int, dcToSpot: Int, dcToMove: Int, visibleCreatures: Boolean)
  case class ContinueSpottingSpecificCreature(creatureToSpot: ActorRef, locationOfCreature: ActorRef, spotRoll: Int)
  case class MoveOut()
  case class UseTravelOptionIfAvailable()
  case class UseNonTravelOptionIfAvailable(useRoll: Int)
  case class AddItem(item: Object)
  case class PickUp(item: Object)
  case class Check(spotRoll: Int)
  case class Tombstones(spotRoll: Int)
  case class AddTombstone(tombstone: ActorRef)
  case class RemoveTombstone(tombstone: ActorRef)
  case class AddLink(to: ActorRef, description: Descriptor, spotDC: Int, moveDC: Int, willDC: Int, fullVisibility: Boolean)
  case class Reset()
  case class FeatureUsed()
  case class AddLinkItemToThis(locationTo: ActorRef, itemDetails: LinkItem.Value)
  case class AddLinkItemToThisAndToLinkedLocation(locationTo: ActorRef, itemDetails: LinkItem.Value)
  case class AddLinkItemFromLinkedLocation(location: ActorRef, item: ActorRef)
  case class TriggerLinkItems(creature: ActorRef)

  def is(actor: ActorRef) = actor.path.elements.toSet.contains(WorldBuilder.NAME)
}
