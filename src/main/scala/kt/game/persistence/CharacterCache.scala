package kt.game.persistence

import scala.collection.mutable.Map
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.items.Object
import kt.game.domain.items.Receipe
import kt.game.domain.items.SuitItem
import kt.game.domain.items.TrinketItem
import kt.game.domain.items.WeaponItem
import kt.game.domain.creatures.characters.Character

class CharacterCache private extends Actor {
  import CharacterCache._
  import context._

  val cache: Map[ActorRef, Tuple2[Deadline, Int]] = Map()

  def handle(character: Character.SnapshotV003) = {
    val cached = cache.get(sender)
    if (cached == None || cached.get._1.isOverdue || cached.get._2 != character.hashCode) {
      cache.put(sender, Tuple2((180 seconds).fromNow, character.hashCode))
      parent ! DBManager.WriteCharacter(character.characterName, character)
    }
  }

  def receive = {
    case x: Character.SnapshotV003 => handle(x)
    case Remove(character)         => cache -= character
  }
}

object CharacterCache {
  def props() = Props(new CharacterCache())
  case class Remove(character: ActorRef)
}

