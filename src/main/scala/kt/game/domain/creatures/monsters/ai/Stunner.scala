package kt.game.domain.creatures.monsters.ai

import scala.annotation.migration
import scala.concurrent.duration.DurationInt
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.MonsterAction
import kt.game.domain.creatures.monsters.Behaviour
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.LoggedEvent
import kt.game.events.MonsterEvent
import kt.game.utility.Result
import kt.game.requests.talents.MonsterStunRequestHandler
import kt.game.domain.creatures.AttackType

trait Stunner extends Monster with Behaviour {
  import context._

  def stunFrequency = 30 - fullSkillModifer(SkillName.ATHL) seconds

  var stunCD = stunFrequency.fromNow

  override def meleeAttack(creature: ActorRef, attackType: AttackType) = {
    super.meleeAttack(creature, attackType)
    if (stunCD.isOverdue && aggroOf(creature) > Monster.AGGRO_MID) {
      actorOf(MonsterStunRequestHandler.props(self, creatureType, creature, roll(skillset.athletics)))
      stunCD = stunFrequency.fromNow
    }
  }

}

object Stunner {
}