package kt.game.requests

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.DurationLong
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import kt.game.player.PlayerRegistry
import kt.game.system.FeedbackBuffer
import kt.game.events.LoggedEvent
import kt.game.events.StatusEvent
import kt.game.events.InfoEvent
import kt.game.events.InfoEvents.OnlineCharacterListingEvent

class ListRequestHandler private (val player: ActorRef, val playerRegistry: ActorRef, val ordering: String) extends Actor {
  import ListRequestHandler._

  context.setReceiveTimeout(100 milliseconds)

  playerRegistry ! PlayerRegistry.ListAll()

  val listings = ListBuffer[Listing]()

  implicit val listingordering = if (ordering == "name") orderingByNameFirstThenLevel else if (ordering == "time") orderingByTimeFirstThenLevel else orderingByLevelFirstThenName

  def receive = {
    case x: Listing => listings += x
    case ReceiveTimeout => {
      player ! FeedbackBuffer.Feedback(LoggedEvent(OnlineCharacterListingEvent(listings.sorted.toSeq)))
      self ! PoisonPill
    }
  }
}

object ListRequestHandler {
  def props(player: ActorRef, playerRegistry: ActorRef, ordering: String) = Props(new ListRequestHandler(player, playerRegistry, ordering))

  case class Listing(name: String, level: Int, currentLocation: String, receipesKnown: Int, timePlayed: Long)

  val orderingByName: Ordering[Listing] = Ordering.by { e => e.name }
  val orderingByLevel: Ordering[Listing] = Ordering.by { e => -e.level }
  val orderingByTime: Ordering[Listing] = Ordering.by { e => e.timePlayed }

  val orderingByTimeFirstThenLevel: Ordering[Listing] = Ordering.by { e: Listing => (e, e) }(Ordering.Tuple2(orderingByTime, orderingByLevel))
  val orderingByNameFirstThenLevel: Ordering[Listing] = Ordering.by { e: Listing => (e, e) }(Ordering.Tuple2(orderingByName, orderingByLevel))
  val orderingByLevelFirstThenName: Ordering[Listing] = Ordering.by { e: Listing => (e, e) }(Ordering.Tuple2(orderingByLevel, orderingByName))
}

