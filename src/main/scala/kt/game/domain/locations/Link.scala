package kt.game.domain.locations

import scala.collection.mutable.Map
import scala.collection.mutable.Set
import scala.concurrent.duration.Deadline
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.skills.SkillName.SkillName
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.utility.Result
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.creatures.Creature
import kt.game.events.ActionFailedEvent
import kt.game.events.MovementFailedReason
import kt.game.events.MovementFailedEvent

class Link(description: Descriptor, to: ActorRef, spotDC: Int, moveDC: Int, willDC: Int, fullVisibility: Boolean) extends Actor {
  import Link._
  import context._

  val otherAccessDCs: Map[SkillName, Int] = Map()
  val items: Set[ActorRef] = Set()

  def spotAttempt(creature: ActorRef, fromLocation: ActorRef, roll: Int) = {
    val result = Result.criticalSteps(roll - spotDC)
    if (result >= 0) {
      to ! Location.SpotFrom(fromLocation, creature, description ! roll, roll, spotDC, moveDC, fullVisibility && result > 0)
    }
  }

  def targettedSpotAttempt(fromCreature: ActorRef, fromLocation: ActorRef, roll: Int, targetCreature: ActorRef) = {
    val result = Result.criticalSteps(roll - spotDC)
    if (result < 1 || !fullVisibility) {
      fromCreature ! Creature.NoLongerSpotting(targetCreature, to)
    }
  }

  def moveAttempt(byCreature: ActorRef, roll: Int) = if (roll - moveDC >= 0) {
    if (willDC > 0) {
      byCreature ! Creature.TestWillpowerForLocation()
    } else {
      to ! Location.MoveSuccess(byCreature, parent)
    }
  } else {
    byCreature ! Creature.Message(MovementFailedEvent(parent, MovementFailedReason.CheckFailure, Some(to)))
    byCreature ! Creature.MoveFailed()
  }

  def moveToScaryAttempt(byCreature: ActorRef, roll: Int) = if (roll - willDC >= 0) {
    to ! Location.MoveSuccess(byCreature, parent)
  } else {
    byCreature ! Creature.Message(MovementFailedEvent(parent, MovementFailedReason.ScaryLocation, Some(to)))
    byCreature ! Creature.MoveFailed()
  }

  def search(creature: ActorRef, roll: Int) = {
    items foreach { _ ! LinkItem.Search(roll, creature, to) }
  }

  def trigger(creature: ActorRef, from: ActorRef, to: ActorRef) = {
    items foreach { _ ! LinkItem.Trigger(creature) }
  }

  def receive = {
    case MoveTestResult(byCreature, roll)                      => moveToScaryAttempt(byCreature, roll)
    case MoveAttempt(byCreature, roll)                         => moveAttempt(byCreature, roll)
    case SpotAttept(byCreature, roll)                          => spotAttempt(byCreature, sender, roll)
    case AddItem(linkItem)                                     => { items += linkItem; linkItem ! LinkItem.ParentLink() }
    case ExpiredItem(linkItem)                                 => items -= linkItem
    case Search(roll)                                          => search(sender, roll)
    case TriggerFor(c, from, to)                               => trigger(c, from, to)
    case TargettedContinueSpotAttept(byCreature, roll, targer) => targettedSpotAttempt(byCreature, sender, roll, targer)
  }

  //def +(item: LinkItem) = Link(description, to, spotDC, moveDC, otherAccessDCs, items :+ item)
  //def refresh() = Link(description, to, spotDC, moveDC, otherAccessDCs, items.filter(_.deadline.hasTimeLeft))
}

object Link {
  def props(description: Descriptor, to: ActorRef, spotDC: Int, moveDC: Int, willDC: Int, fullVisibility: Boolean) = Props(new Link(description, to, spotDC, moveDC, willDC, fullVisibility))
  case class MoveTestResult(byCreature: ActorRef, roll: Int)
  case class MoveAttempt(byCreature: ActorRef, roll: Int)
  case class SpotAttept(byCreature: ActorRef, roll: Int)
  case class TargettedContinueSpotAttept(byCreature: ActorRef, roll: Int, targetCreature: ActorRef)
  case class Search(roll: Int)
  case class AddItem(linkItem: ActorRef)
  case class ExpiredItem(linkItem: ActorRef)
  case class TriggerFor(creature: ActorRef, from: ActorRef, to: ActorRef)
}