package kt.game.domain.creatures.monsters.ai

import scala.annotation.migration
import scala.concurrent.duration.DurationInt
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.MonsterAction
import kt.game.domain.creatures.monsters.Behaviour
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.LoggedEvent
import kt.game.events.MonsterEvent
import kt.game.utility.Result
import kt.game.requests.talents.MonsterStunRequestHandler
import kt.game.requests.talents.MonsterRootRequestHandler
import kt.game.domain.creatures.AttackType

trait Rooter extends Monster with Behaviour {
  import context._

  def rootFrequency = 30 - fullSkillModifer(SkillName.ATHL) seconds

  var rootCD = rootFrequency.fromNow

  override def meleeAttack(creature: ActorRef, attackType: AttackType) = {
    super.meleeAttack(creature, attackType)
    if (rootCD.isOverdue) {
      actorOf(MonsterRootRequestHandler.props(self, spottedCache.values.toSet, currentLocation, roll(skillset.athletics)))
      rootCD = rootFrequency.fromNow
    }
  }

}

object Rooter {
}