package kt.game.requests.talents

import akka.pattern.ask
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.characters.talents.Trap
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.items.Object
import kt.game.domain.locations.LinkItem
import kt.game.domain.locations.Location
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.locations.descriptors.Descriptors
import kt.game.utility.Dice
import kt.game.utility.Result
import kt.game.domain.items.Rarity
import kt.game.domain.items.Common
import kt.game.domain.items.Rare
import kt.game.domain.items.Exotic
import scala.concurrent.Await
import kt.game.domain.creatures.AttackType
import kt.game.events.talents.TrapEvents.TrapFailedEvent
import kt.game.events.ActionFailedReason

class TrapRequestHandler private (character: ActorRef, locationRef: String, componentRef: Option[String]) extends Actor {
  import TrapRequestHandler._
  import context._

  setReceiveTimeout(200 milliseconds)

  implicit val timeout = Timeout(100 milliseconds)
  var location: Option[(ActorRef, Int, ActorRef)] = None
  character ! Creature.GetObservedLocation(locationRef)

  var moveRoll: Option[Int] = None
  character ! Creature.RollAndGetWithName(SkillName.ATHL)

  var placeTrapRoll: Option[Int] = None
  character ! Creature.RollAndGetWithName(SkillName.REFL)

  var fromLocation: Option[ActorRef] = None
  character ! Creature.GetCurrentLocation()

  var componentUsed: Option[Object] = None
  if (componentRef != None) character ! Creature.GetInventoryItem(componentRef.get)

  def receive = {
    case item: Object                                                               => componentUsed = Some(item)
    case Some(loc: ActorRef)                                                        => fromLocation = Some(loc)
    case Some((loc: ActorRef, descr: Descriptor, moveDC: Int, linkToLoc: ActorRef)) => location = Some((loc, moveDC, linkToLoc))
    case (SkillName.ATHL, roll: Int)                                                => moveRoll = Some(roll)
    case (SkillName.REFL, roll: Int)                                                => placeTrapRoll = Some(roll)
    case ReceiveTimeout => {
      if (location == None || fromLocation == None) {
        parent ! Trap.TrapFailed()
        character ! Creature.Message(TrapFailedEvent(ActionFailedReason.CanNoLongerSpot))
      } else if (moveRoll == None || moveRoll.get < location.get._2) {
        parent ! Trap.TrapFailed()
        character ! Creature.Message(TrapFailedEvent(ActionFailedReason.InvalidState))
      } else if (placeTrapRoll == None || placeTrapRoll.get < Trap.DC) {
        parent ! Trap.TrapFailed()
        character ! Creature.Message(TrapFailedEvent(ActionFailedReason.CheckFailure))
      } else {
        val searchDC = placeTrapRoll.get - 5 + componentUsed.value
        val disableDC = placeTrapRoll.get + componentUsed.value
        val evadeDC = placeTrapRoll.get - 2 + componentUsed.value
        if (componentUsed == None || Await.result(character ? Creature.GetConsumedItem(componentUsed.get), 120 milliseconds).asInstanceOf[Boolean])
          fromLocation.get ! Location.AddLinkItemToThis(location.get._1, LinkItem.Value(Some(character), Descriptors.Trap, searchDC, SkillName.CRAF, disableDC, Map(SkillName.REFL -> evadeDC, SkillName.WEAP -> evadeDC), (placeTrapRoll.get / 3 minutes), creature => {
            if (creature != character) creature ! Creature.SufferedEnvironmentDamageBy(Result.criticalSteps(placeTrapRoll.get - 10) + 1, AttackType.Trap, Some(character), Map(SkillName.REFL -> (evadeDC - 10 + Dice.d20), SkillName.WEAP -> (evadeDC - 10 + Dice.d20)))
          }))
        parent ! Trap.TrapSuccess()
      }
      self ! PoisonPill
    }
  }
}

object TrapRequestHandler {
  def props(character: ActorRef, location: String, component: Option[String]) = Props(new TrapRequestHandler(character, location, component))
  implicit class toRarityValue(item: Option[Object]) {
    def value = if (item == None) 0 else item.get.rarity match {
      case Common => 1
      case Rare   => 3
      case Exotic => 5
    }
  }
}