package kt.game.requests

import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.Character
import kt.game.player.PlayerRegistry
import kt.game.events.ChatEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.LoggedEvent
import kt.game.events.NullEvent
import kt.game.player.Player
import kt.game.events.ChatEvents

class WhisperRequestHandler(val player: ActorRef, val playerRegistry: ActorRef, val text: String, val targetName: String) extends Actor {
  import Messages._

  context.setReceiveTimeout(250 milliseconds)
  player ! Character.GetName(self)

  var fromName: String = null

  def receive = {
    case NameOfPlayer(name, player) if (fromName == null) => {
      fromName = name
      playerRegistry ! PlayerRegistry.GetNames(self)
      this.player ! FeedbackBuffer.Feedback(LoggedEvent(NullEvent))
    }
    case NameOfPlayer(name, player) if (name == targetName) => {
      player ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.DirectedChatEvent(fromName, targetName, text)))
      player ! Player.LastWhisperer(this.player)
      this.player ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.DirectedChatAcknowledgementEvent(fromName, targetName)))
    }
    case ReceiveTimeout => self ! PoisonPill
    case _              =>
  }
}

object WhisperRequestHandler {
  def props(player: ActorRef, playerRegistry: ActorRef, text: String, targetName: String) = Props(new WhisperRequestHandler(player, playerRegistry, text, targetName))
}