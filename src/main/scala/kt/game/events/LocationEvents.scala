package kt.game.events

import akka.actor.ActorRef
import kt.game.domain.creatures.RelativeLocation
import kt.game.domain.locations.Features
import kt.game.domain.creatures.Target
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.locations.Feature
import kt.game.domain.items.Object

case class CurrentLocationDescriptionEvent(description: Descriptor, feature: Feature, featureNotRuined: Boolean) extends LocationEvent(40) {
  override def toString = "You are in " + description + (if (!feature.isInstanceOf[Features.Discover]) ". " + (if (featureNotRuined) feature.descriptionWhenAvaliable else feature.descriptionWhenNotAvaliable) else "")
}

case class CurrentLocationCreaturesSpottedEvent(target: Target, description: String, status: Option[String], relativeStrength: Option[Int], location: RelativeLocation) extends CreatureSpottedEvent(target, description, status, relativeStrength, location, 50)

case class LinkedLocationCreatureSpottedEvent(target: Target, description: String, status: Option[String], relativeStrength: Option[Int], location: RelativeLocation) extends CreatureSpottedEvent(target, description, status, relativeStrength, location)

abstract class CreatureSpottedEvent(target: Target, description: String, status: Option[String], relativeStrength: Option[Int], location: RelativeLocation, priority: Int = 100) extends LocationEvent(priority) {
  override def toString = s"You spotted <${target.key.getOrElse("")}>: at $location, $description. " + status.getOrElse("") + (relativeStrength match {
    case Some(x: Int) if (x < -20) => " Looks pathetic to you!"
    case Some(x: Int) if (x < -10) => " Looks much weaker than you!"
    case Some(x: Int) if (x < -2)  => " Looks weaker than you!"
    case Some(x: Int) if (x < 2)   => " Looks of equal strength to you!"
    case Some(x: Int) if (x < 5)   => " Looks stronger than you!"
    case Some(x: Int) if (x < 10)  => " Looks much stronger than you!"
    case Some(x: Int) if (x < 20)  => " Looks very dangerous!"
    case Some(x: Int)              => " Looks extremely dangerous!"
    case _                         => " You cannot discern his strength."
  })
}

case class LocationSpottedEvent(location: RelativeLocation, feature: Option[(Feature, Boolean)], moveDifficultyScale: Int) extends LocationEvent(60) {
  override def toString = s"You observed <${location.key.getOrElse("")}>: ${location.description.getOrElse("")}" + (if (feature == None) "" else {
    (if (!feature.get._1.isInstanceOf[Features.Discover]) ". " + (if (feature.get._2) feature.get._1.descriptionWhenAvaliable else feature.get._1.descriptionWhenNotAvaliable) else "")
  }) + ". " + (moveDifficultyScale match {
    case 0  => ""
    case 5  => "Moving to this location seems difficult"
    case 10 => "Moving to this location seems doable"
    case 20 => "Moving to this location seems impossible. Maybe there is another way..."
    case _  => "This is your current location"
  })
}

case class LinkItemSpottedEvent(key: String, creator: (Boolean, Target), description: Descriptor, disableDifficulty: Int, evadeDifficulty: Int, itemRef: ActorRef, ontheWayTo: RelativeLocation) extends LocationEvent(70) {
  override def toString = s"You located <$key>: " + (creator._1 match {
    case true  => s"your $description on the way to $ontheWayTo. You know how to easily disable it or evade it."
    case false => s"a $description on the way to $ontheWayTo. Disabling it looks ${difficultyMatch(disableDifficulty)}. Evading it looks ${difficultyMatch(evadeDifficulty)}."
  })

  def difficultyMatch(x: Int) = x match {
    case x if x > 1 => "very easy"
    case 1          => "easy"
    case 0          => "relatively easy"
    case -1         => "difficult"
    case -2         => "extremely difficult"
    case _          => "impossible"
  }
}

case class TombstoneSpottedEvent(key: String, tombstone: ActorRef, description: String, level: Int, defiled: Boolean) extends LocationEvent(90) {
  override def toString = s"You located a tombstone <$key>: $description"
}

case class ItemSpottedEvent(key: String, item: Object) extends LocationEvent(130) {
  override def toString = s"You found <$key>: ${item.description}"
}

case class FeatureUseFailedEvent(message: String) extends NonCombatEvent {
  override def toString = message
}

case class CanNoLongerFindPathEvent(location: ActorRef) extends NonCombatEvent {
  override def toString = "You cannot find the path to this area any more..."
}

case class TargetMovedToOtherLocationEvent(target: Target, newlocation: RelativeLocation) extends NonCombatEvent {
  override def toString = s"$target moved to $newlocation"
}

case class TargetGotAttackedByOtherEvent(victim: Target, attacker: Target, damage: Int) extends NonCombatEvent {
  override def toString =s"$victim got attacked for $damage damage by $attacker"
}

case class TargeLostEvent(target: Target, reason: TargetLostReason) extends NonCombatEvent {
  override def toString = s"No longer spotting $target."
}

sealed trait TargetLostReason
object TargetLostReason {
  case object Hidden extends TargetLostReason
  case object MovedAway extends TargetLostReason
  case object Unknown extends TargetLostReason
  
}

case class TargetHideFailedEvent(target: Target) extends NonCombatEvent {
  override def toString = s"$target tried to hide from you but failed!"
}

case class FolloweeLostEvent(target: Target) extends NonCombatEvent {
  override def toString = s"You can no longer spot $target to follow. You no logner know where he went!"
}

case object MonsterDroppedItemsEvent extends NonCombatEvent {
  override def toString = "Monster dropped some items to the ground!"
}
