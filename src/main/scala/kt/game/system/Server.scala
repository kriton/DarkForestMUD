package kt.game.system

import java.net.ServerSocket
import java.net.Socket
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.tombstones.TombstoneGenerator
import kt.game.domain.worldbuilder.WorldBuilder
import kt.game.domain.creatures.monsters.MonsterGenerator
import kt.game.persistence.DBManager
import kt.game.domain.creatures.characters.CharacterGenerator
import kt.game.player.HallOfFame
import kt.game.player.PlayerRegistry
import kt.game.player.gamemaster.GMRegistry
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import kt.game.domain.creatures.CreatureGenerator
import akka.actor.ActorRef
import scala.concurrent.Future

class Server extends Actor {
  import Server._
  import context._
  implicit val timeout = Timeout(5 seconds)

  val serverSocket = new ServerSocket(8386)
  val gmServerSocket = new ServerSocket(8287)
  val db = system.actorOf(DBManager.props("/data/darkForest/"), DBManager.NAME)
  val hallOfFame = system.actorOf(HallOfFame.props(db), HallOfFame.NAME)
  val creatureGenerator = system.actorOf(CreatureGenerator.props(db), CreatureGenerator.NAME)
  val monsterGenerator = Await.result((creatureGenerator ? CreatureGenerator.GetMonsterGenerator()).mapTo[ActorRef], timeout.duration)
  val builder = system.actorOf(WorldBuilder.props(monsterGenerator), WorldBuilder.NAME)
  print("Initializing world ....")
  Await.result(builder ? WorldBuilder.Initialize(), timeout.duration)
  println("done!")
  val tombstoneGenerator = system.actorOf(TombstoneGenerator.props(db, builder), TombstoneGenerator.NAME)
  creatureGenerator ! CreatureGenerator.IntroduceWorldBuilder(builder)
  creatureGenerator ! CreatureGenerator.IntroducerTombstoneGenerator(tombstoneGenerator)
  val characterGenerator = Await.result((creatureGenerator ? CreatureGenerator.GetCharacterGenerator()).mapTo[ActorRef], timeout.duration)
  val playerRegistry = system.actorOf(PlayerRegistry.props(hallOfFame, characterGenerator), PlayerRegistry.NAME)
  val gmRegistry = system.actorOf(GMRegistry.props("P123qwe", builder, playerRegistry, monsterGenerator, characterGenerator, tombstoneGenerator), GMRegistry.NAME)
  self ! Start()
  println("Server started ...")

  def receive = {
    case Start() => {
      println("Awaiting connection...")
      self ! Spin()
      self ! SpinGM()
    }
    case Spin() => Future {
      val clientSocket = serverSocket.accept()
      self ! Spin()
      val connector = context.actorOf(Connection.props(playerRegistry, builder, clientSocket))
      println("new player connected : " + connector.path + " from " + clientSocket.getRemoteSocketAddress.toString)
    }
    case SpinGM() => Future {
      val clientSocket = gmServerSocket.accept()
      self ! SpinGM()
      val connector = context.actorOf(GMConnection.props(gmRegistry, playerRegistry, builder, clientSocket))
      println("new gm connected : " + connector.path + " !")
    }
  }

  override val supervisorStrategy = {
    OneForOneStrategy() {
      case _: Exception => Stop
    }
  }
}

object Server {
  def props = Props(new Server())

  val NAME = "server"

  case class Start()
  case class Spin()
  case class SpinGM()
}