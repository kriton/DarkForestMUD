package kt.game.player

import scala.concurrent.duration.DurationLong
import Player.KeepAliveCheck
import Player.LastWhisperer
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.NotInfluenceReceiveTimeout
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.CCommand
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.characters.CharacterGenerator
import kt.game.domain.worldbuilder.WorldBuilder
import kt.game.events.ActionFailedEvent
import kt.game.system.FeedbackBuffer
import kt.game.system.FeedbackBuffer.Feedback
import kt.game.events.LoggedEvent
import kt.game.events.StatusEvent
import kt.game.events.InfoEvent
import kt.game.events.StatusEvents.HelpTextEvent
import kt.game.events.InfoEvents.CharacterControlSuccessEvent
import kt.game.events.InfoEvents.CharacterControlDroppedEvent
import kt.game.events.InfoEvents.CanOnlyControlOneCharacterEvent
import kt.game.events.InfoEvents.HallOfFameListingEvent
import kt.game.events.InfoEvents.CanNotControlYourCharacterEvent
import kt.game.events.InfoEvents.CanNotRetireForeignCharacterEvent

class Player(out: ActorRef, builder: ActorRef, ip: String, characterGenerator: ActorRef) extends Actor {
  import Player._
  import context._
  import kt.game.system.FeedbackBuffer.Feedback

  var character: ActorRef = null
  var inControl = true

  var lastWhisperedBy: Option[ActorRef] = None

  var keepAlive = true

  system.scheduler.scheduleOnce(3 seconds, self, KeepAliveCheck())
  def keepAliveCheck(): Boolean = {
    if (keepAlive) {
      keepAlive = false
      system.scheduler.scheduleOnce(3 seconds, self, KeepAliveCheck())
      false
    } else {
      println(" keep alive failure for " + self + ". Termination of player initiatied!")
      if (character != null) character ! Character.UnassignFromPlayer()
      system.scheduler.scheduleOnce(1 second, self, PoisonPill)
      true
    }
  }

  def controlStep1(name: String, password: String) = {
    if (character == null) characterGenerator ! CharacterGenerator.CanLoginCharacter(name, password)
    else self ! Feedback(LoggedEvent(CanOnlyControlOneCharacterEvent))
  }

  def controlStep2(character: ActorRef, name: String, password: String) = {
    character ! Character.AssignToPlayer(password)
  }

  def controlSuccess(name: String, requestNewLocation: Boolean) = {
    character = sender()
    self ! Feedback(LoggedEvent(CharacterControlSuccessEvent(name, character)))
    if (requestNewLocation) builder ! WorldBuilder.GetRandomStartingArea()
    parent ! PlayerRegistry.NotifyAll("character " + name + " is now online!")
  }

  def controlDropped(name: String) = {
    character = null
    self ! Feedback(LoggedEvent(CharacterControlDroppedEvent(name, sender)))
    parent ! PlayerRegistry.NotifyAll("character " + name + " is now offline!")
    println("character " + name + " is now offline!")
  }

  def dropControl() = {
    if (character != null) character ! Character.UnassignFromPlayer()
  }

  def createNewCharacter(characterName: String, password: String) = {
    if (character == null) characterGenerator ! CharacterGenerator.NewCharacter(characterName, password)
    else self ! Feedback(LoggedEvent(CanOnlyControlOneCharacterEvent))
  }

  def characterCreated(newCharacter: ActorRef, password: String) = {
    newCharacter ! Character.AssignToPlayer(password)
  }

  def characterRetired(listing: HallOfFame.Listing) = {
    parent ! PlayerRegistry.NotifyAll("character " + listing.name + " is now retired with a ranking of " + listing.rank + "!")
    parent ! listing
  }

  def retire(password: String) = {
    if (character == null) {
      self ! Feedback(LoggedEvent(CanNotRetireForeignCharacterEvent))
    } else {
      character ! Character.RetirementRequest(password)
    }
  }

  def printHallOfFame(listings: Seq[HallOfFame.Listing]) = {
    self ! Feedback(LoggedEvent(HallOfFameListingEvent(listings)))
  }

  def receive = {
    case event: Feedback                                     => out forward event
    case ControlTaken(by)                                    => inControl = false
    case ControlRegained()                                   => inControl = true
    case Control(name, password)                             => controlStep1(name, password)
    case CharacterCanBeLoggedIn(character, name, password)   => controlStep2(character, name, password)
    case Exit()                                              => dropControl()
    case ControlSuccess(name, requestNewLocation)            => controlSuccess(name, requestNewLocation)
    case ControlDropped(name)                                => controlDropped(name)
    case CreateNewCharacter(name, password)                  => createNewCharacter(name, password)
    case CharacterCreated(newCharacter, password)            => characterCreated(newCharacter, password)
    case HallOfFameCommand()                                 => parent ! PlayerRegistry.HallOfFameRequest()
    case HallOfFameListing(listings)                         => printHallOfFame(listings)
    case RetireCommand(password)                             => retire(password)
    case x: HallOfFame.Listing                               => characterRetired(x)
    case KeepAliveCheck()                                    => keepAliveCheck()
    case KeepAlive()                                         => keepAlive = true
    case LastWhisperer(player)                               => this.lastWhisperedBy = Some(player);
    case GetLastWhisperer() if (lastWhisperedBy != None)     => sender ! LastWhisperer(lastWhisperedBy.get)
    case Character.Help(text) if (character == null)         => out ! Feedback(LoggedEvent(HelpTextEvent(text)))
    case other: CCommand if (character != null && inControl) => character forward other
    case other: CCommand if (character != null)              => out ! Feedback(LoggedEvent(CanNotControlYourCharacterEvent))
    case other if (character != null)                        => character ! other
    case other                                               => println(other)
  }
}

object Player {
  def props(out: ActorRef, builder: ActorRef, ip: String, characterGenerator: ActorRef) = Props(new Player(out, builder, ip, characterGenerator))
  case class ControlTaken(by: ActorRef) extends NotInfluenceReceiveTimeout
  case class ControlRegained() extends NotInfluenceReceiveTimeout
  case class Control(name: String, password: String)
  case class CharacterCanBeLoggedIn(character: ActorRef, name: String, password: String)
  case class ControlSuccess(name: String, requestNewLocation: Boolean)
  case class ControlDropped(name: String)
  case class Exit()
  case class CreateNewCharacter(characterName: String, password: String)
  case class CharacterCreated(newCharacter: ActorRef, password: String)
  case class HallOfFameListing(listings: Seq[HallOfFame.Listing])
  case class HallOfFameCommand()
  case class RetireCommand(password: String)
  case class KeepAlive() extends NotInfluenceReceiveTimeout
  case class KeepAliveCheck() extends NotInfluenceReceiveTimeout
  case class LastWhisperer(player: ActorRef) extends NotInfluenceReceiveTimeout
  case class GetLastWhisperer() extends NotInfluenceReceiveTimeout
}