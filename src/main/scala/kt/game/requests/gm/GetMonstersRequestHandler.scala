package kt.game.requests.gm

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.monsters.LifeState
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.creatures.skills.Skillset.toQuickString
import kt.game.domain.items.Object
import kt.game.domain.creatures.monsters.MonsterGenerator
import kt.game.system.FeedbackBuffer
import kt.game.events.GMInfoEvent
import kt.game.events.LoggedEvent

class GetMonstersRequestHandler private (val monsterGenerator: ActorRef, filter: String => Boolean, task: Option[ActorRef => Unit]) extends Actor {
  import GetMonstersRequestHandler._
  import context._

  context.setReceiveTimeout(100 milliseconds)

  monsterGenerator ! MonsterGenerator.ListAll()

  val listings = ListBuffer[Listing]()

  def receive = {
    case x: Listing => listings += x
    case ReceiveTimeout if task == None => {
      parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent(listings.sorted.map(_.toString).filter(s => filter(s.replaceAll("\r\n", " "))).mkString("\r\n"))))
      self ! PoisonPill
    }
    case ReceiveTimeout => {
      val targetMonsters = listings.map(e => (e.toString, e.ref)).filter(s => filter(s._1.replaceAll("\r\n", " "))).map(_._2).toSet
      if (targetMonsters.isEmpty) {
        parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent("No creatures match the selection...")))
      } else {
        parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent("Command sent to " + targetMonsters.map(_.path.name).mkString(", ") + ".")))
        targetMonsters.foreach { task.get }
      }
      self ! PoisonPill
    }
  }
}

object GetMonstersRequestHandler {
  def props(monsterGenerator: ActorRef, filter: String => Boolean, task: Option[ActorRef => Unit]) = Props(new GetMonstersRequestHandler(monsterGenerator, filter, task))

  case class Listing(ref: ActorRef,
                     behaviour: String,
                     id: String,
                     initialLocation: String,
                     currentLocation: String,
                     currentLocationDescription: String,
                     creatureType: String,
                     skillset: Skillset,
                     wounds: Int,
                     woundStatus: String,
                     inventory: Seq[Object],
                     currentBehaviour: String,
                     state: LifeState) {
    override def toString = {
      ("%-28s" format id) + " -> " +
        ("%-35s" format ("[ " + currentLocation + " / " + initialLocation + " ] - " + currentLocationDescription)) + ". AI: [" + behaviour + "]" + "\r\n" +
        ("%30s" format " | ") + creatureType + " " + toQuickString(skillset) + " " + woundStatus + "\r\n" +
        ("%30s" format " | ") + inventory.map { x => x.name }.mkString("Inventory: ", ", ", ".") +
        (if (currentBehaviour == null) "" else "\r\n" + ("%30s" format " | ") + "Current Aggro: " + currentBehaviour)
    }
  }

  val orderingByBehaviour: Ordering[Listing] = Ordering.by { e => e.behaviour }
  val orderingByCreatureType: Ordering[Listing] = Ordering.by { e => e.creatureType }
  implicit val finalOrdering: Ordering[Listing] = Ordering.by { e: Listing => (e, e) }(Ordering.Tuple2(orderingByBehaviour, orderingByCreatureType))
}

