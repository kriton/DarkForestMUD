package kt.game.events

import akka.actor.ActorRef
import kt.game.domain.items.Object
import kt.game.events.StatusEvents.StatusOfEvent
import kt.game.domain.creatures.Target
import kt.game.domain.creatures.skills.SkillName._
import kt.game.domain.items.Receipe
import kt.game.domain.items.WeaponItem
import kt.game.domain.items.TrinketItem
import kt.game.domain.items.SuitItem
import kt.game.domain.creatures.Action

case class ActionOnCoolDownEvent(action: Option[Action], seconds: Double) extends NonCombatEvent with ActionFailedEvent {
  override def toString = action match {
    case None    => s"You have to wait $seconds more seconds before taking any action!"
    case Some(x) => s"You have to wait $seconds more seconds before this action!"
  }
}

case class StatusOfOtherEvent(target: Target, event: StatusOfEvent) extends StatusEvent {
  override def toString = event toString
}

case class RestedSuccesfullyEvent(fatigueRestored: Int, fatePointCost: Int) extends NonCombatEvent {
  override def toString = s"You feel rested. You restored $fatigueRestored fatigue at the cost of $fatePointCost fate point${(if (fatePointCost > 1) "s" else "")}!"
}

case class RestFailedEvent(reason: RestFailedReason) extends NonCombatEvent with ActionFailedEvent {
  import RestFailedReason._
  override def toString = reason match {
    case NotEnoughFatePoints => "You need a fate point to perform a quick rest!"
    case YouAreNotTired      => "You can olny quick rest if your fatigue exceeds your stamina..."
  }
}

sealed trait RestFailedReason

object RestFailedReason {
  case object NotEnoughFatePoints extends RestFailedReason
  case object YouAreNotTired extends RestFailedReason
}

case class BefriendedYourTargetSuccessfullyEvent(target: Target) extends NonCombatEvent {
  override def toString = s"You successfully befriended $target"
}

case class BefriendFailedEvent(target: Target, reason: ActionFailedReason) extends NonCombatEvent with ActionFailedEvent {
  import ActionFailedReason._
  override def toString = reason match {
    case CanNoLongerSpot   => s"You can no longet spot $target"
    case NotInSameLocation => s"You cannot befriend $target, as he is not close to you!"
    case _                 => s"You failed to befriend $target!"
  }
}

case class GotBefriendedByEvent(target: Target) extends NonCombatEvent {
  override def toString = s"You got befriended by $target"
}

case class TeachAttemptReceivedEvent(from: Target, recipe: Receipe) extends NonCombatEvent {
  override def toString = s"$from just tried to teach you how to craft ${recipe.item.name}"
}

case class TeachAttemptEvent(target: Target, recipe: Receipe) extends NonCombatEvent {
  override def toString = s"You try to teach $target the recipe for ${recipe.item.name}"
}

case class TeachAttemptFailedEvent(target: Target, reason: ActionFailedReason) extends NonCombatEvent with ActionFailedEvent {
  import ActionFailedReason._
  override def toString = reason match {
    case CanNoLongerSpot => s"You can no longet spot $target"
    case _               => "You have selected an invalid recipe to teach."
  }
}

case class CraftedItemEvent(item: Object) extends NonCombatEvent {
  override def toString = s"You have succesfully crafted: $item"
}

case class CraftFailedEvent(reason: CraftFailedReason) extends NonCombatEvent with ActionFailedEvent {
  import CraftFailedReason._
  override def toString = reason match {
    case RecipeNotKnown     => "You do not know selected recipe..."
    case CheckTotalFailure  => "You failed badly in crafting item. You lost all the ingridients!"
    case CheckSmallFailure  => "You failed to craft item, but you managed to save all the ingridients used!"
    case MissingIngridients => "You do not have all of the required ingridients..."
  }
}

sealed trait CraftFailedReason
object CraftFailedReason {
  case object RecipeNotKnown extends CraftFailedReason
  case object CheckTotalFailure extends CraftFailedReason
  case object CheckSmallFailure extends CraftFailedReason
  case object MissingIngridients extends CraftFailedReason
}

case class EquippedItemEvent(item: Object) extends NonCombatEvent {
  override def toString = item match {
    case x: WeaponItem  => s"Equipped new weapon $x"
    case x: SuitItem    => s"Equipped new suit $x"
    case x: TrinketItem => s"Equipped new trinket $x"
    case _              => "Could not have possibly equipped $x!"
  }
}

case object CannotEquipSelectedItemEvent extends NonCombatEvent with ActionFailedEvent {
  override def toString = "Cannot equip selected item..."
}

case class UnequippedItemEvent(item: Object) extends NonCombatEvent {
  override def toString = item match {
    case x: WeaponItem  => s"No longer wielding weapon $x"
    case x: SuitItem    => s"No longer wielding suit $x"
    case x: TrinketItem => s"No longer wielding trinket $x"
    case _              => "Could not have possibly unequipped $x!"
  }
}

case object CannotUnequipSelectedItemEvent extends NonCombatEvent with ActionFailedEvent {
  override def toString = "You cannot take off selected item. Inventory may be full. Try Dropping something first!"
}

case class DroppedItemEvent(item: Object) extends NonCombatEvent {
  override def toString = s"Dropped $item"
}

case object CannotDropSelectedItemEvent extends NonCombatEvent with ActionFailedEvent {
  override def toString = "You cannot drop selected item."
}

case class PickedUpItemEvent(item: Object) extends NonCombatEvent {
  override def toString = s"You succesfully picked up $item and put it in your inventory."
}

case object FullInventoryEvent extends NonCombatEvent with ActionFailedEvent {
  override def toString = "Your Inventory is full! Item left on the ground!"
}

case object ItemNoLongerAvailableToPickUpEvent extends NonCombatEvent with ActionFailedEvent {
  override def toString = "Item is no longer available to pick up. Someone else may have picked it up or it got lost ..."
}

case object CanNoLongerSpotItemToPickUpEvent extends NonCombatEvent with ActionFailedEvent {
  override def toString = "You can no longer see wanted item to pick up."
}

case class DisabledLinkItemSucceededEvent(itemRef: ActorRef) extends NonCombatEvent {
  override def toString = "You succesfully disabled your target!"
}

case class DisabledLinkItemFailedEvent(itemRef: Option[ActorRef], reason: ActionFailedReason = ActionFailedReason.CheckFailure) extends NonCombatEvent with ActionFailedEvent {
  override def toString = reason match {
    case ActionFailedReason.CanNoLongerSpot => "You can no longer spot target to disable."
    case _                                  => "Your attempt for disabling your target failed!"
  }
}

case class StolenSuccessfullyEvent(item: Object) extends NonCombatEvent {
  override def toString = s"You successfully stole ${item.name} from your target!"
}

case class StolenFromYouEvent(target: Target, item: Object) extends NonCombatEvent {
  override def toString = s"$target stole ${item.name} from you!"
}

case class StealAttemptOnYouFailedEvent(target: Target) extends NonCombatEvent {
  override def toString = s"$target tried to steal from you and failed!"
}

case class DiscoveryMadeEvent(discovery: String) extends NonCombatEvent {
  override def toString = s"You discovered $discovery"
}

case class StealAttemptFailedEvent(target: Target, reason: ActionFailedReason) extends NonCombatEvent with ActionFailedEvent {
  import ActionFailedReason._
  override def toString = reason match {
    case CanNoLongerSpot   => s"You can no longet spot $target"
    case CheckFailure      => "You failed to steal anything!"
    case NotInSameLocation => s"$target is not in the same location as you are. You need to find it and move closer to it."
    case _                 => "Target has nothing that you can steal!"
  }
}

case class ReceivedHelingEvent(ammount: Int, description: String) extends NonCombatEvent {
  override def toString = description
}

case object AlreadyInFullHealthEvent extends NonCombatEvent with ActionFailedEvent {
  override def toString = "You are already in full health! There is no point refreshing yourself yet."
}

case class RessurectedFromTombEvent(name: String) extends NonCombatEvent {
  override def toString = s"Character $name has been revived from his tomb!"
}

case class TombstoneDigAttemptEvent(tombstone: ActorRef) extends NonCombatEvent {
  override def toString = "You try to dig the tombstone site"
}

case class TombstoneDigSucceededEvent(tombstone: ActorRef, item: Object) extends NonCombatEvent {
  override def toString = "You found something!"
}

case class TombstoneDigFailedEvent(reason: TombstoneDiggingFailedReason, tombstone: Option[ActorRef] = None) extends NonCombatEvent with ActionFailedEvent {
  import kt.game.events.TombstoneDiggingFailedReason._
  override def toString = reason match {
    case CanNoLongerSpot  => "You can no longet locate wanted tombstone."
    case FoundNothing     => "You could find nothing of value in this tombstone."
    case TombstoneIsEmpty => "There is certainly nothing of value in this tombstone."
  }
}

sealed trait TombstoneDiggingFailedReason

object TombstoneDiggingFailedReason {
  case object CanNoLongerSpot extends TombstoneDiggingFailedReason
  case object FoundNothing extends TombstoneDiggingFailedReason
  case object TombstoneIsEmpty extends TombstoneDiggingFailedReason
}

case class PuttingEffortEvent(ammount: Int, skill: Option[SkillName]) extends NonCombatEvent {
  override def toString = (ammount, skill) match {
    case (0, None)    => "You are no longer putting effort an nay of your skills. Fatigue cost per roll reset to 1."
    case (x, None)    => s"You are now putting x$ammount effort on all of your skills. Fatigue cost per roll is now ${((ammount + 1) * (ammount + 1))}."
    case (x, Some(s)) => s"You are now putting x$ammount effort for your ${skill.get} rolls. Fatigue cost for those is now ${((ammount + 1) * (ammount + 1))}!"
  }
}

case class PuttingEffortErrorEvent(reason: EffortFailedReason) extends NonCombatEvent with ActionFailedEvent {
  import EffortFailedReason._
  override def toString = reason match {
    case InvalidSkillSelected    => "You selected an invalid skill to put effort on!"
    case OnlyUpToX3EffortAllowed => "You can only put an ammount of effort up to 3 !"
  }
}

sealed trait EffortFailedReason

object EffortFailedReason {
  case object OnlyUpToX3EffortAllowed extends EffortFailedReason
  case object InvalidSkillSelected extends EffortFailedReason
}
