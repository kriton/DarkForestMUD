package kt.game.requests

import akka.actor.ActorRef

object Messages {
  case class AreaOfPlayer(area: ActorRef, player: ActorRef)
  case class NameOfPlayer(name: String, player: ActorRef)
  case class AreaAndNameOfPlayer(area: ActorRef, name: String, player: ActorRef)
}