package kt.game.domain.creatures.characters

import scala.collection.immutable.ListSet
import scala.collection.mutable.Map
import scala.collection.mutable.Set
import scala.compat.Platform
import scala.concurrent.duration._
import akka.actor.ActorRef
import akka.actor.NotInfluenceReceiveTimeout
import akka.actor.Props
import akka.actor.ReceiveTimeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.Stunned
import kt.game.domain.creatures.characters.talents.Talent
import kt.game.domain.creatures.skills.Skill
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.SkillName.SkillName
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.items.AllObjects
import kt.game.domain.items.AllObjects
import kt.game.domain.items.AllReceipes
import kt.game.domain.items.Object
import kt.game.domain.items.Receipe
import kt.game.domain.items.Receipe
import kt.game.domain.items.Receipe
import kt.game.domain.items.SuitItem
import kt.game.domain.items.TrinketItem
import kt.game.domain.items.TrinketItem
import kt.game.domain.items.WeaponItem
import kt.game.domain.locations.Location
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.tombstones.Tombstone
import kt.game.domain.tombstones.Tombstone
import kt.game.player.HallOfFame
import kt.game.player.Player
import kt.game.requests.ListRequestHandler
import kt.game.requests.Messages
import kt.game.events.ActionFailedEvent
import kt.game.events.CombatEvent
import kt.game.events.DevelopmentEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.ItemSpottedEvent
import kt.game.events.LoggedEvent
import kt.game.events.LoggedEvent
import kt.game.events.NonCombatEvent
import kt.game.events.RollEvent
import kt.game.events.StatusEvent
import kt.game.events.TombstoneSpottedEvent
import kt.game.utility.Result
import kt.game.domain.tombstones.TombstoneGenerator
import kt.game.utility.Dice
import kt.game.events.Personal
import kt.game.domain.creatures.CCommand
import kt.game.domain.creatures.InControl
import kt.game.domain.creatures.ACCommand
import kt.game.domain.creatures.ACCommand
import kt.game.domain.creatures.Action
import kt.game.domain.creatures.skills.Skill
import kt.game.domain.creatures.AttackType
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.worldbuilder.WorldBuilder
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import scala.util.Try
import kt.game.events.ParrySucceededEvent
import kt.game.events.TargetIsNotFearsomeEvent
import kt.game.events.TargetIsFearsomeEvent
import kt.game.events.TargetIsPunnyEvent
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.events.RestedSuccesfullyEvent
import kt.game.events.TombstoneDigAttemptEvent
import kt.game.events.TombstoneDigFailedEvent
import kt.game.events.TombstoneDiggingFailedReason
import kt.game.events.StatusEvents._
import kt.game.events.LostControlToEvent
import kt.game.events.RegainedControlEvent
import kt.game.events.InfoEvent
import kt.game.events.InfoEvents.WrongPasswordEvent
import kt.game.events.DevelopmentEvents.LevelUpEvent
import kt.game.events.DevelopmentEvents.InvalidLevelupSkillSelectedEvent
import kt.game.events.DevelopmentEvents.CannotLevelUpEvent
import kt.game.events.DevelopmentEvents.CannotLevelupSelectedSkillEvent
import kt.game.events.DevelopmentEvents.XPGainedEvent
import kt.game.events.DevelopmentEvents.CanLevelUpEvent
import kt.game.events.DevelopmentEvents.XPLostEvent
import kt.game.events.DevelopmentEvents.MonsterWasTooWeakToGrantXPEvent
import kt.game.events.PuttingEffortEvent
import kt.game.events.EffortFailedReason
import kt.game.events.PuttingEffortErrorEvent
import kt.game.events.RestFailedEvent
import kt.game.events.RestFailedReason
import kt.game.events.BefriendedYourTargetSuccessfullyEvent
import kt.game.events.GotBefriendedByEvent
import kt.game.events.TeachAttemptEvent
import kt.game.events.CraftedItemEvent
import kt.game.events.EquippedItemEvent
import kt.game.events.UnequippedItemEvent
import kt.game.events.DroppedItemEvent
import kt.game.events.PickedUpItemEvent
import kt.game.events.DevelopmentEvents.NewRecipeLEarnedEvent
import kt.game.events.DevelopmentEvents.NewTalentAcquiredEvent
import kt.game.events.BefriendedYourTargetSuccessfullyEvent
import kt.game.events.ReceivedHelingEvent
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.ParryFailedEvent
import kt.game.events.ActionFailedReason
import kt.game.events.InfoEvents.EnteredHallOfFameOnDeathEvent
import kt.game.events.InfoEvents.CouldNotEnterHallOfFameEvent
import kt.game.events.BefriendFailedEvent
import kt.game.events.TeachAttemptFailedEvent
import kt.game.events.CraftFailedEvent
import kt.game.events.CraftFailedReason
import kt.game.events.CannotEquipSelectedItemEvent
import kt.game.events.CannotUnequipSelectedItemEvent
import kt.game.events.CannotDropSelectedItemEvent
import kt.game.events.FullInventoryEvent
import kt.game.events.CanNoLongerSpotItemToPickUpEvent
import kt.game.events.DevelopmentEvents.RecipeNotComprehendedEvent
import kt.game.events.DevelopmentEvents.RecipeAlreadyKnownEvent
import kt.game.events.AlreadyInFullHealthEvent
import kt.game.events.DevelopmentEvents.CanNotLearnTalentEvent
import kt.game.events.InfoEvents.ACharacrerDiedBroadcastEvent
import kt.game.events.InfoEvents.PlayerKillEvent
import kt.game.events.TeachAttemptReceivedEvent
import kt.game.events.TargetGotAttackedByOtherEvent

sealed trait LoggedState
case object LoggedIn extends LoggedState { override def toString = "ON-LINE" }
case object LoggedOut extends LoggedState { override def toString = "OFF-LINE" }

class Character private (characterName: String, password: String, cskillset: Skillset) extends Creature(Character.creatureType, cskillset) {
  import Character._
  import context._

  setReceiveTimeout(10 seconds)

  var timePlayedInMillis: Long = 0;
  var lastLoginTimestamp: Long = 0;

  var owner: ActorRef = null

  var loggedState: LoggedState = LoggedOut

  var XP: Int = 0
  override def level() = skillset.ranksSum()
  var fatigue: Int = 0

  var globalCD = Deadline.now
  val cooldowns = Map[Action, Deadline]()
  var deflecting: Option[(Int, Option[ActorRef], Deadline)] = None

  override def actionCheck(action: Action): Boolean = super.actionCheck(action) && (cooldowns.get(action) match {
    case None if globalCD.isOverdue                   => { globalCD = globalDelay.fromNow; true }
    case None                                         => { log(LoggedEvent(ActionOnCoolDownEvent(None, globalCD.timeLeft.toMillis / 1000.0))); false }
    case cd if cd.get.isOverdue && globalCD.isOverdue => { globalCD = globalDelay.fromNow; true }
    case cd                                           => { log(LoggedEvent(ActionOnCoolDownEvent(Some(action), cd.get.timeLeft.toMillis.max(globalCD.timeLeft.toMillis) / 1000.0))); false }
  })

  var nonFearedCachedCreature: ActorRef = null
  val tombstonesCache: Map[String, ActorRef] = Map()
  val itemsCache: Map[String, Object] = Map()

  var receipesLearned = ListSet[Receipe]()
  val friends: Map[String, String] = Map()
  var talents = ListSet[(ActorRef, String, String)]() // description & helpText

  val dicePerRoll = Map[SkillName, (Int, Int)](
    SkillName.ATHL -> (1, 0),
    SkillName.BALL -> (1, 0),
    SkillName.CRAF -> (1, 0),
    SkillName.INFL -> (1, 0),
    SkillName.PERC -> (1, 0),
    SkillName.REFL -> (1, 0),
    SkillName.WEAP -> (1, 0),
    SkillName.WILL -> (1, 0))

  var weaponEquiped: WeaponItem = null
  var suitEquiped: SuitItem = null
  var trinketEquiped: TrinketItem = null

  def equipedItemSet = {
    val set = Set[Object]()
    if (weaponEquiped != null) set += weaponEquiped
    if (suitEquiped != null) set += suitEquiped
    if (trinketEquiped != null) set += trinketEquiped
    set
  }

  def skillModiferFromItems(skill: SkillName): Int = {
    (if (weaponEquiped != null) weaponEquiped.bonuses.skill(skill).bonus else 0) +
      (if (suitEquiped != null) suitEquiped.bonuses.skill(skill).bonus else 0) +
      (if (trinketEquiped != null) trinketEquiped.bonuses.skill(skill).bonus else 0)
  }

  override def defenseAgainst(attacker: ActorRef, attackType: AttackType) = super.defenseAgainst(attacker, attackType) + (deflecting match {
    case None                                                           => 0
    case Some(Tuple3(_, _, deadline)) if deadline.isOverdue             => { deflecting = None; 0 }
    case Some(Tuple3(bonus, None, _)) if attackType == AttackType.Melee => bonus
    case Some(Tuple3(bonus, against, _)) if attacker == against.get     => bonus
    case _                                                              => 0
  })

  def deflect(against: Option[String]) = {
    val creature = if (against != None) spottedCache.get(against.get) else None
    if (against != None && creature == None) {
      log(LoggedEvent(ParryFailedEvent(ActionFailedReason.CanNoLongerSpot)))
    } else {
      cooldowns += Action.Parry -> actionTimers.get(Action.Parry).get(skillset).fromNow
      val bonus = 5 * Result.criticalSteps(roll(skillset.weapons) - baseDefense)
      if (bonus > 0) {
        deflecting = Some(bonus, creature, (12 + bonus seconds).fromNow)
        log(LoggedEvent(ParrySucceededEvent(bonus)))
      } else {
        deflecting = None
        log(LoggedEvent(ParryFailedEvent(ActionFailedReason.CheckFailure)))
      }
    }
  }

  override def updateRecentAttacks(attacker: ActorRef, success: Boolean, attackType: AttackType) = {
    super.updateRecentAttacks(attacker, success, attackType)
    if (deflecting != None && (deflecting.get._2 == None || deflecting.get._2.get == attacker)) {
      val newBonus = deflecting.get._1 - (if (success) 3 else 2)
      deflecting = if (newBonus > 0) Some(newBonus, deflecting.get._2, deflecting.get._3) else None
    }
  }

  override def detailedDescription = if (!equipedItemSet.isEmpty) { equipedItemSet.map(_.name).mkString(super.detailedDescription + " He is equipped with ", ", ", ".") } else super.detailedDescription

  override def abilityModifer(ability: String): Int = super.abilityModifer(ability) + Try { skillModiferFromItems(SkillName.withName(ability)) }.getOrElse(0)

  def carryLimit = 5 + skillset.athletics.bonus + abilityModifer(SkillName.ATHL)

  def receipesKnownStatus = ReceipesKnownStatus(receipesLearned.toSeq)

  override def death() = {
    parent ! CharacterGenerator.DeadCharacter(self, owner)
    val items: Set[Object] = Set()
    val timePlayedInMillis = this.timePlayedInMillis + (Platform.currentTime - lastLoginTimestamp)
    if (weaponEquiped != null) items += weaponEquiped
    if (suitEquiped != null) items += suitEquiped
    if (trinketEquiped != null) items += trinketEquiped
    items ++= inventory.takeRight(5 - items.size)
    parent ! TombstoneGenerator.NewTombstone(characterName, password, (timePlayedInMillis milliseconds).toMinutes.toInt, level, currentLocation, items.toSet, skillset,
      friends.map(_._2).toSet,
      receipesLearned.map(_.item.name).toSet,
      talents.map { _._1.path.name }.toSet)
    restoreControl()
    parent ! CharacterGenerator.Broadcast(LoggedEvent(ACharacrerDiedBroadcastEvent(characterName)))
    toHallOfFame(password, false)
    super.death()
  }

  def toHallOfFame(password: String, retiredAlive: Boolean) = {
    if (password == this.password && (sender == owner || !retiredAlive) && level >= 15 && weaponEquiped != null && suitEquiped != null && trinketEquiped != null) {
      timePlayedInMillis += (Platform.currentTime - lastLoginTimestamp)
      owner ! HallOfFame.Listing(characterName,
        password,
        level,
        skillset,
        receipesLearned.size,
        timePlayedInMillis,
        weaponEquiped.name,
        suitEquiped.name,
        trinketEquiped.name,
        Platform.currentTime,
        talents.size,
        retiredAlive)
      if (retiredAlive) {
        context become retired
        parent ! CharacterGenerator.RetiredCharacter(self)
      } else {
        owner ! FeedbackBuffer.Feedback(LoggedEvent(EnteredHallOfFameOnDeathEvent))
      }
    } else if (retiredAlive && (level < 15 || weaponEquiped == null || suitEquiped == null || trinketEquiped == null)) {
      owner ! FeedbackBuffer.Feedback(LoggedEvent(CouldNotEnterHallOfFameEvent))
    } else if (retiredAlive) {
      owner ! FeedbackBuffer.Feedback(LoggedEvent(WrongPasswordEvent))
    }
  }

  def xpNeededToLevelUp() = 1 + (level / 5)

  def levelUp(skill: String) = {
    val xpNeededToNextLevel = xpNeededToLevelUp()
    try {
      val incrementedSkillset = skillset.increment(SkillName.withName(skill))
      if (xpNeededToNextLevel <= XP && skillset != incrementedSkillset && skillset.skill(SkillName.withName(skill)).bonus < STATLIMIT(level)) {
        skillset = incrementedSkillset
        XP -= xpNeededToNextLevel
        owner ! FeedbackBuffer.Feedback(LoggedEvent(LevelUpEvent(level)))
        fate_points_remaining = FATE
      } else if (skillset.skill(SkillName.withName(skill)).bonus >= STATLIMIT(level)) {
        owner ! FeedbackBuffer.Feedback(LoggedEvent(CannotLevelupSelectedSkillEvent(level)))
      } else {
        owner ! FeedbackBuffer.Feedback(LoggedEvent(CannotLevelUpEvent))
      }
    } catch {
      case e: NoSuchElementException => owner ! FeedbackBuffer.Feedback(LoggedEvent(InvalidLevelupSkillSelectedEvent))
    }
  }

  def gainedXP(xp: Int) = {
    XP += xp
    owner ! FeedbackBuffer.Feedback(LoggedEvent(XPGainedEvent(xp)))
    if (xpNeededToLevelUp <= XP) owner ! FeedbackBuffer.Feedback(LoggedEvent(CanLevelUpEvent))
  }

  def lostXP(xp: Int) = {
    XP -= xp
    owner ! FeedbackBuffer.Feedback(LoggedEvent(XPLostEvent(xp)))
  }

  override def killed(creature: ActorRef, level: Int) = {
    super.killed(creature, level)
    if (Character.is(creature)) {
      if (level + 2 > this.level) {
        parent ! CharacterGenerator.Broadcast(LoggedEvent(PlayerKillEvent(self, characterName, creature, true)))
        gainedXP(1)
      } else if (level + 10 < this.level) {
        parent ! CharacterGenerator.Broadcast(LoggedEvent(PlayerKillEvent(self, characterName, creature, false)))
        lostXP(1)
      } else {
        parent ! CharacterGenerator.Broadcast(LoggedEvent(PlayerKillEvent(self, characterName, creature, true)))
      }
    } else if (level + 20 > this.level) {
      gainedXP(1)
    } else {
      owner ! FeedbackBuffer.Feedback(LoggedEvent(MonsterWasTooWeakToGrantXPEvent(level)))
    }
  }

  override def discovered(discovery: String) = {
    super.discovered(discovery)
    gainedXP(1)
  }

  override def fatiguedPenalty() = fatigue / STAMINA

  def sufferFatigue() = this.fatigue += 1

  def sufferFatigue(fatigue: Int) = {
    this.fatigue += fatigue
    if (this.fatigue < 0) this.fatigue = 0
  }

  override def roll(skill: Skill, withBonus: Option[Int]): Int = {
    val diceCounts = dicePerRoll.get(skill.name).get
    val dice = diceCounts._1 + diceCounts._2 match {
      case x if x < 1 => (-x + 1, Math.min _)
      case y          => (y, Math.max _)
    }
    val results = for { r <- 0 until dice._1 } yield Dice.d20
    val resultToKeep = results reduce dice._2
    val modifier = fullSkillModifer(skill.name) + withBonus.getOrElse(0)
    val finalResult = resultToKeep + modifier
    log(LoggedEvent(RollEvent(results, modifier, finalResult, skill)))
    sufferFatigue((diceCounts._2 + 1) * (diceCounts._2 + 1))
    finalResult
  }

  def stopAllEffort() {
    dicePerRoll.foreach { e => dicePerRoll += e._1 -> (e._2._1, 1) }
    log(LoggedEvent(PuttingEffortEvent(0, None)))
  }

  def startAllEffort(ammount: Int) {
    if (ammount < 4 && ammount >= 0) {
      dicePerRoll.foreach { e => dicePerRoll += e._1 -> (e._2._1, ammount) }
      if (ammount == 0) log(LoggedEvent(PuttingEffortEvent(0, None)))
      else log(LoggedEvent(PuttingEffortEvent(ammount, None)))
    } else {
      log(LoggedEvent(PuttingEffortErrorEvent(EffortFailedReason.OnlyUpToX3EffortAllowed)))
    }
  }

  def effortOn(skill: SkillName) = dicePerRoll.get(skill).get._2

  def putEffortOn(skillRef: String, ammount: Int) = {
    if (ammount < 4 && ammount >= 0) {
      try {
        val skill = SkillName.withName(skillRef)
        dicePerRoll += skill -> (dicePerRoll.get(skill).get._1, ammount)
        log(LoggedEvent(PuttingEffortEvent(ammount, Some(skill))))
      } catch {
        case e: NoSuchElementException => log(LoggedEvent(PuttingEffortErrorEvent(EffortFailedReason.InvalidSkillSelected)))
      }
    } else {
      log(LoggedEvent(PuttingEffortErrorEvent(EffortFailedReason.OnlyUpToX3EffortAllowed)))
    }
  }

  override def spot() = {
    itemsCache.clear()
    super.spot
  }

  override def move(locationRef: String, sneak: Boolean = false) = {
    super.move(locationRef, sneak)
    if (sneak && observedCache.get(locationRef) != None) cooldowns += Action.Sneak -> actionTimers.get(Action.Sneak).get(skillset).fromNow
  }

  override def moved(location: ActorRef, descriptors: Descriptor) = {
    super.moved(location, descriptors)
    tombstonesCache.clear()
  }

  def movedWhenLoaded(location: ActorRef, descriptors: Descriptor) = {
    if (currentLocation == null) {
      currentLocation = location
      locationDescription = descriptors
    }
  }

  def found(item: Object) = {
    val reference = (itemsCache.size + 1) + ""
    log(LoggedEvent(ItemSpottedEvent(reference, item)))
    itemsCache.put(reference, item)
  }

  override def spotted(creature: ActorRef, inLocation: ActorRef, description: String, strength: Option[Int], status: Option[String]): Unit = {
    super.spotted(creature, inLocation, friends.getOrElse(creature.path.name, description), strength, status)
  }

  override def log(event: LoggedEvent) = {
    owner ! FeedbackBuffer.Feedback(event)
    if (!event.event.isInstanceOf[Personal] && !event.event.isInstanceOf[StatusEvent] && !viewers.isEmpty) viewers.foreach { _ ! FeedbackBuffer.Feedback(event) }
    if (!event.event.isInstanceOf[Personal] && controller != None && controller.get._1 != owner) controller.get._1 ! FeedbackBuffer.Feedback(event)
  }

  override def status() = StatusOfEvent(
    levelStatus, woundStatus, fatigueStatus, fatePointsStatus, globalCDStatus,
    cooldownsStatus, defenseStatus, conditionsStatus, locationStatus, equippedItemsStatus)

  def fatigueStatus() = FatigueStatus(fatigue, STAMINA, STAMINA_BONUS + 1, fatiguedPenalty())

  def globalCDStatus() = GlobalCDStatus(INITIATIVE, globalDelay.toMillis / 1000.0)

  def cooldownsStatus() = CooldownsStatus(cooldowns.filter(_._2.hasTimeLeft()).map(e => (e._1, e._2.timeLeft.toSeconds.toInt)).toSeq)

  def fatePointsStatus() = FatePointStatus(fate_points_remaining, FATE)

  def equippedItemsStatus() = EquippedItemsStatus(weaponEquiped, suitEquiped, trinketEquiped)

  def levelStatus() = LevelStatus(level, XP, xpNeededToLevelUp)

  def defenseStatus() = DefenseStatus(baseDefense, deflecting match {
    case None                                 => None
    case Some(Tuple3(_, _, x)) if x.isOverdue => { deflecting = None; None }
    case Some(Tuple3(x, None, y))             => Some(x, None, y.timeLeft.toSeconds)
    case Some(Tuple3(x, Some(y), z))          => Some(x, Some(y), z.timeLeft.toSeconds)
  })

  def fullSkillSetWithDescriptionsStatus() = FullSkillsetWithDescriptionStatus(Seq(
    (skillset.athletics, abilityModifer(SkillName.ATHL), effortOn(SkillName.ATHL)),
    (skillset.ballistics, abilityModifer(SkillName.BALL), effortOn(SkillName.BALL)),
    (skillset.craft, abilityModifer(SkillName.CRAF), effortOn(SkillName.CRAF)),
    (skillset.influence, abilityModifer(SkillName.INFL), effortOn(SkillName.INFL)),
    (skillset.perception, abilityModifer(SkillName.PERC), effortOn(SkillName.PERC)),
    (skillset.reflexes, abilityModifer(SkillName.REFL), effortOn(SkillName.REFL)),
    (skillset.weapons, abilityModifer(SkillName.WEAP), effortOn(SkillName.WEAP)),
    (skillset.willpower, abilityModifer(SkillName.WILL), effortOn(SkillName.WILL))))

  def detailedstatusForOthers() = statusForOthers ++ StatusOfEvent(fullSkillSetWithDescriptionsStatus, equippedItemsStatus, talentstatus)

  def talentstatus() = TalentsStatus(level, talents.toSeq)

  override def simpleStatus() = StatusOfEvent(locationStatus, woundStatus, fatigueStatus, conditionsStatus)

  def detailedStatus() = status :+ fullSkillSetWithDescriptionsStatus :+ talentstatus :+ receipesKnownStatus :+ inventoryStatus

  def assign(password: String) = {
    if (password == this.password) {
      owner = sender()
      if (controller == None) controller = Some(owner, None, None)
      loggedState = LoggedIn
      lastLoginTimestamp = Platform.currentTime
      owner ! Player.ControlSuccess(characterName, currentLocation == null)
    } else {
      sender ! FeedbackBuffer.Feedback(LoggedEvent(WrongPasswordEvent))
    }
  }

  def unassign() = {
    if (sender() == owner) {
      owner ! Player.ControlDropped(characterName)
      loggedState = LoggedOut
      timePlayedInMillis += (Platform.currentTime - lastLoginTimestamp)
      owner = null
      controller = None
    }
  }

  def quickRest() = if (fatigue > STAMINA) {
    if (fate_points_remaining >= 1) {
      fate_points_remaining -= 1
      val fatigueToRestore = fatigue / 2
      fatigue -= fatigueToRestore
      cooldowns += Action.QuickRest -> actionTimers.get(Action.QuickRest).get(skillset).fromNow
      log(LoggedEvent(RestedSuccesfullyEvent(fatigueToRestore, 1)))
    } else {
      log(LoggedEvent(RestFailedEvent(RestFailedReason.NotEnoughFatePoints)))
    }
  } else {
    log(LoggedEvent(RestFailedEvent(RestFailedReason.YouAreNotTired)))
  }

  def rest() = if (fatigue > 0) {
    fatigue -= (STAMINA_BONUS + 1)
    if (fatigue < 0) fatigue = 0
  }

  override def search() = {
    val searchRoll = super.search()
    tombstonesCache.clear()
    currentLocation ! Location.Tombstones(searchRoll)
    searchRoll
  }

  def spottedTombstone(creature: ActorRef, description: String, level: Int, defiled: Boolean): Unit = {
    val reference = (tombstonesCache.size + 1) + ""
    log(LoggedEvent(TombstoneSpottedEvent(reference, creature, description, level, defiled)))
    tombstonesCache.put(reference, creature)
  }

  def dig(tombstoneRef: String) = {
    val tombstone = tombstonesCache.get(tombstoneRef)
    if (tombstone != None) {
      log(LoggedEvent(TombstoneDigAttemptEvent(tombstone.get)))
      tombstone.get ! Tombstone.Dig(roll(skillset.athletics))
    } else {
      log(LoggedEvent(TombstoneDigFailedEvent(TombstoneDiggingFailedReason.CanNoLongerSpot)))
    }
  }

  override def meleeAttack(creature: ActorRef, attackType: AttackType) = {
    if (creature.path.parent.name == "characters" && nonFearedCachedCreature != creature) creature ! Creature.TestPresence(roll(skillset.willpower), currentLocation, attackType)
    else super.meleeAttack(creature, attackType)
  }

  def presence(successes: Int, location: ActorRef, attackType: AttackType) = {
    if (successes >= 0) {
      log(LoggedEvent(TargetIsNotFearsomeEvent(sender)))
      performWeaponsAttack(sender, attackType)
    } else log(LoggedEvent(TargetIsFearsomeEvent(sender)))
    if (successes > 0) {
      nonFearedCachedCreature = sender
      log(LoggedEvent(TargetIsPunnyEvent(sender)))
    }
  }

  override def performWeaponsAttack(creature: ActorRef, attackType: AttackType) = {
    if (attackType == AttackType.Flank) cooldowns += Action.Flank -> actionTimers.get(Action.Flank).get(skillset).fromNow
    super.performWeaponsAttack(creature, attackType)
  }

  override def protect(creatureRef: String) = {
    if (spottedCache.contains(creatureRef)) cooldowns += Action.Protect -> actionTimers.get(Action.Protect).get(skillset).fromNow
    super.protect(creatureRef)
  }

  def befriend(creatureRef: String) = {
    val creature = spottedCache.get(creatureRef)
    if (creature != None) {
      cooldowns += Action.Befriend -> actionTimers.get(Action.Befriend).get(skillset).fromNow
      creature.get ! Creature.BefriendAttempt(roll(skillset.influence), currentLocation)
    } else {
      log(LoggedEvent(BefriendFailedEvent(creatureRef, ActionFailedReason.CanNoLongerSpot)))
    }
  }

  def befriendAttempt(roll: Int, fromLocation: ActorRef) = {
    val successes = Result.criticalSteps(roll - skillset.willpower.passive)
    if (fromLocation == currentLocation) {
      if (successes >= 0) {
        sender ! Creature.Befriended(characterName)
      } else {
        sender ! Creature.Message(BefriendFailedEvent(sender, ActionFailedReason.CheckFailure))
      }
    } else sender ! Creature.Message(BefriendFailedEvent(sender, ActionFailedReason.NotInSameLocation))
  }

  def befriended(name: String) = {
    friends += sender.path.name -> name
    log(LoggedEvent(BefriendedYourTargetSuccessfullyEvent(sender)))
    sender ! Creature.BefriendedBy(characterName)
  }

  def befriendedBy(name: String) = {
    friends += sender.path.name -> name
    log(LoggedEvent(GotBefriendedByEvent(sender)))
  }

  def use() = {
    if (currentLocation != null) currentLocation ! Location.UseNonTravelOptionIfAvailable(roll(skillset.craft))
  }

  override def hide() = {
    cooldowns += Action.Hide -> actionTimers.get(Action.Hide).get(skillset).fromNow
    super.hide()
  }

  override def steal(creatureRef: String) = {
    if (spottedCache.get(creatureRef) != None) cooldowns += Action.Steal -> actionTimers.get(Action.Steal).get(skillset).fromNow
    super.steal(creatureRef)
  }

  def teach(receipeReference: String, creatureRef: String) = {
    val creature = spottedCache.get(creatureRef)
    val receipe = receipesLearned.zipWithIndex.map { case (e, i) => (i, e) }.toMap.get(receipeReference.toInt)
    if (creature != None && receipe != None) {
      log(LoggedEvent(TeachAttemptEvent(creature.get, receipe.get)))
      creature.get ! Learned(receipe.get)
    } else if (creature == None) {
      log(LoggedEvent(TeachAttemptFailedEvent(creatureRef, ActionFailedReason.CanNoLongerSpot)))
    } else {
      log(LoggedEvent(TeachAttemptFailedEvent(creatureRef, ActionFailedReason.WrongInput)))
    }
  }

  def craft(receipeReference: String) = {
    if (receipeReference.toInt >= receipesLearned.size) {
      log(LoggedEvent(CraftFailedEvent(CraftFailedReason.RecipeNotKnown)))
    } else {
      val receipe = receipesLearned.zipWithIndex.map { case (e, i) => (i, e) }.toMap.get(receipeReference.toInt)
      if (receipe.isEmpty) {
        log(LoggedEvent(CraftFailedEvent(CraftFailedReason.RecipeNotKnown)))
      } else {
        if (receipe.get.ingridients forall { inventory contains _ }) {
          val rollResult = roll(skillset.craft) - receipe.get.craftDC
          if (rollResult >= 0) {
            crafted(receipe.get)
          } else if ((Result criticalSteps rollResult) < -1) {
            receipe.get.ingridients foreach { inventory -= _ }
            log(LoggedEvent(CraftFailedEvent(CraftFailedReason.CheckTotalFailure)))
          } else {
            log(LoggedEvent(CraftFailedEvent(CraftFailedReason.CheckSmallFailure)))
          }
        } else {
          log(LoggedEvent(CraftFailedEvent(CraftFailedReason.MissingIngridients)))
        }
      }
    }
  }

  def crafted(receipe: Receipe) = {
    receipe.ingridients foreach { inventory -= _ }
    inventory += receipe.item
    log(LoggedEvent(CraftedItemEvent(receipe.item)))
    gainedXP(1)
  }

  def equip(itemReference: String) = {
    if (itemReference.toInt < inventory.size) {
      val item = inventory(itemReference.toInt)
      item match {
        case item: WeaponItem => {
          if (weaponEquiped != null) inventory += weaponEquiped
          weaponEquiped = item
          inventory -= item
          log(LoggedEvent(EquippedItemEvent(item)))
        }
        case item: SuitItem => {
          if (suitEquiped != null) inventory += suitEquiped
          suitEquiped = item
          inventory -= item
          log(LoggedEvent(EquippedItemEvent(item)))
        }
        case item: TrinketItem => {
          if (trinketEquiped != null) inventory += trinketEquiped
          trinketEquiped = item
          inventory -= item
          log(LoggedEvent(EquippedItemEvent(item)))
        }
        case _ => {
          log(LoggedEvent(CannotEquipSelectedItemEvent))
        }
      }
    } else {
      log(LoggedEvent(CannotEquipSelectedItemEvent))
    }
  }

  def takeOff(itemReference: String) = {
    if (carryLimit <= inventory.size) {
      log(LoggedEvent(CannotUnequipSelectedItemEvent))
    } else {
      itemReference match {
        case itemReference if ((itemReference matches "(?i)weapon") && (weaponEquiped != null)) => {
          inventory += weaponEquiped
          log(LoggedEvent(UnequippedItemEvent(weaponEquiped)))
          weaponEquiped = null
        }
        case itemReference if ((itemReference matches "(?i)suit") && (suitEquiped != null)) => {
          inventory += suitEquiped
          log(LoggedEvent(UnequippedItemEvent(suitEquiped)))
          suitEquiped = null
        }
        case itemReference if ((itemReference matches "(?i)trinket") && (trinketEquiped != null)) => {
          inventory += trinketEquiped
          log(LoggedEvent(UnequippedItemEvent(trinketEquiped)))
          trinketEquiped = null
        }
        case _ => {
          log(LoggedEvent(CannotUnequipSelectedItemEvent))
        }
      }
    }
  }

  def drop(itemReference: String): Unit = {
    if (itemReference.toInt < inventory.size && currentLocation != null) {
      val item = inventory(itemReference.toInt)
      inventory -= item
      currentLocation ! Location.AddItem(item)
      log(LoggedEvent(DroppedItemEvent(item)))
    } else {
      log(LoggedEvent(CannotDropSelectedItemEvent))
    }
  }

  def pickUp(itemReference: String) = {
    val item = itemsCache.get(itemReference)
    if (carryLimit <= inventory.size) {
      log(LoggedEvent(FullInventoryEvent))
    } else if (item != None && currentLocation != null) {
      currentLocation ! Location.PickUp(item.get)
    } else {
      log(LoggedEvent(CanNoLongerSpotItemToPickUpEvent))
    }
  }

  def pickedUp(item: Object) = {
    if (receiveItem(item)) {
      log(LoggedEvent(PickedUpItemEvent(item)))
    }
  }

  override def receiveItem(item: Object) = {
    if (carryLimit > inventory.size) super.receiveItem(item)
    else {
      log(LoggedEvent(FullInventoryEvent))
      currentLocation ! Location.AddItem(item)
      false
    }
  }

  def learned(receipe: Receipe) = {
    if (Character.is(sender)) log(LoggedEvent(TeachAttemptReceivedEvent(sender, receipe)))
    if (receipesLearned.contains(receipe)) log(LoggedEvent(RecipeAlreadyKnownEvent(receipe)))
    else if (receipe.requirements forall { receipesLearned contains _ }) {
      receipesLearned += receipe
      log(LoggedEvent(NewRecipeLEarnedEvent(receipe)))
      sender ! Location.FeatureUsed()
    } else {
      log(LoggedEvent(RecipeNotComprehendedEvent(receipe)))
    }
  }

  def heal(wounds: Int, description: String) = {
    if (this.wounds > 0) {
      log(LoggedEvent(ReceivedHelingEvent(wounds, description)))
      sufferWounds(-wounds)
      sender ! Location.FeatureUsed()
    } else {
      log(LoggedEvent(AlreadyInFullHealthEvent))
    }
  }

  def acquire(talent: Props, cost: Int, description: String, name: String, helpText: String) = {
    XP -= cost
    val newTalent = actorOf(talent, name)
    talents += Tuple3(newTalent, description, helpText)
    log(LoggedEvent(NewTalentAcquiredEvent(Talent.fromName(name))))
  }

  def train(talentName: String) = {
    val talent = Talent.fromName(talentName)
    if (talents.size < TALENTLIMIT(level)) {
      if (talent != None && !talents.map(_._1.path.name).contains(talent.get.name)) {
        if (talent.get.learn(this) && Location.is(sender)) sender ! Location.FeatureUsed()
      } else {
        log(LoggedEvent(CanNotLearnTalentEvent(talent.get.xp, talent.get.skill, true, false)))
      }
    } else {
      log(LoggedEvent(CanNotLearnTalentEvent(talent.get.xp, talent.get.skill, false, true)))
    }
  }

  def takecontrolFor(duration: Option[FiniteDuration], onEnd: Option[ActorRef => Unit], onDeath: ActorRef => Unit) = {
    if (duration != None && onEnd != None) {
      controller = Some(sender, Option(onDeath), Some(system.scheduler.scheduleOnce(duration.get, self, Creature.DropControl()), onEnd.get))
    } else {
      controller = Some(sender, Option(onDeath), None)
    }
    if (owner != null) {
      owner ! Player.ControlTaken(sender)
      log(LoggedEvent(LostControlToEvent(sender)))
    }
  }

  def restoreControl() = {
    if (controller != None && controller.get._3 != None) {
      controller.get._3.get._1.cancel()
      controller.get._3.get._2(self)
    }
    controller = Option(owner, None, None)
    if (owner != null) {
      owner ! Player.ControlRegained()
      log(LoggedEvent(RegainedControlEvent()))
    }
  }

  def takeSnapshot() = {
    SnapshotV003(
      characterName,
      password,
      skillset,
      receipesLearned.map(_.item.name).toSet,
      friends.map(_._2).toSet,
      if (weaponEquiped != null) weaponEquiped.name else null,
      if (suitEquiped != null) suitEquiped.name else null,
      if (trinketEquiped != null) trinketEquiped.name else null,
      inventory.map(_.name),
      Tuple2(currentLocation.path.name, locationDescription toString),
      wounds,
      fatigue,
      fate_points_remaining,
      XP,
      timePlayedInMillis,
      talents.map { _._1.path.name }.toSet)
  }

  def loadFromSnapShot(snapshot: SnapshotV003) = {
    XP = snapshot.xp
    fatigue = snapshot.fatigue
    fate_points_remaining = snapshot.fatePoints
    wounds = snapshot.wounds
    inventory ++= snapshot.inventory.map { AllObjects.map.get(_) }.filter { _ != None }.map { _.get }
    trinketEquiped = AllObjects.map.get(snapshot.trinketEquiped).orNull.asInstanceOf[TrinketItem]
    suitEquiped = AllObjects.map.get(snapshot.suitEquiped).orNull.asInstanceOf[SuitItem]
    weaponEquiped = AllObjects.map.get(snapshot.weaponEquiped).orNull.asInstanceOf[WeaponItem]
    receipesLearned ++= snapshot.receipesLearned.map { AllReceipes.map.get(_) }.filter { _ != None }.map { _.get }
    friends ++= snapshot.friends.map { x => x -> x }
    timePlayedInMillis = snapshot.timePlayed
    talents ++= snapshot.talents.map { Talent.fromName(_) }.filter { _ != None }.map { e => Tuple3(actorOf(e.get.props(self), e.get.name), e.get.description, e.get.commandsHelp) }
    parent ! WorldBuilder.PositionTo(self, snapshot.location._1)
  }

  def listing(requester: ActorRef) = {
    requester ! ListRequestHandler.Listing(characterName, level, locationDescription toString, receipesLearned.size, timePlayedInMillis + Platform.currentTime - lastLoginTimestamp)
  }

  def detailedListing(requester: ActorRef) = {
    requester ! (self, loggedState, takeSnapshot(), level)
  }

  override def recognize(creature: ActorRef) = friends.getOrElse(creature.path.name, super.recognize(creature))

  override def receive = characterControlReceive orElse characterCommands orElse super.receive

  override def dead: Receive = ({
    case ReviveWith(items, location) => {
      context.unbecome()
      wounds = 5
      fatigue = level * 2
      XP = 0
      weaponEquiped = null
      trinketEquiped = null
      suitEquiped = null
      inventory.clear()
      inventory ++= items.map { AllObjects.map.get(_) }.filter { _ != None }.map { _.get }
      parent ! WorldBuilder.PositionTo(self, location)
    }
    case RequestSnapshot()        =>
    case AssignToPlayer(password) =>
  }: Receive) orElse characterControlReceive orElse super.dead

  def retired: Receive = ({
    case RequestSnapshot()        =>
    case AssignToPlayer(password) =>
  }: Receive) orElse characterControlReceive orElse ({
    case _ => //TODO: ControlCommand check to log back message
  }: Receive)

  def characterCommands: Receive = {
    case PotentialTalentCommand(x) if controlState == InControl    => Talent.parse(x, this)
    case PotentialTalentCommand(x) if x == "refocus"               => Talent.parse(x, this)
    case SpottedTombstone(description, tombstone, level, defiled)  => spottedTombstone(tombstone, description, level, defiled)
    case DigCommand(tombstone)                                     => dig(tombstone)
    case Creature.Presence(successes, location, attackType)        => presence(successes, location, attackType)
    case Creature.BefriendAttempt(roll, fromLocation)              => befriendAttempt(roll, fromLocation)
    case Creature.Befriended(name) if (Monster.is(sender))         => log(LoggedEvent(BefriendedYourTargetSuccessfullyEvent(sender)))
    case Creature.Befriended(name)                                 => befriended(name)
    case Creature.BefriendedBy(name)                               => befriendedBy(name)
    case UsedHealingFeature(wounds, description)                   => heal(wounds, description)
    case Learned(receipe)                                          => learned(receipe)
    case Found(item)                                               => found(item)
    case PickedUp(item)                                            => pickedUp(item)
    case QuickRestCommand()                                        => { if (actionCheck(Action.QuickRest)) quickRest() }
    case BefriendCommand(creature)                                 => { if (actionCheck(Action.Befriend)) befriend(creature) }
    case TeachCommand(receipe, creature)                           => { if (actionCheck(Action.Teach)) teach(receipe, creature) }
    case CraftCommand(item)                                        => { if (actionCheck(Action.Craft)) craft(item) }
    case PickUpCommand(item)                                       => { if (actionCheck(Action.Pick)) pickUp(item) }
    case EquipCommand(item)                                        => { if (actionCheck(Action.Equip)) equip(item) }
    case TakeOffCommand(item)                                      => { if (actionCheck(Action.Unequip)) takeOff(item) }
    case DropCommand(item)                                         => { if (actionCheck(Action.Drop)) drop(item) }
    case UseCommand()                                              => { if (actionCheck(Action.Use)) use() }
    case TargettedParryCommand(creature)                           => { if (actionCheck(Action.Parry)) deflect(Some(creature)) }
    case ParryCommand()                                            => { if (actionCheck(Action.Parry)) deflect(None) }
    case ReceiveTimeout                                            => if (controlState == InControl) rest()
    case Creature.SpottedTargetGotAttacked(c, by, dmg)             => if (c != self && by != self) log(LoggedEvent(TargetGotAttackedByOtherEvent(c, by, dmg)))
    case Train(talent)                                             => train(talent)
    case SufferFatigue(value)                                      => sufferFatigue(value)
    case PutEffortCommand(ammount, skill)                          => putEffortOn(skill, ammount.toInt)
    case StartAllEffortCommand(ammount)                            => startAllEffort(ammount.toInt)
    case StopAllEffortCommand()                                    => stopAllEffort()
    case GetSpottedTombstone(ref)                                  => sender ! tombstonesCache.get(ref)
    case GetActionCooldown(action)                                 => sender ! cooldowns.get(action)
    case SetActionCooldown(action, cd)                             => cooldowns += action -> cd(skillset)
    case Creature.StatusRequest(req, succcesses) if succcesses > 0 => req ! Creature.StatusResponse(self, detailedstatusForOthers)
    case x: CharacterGenerator.ReviveFrom                          => parent forward x
  }

  def characterControlReceive: Receive = {
    case Creature.ForceTakeControl(onDeath)                        => takecontrolFor(None, None, onDeath)
    case Creature.TakeControlFor(duration, onEnd, onDeath)         => takecontrolFor(Some(duration), Some(onEnd), onDeath)
    case Creature.DropControl()                                    => restoreControl()
    case RetirementRequest(password)                               => toHallOfFame(password, true)
    case Listing(requester)                                        => listing(requester)
    case DetailedListingRequest(requester)                         => detailedListing(requester)
    case RequestSnapshot()                                         => sender ! takeSnapshot()
    case LoadFromSnapshot(snapshot)                                => loadFromSnapShot(snapshot)
    case Creature.StatusCommand()                                  => log(LoggedEvent(status))
    case ReceipesKnownCommand()                                    => log(LoggedEvent(receipesKnownStatus))
    case InventoryCommand()                                        => log(LoggedEvent(inventoryStatus))
    case DetailedStatusCommand()                                   => log(LoggedEvent(detailedStatus))
    case TalentsCommand()                                          => log(LoggedEvent(talentstatus))
    case SkillsCommand()                                           => log(LoggedEvent(fullSkillSetWithDescriptionsStatus))
    case GainedXP(xp)                                              => gainedXP(xp)
    case LevelUpCommand(skill)                                     => levelUp(skill)
    case AssignToPlayer(password)                                  => assign(password)
    case UnassignFromPlayer()                                      => unassign()
    case Creature.Moved(location, d) if (loggedState == LoggedOut) => movedWhenLoaded(location, d)
    case GetArea(requester)                                        => requester ! Messages.AreaOfPlayer(currentLocation, owner)
    case GetName(requester)                                        => requester ! Messages.NameOfPlayer(characterName, owner)
    case GetAreaAndName(requester)                                 => requester ! Messages.AreaAndNameOfPlayer(currentLocation, characterName, owner)
    case SetFatigue(value)                                         => fatigue = value
    case SetXP(value)                                              => XP = value
    case Help(helpText) if (talents.isEmpty)                       => log(LoggedEvent(HelpTextEvent(helpText)))
    case Help(helpText)                                            => log(LoggedEvent(HelpTextEvent(helpText + talents.map(_._3).mkString("\r\n\r\n", "\r\n", ""))))
    case _ if (loggedState == LoggedOut)                           =>
  }

  override val supervisorStrategy = {
    OneForOneStrategy() {
      case _: Exception => Resume
    }
  }
}

object Character {
  val creatureType = "human"

  val actionTimers = scala.Predef.Map[Action, Skillset => FiniteDuration](
    Action.Flank -> (s => (25 - s.weapons.bonus).max(10) seconds),
    Action.Parry -> (s => (30 - s.reflexes.bonus).max(12) seconds),
    Action.Protect -> (s => (60 - 3 * s.willpower.bonus).max(15) seconds),
    Action.Hide -> (s => (30 - s.reflexes.bonus).max(10) seconds),
    Action.Steal -> (s => (210 - 5 * s.reflexes.bonus).max(60) seconds),
    Action.Sneak -> (s => (60 - 2 * s.reflexes.bonus).max(20) seconds),
    Action.Befriend -> (s => (90 - 2 * s.influence.bonus).max(30) seconds),
    Action.QuickRest -> (s => 150 seconds))

  def STATLIMIT(level: Int) = 5 + (level / 5)
  def TALENTLIMIT(level: Int) = (level / 10)
  def props(characterName: String, password: String, skillset: Skillset) = Props(new Character(characterName, password, skillset))

  def is(actor: ActorRef) = actor.path.elements.toSet.contains(CharacterGenerator.NAME)

  case class GainedXP(xp: Int) extends NotInfluenceReceiveTimeout
  case class LevelUpCommand(skill: String) extends NotInfluenceReceiveTimeout
  case class AssignToPlayer(password: String) extends NotInfluenceReceiveTimeout
  case class UnassignFromPlayer() extends NotInfluenceReceiveTimeout
  case class GetArea(requester: ActorRef) extends NotInfluenceReceiveTimeout
  case class GetName(requester: ActorRef) extends NotInfluenceReceiveTimeout
  case class GetAreaAndName(requester: ActorRef) extends NotInfluenceReceiveTimeout
  case class GetActionCooldown(action: Action) extends NotInfluenceReceiveTimeout
  case class SetActionCooldown(action: Action, cd: Skillset => Deadline) extends NotInfluenceReceiveTimeout
  case class GetSpottedTombstone(ref: String) extends NotInfluenceReceiveTimeout
  case class SpottedTombstone(description: String, tombstone: ActorRef, level: Int, defiled: Boolean) extends NotInfluenceReceiveTimeout
  case class Learned(receipe: Receipe) extends NotInfluenceReceiveTimeout
  case class PickedUp(item: Object) extends NotInfluenceReceiveTimeout
  case class Found(item: Object) extends NotInfluenceReceiveTimeout
  case class UsedHealingFeature(amountHealed: Int, description: String) extends NotInfluenceReceiveTimeout
  case class QuickRestCommand() extends ACCommand
  case class CraftCommand(receipe: String) extends ACCommand
  case class TeachCommand(receipe: String, creature: String) extends ACCommand
  case class PickUpCommand(item: String) extends ACCommand
  case class EquipCommand(item: String) extends ACCommand
  case class TakeOffCommand(item: String) extends ACCommand
  case class DropCommand(item: String) extends ACCommand
  case class UseCommand() extends ACCommand
  case class TargettedParryCommand(creature: String) extends ACCommand
  case class ParryCommand() extends ACCommand
  case class DigCommand(tombstone: String) extends ACCommand
  case class PotentialTalentCommand(command: String) extends NotInfluenceReceiveTimeout with ACCommand
  case class BefriendCommand(creature: String) extends CCommand
  case class StopAllEffortCommand() extends CCommand
  case class StartAllEffortCommand(ammount: String) extends CCommand
  case class PutEffortCommand(ammount: String, skill: String) extends CCommand
  case class ReceipesKnownCommand() extends NotInfluenceReceiveTimeout with CCommand
  case class InventoryCommand() extends NotInfluenceReceiveTimeout with CCommand
  case class DetailedStatusCommand() extends NotInfluenceReceiveTimeout with CCommand
  case class TalentsCommand() extends NotInfluenceReceiveTimeout with CCommand
  case class SkillsCommand() extends NotInfluenceReceiveTimeout with CCommand
  case class Help(text: String) extends NotInfluenceReceiveTimeout
  case class RequestSnapshot() extends NotInfluenceReceiveTimeout
  case class LoadFromSnapshot(snapshot: SnapshotV003) extends NotInfluenceReceiveTimeout
  case class Listing(requester: ActorRef) extends NotInfluenceReceiveTimeout
  case class RetirementRequest(password: String) extends NotInfluenceReceiveTimeout
  case class DetailedListingRequest(requester: ActorRef) extends NotInfluenceReceiveTimeout
  case class SetFatigue(value: Int) extends NotInfluenceReceiveTimeout
  case class SetXP(value: Int) extends NotInfluenceReceiveTimeout
  case class Train(talentName: String) extends NotInfluenceReceiveTimeout
  case class SufferFatigue(value: Int) extends NotInfluenceReceiveTimeout
  case class ReviveWith(items: Seq[String], location: String) extends NotInfluenceReceiveTimeout

  case class SnapshotV002(characterName: String,
                          password: String,
                          skillset: String,
                          receipesLearned: scala.Predef.Set[String],
                          friends: scala.Predef.Set[String],
                          weaponEquiped: String,
                          suitEquiped: String,
                          trinketEquiped: String,
                          inventory: Seq[String],
                          location: Tuple2[String, String],
                          wounds: Int,
                          fatigue: Int,
                          xp: Int,
                          timePlayed: Long,
                          talents: scala.Predef.Set[String]) extends NotInfluenceReceiveTimeout

  case class SnapshotV003(characterName: String,
                          password: String,
                          skillset: String,
                          receipesLearned: scala.Predef.Set[String],
                          friends: scala.Predef.Set[String],
                          weaponEquiped: String,
                          suitEquiped: String,
                          trinketEquiped: String,
                          inventory: Seq[String],
                          location: Tuple2[String, String],
                          wounds: Int,
                          fatigue: Int,
                          fatePoints: Int,
                          xp: Int,
                          timePlayed: Long,
                          talents: scala.Predef.Set[String]) extends NotInfluenceReceiveTimeout
}