package kt.game.events

import kt.game.requests.ListRequestHandler
import scala.concurrent.duration.DurationLong
import akka.actor.ActorRef
import kt.game.player.HallOfFame

object InfoEvents {
  case object WrongPasswordEvent extends InfoEvent {
    override def toString = "Password does NOT match. Try again..."
  }
  case object NowExitingEvent extends InfoEvent {
    override def toString = "\r\nNow exiting..."
  }
  case class OnlineCharacterListingEvent(listings: Seq[ListRequestHandler.Listing]) extends InfoEvent {
    override def toString = listings.map(x => ("%5s" format x.level) + "    " +
      ("%15s" format x.name) + "    " +
      ("%15s" format ((x.timePlayed) milliseconds).toMinutes) + "    " +
      ("%8s" format x.receipesKnown) + "    " +
      ("%25s" format x.currentLocation))
      .mkString(("level" + "    " +
        ("%15s" format "  name") + "    " +
        ("%15s" format "minutes played") + "    " +
        ("%8s" format "recipes") + "    " +
        ("%25s" format "current location") + "\r\n"), "\r\n", "")
  }
  case object DublicateTombstoneNameEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "Duplicate tombstone name ..."
  }
  case object CanOnlyControlOneCharacterEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "You can only control one character at a time!"
  }
  case class CharacterControlSuccessEvent(characterName: String, character: ActorRef) extends InfoEvent {
    override def toString = s"You are now controlling $characterName"
  }
  case class CharacterControlDroppedEvent(characterName: String, character: ActorRef) extends InfoEvent {
    override def toString = s"You are NO longer controlling character $characterName"
  }
  case object CanNotControlYourCharacterEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "You do not have control of your character!"
  }
  case class HallOfFameListingEvent(listings: Seq[HallOfFame.Listing]) extends InfoEvent {
    override def toString = listings.map(x => ("%5s" format x.rank) + "    " +
      ("%5s" format x.alive) + "    " +
      ("%5s" format x.level) + "    " +
      ("%15s" format x.name) + "    " +
      ("%12s" format x.receipesKnown) + "    " +
      ("%12s" format x.talentsKnown) + "    " +
      ("%8s" format ((x.timePlayed) milliseconds).toMinutes) + "    " +
      ("%12s" format x.weapon) + "    " +
      ("%12s" format x.suit) + "    " +
      ("%12s" format x.trinket) + "    ")
      .mkString((("%5s" format "rank") + "    " +
        ("%5s" format "alive") + "    " +
        ("%5s" format "level") + "    " +
        ("%15s" format "name") + "    " +
        ("%12s" format "receipes") + "    " +
        ("%12s" format "talents") + "    " +
        ("%8s" format "minutes") + "    " +
        ("%12s" format "weapon") + "    " +
        ("%12s" format "suit") + "    " +
        ("%12s" format "trinket") + "\r\n"), "\r\n", "")
  }
  case object EnteredHallOfFameOnDeathEvent extends InfoEvent {
    override def toString = "Though your death was shameful, you were strong enough and thus still enter the hall of fame, albeit with a penalty in ranking!"
  }
  case object CouldNotEnterHallOfFameEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "You need to be at least level 15 to retire and have an item equiped in each slot!"
  }
  case object CanNotRetireForeignCharacterEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "You do not control any character to retire..."
  }
  case object NameIsNotUniqueForNewCharacterEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "Character could not be created! name is not unique."
  }
  case object NameIsNotFormattedForNewCharacterEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "Character name must be between 3 and 15 characters long and contain only alphanumerical characters and must not start or end with a number."
  }
  case object CharacterNotFoundLoginFailureEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "Character does not exist!"
  }
  case object CharacterIsDeadOrRetiredLoginFailureEvent extends InfoEvent with ActionFailedEvent {
    override def toString = "Character is dead or retired!"
  }

  case class ACharacrerDiedBroadcastEvent(name: String) extends InfoEvent {
    override def toString = s"$name just died! He has left behind his tombstone!"
  }

  case class PlayerKillEvent(killer: ActorRef, killerName: String, victim: ActorRef, honorableKill: Boolean) extends InfoEvent {
    override def toString = if (honorableKill) s"$killerName just killed a character in honorable combat!"
    else s"$killerName just killed a character of much lower level! Shame on him!"
  }
}