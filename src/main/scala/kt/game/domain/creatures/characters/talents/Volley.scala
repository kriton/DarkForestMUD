package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.Ballistics
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import kt.game.events.NonCombatEvent
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.AttackType
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.ActionFailedEvent
import kt.game.events.MultiAttackImpossibleEvent
import kt.game.events.AttackImpossibleReason

class Volley private (val character: ActorRef) extends Actor with Talent {
  import Volley._
  import context._

  val name = Volley.name
  val description = Volley.description
  val help = Volley.commandsHelp

  def setCD() = character ! Character.SetActionCooldown(action, s => ((45 - s.ballistics.bonus * 2).max(10) seconds).fromNow)

  implicit val timeout = Timeout(200 milliseconds)

  def volleyAttempt(targets: Option[Seq[String]]) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      val creatures = Await.result(character ? Creature.GetSpottedTargets(targets), timeout.duration).asInstanceOf[Option[Seq[ActorRef]]]
      if (creatures != None) {
        volleyCheck(creatures)
      } else {
        character ! Creature.Message(MultiAttackImpossibleEvent(AttackType.Ranged, AttackImpossibleReason.CanNoLongerSpot))
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def volleyCheck(targets: Option[Seq[ActorRef]]) = {
    val volleyRoll = Await.result(character ? Creature.Roll(SkillName.BALL), timeout.duration).asInstanceOf[Option[Int]].get
    val charLocation = Await.result(character ? Creature.GetCurrentLocation(), timeout.duration).asInstanceOf[Option[ActorRef]]
    setCD()
    targets.get foreach { target => target.tell(Creature.Attacked(volleyRoll, charLocation.get, AttackType.Ranged), character) }
  }

  def receive = {
    case TalentVolleyCommand(refs)  => volleyAttempt(Some(refs))
    case TalentVolleyCommandToAll() => volleyAttempt(None)
  }
}

object Volley extends TalentContext {
  def props(character: ActorRef) = Props(new Volley(character))

  case class TalentVolleyCommand(ref: Seq[String])
  case class TalentVolleyCommandToAll()

  val skill = Ballistics(12)

  val action = TalentAction.Volley
  val name = action.toString

  val description = "You can make a volley of ranged attacks as a single action. The frequency that you can do so, depends on your ballistics skill."
  val trainingDescription = "You can train your arcane skills and learn the secret of arcane volley attacks! " + requirementsString

  val TCP_volleyAll = "^volley$".r
  val TCP_volleySelected = "^volley (\\d+(?: \\d+)*)$".r

  val commandsHelp =
    ("%-32s" format "volley <target1> <target2> etc") + " -->  Make a ranged attack against each selected target.\r\n" +
      ("%-32s" format "volley") + " -->  Make a ranged attack against each spotted target.\r\n"
}