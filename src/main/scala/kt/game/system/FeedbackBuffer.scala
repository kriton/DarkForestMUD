package kt.game.system

import java.util.Date

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.FiniteDuration

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.NotInfluenceReceiveTimeout
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import kt.game.events.LoggedEvent

class FeedbackBuffer private (flushFrequency: FiniteDuration) extends Actor  {
  import FeedbackBuffer._
  import context._

  setReceiveTimeout(flushFrequency)
  println("feedback buffer created..")
  val eventBuffer = ListBuffer[(LoggedEvent, ActorRef)]()

  def receive = {
    case Feedback(x)                             => eventBuffer += Tuple2(x, sender);
    case ReceiveTimeout if (eventBuffer.isEmpty) =>
    case ReceiveTimeout => {
      eventBuffer foreach { e => println(s"${new Date(e._1.event.time)}: ${e._1.generator.path.toStringWithoutAddress} -> ${e._2.path.name} ==> ${e._1.event}") }
      parent ! Connection.Feedback(eventBuffer.map(_._1).toSeq); eventBuffer.clear()
    }
  }
}

object FeedbackBuffer {
  def props(flushFrequency: FiniteDuration) = Props(new FeedbackBuffer(flushFrequency))
  case class Feedback(message: LoggedEvent) extends NotInfluenceReceiveTimeout
}