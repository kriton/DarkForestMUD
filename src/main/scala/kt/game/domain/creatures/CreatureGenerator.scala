package kt.game.domain.creatures

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.OneForOneStrategy
import akka.actor.Props
import akka.actor.SupervisorStrategy.Resume
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.CharacterGenerator
import kt.game.domain.creatures.monsters.MonsterGenerator
import kt.game.domain.tombstones.TombstoneGenerator
import kt.game.domain.worldbuilder.WorldBuilder

class CreatureGenerator private (db: ActorRef) extends Actor {
  import CreatureGenerator._

  var monsterGenerator: Option[ActorRef] = None
  var characterGenerator: Option[ActorRef] = None
  var worldBuilder: Option[ActorRef] = None
  var tombstoneGenerator: Option[ActorRef] = None

  def receive = {
    case GetMonsterGenerator() => {
      if (monsterGenerator == None) monsterGenerator = Some(context.actorOf(MonsterGenerator.props, MonsterGenerator.NAME))
      sender ! monsterGenerator.get
    }
    case GetCharacterGenerator() => {
      if (characterGenerator == None) characterGenerator = Some(context.actorOf(CharacterGenerator.props(db), CharacterGenerator.NAME))
      sender ! characterGenerator.get
    }
    case IntroducerTombstoneGenerator(actor)                              => tombstoneGenerator = Some(actor)
    case IntroduceWorldBuilder(actor)                                     => worldBuilder = Some(actor)
    case x: TombstoneGenerator.NewTombstone if tombstoneGenerator != None => tombstoneGenerator.get forward x
    case x: WorldBuilder.PositionTo if worldBuilder != None               => worldBuilder.get forward x
  }

  override val supervisorStrategy = {
    OneForOneStrategy() {
      case _: Exception => Resume
    }
  }
}

object CreatureGenerator {
  val NAME = "creatures"
  def props(db: ActorRef) = Props(new CreatureGenerator(db))

  case class GetMonsterGenerator()
  case class GetCharacterGenerator()
  case class IntroduceWorldBuilder(actor: ActorRef)
  case class IntroducerTombstoneGenerator(actor: ActorRef)
}