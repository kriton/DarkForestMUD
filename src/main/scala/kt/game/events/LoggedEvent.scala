package kt.game.events

import java.util.ArrayList
import scala.collection.JavaConversions.asScalaBuffer
import kt.game.domain.creatures.skills.Skill
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.locations.Feature
import kt.game.domain.locations.Features
import kt.game.domain.creatures.Target
import kt.game.domain.creatures.RelativeLocation
import kt.game.domain.creatures.RelativeLocation
import akka.actor.ActorRef
import kt.game.domain.items.Object

sealed class GameEvent(val priority: Int, val time: Long = System.currentTimeMillis)

sealed trait Personal

abstract class CombatEvent extends GameEvent(290)

case class RollEvent(rolls: Seq[Int], modifier: Int, result: Int, skill: Skill) extends GameEvent(10) {
  override def toString = "You rolled [" + rolls.mkString("(", ", ", ")") + " + " + modifier + " = " + result + "]  on your " + skill.getClass.getSimpleName + " check."
}

abstract class MovementEvent extends GameEvent(20)

abstract class LocationEvent(priority: Int) extends GameEvent(priority)

case class MonsterEvent(message: String) extends GameEvent(240) {
  override def toString = message
}

abstract class NonCombatEvent extends GameEvent(300)

abstract class StatusEvent extends GameEvent(-80)

abstract class InfoEvent extends GameEvent(-170) with Personal

case class GMInfoEvent(message: String) extends GameEvent(-180) with Personal {
  override def toString = message
}

abstract class DevelopmentEvent extends GameEvent(250)

trait ActionFailedEvent // extends GameEvent(310)

sealed trait ActionFailedReason
object ActionFailedReason {
  case object CanNoLongerSpot extends ActionFailedReason
  case object CheckFailure extends ActionFailedReason
  case object NotInSameLocation extends ActionFailedReason
  case object WrongInput extends ActionFailedReason
  case object InvalidState extends ActionFailedReason
}

case object UnrecognizedCommandEvent extends GameEvent(400) with ActionFailedEvent {
  override def toString = "Unrecognized command or arguements!"
}

abstract class ChatEvent extends GameEvent(-20) with Personal

case object NullEvent extends GameEvent(370) {
  override def toString = ""
}

case class LoggedEvent(val event: GameEvent)(implicit val generator: ActorRef)

//TODO: All LoggedEvents should carry a timestamp, the actor that generated the event, the recipient creature ActorRef & later enriched with the recipient playerRef?

object LoggedEvent { 
  implicit def gameEvent2LoggedEvent(event: GameEvent)(implicit generator: ActorRef) = LoggedEvent(event)(generator)
  
  val orderingByType: Ordering[LoggedEvent] = Ordering.by { e => e.event.priority }
  val orderingByLex: Ordering[LoggedEvent] = Ordering.by { e => e.event.toString }

  val orderingByTypeAndMessageLex: Ordering[LoggedEvent] = Ordering.by { e: LoggedEvent => (e, e) }(Ordering.Tuple2(orderingByType, orderingByLex))
  
}

