package kt.game.requests

import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.Character
import kt.game.player.PlayerRegistry
import kt.game.events.ChatEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.LoggedEvent
import kt.game.events.NullEvent
import kt.game.events.ChatEvents

class ShoutRequestHandler(val player: ActorRef, val playerRegistry: ActorRef, val text: String) extends Actor {
  import Messages._

  context.setReceiveTimeout(250 milliseconds)
  player ! Character.GetAreaAndName(self)

  var playerArea: ActorRef = null
  var characterName: String = null

  def receive = {
    case AreaOfPlayer(area, player) if (player != this.player && area == playerArea) => player ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.AreaChatEvent(characterName, playerArea, text)))
    case AreaAndNameOfPlayer(area, name, player)                                     => { playerArea = area; characterName = name; playerRegistry ! PlayerRegistry.GetAreas(self); this.player ! FeedbackBuffer.Feedback(LoggedEvent(NullEvent)) }
    case ReceiveTimeout                                                              => self ! PoisonPill
    case _                                                                           =>
  }
}

object ShoutRequestHandler {
  def props(player: ActorRef, playerRegistry: ActorRef, text: String) = Props(new ShoutRequestHandler(player, playerRegistry, text))
}