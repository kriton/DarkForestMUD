package kt.game.system

import java.net.Socket
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.player.gamemaster.GMRegistry
import kt.game.player.gamemaster.GameMaster
import kt.game.player.PlayerRegistry

class GMConnection(gmRegistry: ActorRef, playerRegistry: ActorRef, builder: ActorRef, clientSocket: Socket) extends Connection(playerRegistry, builder, clientSocket) {
  import GMConnection._

  override def newPlayer() = {
    implicit val timeout = Timeout(5 seconds)
    print("Enter password: ")
    val password = filterInput(in.readLine())
    val future = gmRegistry ? GMRegistry.Login(password, feedbackBuffer, clientSocket.getRemoteSocketAddress.toString)
    val result = Await.result(future, timeout.duration)
    if (result.isInstanceOf[ActorRef]) {
      player = result.asInstanceOf[ActorRef]
      true
    } else {
      print(result.toString())
      predestroy()
      false
    }
  }
  override def handle(input: String) = input match {
    case allItemsCP()                           => player ! GameMaster.AllItemsCommand()
    case filteredItemsCP(filter)                => player ! GameMaster.FilteredItemsCommand(filter.trim)
    case addItemsToMonstersCP(filter, items)    => player ! GameMaster.AddToInventoryOfMonstersCommand(filter.trim, items.trim)
    case clearMonsterInventoryCP(filter)        => player ! GameMaster.ClearInventoryOfMonstersCommand(filter.trim)
    case reskillMonstersCP(filter, skills)      => player ! GameMaster.ChangeSkillsOfMonstersCommand(filter.trim, skills.trim)
    case repositionMonstersCP(filter, location) => player ! GameMaster.RepositionMonsterCommand(filter.trim, location.trim)
    case resetMonstersCP(filter)                => player ! GameMaster.ResetMonsterCommand(filter.trim)
    case startMonitoringMonstersCP(filter)      => player ! GameMaster.MonitorMonsterStartCommand(filter.trim)
    case stopMonitoringMonstersCP(filter)       => player ! GameMaster.MonitorMonsterStopCommand(filter.trim)
    case controlMonstersCP(filter)              => player ! GameMaster.ControlMonsterStartCommand(filter.trim)
    case uncontrolMonstersCP(filter)            => player ! GameMaster.ControlMonsterStopCommand(filter.trim)
    case allMonstersCP()                        => player ! GameMaster.AllMonstersCommand()
    case filteredMonstersCP(filter)             => player ! GameMaster.FilteredMonstersCommand(filter.trim)
    case newTombstoneCP(name, lvl, loc, items)  => player ! GameMaster.CreateTombstoneCommand(name, lvl, loc, items)
    case allTombstonesCP()                      => player ! GameMaster.AllTombstonesCommand()
    case reviveFromTombstonesCP(filter)         => player ! GameMaster.ReviveFromTombstoneCommand(filter.trim)
    case filteredTombstonesCP(filter)           => player ! GameMaster.FilteredTombstonesCommand(filter.trim)
    case woundsToCharsCP(filter, value)         => player ! GameMaster.SetWoundsToCharsCommand(filter.trim, value)
    case fatigueToCharsCP(filter, value)        => player ! GameMaster.SetFatigueToCharsCommand(filter.trim, value)
    case xpToCharsCP(filter, value)             => player ! GameMaster.SetXPToCharsCommand(filter.trim, value)
    case reskillCharsCP(filter, skills)         => player ! GameMaster.ChangeSkillsOfCharsCommand(filter.trim, skills.trim)
    case repositionCharsCP(filter, location)    => player ! GameMaster.RepositionCharsCommand(filter.trim, location.trim)
    case startMonitoringCharsCP(filter)         => player ! GameMaster.MonitorCharStartCommand(filter.trim)
    case stopMonitoringCharsCP(filter)          => player ! GameMaster.MonitorCharStopCommand(filter.trim)
    case controlCharsCP(filter)                 => player ! GameMaster.ControlCharStartCommand(filter.trim)
    case uncontrolCharsCP(filter)               => player ! GameMaster.ControlCharStopCommand(filter.trim)
    case addItemsToCharsCP(filter, items)       => player ! GameMaster.AddToInventoryOfCharsCommand(filter.trim, items.trim)
    case clearCharsInventoryCP(filter)          => player ! GameMaster.ClearInventoryOfCharsCommand(filter.trim)
    case addTalentToCharsCP(filter, talent)     => player ! GameMaster.TrainTalentOnCharsCommand(filter.trim, talent.trim)
    case allCharsCP()                           => player ! GameMaster.AllCharsCommand()
    case filtertedCharsCP(filter)               => player ! GameMaster.FilteredCharsCommand(filter.trim)
    case hintAllCP(hint)                        => playerRegistry ! PlayerRegistry.HintAll(hint)
    case _                                      => super.handle(input)
  }

}

object GMConnection {
  def props(gmRegistry: ActorRef, playerRegistry: ActorRef, builder: ActorRef, clientSocket: Socket) = Props(new GMConnection(gmRegistry, playerRegistry, builder, clientSocket))

  val allItemsCP = "^items all$".r
  val filteredItemsCP = "^items (.{3,})$".r
  val allMonstersCP = "^monsters all$".r
  val filteredMonstersCP = "^monsters (.{3,})$".r
  val resetMonstersCP = "^monsters (.{3,}?) reset$".r
  val repositionMonstersCP = "^monsters (.{3,}?) to (.{3,})$".r
  val addItemsToMonstersCP = "^monsters (.{3,}?) add (.{3,})$".r
  val clearMonsterInventoryCP = "^monsters (.{3,}?) clear inventory$".r
  val reskillMonstersCP = "^monsters (.{3,}?) reskill((?: [A-z]{4} \\-?\\d+)+)$".r
  val startMonitoringMonstersCP = "^monsters (.{2,}?) monitor start$".r
  val stopMonitoringMonstersCP = "^monsters (.{2,}?) monitor stop$".r
  val controlMonstersCP = "^monsters (.{2,}?) control start$".r
  val uncontrolMonstersCP = "^monsters (.{2,}?) control stop$".r
  val reviveFromTombstonesCP = "^tombstones (.{3,}) revive$".r
  val newTombstoneCP = "^tombstones new (.{3,}?) of (\\d+) at (.{3,}?) with ((?:.*?)(?:, .*?)*)$".r
  val allTombstonesCP = "^tombstones all$".r
  val filteredTombstonesCP = "^tombstones (.{3,})$".r
  val allCharsCP = "^chars all$".r
  val filtertedCharsCP = "^chars (.{2,})$".r
  val woundsToCharsCP = "^chars (.{2,}?) wounds (\\d+)$".r
  val xpToCharsCP = "^chars (.{2,}?) xp (\\d+)$".r
  val fatigueToCharsCP = "^chars (.{2,}?) fatigue (\\d+)$".r
  val reskillCharsCP = "^chars (.{2,}?) reskill((?: [A-z]{4} \\-?\\d+)+)$".r
  val repositionCharsCP = "^chars (.{2,}?) to (.{3,})$".r
  val startMonitoringCharsCP = "^chars (.{2,}?) monitor start$".r
  val stopMonitoringCharsCP = "^chars (.{2,}?) monitor stop$".r
  val controlCharsCP = "^chars (.{2,}?) control start$".r
  val uncontrolCharsCP = "^chars (.{2,}?) control stop$".r
  val addItemsToCharsCP = "^chars (.{3,}?) add (.{3,})$".r
  val clearCharsInventoryCP = "^chars (.{3,}?) clear inventory$".r
  val addTalentToCharsCP = "^chars (.{3,}?) train (.{3,})$".r
  val hintAllCP = "^hint (.+)$".r
}