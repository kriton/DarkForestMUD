package kt.game.requests.talents

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import kt.game.events.CombatEvent
import kt.game.utility.Result
import kt.game.events.StunEvent
import kt.game.domain.creatures.Target
import kt.game.events.ActionFailedReason
import kt.game.events.talents.StunEvents.StunFailedEvent

class MonsterStunRequestHandler private (monster: ActorRef, creatureType: String, target: Target, roll: Int) extends Actor {
  import MonsterStunRequestHandler._
  import context._

  setReceiveTimeout(300 milliseconds)

  implicit val timeout = Timeout(120 milliseconds)
  val defense = Await.result(target.ref.get ? Creature.GetMaxSkillPassive(Seq(SkillName.REFL, SkillName.ATHL)), timeout.duration).asInstanceOf[Option[Int]].get
  val successes = Result.criticalSteps(roll - defense)
  if (successes >= 0) {
    val duration = 5 * (successes + 1)
    target.ref.get tell (Creature.StunnedConditionStart(duration seconds), monster)
    monster ! Creature.Message(StunEvent(target, duration))
  } else {
    monster ! Creature.Message(StunFailedEvent(ActionFailedReason.CheckFailure))
  }

  def receive = {
    case ReceiveTimeout => self ! PoisonPill
  }
}

object MonsterStunRequestHandler {
  def props(monster: ActorRef, creatureType: String, target: Target, roll: Int) = Props(new MonsterStunRequestHandler(monster, creatureType, target, roll))
}