package kt.game.domain.creatures.monsters

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.DurationInt
import scala.util.Random
import Monster.ForgetFriend
import Monster.Respawn
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.InControl
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.items.Object
import kt.game.domain.locations.Location
import kt.game.requests.gm.GetMonstersRequestHandler
import kt.game.events.ActionFailedEvent
import kt.game.events.CombatEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.LoggedEvent
import kt.game.events.MonsterEvent
import kt.game.events.Personal
import kt.game.events.StatusEvent
import kt.game.utility.Result
import kt.game.domain.creatures.Action
import akka.actor.Cancellable
import scala.concurrent.duration.Deadline
import kt.game.domain.creatures.monsters.ai.Berserker
import kt.game.events.NonCombatEvent
import kt.game.domain.creatures.Stunned
import kt.game.domain.creatures.monsters.ai.Stunner
import kt.game.domain.creatures.monsters.ai.Rooter
import scala.concurrent.duration.FiniteDuration
import kt.game.domain.creatures.AttackType
import kt.game.domain.creatures.characters.Character
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.events.StatusEvents._
import kt.game.events.BefriendFailedEvent
import kt.game.events.ActionFailedReason
import kt.game.events.ActionFailedEvent
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.MonsterDroppedItemsEvent

sealed trait LifeState
case object Alive extends LifeState
case object Dead extends LifeState

trait Behaviour

class Monster private (creatureType: String, mskillset: Skillset) extends Creature(creatureType, mskillset) {
  import Monster._
  import context._

  def healFrequency() = 90 seconds
  def aggroDecayFrequency() = 10 seconds

  def actionFrequency = globalDelay + (1 second)

  val allSchedules = ListBuffer[() => Cancellable]()

  var globalCD = Deadline.now
  override def FATE: Int = 0

  override def actionCheck(action: Action): Boolean = super.actionCheck(action) && (inControl() || fromController())

  override def baseDefense = passive(skillset.weapons)

  def inControl() = controller != None && controller.get._1 == self
  def fromController() = if (controller != None && sender == controller.get._1 && globalCD.isOverdue) {
    globalCD = (globalDelay + (1 second)).fromNow
    true
  } else {
    if (controller != None && sender == controller.get._1 && globalCD.hasTimeLeft) {
      sender ! FeedbackBuffer.Feedback(LoggedEvent(ActionOnCoolDownEvent(None, globalCD.timeLeft.toMillis / 1000.0)))
    }
    false
  }

  var aggro = Map[ActorRef, (Int, Boolean)]()

  system.scheduler.schedule(healFrequency, healFrequency, self, Creature.Heal(1))
  system.scheduler.schedule(aggroDecayFrequency, aggroDecayFrequency, self, Monster.AggroDecay())

  val befriendedCharacters: scala.collection.mutable.Set[ActorRef] = scala.collection.mutable.Set()
  val initialInventory = ListBuffer[Object]()

  var initialLocation: ActorRef = null
  def globalCDStatus() = GlobalCDStatus(INITIATIVE, actionFrequency.toMillis / 1000.0)
  override def status() = StatusOfEvent(creatureTypeStatus, locationStatus, woundStatus, conditionsStatus, simpleSkillStatus)
  def detailedStatus() = StatusOfEvent(creatureTypeStatus, locationStatus, woundStatus, conditionsStatus, fullSkillStatus, globalCDStatus, inventoryStatus)

  def creatureTypeStatus() = CreatureTypeStatus(creatureType, Option(detailedDescription))
  
  def behaviour = this.getClass.getInterfaces.filter { classOf[Behaviour].isAssignableFrom(_) } match {
    case x if (x.isEmpty) => "Static"
    case x                => x.map(_.getSimpleName).mkString("-") //.head.getSimpleName
  }

  override def detailedDescription = ""

  def currentBehaviour: String = aggro.toSeq.sortBy(-_._2._1).map(e => e._1.path.name + " -> (" + e._2._1 + (if (!e._2._2) "!" else "") + ")").mkString(", ")

  def aggroDecay() = if (!aggro.isEmpty) aggro = aggro.map(e => e._1 -> (e._2._1 * (if (spottedCache.values.toSet.contains(e._1)) 8 else 3) / 10, e._2._2)).filter(_._2._1 > 0)
  def dropAggro(target: ActorRef) = {
    aggro = aggro.filter(_._1 != target)
  }
  def addAggro(target: ActorRef, ammount: Int) = {
    aggro += target -> (aggro.get(target).orElse(Some((0, true))).get._1 + ammount, true)
    singleSpotAttempt(target, 10000)
  }
  def aggroNotAvailable(target: ActorRef) = {
    aggro += target -> (aggro.get(target).orElse(Some((0, true))).get._1, false)
  }
  def targetFrom(targets: Set[ActorRef]) = highTarget(aggro.filter(targets contains _._1))
  def priorityTarget() = highTarget(aggro)
  def aggroOf(target: ActorRef) = aggro.filter(e => !isFriend(e._1)).get(target).getOrElse(0, false)._1
  def allAggro() = aggro.filter(e => !isFriend(e._1) && (spottedCache.values.toSet contains e._1))

  def highTarget(aggro: Map[ActorRef, (Int, Boolean)]) = Option(aggro.filter(e => !isFriend(e._1)).foldLeft((null: ActorRef, 0))((r, c) => c._2._1 match {
    case a if a < r._2 && c._2._2 => r
    case _                        => (c._1, c._2._1)
  })._1)

  def listing(requester: ActorRef, state: LifeState) = {
    requester ! GetMonstersRequestHandler.Listing(self,
      behaviour,
      self.path.name,
      initialLocation.path.name,
      (if (currentLocation != null) currentLocation.path.name else "DEAD"),
      locationDescription toString,
      creatureType,
      skillset,
      wounds,
      woundStatus.toString,
      inventory,
      currentBehaviour,
      state)
  }

  override def spotted(creature: ActorRef, inLocation: ActorRef, description: String, strength: Option[Int], status: Option[String]) = {
    super.spotted(creature, inLocation, description, strength, status)
    if (Character.is(creature) && !isFriend(creature)) {
      sender ! Creature.TestPresence(roll(skillset.willpower), currentLocation, AttackType.NonAttack)
    }
  }

  override def death() = {
    if (!inventory.isEmpty) {
      sender ! Creature.Message(MonsterDroppedItemsEvent)
      inventory foreach { currentLocation ! Location.AddItem(_) }
    }
    super.death()
    controller = Some(self, None, None)
    system.scheduler.scheduleOnce((150 + Random.nextInt(150)) seconds, self, Respawn())
  }

  override def log(event: LoggedEvent) = {
    if (!event.event.isInstanceOf[Personal] && !event.event.isInstanceOf[StatusEvent] && !viewers.isEmpty) viewers.foreach { _ ! FeedbackBuffer.Feedback(event) }
    if (!event.event.isInstanceOf[Personal] && controller != None && controller.get._1 != self) controller.get._1 ! FeedbackBuffer.Feedback(event)
  }

  def reset() = {
    this.aggro = Map()
    buffsAndPenalties.clear
    if (controller != None && controller.get._2 != None) controller.get._2.get(self)
    controller = Some(self, None, None)
    controlState = InControl
    wounds = 0;
    skillset = mskillset
    befriendedCharacters.clear()
    inventory.clear()
    inventory ++= initialInventory
    if (currentLocation != initialLocation) position(initialLocation)
  }

  override def position(location: ActorRef) = {
    if (initialLocation == null) {
      controller = Some(self, None, None)
      initialLocation = location
    } else println("Repositioning monster to " + location + ". Request by: " + sender)
    super.position(location)
  }

  override def statusDescriptionAgainst(creature: ActorRef, spotResult: Int) = {
    super.statusDescriptionAgainst(creature, spotResult) + (if (isFriend(creature)) " It is friendly." else "")
  }

  def befriendCheck(roll: Int) = Result.criticalSteps(roll - skillset.willpower.passive)

  def befriendAttempt(roll: Int, fromLocation: ActorRef) = {
    val penalty = aggro.get(sender).orElse(Some((0, true))).get._1 / 10
    val successes = befriendCheck(roll - penalty)
    if (fromLocation == currentLocation) {
      if (successes >= 0) {
        befriendedCharacters += sender
        sender ! Creature.Befriended(creatureType)
        system.scheduler.scheduleOnce(120 + (120 * successes) seconds, self, ForgetFriend(sender))
        log(LoggedEvent(MonsterEvent(sender.path.name + " befriended you")))
      } else {
        sender ! Creature.Message(BefriendFailedEvent(sender, ActionFailedReason.CheckFailure))
      }
    } else sender ! Creature.Message(BefriendFailedEvent(sender, ActionFailedReason.NotInSameLocation))
  }

  def isFriend(character: ActorRef) = befriendedCharacters.contains(character)

  override def attacked(attackRoll: Int, from: ActorRef, attackType: AttackType) = {
    super.attacked(attackRoll, from, attackType)
    if (befriendedCharacters.contains(sender)) {
      befriendedCharacters -= sender
      addAggro(sender, AGGRO_MAX)
    } else addAggro(sender, AGGRO_MID)
    if (!isDead && inControl()) attackedReaction(attackRoll, from)
  }

  def attackedReaction(attackRoll: Int, from: ActorRef) = {}

  override def caughtStealing(creature: ActorRef, item: Option[Object]) = {
    super.caughtStealing(creature, item);
    addAggro(creature, AGGRO_MAX)
  }

  def tryRangedAttack(target: ActorRef) {
    val targetRef = spottedCache.map(e => e._2 -> e._1).get(target)
    if (targetRef != None && skillset.ballistics.bonus > -10) performRangedAttack(target)
    else aggroNotAvailable(target)
  }

  def usedInYourArea(creature: ActorRef) = {}
  def presence(result: Int, location: ActorRef) = {}
  def meleeAttackOnPriorityTargetNotPossible() = tryRangedAttack(sender)

  override def creatureDied(name: String, by: ActorRef) = {
    super.creatureDied(name, by)
    dropAggro(sender)
  }

  def takecontrolFor(duration: Option[FiniteDuration], onEnd: Option[ActorRef => Unit], onDeath: ActorRef => Unit) = {
    if (duration != None && onEnd != None) {
      controller = Some(sender, Option(onDeath), Some(system.scheduler.scheduleOnce(duration.get, self, Creature.DropControl()), onEnd.get))
    } else {
      controller = Some(sender, Option(onDeath), None)
    }
  }

  def restoreControl() = {
    if (controller != None && controller.get._3 != None) {
      controller.get._3.get._1.cancel()
      controller.get._3.get._2(self)
    }
    controller = Some(self, None, None)
  }

  override def receive = ({
    case StartSchedules()                                                             => allSchedules foreach (_())
    case Creature.TakeControlFor(duration, onEnd, onDeath)                            => takecontrolFor(Some(duration), Some(onEnd), onDeath)
    case Creature.ForceTakeControl(onDeath)                                           => takecontrolFor(None, None, onDeath)
    case Creature.DropControl()                                                       => restoreControl()
    case Creature.MeleeAttackNotPossible(target, attackType) if inControl()           => meleeAttackOnPriorityTargetNotPossible()
    case Creature.UsedInYourArea(c) if Character.is(c) && inControl() && !isFriend(c) => usedInYourArea(c)
    case Creature.Presence(result, location, _) if inControl()                        => presence(result, location)
    case Inventory(items)                                                             => { this.inventory ++= items; initialInventory ++= items }
    case Creature.BefriendAttempt(roll, fromLocation)                                 => befriendAttempt(roll, fromLocation)
    case ForgetFriend(character) if (isFriend(character))                             => befriendedCharacters -= character
    case ListingRequest(requester)                                                    => listing(requester, Alive)
    case Respawn()                                                                    => reset()
    case AggroDecay()                                                                 => aggroDecay()
    case Creature.StatusRequest(req, succcesses) if succcesses > 0                    => req ! Creature.StatusResponse(self, detailedStatus)
  }: Receive) orElse super.receive

  override def dead: Receive = ({
    case Respawn()                 => { context.unbecome(); reset(); self ! StartSchedules() }
    case ListingRequest(requester) => listing(requester, Dead)
  }: Receive) orElse super.dead
}

object Monster {
  import kt.game.domain.creatures.monsters.ai.Aggressive
  import kt.game.domain.creatures.monsters.ai.Defensive
  import kt.game.domain.creatures.monsters.ai.Reflexive
  import kt.game.domain.creatures.monsters.ai.Roamer

  val AGGRO_MIN = 1
  val AGGRO_LOW = 5
  val AGGRO_MID = 10
  val AGGRO_MAX = 25

  def props(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset))
  def propsWithAggressive(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Aggressive)
  def propsWithDefensive(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive)
  def propsWithReflexive(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Reflexive)
  def propsWithDefensiveRooter(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive with Rooter)
  def propsWithDefensiveZerkerRooter(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive with Berserker with Rooter)
  def propsWithDefensiveRoamingRooter(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive with Roamer with Rooter)
  def propsWithAggressiveStunner(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Aggressive with Stunner)
  def propsWithDefensiveStunner(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive with Stunner)
  def propsWithReflexiveStunner(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Reflexive with Stunner)
  def propsWithRoamer(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Roamer)
  def propsWithAggressiveZerker(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Aggressive with Berserker)
  def propsWithDefensiveZerker(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive with Berserker)
  def propsWithAggressiveZerkerStunner(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Aggressive with Berserker with Stunner)
  def propsWithDefensiveZerkerStunner(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive with Berserker with Stunner)
  def propsWithAggressiveRoamer(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Aggressive with Roamer)
  def propsWithDefensiveRoamer(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive with Roamer)
  def propsWithReflexiveRoamer(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Reflexive with Roamer)
  def propsWithAggressiveRoamerStunner(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Aggressive with Roamer with Stunner)
  def propsWithDefensiveRoamerStunner(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Defensive with Roamer with Stunner)
  def propsWithAggressiveRoamingZerker(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Aggressive with Roamer with Berserker)
  def propsWithAggressiveRoamingZerkerStunner(creatureType: String, skillset: Skillset) = Props(new Monster(creatureType, skillset) with Aggressive with Roamer with Berserker with Stunner)

  def is(actor: ActorRef) = actor.path.elements.toSet.contains(MonsterGenerator.NAME)
  def nameFromRef(actor: ActorRef): String = actor.path.name.substring(actor.path.name.indexOf("_") + 1).replace("_", " ")

  case class Inventory(items: Set[Object])
  case class Respawn()
  case class ForgetFriend(character: ActorRef)
  case class ListingRequest(requester: ActorRef)
  case class AggroDecay()
  case class StartSchedules()

  def basic_veryWeak = Skillset.fromBonus(0, -20, 0, -5, 0, -10, -8, 0)
  def basic_weak = Skillset.fromBonus(2, -20, 0, -2, 2, -10, -5, 2)
  def basic_average = Skillset.fromBonus(5, -20, 0, 0, 2, -10, 0, 2)
  def basic_stronger = Skillset.fromBonus(8, -20, 0, 0, 5, -8, 2, 5)
  def basic_strong = Skillset.fromBonus(8, -20, 0, 2, 8, -8, 5, 8)
  def basic_veryStrong = Skillset.fromBonus(10, -20, 0, 2, 10, -5, 8, 10)
  def basic_superStrong = Skillset.fromBonus(12, -20, 0, 5, 10, -5, 10, 12)
  def basic_strongest = Skillset.fromBonus(15, -20, 0, 8, 15, -5, 15, 15)

  def sneak_veryWeak = Skillset.fromBonus(0, -20, 0, -5, 0, 2, -8, -2)
  def sneak_weak = Skillset.fromBonus(0, -20, 0, -2, 2, 2, -5, 0)
  def sneak_average = Skillset.fromBonus(0, -20, 0, 0, 2, 5, 0, 0)
  def sneak_stronger = Skillset.fromBonus(2, -20, 0, 0, 5, 5, 2, 2)
  def sneak_strong = Skillset.fromBonus(2, -20, 0, 2, 5, 8, 5, 5)
  def sneak_veryStrong = Skillset.fromBonus(5, -20, 0, 2, 10, 8, 8, 8)
  def sneak_superStrong = Skillset.fromBonus(8, -20, 0, 5, 10, 10, 10, 10)
  def sneak_strongest = Skillset.fromBonus(10, -20, 0, 8, 15, 15, 15, 15)

  def range_veryWeak = Skillset.fromBonus(0, -8, 0, -5, 2, 0, -8, -2)
  def range_weak = Skillset.fromBonus(0, -5, 0, -2, 2, 0, -5, -2)
  def range_average = Skillset.fromBonus(0, 0, 0, 0, 5, 0, 0, 0)
  def range_stronger = Skillset.fromBonus(0, 2, 0, 0, 8, 0, 2, 2)
  def range_strong = Skillset.fromBonus(2, 5, 0, 2, 10, 0, 5, 5)
  def range_veryStrong = Skillset.fromBonus(2, 8, 0, 2, 12, 0, 8, 8)
  def range_superStrong = Skillset.fromBonus(5, 10, 0, 5, 15, 0, 10, 10)
  def range_strongest = Skillset.fromBonus(8, 15, 0, 8, 15, 0, 15, 12)

}