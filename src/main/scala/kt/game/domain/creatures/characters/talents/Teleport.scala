package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.RelativeLocation
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Reflexes
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.locations.Location
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.events.ActionFailedEvent
import kt.game.events.talents.TeleportEvents.AnchorFailedEvent
import kt.game.events.talents.TeleportEvents.AnchorSetEvent
import kt.game.events.talents.TeleportEvents.AnchorStatusEvent
import kt.game.events.talents.TeleportEvents.FailureReason
import kt.game.events.talents.TeleportEvents.NoAnchorStatusEvent
import kt.game.events.talents.TeleportEvents.TeleportationFailedEvent
import kt.game.events.talents.TeleportEvents.TeleportationSucceededEvent
import kt.game.utility.Result
import kt.game.events.ActionOnCoolDownEvent

class Teleport private (val character: ActorRef) extends Actor with Talent {
  import Teleport._
  import context._

  val name = Teleport.name
  val description = Teleport.description
  val help = Teleport.commandsHelp

  def setAnchorCD() = character ! Character.SetActionCooldown(action, s => (5 seconds).fromNow)
  def setTpCD() = character ! Character.SetActionCooldown(action, s => ((120 - s.reflexes.bonus * 4).max(40) seconds).fromNow)

  implicit val timeout = Timeout(200 milliseconds)

  var anchor: Option[(ActorRef, Descriptor, Deadline, Int)] = None

  def setUpAnchor() = {
    val cooldown = Await.result((character ? Character.GetActionCooldown(action)).mapTo[Option[Deadline]], timeout.duration)
    if (cooldown == None || cooldown.get.isOverdue) {
      setAnchorCD()
      val location = Await.result((character ? Creature.GetCurrentLocationWithDescription()).mapTo[Option[(ActorRef, Descriptor)]], timeout.duration)
      val result = Result.criticalSteps(Await.result((character ? Creature.Roll(SkillName.REFL)).mapTo[Option[Int]], timeout.duration).get - anchorDC)
      if (result >= 0) {
        val deadline = (60 * (1 + result) minutes).fromNow
        anchor = Some(location.get._1, location.get._2, deadline, result * 2)
        character ! Creature.Message(AnchorSetEvent(deadline.timeLeft.toMinutes))
      } else {
        anchor = None
        character ! Creature.Message(AnchorFailedEvent)
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def tpToLocation(location: ActorRef) = {
    setTpCD()
    val result = Await.result((character ? Creature.Roll(SkillName.REFL)).mapTo[Option[Int]], timeout.duration).get
    if (result >= 0) {
      character ! Creature.Message(TeleportationSucceededEvent(location))
      location ! Location.Position(character)
    } else {
      character ! Creature.Message(TeleportationFailedEvent(FailureReason.CheckFailure, Some(location)))
    }
  }

  def teleport(target: Option[String] = None) = {
    val cooldown = Await.result((character ? Character.GetActionCooldown(action)).mapTo[Option[Deadline]], timeout.duration)
    if (cooldown == None || cooldown.get.isOverdue) {
      if (target == None) tpToLocation(anchor.get._1)
      else {
        val targetLocation = Await.result((character ? Creature.GetObservedLocation(target.get)).mapTo[Option[(ActorRef, Descriptor, Int, ActorRef)]], timeout.duration)
        if (targetLocation != None) tpToLocation(targetLocation.get._1)
        else character ! Creature.Message(TeleportationFailedEvent(FailureReason.InvalidLocation, None))
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentAnchorCommand()                                                          => setUpAnchor()
    case TalentAnchorInfoCommand() if anchor == None || anchor.get._3.isOverdue         => character ! Creature.Message(NoAnchorStatusEvent)
    case TalentAnchorInfoCommand()                                                      => character ! Creature.Message(AnchorStatusEvent(RelativeLocation(None, Some(anchor.get._2), Some(anchor.get._1)), anchor.get._3.timeLeft.toMinutes))
    case TalentTeleportToAnchorCommand() if anchor != None && anchor.get._3.hasTimeLeft => teleport()
    case TalentTeleportToAnchorCommand()                                                => character ! Creature.Message(TeleportationFailedEvent(FailureReason.NoAnchor, None))
    case TalentTeleportCommand(ref)                                                     => teleport(Some(ref))
  }
}

object Teleport extends TalentContext {
  def props(character: ActorRef) = Props(new Teleport(character))

  case class TalentTeleportCommand(ref: String)
  case class TalentAnchorCommand()
  case class TalentTeleportToAnchorCommand()
  case class TalentAnchorInfoCommand()

  val skill = Reflexes(12)

  val teleportDC = 20
  val anchorDC = 10

  val action = TalentAction.Teleport
  val name = action.toString

  val description = "You can teleport to any location you can see or to any anchored location. The frequency that you can do so, depends on your reflexes skill."
  val trainingDescription = "You can train your arcane skills and learn the secret of teleportation! " + requirementsString

  val TCP_teleport = "^teleport (\\d+)$".r
  val TCP_teleportToAnchor = "^anchor teleport".r
  val TCP_anchor = "^anchor place".r
  val TCP_anchorInfo = "^anchor info".r

  val commandsHelp =
    ("%-32s" format "teleport <area>") + " -->  Teleport to target location that you can spot.\r\n" +
      ("%-32s" format "anchor place") + " -->  Set up a temporary anchor to your current location. You may only have one anchor at a time.\r\n" +
      ("%-32s" format "anchor info") + " -->  Get information about your current anchor if any.\r\n" +
      ("%-32s" format "anchor teleport") + " -->  Teleport to anchored location.\r\n"
}