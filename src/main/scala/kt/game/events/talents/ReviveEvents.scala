package kt.game.events.talents

import kt.game.events.NonCombatEvent
import akka.actor.ActorRef
import kt.game.domain.creatures.characters.talents.Revive
import kt.game.events.ActionFailedReason
import kt.game.events.ActionFailedEvent

object ReviveEvents {

  case object RessurectSucceededEvent extends NonCombatEvent {
    override def toString = "You succesfully ressurected your target!"
  }

  case class RessurectFailedEvent(reason: RessurectFailureReason) extends NonCombatEvent with ActionFailedEvent {
    import kt.game.events.talents.ReviveEvents.RessurectFailureReason._
    override def toString = reason match {
      case NotEnoughFatePoints => s"You need ${Revive.ressurectFPCost} Fate points to ressurect!"
      case CheckFailure        => "You failed in resurecting your target!"
      case CanNoLongerSpot     => "You can no longet spot wanted tombstone."
    }
  }

  case class CleanseFailedEvent(reason: ActionFailedReason) extends NonCombatEvent with ActionFailedEvent {
    import kt.game.events.ActionFailedReason._
    override def toString = reason match {
      case NotInSameLocation => "Target creature is too far away to cleanse!"
      case InvalidState      => "Target is not stunned!"
      case CanNoLongerSpot   => "You can no longet spot wanted creature."
      case _                 => "You failed in cleansing your target!"
    }
  }

  sealed trait RessurectFailureReason
  object RessurectFailureReason {
    case object CanNoLongerSpot extends RessurectFailureReason
    case object NotEnoughFatePoints extends RessurectFailureReason
    case object CheckFailure extends RessurectFailureReason
  }

}