package kt.game.domain
import collection.mutable.Stack
import org.scalatest._
import kt.game.utility.Result

class TestSuccess extends WordSpecLike with Matchers {
  
  "A Roll" should {
    "follow these rules" in {
      Result criticalSteps 15 should be (3)
      Result criticalSteps 14 should be (2)
      Result criticalSteps 11 should be (2)
      Result criticalSteps 10 should be (2)
      Result criticalSteps 9 should be (1)
      Result criticalSteps 6 should be (1)
      Result criticalSteps 5 should be (1)
      Result criticalSteps 4 should be (0)
      Result criticalSteps 1 should be (0)
      Result criticalSteps 0 should be (0)
      Result criticalSteps -1 should be (-1)
      Result criticalSteps -4 should be (-1)
      Result criticalSteps -5 should be (-2)
      Result criticalSteps -6 should be (-2)
      Result criticalSteps -9 should be (-2)
      Result criticalSteps -10 should be (-3)
    }
  }
}