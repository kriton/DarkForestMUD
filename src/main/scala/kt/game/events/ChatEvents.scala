package kt.game.events

import akka.actor.ActorRef

object ChatEvents {
  case class DirectedChatEvent(from: String, to: String, message: String) extends ChatEvent {
    override def toString = s"$from whispers to you: $message"
  }
  case class DirectedChatAcknowledgementEvent(from: String, to: String) extends ChatEvent {
    override def toString = s"You whispered to $to."
  }
  case class GlobalChatEvent(from: String, message: String) extends ChatEvent {
    override def toString = s"$from says: $message"
  }
  case class AreaChatEvent(from: String, area: ActorRef, message: String) extends ChatEvent {
    override def toString = s"$from shouts: $message"
  }
  case class HintEvent(message: String) extends ChatEvent {
    override def toString = s"hint: $message"
  }
  case class GlobalNotificationEvent(message: String) extends ChatEvent {
    override def toString = message
  }
  case object ReplyImpossibleEvent extends ChatEvent {
    override def toString = "Nobody has whispered to you recently..."
  }
}