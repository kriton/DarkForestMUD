name := """EGame"""

version := "1.0"

scalaVersion := "2.11.6"

enablePlugins(DockerPlugin)
enablePlugins(JavaAppPackaging)


libraryDependencies ++= Seq(
  "com.typesafe.akka" % "akka-actor_2.11" % "2.4.1",
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.4.1" % "test",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test")
  
//libraryDependencies += "com.google.guava" % "guava" % "19.0"
//libraryDependencies += "com.typesafe.play" % "play-json_2.11" % "2.4.6"


fork in run := true

dockerExposedPorts := Seq(8386)
dockerExposedVolumes := Seq("/data/darkForest")


fork in run := true