package kt.game.requests.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.AttackType
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.characters.talents.Dominate
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.talents.DominateEvents.DominateFailedEvent
import kt.game.events.talents.DominateEvents.DominateFailureReason
import kt.game.events.talents.DominateEvents.SomeoneTriedToDominateYouEvent
import kt.game.utility.Result

class DominateRequestHandler private (character: ActorRef, target: String) extends Actor {
  import DominateRequestHandler._
  import context._

  setReceiveTimeout(300 milliseconds)

  implicit val timeout = Timeout(120 milliseconds)
  val cooldownF = character ? Character.GetActionCooldown(Dominate.action)
  val creatureF = character ? Creature.GetSpottedTarget(target)
  val locationF = character ? Creature.GetCurrentLocation()
  val fatepoinF = character ? Creature.GetFatePoints()

  val cooldown = Await.result(cooldownF.mapTo[Option[Deadline]], timeout.duration)
  val creature = Await.result(creatureF.mapTo[Option[ActorRef]], timeout.duration)
  val location = Await.result(locationF.mapTo[Option[ActorRef]], timeout.duration)

  if (creature != None && location != None) {
    if (Await.result(fatepoinF.mapTo[Int], timeout.duration) >= Dominate.fpCost) creature.get ! Creature.CanMeleeAttackFrom(character, location.get, AttackType.NonAttack)
    else {
      parent ! Dominate.DominateFailedToOccur()
      character ! Creature.Message(DominateFailedEvent(DominateFailureReason.CheckFailure))
    }
  } else {
    parent ! Dominate.DominateFailedToOccur()
    character ! Creature.Message(DominateFailedEvent(DominateFailureReason.CanNoLongerSpot))
  }

  def dominate() = {
    val roll = Await.result(character ? Creature.Roll(SkillName.INFL), timeout.duration).asInstanceOf[Option[Int]].get
    val defense = Await.result(creature.get ? Creature.GetSkillPassive(SkillName.WILL), timeout.duration).asInstanceOf[Option[Int]].get + (if (Character.is(creature.get)) 10 else 5)
    val result = Result.criticalSteps(roll - defense)
    if (result >= 0) {
      character ! Creature.AddFatePoints(-Dominate.fpCost)
      parent ! Dominate.DominateSuccess(creature.get, 24 + (result * 12) seconds)
    } else {
      character ! Creature.Message(DominateFailedEvent(DominateFailureReason.CheckFailure))
      creature.get ! Creature.Message(SomeoneTriedToDominateYouEvent)
    }
  }

  def receive = {
    case Creature.MeleeAttackPossible(creature, _) => dominate()
    case Creature.MeleeAttackNotPossible(_, _)     => { parent ! Dominate.DominateFailedToOccur(); character ! Creature.Message(DominateFailedEvent(DominateFailureReason.TooFarAway)) }
    case ReceiveTimeout                            => self ! PoisonPill
  }
}

object DominateRequestHandler {
  def props(character: ActorRef, target: String) = Props(new DominateRequestHandler(character, target))
}