package kt.game.domain.creatures.monsters.ai

import scala.annotation.migration
import scala.concurrent.duration.DurationInt
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.MonsterAction
import kt.game.domain.creatures.monsters.Behaviour
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.LoggedEvent
import kt.game.events.MonsterEvent
import kt.game.utility.Result
import kt.game.events.CombatEvent
import kt.game.domain.creatures.AttackType
import kt.game.events.AttemptAttackEvent

trait Aggressive extends Monster with Behaviour {
  import context._

  def updateHuntingSchedule() = system.scheduler.scheduleOnce(actionFrequency, self, Aggressive.Hunt())
  allSchedules += updateHuntingSchedule

  def smiteFrequency = 40 - fullSkillModifer(SkillName.REFL) seconds
  var smiteCD = smiteFrequency.fromNow

  override def detailedDescription = "Looks to be aggresive." + (if (super.detailedDescription != "") " " + super.detailedDescription else "")

  override def attackedReaction(attackRoll: Int, from: ActorRef) = {
    if (spottedCache.map(_.swap).get(sender) != None) {
      sender ! Creature.TestPresence(roll(skillset.willpower), currentLocation, AttackType.NonAttack)
    } else {
      spot()
    }
  }

  override def befriendCheck(roll: Int) = Result.criticalSteps(roll - skillset.willpower.passive - 5)

  override def presence(result: Int, location: ActorRef) = {
    log(LoggedEvent(MonsterEvent("You are testing " + sender.path.name + "' s presence with a conviction of " + result)))
    if (result >= 0) {
      addAggro(sender, Monster.AGGRO_LOW * result + (if (location == currentLocation) Monster.AGGRO_MIN else 0))
    }
    if (priorityTarget().orNull == sender) {
      if (following == None || following.get != sender) sender ! Creature.Follow(currentLocation)
      log(LoggedEvent(MonsterEvent("You are now hunting " + sender.path.name + " with a priority of " + aggro.get(sender).get._1 + ".")))
    }
  }

  def hunt() = {
    val target = targetFrom(spottedCache.values.toSet)
    if (target != None) target.get ! Creature.CanMeleeAttackFrom(self, currentLocation, AttackType.Melee)
    else spot()
  }

  override def meleeAttack(creature: ActorRef, attackType: AttackType) = {
    if (smiteCD.isOverdue && fullSkillModifer(SkillName.WILL) >= 5) {
      val attack = AttackType.SpecialAttack(12, " smite")
      log(LoggedEvent(AttemptAttackEvent(attack, creature)))
      performWeaponsAttack(creature, attack)
      smiteCD = smiteFrequency.fromNow
    } else super.meleeAttack(creature, attackType)
    addAggro(creature, Monster.AGGRO_MIN)
  }

  override def receive = ({
    case Aggressive.Hunt() => {
      updateHuntingSchedule()
      if (actionCheck(MonsterAction.Hunt)) hunt()
    }
  }: Receive) orElse super.receive
}

object Aggressive {
  case class Hunt()
}