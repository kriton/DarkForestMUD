package kt.game.events.talents

import kt.game.events.NonCombatEvent
import akka.actor.ActorRef
import kt.game.events.talents.CreateEvents.CreateFailureReason._
import kt.game.domain.items.Object
import kt.game.events.ActionFailedEvent

object CreateEvents {

  case class CreateSucceededEvent(item: Object) extends NonCombatEvent {
    override def toString = s"You succesfully created: $item.name}!"
  }

  case class CreateFailedEvent(reason: CreateFailureReason) extends NonCombatEvent with ActionFailedEvent {
    override def toString = reason match {
      case CanOnlyCreateIngridients => "You selected an invalid item or non-existing item. Only ingridients can be created!"
      case NotEnoughFatePoints(x)   => s"You do not have enough fate points, $x are needed!"
      case CheckFailure             => "Your attempt to create the item failed. Half of the required fate points are lost!"
    }
  }

  sealed trait CreateFailureReason
  object CreateFailureReason {
    case object CanOnlyCreateIngridients extends CreateFailureReason
    case class NotEnoughFatePoints(requirement: Int) extends CreateFailureReason
    case object CheckFailure extends CreateFailureReason
  }

}