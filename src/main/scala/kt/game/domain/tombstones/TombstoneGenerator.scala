package kt.game.domain.tombstones

import scala.collection.mutable.Set
import scala.compat.Platform
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.items.Object
import kt.game.persistence.DBManager
import scala.collection.mutable.Map
import kt.game.requests.gm.GetTombstonesRequestHandler
import kt.game.events.LoggedEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.StatusEvent
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.worldbuilder.WorldBuilder
import kt.game.events.InfoEvent
import kt.game.events.InfoEvents.DublicateTombstoneNameEvent

class TombstoneGenerator private (db: ActorRef, worldBuilder: ActorRef) extends Actor {
  import TombstoneGenerator._
  import context._

  val tombStones: Map[ActorRef, Tombstone.SnapshotT002] = Map()

  db ! DBManager.LoadAllTombstones()

  def newTombstone(name: String, password: String, minutes: Int, level: Int, location: ActorRef, items: scala.Predef.Set[Object], skillset: Skillset, friends: scala.Predef.Set[String], receipesLearned: scala.Predef.Set[String], talents: scala.Predef.Set[String]) = {
    if (tombStones.map(_._2.characterName).toSet.contains(name)) {
      sender ! FeedbackBuffer.Feedback(LoggedEvent(DublicateTombstoneNameEvent))
    } else {
      val description = "Here rest the remains of " + name + ". He was " + (level match {
        case x: Int if (x < 5)  => "a weakling!"
        case x: Int if (x < 10) => "a promising adventurer!"
        case x: Int if (x < 20) => "an honorable adventurer!"
        case x: Int if (x < 30) => "a great explorer!"
        case x: Int if (x < 50) => "a renowned warrior!"
        case _                  => "a legendary figure!"
      })
      val tombstone = context.actorOf(Tombstone.props(name, password, minutes, description, Platform.currentTime, level, skillset, friends, receipesLearned, talents), name)
      tombstone ! Tombstone.Place(location)
      tombstone ! Tombstone.Add(items)
      system.scheduler.scheduleOnce(2 seconds, tombstone, Tombstone.RequestSnapshot())
    }
  }

  def lostTombstone(tombstone: ActorRef) = {
    tombStones -= tombstone
    tombstone ! PoisonPill
    db ! DBManager.DeleteTombstone(tombstone, tombstone.path.name)
  }

  def loadTombstoneFrom(snapshot: Tombstone.SnapshotT002) = {
    val tombstone = context.actorOf(Tombstone.props(snapshot.characterName, snapshot.password, snapshot.minutesPlayed, snapshot.description, snapshot.dateCreated, snapshot.level, snapshot.skillset, snapshot.friends, snapshot.receipesLearned, snapshot.talents), snapshot.characterName)
    tombStones += tombstone -> snapshot
    tombstone ! Tombstone.LoadFromSnapshot(snapshot)
  }

  implicit def toSnapShopT002(snapshot: Tombstone.Snapshot_T001) = Tombstone.SnapshotT002(
    snapshot.characterName,
    snapshot.description,
    snapshot.dateCreated,
    snapshot.level,
    snapshot.treasures,
    snapshot.digDC,
    snapshot.defiled,
    snapshot.location,
    snapshot.skillset,
    snapshot.friends,
    snapshot.receipesLearned,
    snapshot.talents,
    snapshot.password, 0)

  def receive = {
    case LostTombstone(tombstone)                                                                  => lostTombstone(tombstone)
    case NewTombstone(name, pass, mins, level, location, items, skills, friends, recipes, talents) => newTombstone(name, pass, mins, level, location, items, skills, friends, recipes, talents)
    case Dug()                                                                                     => sender ! Tombstone.RequestSnapshot()
    case x: Tombstone.SnapshotT002 if (sender != db)                                               => { tombStones += sender -> x; db forward x }
    case x: Tombstone.Snapshot_T001                                                                => loadTombstoneFrom(x)
    case x: Tombstone.SnapshotT002                                                                 => loadTombstoneFrom(x)
    case ListAll()                                                                                 => sender ! GetTombstonesRequestHandler.AllTombstones(tombStones.toSeq)
    case x: WorldBuilder.NewTombstoneTo                                                            => worldBuilder forward x
  }
}

object TombstoneGenerator {
  def props(db: ActorRef, worldBuilder: ActorRef) = Props(new TombstoneGenerator(db, worldBuilder))

  val NAME = "tombstones"

  case class NewTombstone(name: String, password: String, minutes: Int, level: Int, location: ActorRef, items: scala.Predef.Set[Object], skillset: Skillset, friends: scala.Predef.Set[String], receipesLearned: scala.Predef.Set[String], talents: scala.Predef.Set[String])
  case class LostTombstone(tombstone: ActorRef)
  case class Dug()
  case class ListAll()
}