package kt.game.system

import akka.actor.ActorSystem

object Main extends App{
  val system = ActorSystem("mySystem")
  system.actorOf(Server.props, Server.NAME)
}