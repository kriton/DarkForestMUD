package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.Athletics
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import kt.game.events.NonCombatEvent
import kt.game.utility.Result
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Perception
import kt.game.events.ActionFailedEvent
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.ActionFailedEvent
import kt.game.events.talents.StudyEvents.StudyFailedEvent
import kt.game.events.ActionFailedReason

class Study private (val character: ActorRef) extends Actor with Talent {
  import Study._
  import context._

  val name = Study.name
  val description = Study.description
  val help = Study.commandsHelp

  def setCD() = character ! Character.SetActionCooldown(action, s => ((25 - s.perception.bonus).max(5) seconds).fromNow)

  implicit val timeout = Timeout(200 milliseconds)

  def studyAttempt(target: String) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      val creature = Await.result(character ? Creature.GetSpottedTarget(target), timeout.duration).asInstanceOf[Option[ActorRef]]
      if (creature != None) {
        val defense = Await.result(creature.get ? Creature.GetSkillPassive(SkillName.INFL), timeout.duration).asInstanceOf[Option[Int]]
        val roll = Await.result(character ? Creature.Roll(SkillName.PERC), timeout.duration).asInstanceOf[Option[Int]]
        if (defense != None && roll != None && roll.get >= defense.get) creature.get ! Creature.StatusRequest(character, Result.criticalSteps(roll.get - defense.get))
        else character ! Creature.Message(StudyFailedEvent(ActionFailedReason.CheckFailure))
        setCD()
      } else {
        character ! Creature.Message(StudyFailedEvent(ActionFailedReason.CanNoLongerSpot))
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentStudyCommand(ref) => studyAttempt(ref)
  }
}

object Study extends TalentContext {
  def props(character: ActorRef) = Props(new Study(character))

  case class TalentStudyCommand(ref: String)

  val skill = Perception(4)

  val action = TalentAction.Study
  val name = action.toString

  val description = "You can study any other creature you can see and learn his skills, abilities and full status. The frequency that you can do so, depends on your perception skill."
  val trainingDescription = "You can train your arcane perceptive powers here and learn how to read the skilsl of other creatures! " + requirementsString

  val TCP_studyother = "^study (\\d+)$".r

  val commandsHelp = ("%-32s" format "study <target>") + " -->  Study target creature and learn his skills.\r\n"
}