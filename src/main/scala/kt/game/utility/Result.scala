package kt.game.utility

object Result {
  def criticalSteps(value: Int): Int = if (value >= 0) value / 5 else (value / 5) - 1;
}