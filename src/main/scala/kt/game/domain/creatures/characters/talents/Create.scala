package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Athletics
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import kt.game.events.NonCombatEvent
import kt.game.domain.creatures.skills.Willpower
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.skills.Craft
import kt.game.domain.items.AllObjects
import kt.game.domain.items.Ingridient
import kt.game.domain.items.RawIngridient
import kt.game.domain.items.Common
import kt.game.domain.items.Rare
import kt.game.domain.items.Exotic
import kt.game.domain.items.CraftedIngridient
import kt.game.events.talents.CreateEvents._
import kt.game.events.ActionOnCoolDownEvent

class Create private (val character: ActorRef) extends Actor with Talent {
  import Create._

  val name = Create.name
  val description = Create.description
  val help = Create.commandsHelp

  def setCD() = character ! Character.SetActionCooldown(action, s => ((240 - s.craft.bonus * 12).max(75) seconds).fromNow)

  implicit val timeout = Timeout(200 milliseconds)

  def creationAttempt(key: String) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      val itemToCraft = AllObjects.map.get(key)
      if (itemToCraft != None && itemToCraft.get.isInstanceOf[Ingridient]) {
        val fps = Await.result(character ? Creature.GetFatePoints(), timeout.duration).asInstanceOf[Int]
        val requirements = itemToCraft.get match {
          case x: RawIngridient if x.rarity == Common     => (1, 20)
          case x: RawIngridient if x.rarity == Rare       => (3, 24)
          case x: RawIngridient if x.rarity == Exotic     => (5, 28)
          case x: CraftedIngridient if x.rarity == Common => (3, 22)
          case x: CraftedIngridient if x.rarity == Rare   => (5, 26)
          case x: CraftedIngridient if x.rarity == Exotic => (7, 30)
        }
        if (requirements._1 <= fps) {
          setCD()
          val roll = Await.result(character ? Creature.Roll(SkillName.CRAF), timeout.duration).asInstanceOf[Option[Int]].get
          if (roll >= requirements._2) {
            character ! Creature.AddFatePoints(-requirements._1)
            character ! Creature.Message(CreateSucceededEvent(itemToCraft.get))
            character ! Character.PickedUp(itemToCraft.get)
          } else {
            character ! Creature.Message(CreateFailedEvent(CreateFailureReason.CheckFailure))
            character ! Creature.AddFatePoints(-requirements._1 / 2)
          }
        } else {
          character ! Creature.Message(CreateFailedEvent(CreateFailureReason.NotEnoughFatePoints(requirements._1)))
        }
      } else {
        character ! Creature.Message(CreateFailedEvent(CreateFailureReason.CanOnlyCreateIngridients))
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentCreateCommand(key) => creationAttempt(key)
  }
}

object Create extends TalentContext {
  def props(character: ActorRef) = Props(new Create(character))

  case class TalentCreateCommand(key: String)

  val commonDC = 20
  val rareDC = 24
  val exoticDC = 28
  val craftedDCMod = 2

  val skill = Craft(4)

  val action = TalentAction.Create
  val name = action.toString

  val description = "You can create a material ingridient out of nothing. Requires 1 fate point for a common ingridient, 3 for a rare and 5 for an exotic one, further increased by 2 for non-raw ones. DC also depends on the rarity of the ingridient."
  val trainingDescription = "You can train your arcane skills and get access to metacreativity powers here! " + requirementsString

  val TCP_create = "^create (.+)$".r

  val commandsHelp =
    ("%-32s" format "create <name>") + " -->  Create an ingridient. Name must match the desired ingridient.\r\n"
}