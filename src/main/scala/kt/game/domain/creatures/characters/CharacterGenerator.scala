package kt.game.domain.creatures.characters

import scala.collection.mutable.Set
import scala.concurrent.duration.DurationInt
import CharacterGenerator.ExpireOwnerOfDeadOrRetired
import CharacterGenerator.TakeSnapshots
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.creatures.skills.Skillset.fromPersistableString
import kt.game.domain.tombstones.Tombstone
import kt.game.persistence.DBManager
import kt.game.player.Player
import kt.game.events.ActionFailedEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.LoggedEvent
import kt.game.events.DevelopmentEvent
import akka.actor.InvalidActorNameException
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import kt.game.domain.tombstones.TombstoneGenerator
import kt.game.domain.locations.Location
import kt.game.domain.worldbuilder.WorldBuilder
import kt.game.events.RessurectedFromTombEvent
import kt.game.events.InfoEvents.NameIsNotFormattedForNewCharacterEvent
import kt.game.events.InfoEvents.NameIsNotUniqueForNewCharacterEvent
import kt.game.events.InfoEvents.CharacterNotFoundLoginFailureEvent
import kt.game.events.InfoEvents.CharacterIsDeadOrRetiredLoginFailureEvent

class CharacterGenerator private (db: ActorRef) extends Actor {
  import CharacterGenerator._
  import context._

  val characters: Set[ActorRef] = Set()
  val deadCharacters: Set[ActorRef] = Set()
  val oldAndCurrentCharacterNames: Set[String] = Set()

  val ownersOfTherecentlyDeadAndRetired: Set[ActorRef] = Set()

  system.scheduler.schedule(10 seconds, 10 seconds, self, TakeSnapshots())

  db ! DBManager.LoadAllCharacters()
  db ! DBManager.LoadAllNames()

  def newChar(name: String, password: String) = {
    if (oldAndCurrentCharacterNames contains name)
      sender ! FeedbackBuffer.Feedback(LoggedEvent(NameIsNotUniqueForNewCharacterEvent))
    else if (invalidate(name))
      sender ! FeedbackBuffer.Feedback(LoggedEvent(NameIsNotFormattedForNewCharacterEvent))
    else {
      val character = context.actorOf(Character.props(name, password, Skillset.zero), name)
      characters += character
      sender ! Player.CharacterCreated(character, password)
      oldAndCurrentCharacterNames += name
      db ! DBManager.UpdateAllNames(oldAndCurrentCharacterNames.toList)
    }
  }

  def canLogin(name: String, password: String) = {
    if (!oldAndCurrentCharacterNames.contains(name))
      sender ! FeedbackBuffer.Feedback(LoggedEvent(CharacterNotFoundLoginFailureEvent))
    else if (!characters.map(_.path.name).contains(name))
      sender ! FeedbackBuffer.Feedback(LoggedEvent(CharacterIsDeadOrRetiredLoginFailureEvent))
    else {
      val character = characters.filter(_.path.name == name).head
      sender ! Player.CharacterCanBeLoggedIn(character, name, password)
    }
  }

  def reviveCharFromTombstone(tombstone: Tombstone.SnapshotT002) = {
    try {
      val character = context.actorOf(Character.props(tombstone.characterName, tombstone.password, tombstone.skillset), tombstone.characterName)
      characters += character
      oldAndCurrentCharacterNames += tombstone.characterName
      character ! Character.LoadFromSnapshot(Character.SnapshotV003(
        tombstone.characterName,
        tombstone.password,
        tombstone.skillset,
        tombstone.receipesLearned,
        tombstone.friends,
        null, null, null,
        tombstone.treasures,
        (tombstone.location, "tombstone"),
        5, tombstone.level * 3, 0, 0, (tombstone.minutesPlayed minutes).toMillis,
        tombstone.talents))
    } catch {
      case e: InvalidActorNameException => {
        val character = deadCharacters.find { _.path.name == tombstone.characterName }.get
        character ! Character.ReviveWith(tombstone.treasures, tombstone.location)
        characters += character
        deadCharacters -= character
      }
    }
    broadcast(LoggedEvent(RessurectedFromTombEvent(tombstone.characterName)))
  }

  def invalidate(name: String) = !((name matches "^[A-z]{2,}[0-9]{0,4}[A-z]{1,}$") && name.length <= 15)

  def deadChar(character: ActorRef, owner: ActorRef) = {
    characters -= character
    deadCharacters += character
    db ! DBManager.DeleteCharacter(character, character.path.name)
    ownersOfTherecentlyDeadAndRetired += owner
    system.scheduler.scheduleOnce(10 seconds, self, ExpireOwnerOfDeadOrRetired(owner))
  }

  def retiredChar(character: ActorRef) = {
    characters -= character
    db ! DBManager.DeleteCharacter(character, character.path.name)
  }

  def loadCharFrom(snapshot: Character.SnapshotV003) = {
    val character = context.actorOf(Character.props(snapshot.characterName, snapshot.password, snapshot.skillset), snapshot.characterName)
    characters += character
    character ! Character.LoadFromSnapshot(snapshot)
    oldAndCurrentCharacterNames += snapshot.characterName
  }

  def broadcast(event: LoggedEvent) = {
    characters foreach { c => c ! Creature.Message(event.event) }
    ownersOfTherecentlyDeadAndRetired foreach { p => p ! FeedbackBuffer.Feedback(LoggedEvent(event.event)) }
  }

  implicit def toSnapShopV003(snapshot: Character.SnapshotV002) = Character.SnapshotV003(
    snapshot.characterName,
    snapshot.password,
    snapshot.skillset,
    snapshot.receipesLearned,
    snapshot.friends,
    snapshot.weaponEquiped,
    snapshot.suitEquiped,
    snapshot.trinketEquiped,
    snapshot.inventory.toSeq,
    snapshot.location,
    snapshot.wounds,
    snapshot.fatigue,
    0,
    snapshot.xp,
    snapshot.timePlayed,
    snapshot.talents)

  def receive = {
    case Broadcast(event)                            => broadcast(event)
    case DeadCharacter(character, owner)             => deadChar(character, owner)
    case ExpireOwnerOfDeadOrRetired(owner)           => ownersOfTherecentlyDeadAndRetired -= owner
    case RetiredCharacter(character)                 => retiredChar(character)
    case NewCharacter(characterName, password)       => newChar(characterName, password)
    case CanLoginCharacter(characterName, password)  => canLogin(characterName, password)
    case TakeSnapshots()                             => characters foreach { character => character ! Character.RequestSnapshot() }
    case x: Character.SnapshotV002 if (sender == db) => loadCharFrom(x)
    case x: List[String @unchecked]                  => oldAndCurrentCharacterNames ++= x
    case x: Character.SnapshotV003 if (sender != db) => db forward x
    case x: Character.SnapshotV003                   => loadCharFrom(x)
    case ListAll()                                   => characters foreach { _ ! Character.DetailedListingRequest(sender) }
    case ReviveFrom(tombstone)                       => reviveCharFromTombstone(tombstone)
    case x: TombstoneGenerator.NewTombstone          => parent forward x
    case x: WorldBuilder.PositionTo                  => parent forward x
  }

  override val supervisorStrategy = {
    OneForOneStrategy() {
      case _: Exception => Resume
    }
  }
}

object CharacterGenerator {
  val NAME = "characters"
  def props(db: ActorRef) = Props(new CharacterGenerator(db))
  case class NewCharacter(name: String, password: String)
  case class CanLoginCharacter(name: String, password: String)
  case class DeadCharacter(character: ActorRef, owner: ActorRef)
  case class RetiredCharacter(character: ActorRef)
  case class TakeSnapshots()
  case class Broadcast(event: LoggedEvent)
  case class ListAll()
  case class ExpireOwnerOfDeadOrRetired(owner: ActorRef)
  case class ReviveFrom(tombstone: Tombstone.SnapshotT002)
}