package kt.game.requests

import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.Character
import kt.game.player.PlayerRegistry
import kt.game.system.FeedbackBuffer
import kt.game.events.LoggedEvent
import kt.game.events.NullEvent

class TellAllRequestHandler(val player: ActorRef, val playerRegistry: ActorRef, val text: String) extends Actor {
  import Messages._

  context.setReceiveTimeout(250 milliseconds)
  player ! Character.GetName(self)

  var fromName: String = null

  def receive = {
    case NameOfPlayer(name, player) => { playerRegistry.tell(PlayerRegistry.TellAll(text, name), player); player ! FeedbackBuffer.Feedback(LoggedEvent(NullEvent)) }
    case ReceiveTimeout             => self ! PoisonPill
  }
}

object TellAllRequestHandler {
  def props(player: ActorRef, playerRegistry: ActorRef, text: String) = Props(new TellAllRequestHandler(player, playerRegistry, text))
}