package kt.game.domain.creatures.monsters.ai

import scala.concurrent.duration.DurationInt

import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.AttackType
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.MonsterAction
import kt.game.domain.creatures.monsters.Behaviour
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.SkillName.SkillName2String
import kt.game.events.LoggedEvent
import kt.game.events.MonsterEvent

trait Defensive extends Monster with Behaviour {
  import context._

  override def detailedDescription = "Looks to be defending its location." + (if (super.detailedDescription != "") " " + super.detailedDescription else "")

  def updateDefenseSchedule() = system.scheduler.scheduleOnce(actionFrequency, self, Defensive.Defend())
  allSchedules += updateDefenseSchedule

  def aoeFrequency = 25 - fullSkillModifer(SkillName.WEAP) seconds
  var aoeCD = aoeFrequency.fromNow

  override def attackedReaction(attackRoll: Int, from: ActorRef) = spot()

  def defend() = {
    val allTargets = allAggro()
    val target = highTarget(allTargets)
    if (allTargets.size > 1 && aoeCD.isOverdue && skillset.weapons.bonus.max(skillset.ballistics.bonus) > 5) {
      allTargets foreach (_._1 ! Creature.CanMeleeAttackFrom(self, currentLocation, AttackType.Melee))
      aoeCD = aoeFrequency.fromNow
    } else if (target != None) target.get ! Creature.CanMeleeAttackFrom(self, currentLocation, AttackType.Melee)
    else spot()
  }

  override def presence(result: Int, location: ActorRef) = {
    log(LoggedEvent(MonsterEvent("You are testing " + sender.path.name + "' s presence with a conviction of " + result)))
    if (result >= 0) {
      addAggro(sender, Monster.AGGRO_MIN * result + (if (location == currentLocation) Monster.AGGRO_LOW else 0))
    }
    if (priorityTarget().orNull == sender) {
      log(LoggedEvent(MonsterEvent("New priority target is " + sender.path.name + " with a convition of " + aggro.get(sender).get._1)))
    }
  }

  override def usedInYourArea(creature: ActorRef) = {
    addAggro(creature, Monster.AGGRO_MAX * 2)
    spottedByCache foreach { _ ! Creature.MonsterStateChanged(self, "taunted") }
    log(LoggedEvent(MonsterEvent("You got taunted by " + creature.path.name + "!")))
    addBuffOrPenalty(SkillName.PERC, 5, (15 seconds).fromNow, "Taunted")
    addBuffOrPenalty(SkillName.WEAP, 2, (15 seconds).fromNow, "Taunted")
    addBuffOrPenalty(SkillName.REFL, 2, (15 seconds).fromNow, "Taunted")
    addBuffOrPenalty("INITIATIVE", 25, (15 seconds).fromNow, "Taunted")
  }

  override def receive = ({
    case Defensive.Defend() => {
      updateDefenseSchedule()
      if (actionCheck(MonsterAction.Defend)) defend()
    }
  }: Receive) orElse super.receive

}

object Defensive {
  case class Defend()
}