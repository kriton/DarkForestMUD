package kt.game.events.talents

import kt.game.events.NonCombatEvent
import akka.actor.ActorRef
import kt.game.events.talents.DominateEvents.DominateFailureReason._
import kt.game.domain.creatures.characters.talents.Dominate
import kt.game.events.ActionFailedEvent
import kt.game.events.CombatEvent

object DominateEvents {

  case class DominateSucceededEvent(durationInSeconds: Long) extends NonCombatEvent {
    override def toString = s"Your target is now uder your control for the next $durationInSeconds seconds!"
  }
  
  case object SomeoneTriedToDominateYouEvent extends CombatEvent {
    override def toString = "Somebody tried to dominate you, but failed. You resisted succesfully!"
  }
  
  case object ThrallISNoLongerUnderYourControlEvent extends NonCombatEvent {
    override def toString = "Your thrall is no longer under your control!"
  }
  
  case object ThrallIsDeadEvent extends NonCombatEvent {
    override def toString = "Your thrall is dead!"
  }
  
  case object NoThrallEvent extends NonCombatEvent with ActionFailedEvent {
    override def toString = "You have no creature under your Control!"
  }

  case class DominateFailedEvent(reason: DominateFailureReason) extends NonCombatEvent with ActionFailedEvent {
    override def toString = reason match {
      case TooFarAway          => "Dominate Failed! Target creature is not close enough!"
      case NotEnoughFatePoints => s"You need ${Dominate.fpCost} Fate points to dominate!"
      case CheckFailure        => "Your dominate attempt failed! Target succesfully resisted."
      case CanNoLongerSpot     => "Dominate Failed! Can no longer spot target creture!"
    }
  }

  sealed trait DominateFailureReason
  object DominateFailureReason {
    case object CanNoLongerSpot extends DominateFailureReason
    case object TooFarAway extends DominateFailureReason
    case object NotEnoughFatePoints extends DominateFailureReason
    case object CheckFailure extends DominateFailureReason
  }

}