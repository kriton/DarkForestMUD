package kt.game.events.talents

import kt.game.events.ActionFailedReason
import kt.game.events.CombatEvent
import kt.game.events.ActionFailedEvent

object StunEvents {

  case class StunFailedEvent(reason: ActionFailedReason) extends CombatEvent with ActionFailedEvent {
    import kt.game.events.ActionFailedReason._
    override def toString = reason match {
      case CanNoLongerSpot   => "You can no longet spot wanted creature."
      case NotInSameLocation => "Target creature is too far away to stun!"
      case _                 => "Your stun attempt failed!"
    }
  }

}