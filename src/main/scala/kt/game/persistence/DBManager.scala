package kt.game.persistence

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.Character
import kt.game.domain.tombstones.Tombstone
import kt.game.player.HallOfFame

class DBManager private (val basePath: String) extends Actor {
  import DBManager._

  val charactersCache = context.actorOf(CharacterCache.props, "characters")

  new File(basePath + charactersCollection).mkdirs()
  new File(basePath + tombstonesCollection).mkdirs()
  new File(basePath + hallOfFameCollection).mkdirs()

  def writeToFile(p: String, value: Serializable): Unit = {
    val pw = new java.io.FileOutputStream(new File(basePath + p))
    try pw.write(Serializer.serialize(value)) finally pw.close()
  }

  def readAll[T <: Serializable](collection: String) = {
    val file = new File(basePath + collection)
    for (file <- file.listFiles()) {
      val snapshot = Serializer.deserialize[T](java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(file.toURI())))
      println(snapshot)
      sender ! snapshot
    }
  }

  def read[T <: Serializable](fileName: String) = {
    val file = new File(basePath + fileName)
    if (file.exists && file.length != 0) {
      val contents = Serializer.deserialize[T](java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(file.toURI())))
      println(contents)
      sender ! contents
    }

  }

  def deleteCharacter(ref: ActorRef, name: String) = {
    charactersCache ! CharacterCache.Remove(ref)
    val file = new File(basePath + name)
    file.delete()
  }

  def deleteTombstone(ref: ActorRef, name: String) = {
    val file = new File(basePath + name)
    file.delete()
  }

  def receive = {
    case WriteCharacter(name, character) => writeToFile(charactersCollection + "/" + name, character)
    case LoadAllCharacters()             => readAll(charactersCollection)
    case LoadAllTombstones()             => readAll(tombstonesCollection)
    case LoadAllNames()                  => read(allNamesFile)
    case UpdateAllNames(names)           => writeToFile(allNamesFile, names.asInstanceOf[Serializable])
    case DeleteCharacter(ref, name)      => deleteCharacter(ref, charactersCollection + "/" + name)
    case DeleteTombstone(ref, name)      => deleteTombstone(ref, tombstonesCollection + "/" + name)
    case x: Character.SnapshotV003       => charactersCache forward x
    case x: Tombstone.SnapshotT002       => writeToFile(tombstonesCollection + "/" + x.characterName, x)
    case HallOfFameEntry(listing)        => writeToFile(hallOfFameCollection + "/" + listing.name, listing)
    case LoadHallOfFame()                => readAll(hallOfFameCollection)
  }
}

object DBManager {
  def props(basePath: String) = Props(new DBManager(basePath))
  
  val NAME = "persistence"
  
  val charactersCollection = "characters"
  val tombstonesCollection = "tombstones"
  val hallOfFameCollection = "halloffame"
  val allNamesFile = "all_names"

  case class WriteCharacter(name: String, value: Character.SnapshotV003)
  case class DeleteCharacter(ref: ActorRef, name: String)
  case class DeleteTombstone(ref: ActorRef, name: String)
  case class LoadAllCharacters()
  case class LoadAllTombstones()
  case class LoadAllNames()
  case class UpdateAllNames(names: List[String])
  case class HallOfFameEntry(entry: HallOfFame.Listing)
  case class LoadHallOfFame()
}

object Serializer {

  def serialize[T <: Serializable](obj: T): Array[Byte] = {
    val byteOut = new ByteArrayOutputStream()
    val objOut = new ObjectOutputStream(byteOut)
    objOut.writeObject(obj)
    objOut.close()
    byteOut.close()
    byteOut.toByteArray
  }

  def deserialize[T <: Serializable](bytes: Array[Byte]): T = {
    val byteIn = new ByteArrayInputStream(bytes)
    val objIn = new ObjectInputStream(byteIn)
    val obj = objIn.readObject().asInstanceOf[T]
    byteIn.close()
    objIn.close()
    obj
  }
}