package kt.game.domain.creatures

import akka.actor.ActorRef
import kt.game.domain.locations.descriptors.Descriptor

case class RelativeLocation(key: Option[String], description: Option[Descriptor], ref: Option[ActorRef]) {
  override def toString = (key, description) match {
    case (Some(x), Some(n)) => s"<$x> $n"
    case (None, Some(n))    => n toString
    case (Some(x), None)    => s"location <$x>"
    case _                  => "unknown location"
  }
}

object SameLocation extends RelativeLocation(None, None, None) {
  override def toString = "your location"
}