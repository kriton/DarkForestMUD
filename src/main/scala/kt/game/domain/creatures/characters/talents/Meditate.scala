package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.Willpower
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.talents.MeditateEvents.MeditateFailedEvent
import kt.game.events.talents.MeditateEvents.MeditateSucceededEvent
import kt.game.events.talents.MeditateEvents.RefocusFailedEvent
import kt.game.events.talents.MeditateEvents.RefocusFailureReason

class Meditate private (val character: ActorRef) extends Actor with Talent {
  import Meditate._

  val name = Meditate.name
  val description = Meditate.description
  val help = Meditate.commandsHelp

  def setCDMeditate() = character ! Character.SetActionCooldown(action, s => ((420 - s.willpower.bonus * 10).max(180) seconds).fromNow)
  def setCDRefocus() = character ! Character.SetActionCooldown(action, s => ((300 - s.willpower.bonus * 10).max(120) seconds).fromNow)

  implicit val timeout = Timeout(200 milliseconds)

  def meditateAttempt() {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      setCDMeditate()
      if (Await.result(character ? Creature.Roll(SkillName.WILL), timeout.duration).asInstanceOf[Option[Int]].get >= meditateDC) {
        character ! Creature.AddFatePoints(1)
        character ! Creature.Message(MeditateSucceededEvent)
      } else {
        character ! Creature.Message(MeditateFailedEvent)
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def refocusAttempt() {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      if (Await.result(character ? Creature.GetFatePoints(), timeout.duration).asInstanceOf[Int] >= 1) {
        character ! Creature.AddFatePoints(-1)
        setCDRefocus()
        if (Await.result(character ? Creature.Roll(SkillName.WILL), timeout.duration).asInstanceOf[Option[Int]].get >= refocusDC) {
          character ! Creature.RegainControl()
        } else {
          character ! Creature.Message(RefocusFailedEvent(RefocusFailureReason.CheckFailure))
        }
      } else {
        character ! Creature.Message(RefocusFailedEvent(RefocusFailureReason.NotEnoughActionPoints))
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentMeditateCommand()        => meditateAttempt()
    case TalentMeditateReFocusCommand() => refocusAttempt()
  }
}

object Meditate extends TalentContext {
  def props(character: ActorRef) = Props(new Meditate(character))

  case class TalentMeditateCommand()
  case class TalentMeditateReFocusCommand()

  val meditateDC = 22
  val refocusDC = 17

  val skill = Willpower(8)

  val action = TalentAction.Meditate
  val name = action.toString

  val description = "You can restore a fate point. You can also break the effects of stun and dominate on yourself by spending a fate point."
  val trainingDescription = "You can train your arcane skills and get access to meditating powers here! " + requirementsString

  val TCP_meditate = "^meditate$".r
  val TCP_refocus = "^refocus$".r

  val commandsHelp =
    ("%-32s" format "meditate") + " -->  Restore a fate point.\r\n" +
      ("%-32s" format "refocus") + " -->  Break stun and domination that affact you.\r\n"
}