package kt.game.domain.creatures.characters.talents

import scala.concurrent.duration.DurationInt
import akka.pattern.ask
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Craft
import kt.game.requests.talents.CleanseRequestHandler
import kt.game.requests.talents.RessurectRequestHandler
import kt.game.domain.creatures.skills.Influence
import kt.game.player.Commands
import kt.game.events.ActionFailedEvent
import kt.game.domain.creatures.Creature
import kt.game.events.NonCombatEvent
import scala.concurrent.duration.FiniteDuration
import kt.game.requests.talents.DominateRequestHandler
import scala.concurrent.duration.Deadline
import akka.util.Timeout
import scala.concurrent.Await
import kt.game.events.talents.DominateEvents._
import kt.game.events.ActionOnCoolDownEvent

class Dominate private (val character: ActorRef) extends Actor with Talent {
  import Dominate._
  import context._

  val name = Dominate.name
  val description = Dominate.description
  val help = Dominate.commandsHelp

  var thrall: Option[ActorRef] = None
  implicit val timeout = Timeout(200 milliseconds)

  def setCD() = character ! Character.SetActionCooldown(action, s => ((600 - s.influence.bonus * 20).max(240) seconds).fromNow)

  def lostThrall(target: ActorRef) = if (thrall != None && thrall.get == target) {
    character ! Creature.Message(ThrallISNoLongerUnderYourControlEvent)
    thrall = None
  }

  def deadThrall(target: ActorRef) = if (thrall != None && thrall.get == target) {
    character ! Creature.Message(ThrallIsDeadEvent)
    thrall = None
  }

  def dominate(target: ActorRef, duration: FiniteDuration) = {
    character ! Creature.Message(DominateSucceededEvent(duration.toSeconds))
    thrall = Some(target)
    target.tell(Creature.TakeControlFor(duration, self ! ThrallIsFreed(_), self ! ThrallIsDead(_)), character)
  }

  def dominateAttempt(ref: String) = {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      setCD()
      actorOf(DominateRequestHandler.props(character, ref))
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentDominateCommand(ref)                     => dominateAttempt(ref)
    case TalentThrallCommand(command) if thrall == None => character ! Creature.Message(NoThrallEvent)
    case TalentThrallCommand(command)                   => Commands.sendCommand(character, thrall.get, command)
    case ThrallIsFreed(target)                          => lostThrall(target)
    case ThrallIsDead(target)                           => deadThrall(target)
    case DominateFailedToOccur()                        => character ! Character.SetActionCooldown(action, s => Deadline.now)
    case DominateSuccess(target, duration)              => dominate(target, duration)
  }
}

object Dominate extends TalentContext {
  def props(character: ActorRef) = Props(new Dominate(character))

  case class TalentDominateCommand(ref: String)
  case class TalentThrallCommand(command: String)
  case class ThrallIsFreed(creature: ActorRef)
  case class ThrallIsDead(creature: ActorRef)
  case class DominateFailedToOccur()
  case class DominateSuccess(target: ActorRef, duration: FiniteDuration)

  val fpCost = 1

  val skill = Influence(12)

  val action = TalentAction.Dominate
  val name = action.toString

  val description = "You can dominate other creatures and take them under your control temporarily. Each use costs 1 fate point."
  val trainingDescription = "You can train your arcane skills and get access to mind controlling powers! " + requirementsString

  val TCP_dominate = "^dominate (\\d+)$".r
  val TCP_thrall = "^thrall (.+)$".r

  val commandsHelp = ("%-32s" format "dominate <target>") + " -->  Dominate a target creature for a short duration taking control of his actions.\r\n" +
    ("%-32s" format "thrall <other command>") + " -->  Pass a command to your dominated creature.\r\n"
}