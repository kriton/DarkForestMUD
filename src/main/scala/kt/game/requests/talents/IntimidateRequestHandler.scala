package kt.game.requests.talents

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import scala.concurrent.duration.DurationInt
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Await
import kt.game.domain.creatures.Creature
import akka.actor.ReceiveTimeout
import kt.game.domain.creatures.characters.talents.Intimidate
import akka.actor.PoisonPill
import kt.game.domain.creatures.skills.SkillName
import scala.concurrent.duration.FiniteDuration
import kt.game.domain.creatures.AttackType
import kt.game.events.MultiAttackImpossibleEvent
import kt.game.events.AttackImpossibleReason

class IntimidateRequestHandler private (character: ActorRef, targets: Option[Seq[String]], duration: FiniteDuration) extends Actor {
  import IntimidateRequestHandler._
  import context._

  setReceiveTimeout(300 milliseconds)

  var roll: Option[Int] = None

  implicit val timeout = Timeout(120 milliseconds)
  val creatures = Await.result(character ? Creature.GetSpottedTargets(targets), timeout.duration).asInstanceOf[Option[Seq[ActorRef]]]
  val location = Await.result(character ? Creature.GetCurrentLocation(), timeout.duration).asInstanceOf[Option[ActorRef]]

  if (creatures != None && location != None) {
    creatures.get.foreach { _ ! Creature.CanMeleeAttackFrom(character, location.get, AttackType.NonAttack) }
  } else {
    parent ! Intimidate.IntimidateFailed()
    character ! Creature.Message(MultiAttackImpossibleEvent(AttackType.NonAttack, AttackImpossibleReason.CanNoLongerSpot))
    
  }

  def receive = {
    case Creature.MeleeAttackPossible(creature, _) if roll == None => { roll = Await.result(character ? Creature.Roll(SkillName.INFL), timeout.duration).asInstanceOf[Option[Int]]; creature.tell(Creature.AffectAllSkills(roll.get, Seq(SkillName.WILL), -2, duration.fromNow, Intimidate.effectName), character) }
    case Creature.MeleeAttackPossible(creature, _)                 => creature.tell(Creature.AffectAllSkills(roll.get, Seq(SkillName.WILL), -2, duration.fromNow, Intimidate.effectName), character)
    case Creature.MeleeAttackNotPossible(_, _)                     =>
    case ReceiveTimeout if roll == None                            => { parent ! Intimidate.IntimidateFailed(); character ! Creature.Message(MultiAttackImpossibleEvent(AttackType.NonAttack, AttackImpossibleReason.CanNoLongerSpot)); self ! PoisonPill }
    case ReceiveTimeout                                            => self ! PoisonPill
  }
}

object IntimidateRequestHandler {
  def props(character: ActorRef, targets: Option[Seq[String]], duration: FiniteDuration) = Props(new IntimidateRequestHandler(character, targets, duration))
}