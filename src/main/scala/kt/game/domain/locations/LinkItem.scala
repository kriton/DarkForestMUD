package kt.game.domain.locations

import akka.actor.Actor
import scala.concurrent.duration.Deadline
import kt.game.domain.creatures.skills.SkillName
import akka.actor.ActorRef
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.creatures.skills.SkillName.SkillName
import akka.actor.Props
import kt.game.domain.locations.LinkItem.Value
import scala.collection.mutable.Set
import scala.concurrent.duration.FiniteDuration
import akka.actor.PoisonPill
import kt.game.domain.creatures.Creature
import kt.game.events.ActionFailedEvent
import kt.game.utility.Result
import kt.game.events.NonCombatEvent
import kt.game.events.DisabledLinkItemSucceededEvent
import kt.game.events.DisabledLinkItemFailedEvent
import scala.concurrent.duration.Deadline
import kt.game.domain.creatures.skills.SkillName
import akka.actor.ActorRef
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.creatures.skills.SkillName.SkillName
import akka.actor.Props
import kt.game.domain.locations.LinkItem.Value
import scala.collection.mutable.Set
import scala.concurrent.duration.FiniteDuration
import akka.actor.PoisonPill
import kt.game.domain.creatures.Creature
import kt.game.events.ActionFailedEvent
import kt.game.utility.Result
import kt.game.events.NonCombatEvent

class LinkItem private (details: Value) extends Actor {
  import LinkItem._
  import context._

  system.scheduler.scheduleOnce(details.deadline, self, Terminate())

  val parentLinks: Set[ActorRef] = Set()

  def search(roll: Int, creature: ActorRef, locationTo: ActorRef) = {
    if (roll >= details.searchDC) {
      import details._
      creature ! Creature.FoundLinkItem(locationTo, description, disableSkill, destroyDC, evadeSkills, creator)
    }
  }

  def disable(creature: ActorRef, roll: Int) = {
    val result = Result.criticalSteps(roll - details.destroyDC)
    if (result >= 0 || (details.creator != None && creature == details.creator)) {
      creature ! Creature.Message(DisabledLinkItemSucceededEvent(self))
      self ! Terminate()
    } else {
      creature ! Creature.Message(DisabledLinkItemFailedEvent(Some(self)))
      if (result < -1) details.effectOnTrigger(creature)
    }
  }

  def receive = {
    case Search(roll, creature, locationTo) => search(roll, creature, locationTo)
    case Trigger(creature)                  => details.effectOnTrigger(creature)
    case ParentLink()                       => parentLinks += sender
    case Terminate()                        => { parentLinks foreach { _ ! Link.ExpiredItem(self) }; self ! PoisonPill }
    case Disable(creature, roll)            => disable(creature, roll)
    case _                                  =>
  }
}

object LinkItem {
  def props(details: Value) = Props(new LinkItem(details))

  case class ParentLink()
  case class Value(creator: Option[ActorRef], description: Descriptor, searchDC: Int, disableSkill: SkillName, destroyDC: Int, evadeSkills: Map[SkillName, Int], deadline: FiniteDuration, effectOnTrigger: ActorRef => Unit)
  case class Terminate()
  case class Search(roll: Int, creature: ActorRef, locationTo: ActorRef)
  case class Trigger(creature: ActorRef)
  case class Disable(creature: ActorRef, roll: Int)
}
