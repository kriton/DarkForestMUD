package kt.game.domain.creatures.characters.talents

import scala.concurrent.duration.DurationInt
import akka.pattern.ask
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Craft
import kt.game.requests.talents.CleanseRequestHandler
import kt.game.requests.talents.RessurectRequestHandler
import kt.game.domain.creatures.skills.Influence
import kt.game.player.Commands
import kt.game.domain.creatures.Creature
import kt.game.events.NonCombatEvent
import scala.concurrent.duration.FiniteDuration
import kt.game.requests.talents.ScryRequestHandler
import scala.concurrent.duration.Deadline
import akka.util.Timeout
import scala.concurrent.Await
import kt.game.domain.creatures.skills.Perception
import akka.actor.Cancellable
import kt.game.events.talents.ScryEvents._
import kt.game.events.ActionOnCoolDownEvent

class Scry private (val character: ActorRef) extends Actor with Talent {
  import Scry._
  import context._

  val name = Scry.name
  val description = Scry.description
  val help = Scry.commandsHelp

  var scryee: Option[(ActorRef, Cancellable)] = None
  implicit val timeout = Timeout(200 milliseconds)

  def setCD() = character ! Character.SetActionCooldown(action, s => ((300 - s.perception.bonus * 10).max(90) seconds).fromNow)

  def scrying(target: ActorRef, maxDuration: FiniteDuration) = {
    character ! Creature.Message(ScrySucceededEvent(target))
    scryee = Some(target, system.scheduler.scheduleOnce(maxDuration, self, TalentScryStopCommand()))
  }

  def scryAttempt(ref: String) = if (scryee == None) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      setCD()
      actorOf(ScryRequestHandler.props(character, ref))
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  } else {
    character ! Creature.Message(ScryFailedEvent(FailureReason.CheckFailure, None))
  }

  def stopScrying() = if (scryee != None) {
    scryee.get._1.tell(Creature.StopMonitoring(), character)
    character ! Creature.Message(ScryExpiredEvent)
    scryee.get._2.cancel()
    scryee = None
  }

  def receive = {
    case TalentScryCommand(ref)        => scryAttempt(ref)
    case ScryNotCompleted()            => character ! Character.SetActionCooldown(action, s => Deadline.now);
    case ScrySuccess(target, duration) => scrying(target, duration)
    case TalentScryStopCommand()       => stopScrying()
  }
}

object Scry extends TalentContext {
  def props(character: ActorRef) = Props(new Scry(character))

  case class TalentScryCommand(ref: String)
  case class TalentScryStopCommand()
  case class ScryNotCompleted()
  case class ScrySuccess(target: ActorRef, duration: FiniteDuration)

  val skill = Perception(8)

  val DC = 20
  def duration(successes: Int) = 60 + 30 * successes seconds

  val action = TalentAction.Scry
  val name = action.toString

  val description = "You can scry on other creatures and observe them from afar."
  val trainingDescription = "You can train your arcane skills and get access to scrying powers! " + requirementsString

  val TCP_scry = "^scry (\\d+)$".r
  val TCP_scryStop = "^scry stop$".r

  val commandsHelp = ("%-32s" format "scry <target>") + " -->  Scry a target creature for a short monitoring all his actions.\r\n" +
    ("%-32s" format "scry stop") + " -->  Stop scrying your current target.\r\n"
}