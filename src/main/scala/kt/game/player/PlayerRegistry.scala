package kt.game.player

import akka.actor.Actor
import akka.actor.ActorIdentity
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.Character
import kt.game.events.ChatEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.LoggedEvent
import kt.game.events.ChatEvents

class PlayerRegistry private (hallOfFame: ActorRef, characterGenerator: ActorRef) extends Actor {
  import PlayerRegistry._
  import context._

  var allPlayers = Set[ActorRef]()

  def login(out: ActorRef, builder: ActorRef, ip: String) {
    val player = context.actorOf(Player.props(out, builder, ip, characterGenerator))
    allPlayers += player
    sender ! player
  }

  def tellAll(text: String, from: String) = {
    allPlayers.filter(_ != sender).foreach { _ ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.GlobalChatEvent(from, text))) }
  }

  def hintAll(text: String) = {
    allPlayers.foreach { _ ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.HintEvent(text))) }
  }

  def receive = {
    case x: HallOfFame.Listing          => hallOfFame forward x
    case HallOfFameRequest()            => hallOfFame ! HallOfFame.Print(sender)
    case LogInPlayer(out, builder, ip)  => login(out, builder, ip)
    case RegisterLoggedInPlayer(player) => allPlayers += player
    case LogOutPlayer(player)           => allPlayers -= player
    case ActorIdentity(_, Some(ref))    => println(ref.path)
    case GetAreas(requester)            => allPlayers.foreach { player => player ! Character.GetArea(requester) }
    case GetNames(requester)            => allPlayers.foreach { player => player ! Character.GetName(requester) }
    case NotifyAll(text)                => allPlayers.foreach { player => player ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.GlobalNotificationEvent(text))) }
    case ListAll()                      => allPlayers.foreach { _ ! Character.Listing(sender) }
    case TellAll(text, from)            => tellAll(text, from)
    case HintAll(text)                  => hintAll(text)
  }
}

object PlayerRegistry {
  def props(hallfOfame: ActorRef, characterGenerator: ActorRef) = Props(new PlayerRegistry(hallfOfame, characterGenerator))
  
  val NAME = "players"
  
  case class LogInPlayer(out: ActorRef, builder: ActorRef, ip: String)
  case class RegisterLoggedInPlayer(player: ActorRef)
  case class LogOutPlayer(player: ActorRef)
  case class NotifyAll(text: String)
  case class TellAll(text: String, from: String)
  case class HintAll(text: String)
  case class ListAll()
  case class GetAreas(requester: ActorRef)
  case class GetNames(requester: ActorRef)
  case class HallOfFameRequest()
}
