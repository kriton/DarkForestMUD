package kt.game.domain.creatures

sealed trait Action
sealed trait MonsterAction extends Action
sealed trait TalentAction extends Action

object Action {
  case object Attack extends Action
  case object Ranged extends Action
  case object Move extends Action
  case object Spot extends Action
  case object Use extends Action
  case object Befriend extends Action
  case object Hide extends Action
  case object Sneak extends Action
  case object Pick extends Action
  case object Drop extends Action
  case object Craft extends Action
  case object Equip extends Action
  case object Unequip extends Action
  case object Steal extends Action
  case object Teach extends Action
  case object Search extends Action
  case object Disable extends Action
  case object Dig extends Action
  case object Follow extends Action
  case object Stop extends Action
  case object Protect extends Action
  case object Unprotect extends Action
  case object Flank extends Action
  case object Parry extends Action
  case object Rest extends Action
  case object QuickRest extends Action
}

object TalentAction {
  case object Cleave extends TalentAction
  case object Smite extends TalentAction
  case object Create extends TalentAction
  case object Heal extends TalentAction
  case object Intimidate extends TalentAction
  case object Meditate extends TalentAction
  case object Revive extends TalentAction
  case object Scry extends TalentAction
  case object Study extends TalentAction
  case object Stun extends TalentAction
  case object Trap extends TalentAction
  case object Volley extends TalentAction
  case object Dominate extends TalentAction
  case object Manyshot extends TalentAction
  case object Teleport extends TalentAction
  case object Daze extends TalentAction
}

object MonsterAction {
  case object Hunt extends MonsterAction
  case object Defend extends MonsterAction
  case object Roam extends MonsterAction
  case object CounterStrike extends MonsterAction
}