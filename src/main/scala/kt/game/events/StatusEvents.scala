package kt.game.events

import kt.game.domain.locations.descriptors.Descriptor
import akka.actor.ActorRef
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.Action
import kt.game.domain.creatures.Target
import kt.game.domain.items.WeaponItem
import kt.game.domain.items.SuitItem
import kt.game.domain.items.TrinketItem
import kt.game.domain.creatures.skills.Skill
import kt.game.domain.items.Receipe
import kt.game.domain.items.Object

object StatusEvents {
  
  case class StatusOfEvent(status: StatusEventType*) extends StatusEvent {
    override def toString = status.mkString("\r\n")
    def +:(event: StatusEventType) = StatusOfEvent((event +: status): _*)
    def :+(event: StatusEventType) = StatusOfEvent((status :+ event): _*)
    def ++(other: StatusOfEvent) = StatusOfEvent((status ++ other.status): _*)
  }
  
  case class HelpTextEvent(text: String) extends StatusEvent {
    override def toString = text
  }

  implicit def statusEventType2StatusOfEvent(event: StatusEventType) = StatusOfEvent(event)

  sealed trait StatusEventType

  case class LevelStatus(level: Int, xp: Int, xpNeededToLevelUp: Int) extends StatusEventType {
    override def toString = s"Level: $level. XP: $xp" +
      (if (xpNeededToLevelUp <= xp) ". You Can level up." else s". You need (${xpNeededToLevelUp - xp}) more XP to level up.") +
      " Max skill ranks for your level is: " + Character.STATLIMIT(level)
  }
  case class FatigueStatus(fatigue: Int, stamina: Int, restTick: Int, fatiguePenalty: Int) extends StatusEventType {
    override def toString = s"Fatigue: $fatigue. Stamina: $stamina. Rest tick: $restTick. (Penalty: $fatiguePenalty)"
  }
  case class LocationStatus(ref: ActorRef, description: Descriptor) extends StatusEventType {
    override def toString = s"Location: $description"
  }
  case class Woundstatus(wounds: Int, maxWounds: Int, penalty: Int) extends StatusEventType {
    override def toString = s"Wounds: $wounds / $maxWounds. (Penalty: $penalty)"
  }
  case class FatePointStatus(remainingFPs: Int, maxFPs: Int) extends StatusEventType {
    override def toString = s"Fate Points: $remainingFPs / $maxFPs. Used fate points will be restored upon level up."
  }
  case class GlobalCDStatus(initiative: Int, cdInSeconds: Double) extends StatusEventType {
    override def toString = s"Initiative: $initiative. Global Cooldown is at $cdInSeconds seconds."
  }
  case class CooldownsStatus(cds: Seq[(Action, Int)]) extends StatusEventType {
    override def toString = "Active Cooldowns: " + cds.map(e => s"[${e._1}: ${e._2} secs]").mkString(", ")
  }
  case class DefenseStatus(baseDefense: Int, deflecting: Option[(Int, Option[Target], Long)]) extends StatusEventType {
    override def toString = "Defense: " + baseDefense + ". " + (deflecting match {
      case None                                          => ""
      case Some(Tuple3(_, _, deadline)) if deadline <= 0 => ""
      case Some(Tuple3(bonus, None, deadline))           => s"Deflecting with a $bonus bonus against all for $deadline seconds. "
      case Some(Tuple3(bonus, against, deadline))        => s"Deflecting with a $bonus bonus against $against for $deadline seconds. "
      case _                                             => ""
    }) + "Your defense depends on your athletics or reflexes, whichever is higher."
  }
  case class ConditionsStatus(conditions: Seq[String]) extends StatusEventType {
    override def toString = s"Conditions: ${conditions.mkString(", ")}"
  }
  case class EquippedItemsStatus(weapon: WeaponItem, suit: SuitItem, trinket: TrinketItem) extends StatusEventType {
    override def toString = "Weapon: " + weapon + "\r\n" + "Suit: " + suit + "\r\nTrinket: " + trinket
  }
  case class SimpleSkillStatus(skillset: Skillset) extends StatusEventType {
    override def toString = s"Skills: $skillset"
  }
  case class CreatureTypeStatus(creatureType: String, detailedDescription: Option[String]) extends StatusEventType {
    override def toString = s"CreatureType: $creatureType. ${detailedDescription.getOrElse("")}"
  }
  case class TalentsStatus(level: Int, talents: Seq[(ActorRef, String, String)]) extends StatusEventType {
    override def toString = "Talents Known (up to " + Character.TALENTLIMIT(level) + "): " + (if (talents.isEmpty) "None" else
      talents.map(e => e._1.path.name + ": " + e._2).zipWithIndex.map(e => "(" + e._2 + ") => " + e._1).mkString("\r\n             ", "\r\n             ", ""))
  }
  case class FullSkillsetStatus(skillset: Seq[(Skill, Int)]) extends StatusEventType {
    override def toString = "Athletics (ATHL)  --> " + skillset(0)._1.ranks + " ranks, [" + skillset(0)._1.bonus + " + " + skillset(0)._2 + "]\r\n" + 
      "Ballistics (BALL) --> " + skillset(1)._1.ranks + " ranks, [" + skillset(1)._1.bonus + " + " + skillset(1)._2 + "]\r\n" +
      "Craft (CRAF)      --> " + skillset(2)._1.ranks + " ranks, [" + skillset(2)._1.bonus + " + " + skillset(2)._2 + "]\r\n" +
      "Influence (INFL)  --> " + skillset(3)._1.ranks + " ranks, [" + skillset(3)._1.bonus + " + " + skillset(3)._2 + "]\r\n" + 
      "Perception (PERC) --> " + skillset(4)._1.ranks + " ranks, [" + skillset(4)._1.bonus + " + " + skillset(4)._2 + "]\r\n" +
      "Reflexes (REFL)   --> " + skillset(5)._1.ranks + " ranks, [" + skillset(5)._1.bonus + " + " + skillset(5)._2 + "]\r\n" +
      "Weapons (WEAP)    --> " + skillset(6)._1.ranks + " ranks, [" + skillset(6)._1.bonus + " + " + skillset(6)._2 + "]\r\n" +
      "Willpower (WILL)  --> " + skillset(7)._1.ranks + " ranks, [" + skillset(7)._1.bonus + " + " + skillset(7)._2 + "] "
  }
  case class FullSkillsetWithDescriptionStatus(skillset: Seq[(Skill, Int, Int)]) extends StatusEventType {
    override def toString = "Athletics (ATHL)  --> " + skillset(0)._1.ranks + " ranks, [" + skillset(0)._1.bonus + " + " + skillset(0)._2 + "] x" + skillset(0)._3 + ". Affects maximum wounds. Used for the move and dig actions and limits you inventory capacity.\r\n" +
      "Ballistics (BALL) --> " + skillset(1)._1.ranks + " ranks, [" + skillset(1)._1.bonus + " + " + skillset(1)._2 + "] x" + skillset(1)._3 + ". Affects stamina. Used for the ranged attack action.\r\n" +
      "Craft (CRAF)      --> " + skillset(2)._1.ranks + " ranks, [" + skillset(2)._1.bonus + " + " + skillset(2)._2 + "] x" + skillset(2)._3 + ". Affects maximum fate points. Used for investigating arcane sites with the use action, crafting items and the disable action.\r\n" +
      "Influence (INFL)  --> " + skillset(3)._1.ranks + " ranks, [" + skillset(3)._1.bonus + " + " + skillset(3)._2 + "] x" + skillset(3)._3 + ". Affects maximum fate points. Used for the charm action, makes it harder for others to steal from you and other creatures may cower before attacking you.\r\n" +
      "Perception (PERC) --> " + skillset(4)._1.ranks + " ranks, [" + skillset(4)._1.bonus + " + " + skillset(4)._2 + "] x" + skillset(4)._3 + ". Affects initiative. Used for spot action and makes it harder for other to steal from you.\r\n" +
      "Reflexes (REFL)   --> " + skillset(5)._1.ranks + " ranks, [" + skillset(5)._1.bonus + " + " + skillset(5)._2 + "] x" + skillset(5)._3 + ". Affects initiative. Used for steal and hide, and sneak actions and makes it harder for both monsters and other players to locate you.\r\n" +
      "Weapons (WEAP)    --> " + skillset(6)._1.ranks + " ranks, [" + skillset(6)._1.bonus + " + " + skillset(6)._2 + "] x" + skillset(6)._3 + ". Affects maximum wounds. Used for the melee attack, flank and parry actions.\r\n" +
      "Willpower (WILL)  --> " + skillset(7)._1.ranks + " ranks, [" + skillset(7)._1.bonus + " + " + skillset(7)._2 + "] x" + skillset(7)._3 + ". Affects stamina. Used to enter scary locations and not to cower before influential players."
  }
  case class ReceipesKnownStatus(receipesLearned: Seq[Receipe]) extends StatusEventType {
    override def toString = ("%-16s" format "recipes Known:") + receipesLearned.zipWithIndex.map { case (e, i) => "<" + i + ">: " + e }.mkString("\r\n" + ("%-16s" format "")) + "."
  }
  case class InventoryStatus(inventory: Seq[Object]) extends StatusEventType {
    override def toString = ("%-12s" format "Inventory: ") + (if (inventory.isEmpty) "" else inventory.zipWithIndex.map { case (e, i) => "<" + i + ">: " + e }.mkString("\r\n" + ("%-12s" format "")))
  }
}