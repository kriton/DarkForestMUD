package kt.game.events.talents

import kt.game.events.ActionFailedReason
import kt.game.events.NonCombatEvent
import kt.game.events.ActionFailedEvent

object StudyEvents {

  case class StudyFailedEvent(reason: ActionFailedReason) extends NonCombatEvent with ActionFailedEvent {
    import kt.game.events.ActionFailedReason._
    override def toString = reason match {
      case CanNoLongerSpot   => "You can no longet spot wanted creature."
      case _                 => "Your study attempt failed!"
    }
  }

}