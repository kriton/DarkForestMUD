package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.Target
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Athletics
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedReason
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.StunEvent
import kt.game.events.talents.StunEvents.StunFailedEvent
import kt.game.utility.Result

class Stun private (val character: ActorRef) extends Actor with Talent {
  import Stun._
  import context._

  val name = Stun.name
  val description = Stun.description
  val help = Stun.commandsHelp

  def setCD() = character ! Character.SetActionCooldown(action, s => ((90 - s.athletics.bonus * 4).max(12) seconds).fromNow)

  implicit val timeout = Timeout(200 milliseconds)

  def stunAttempt(targetRef: String) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      val target = Await.result(character ? Creature.GetSpottedTargetAsTarget(targetRef), timeout.duration).asInstanceOf[Target]
      if (target.ref != None) {
        val targetLocation = Await.result(target.ref.get ? Creature.GetCurrentLocation(), timeout.duration).asInstanceOf[Option[ActorRef]]
        val charLocation = Await.result(character ? Creature.GetCurrentLocation(), timeout.duration).asInstanceOf[Option[ActorRef]]
        stunCheck(target, targetLocation, charLocation)
      } else {
        character ! Creature.Message(StunFailedEvent(ActionFailedReason.CanNoLongerSpot))
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def stunCheck(target: Target, targetLocation: Option[ActorRef], charLocation: Option[ActorRef]) = {
    if (targetLocation != None && targetLocation.get == charLocation.get) {
      stun(target, Result.criticalSteps(Await.result(character ? Creature.Roll(SkillName.ATHL), timeout.duration).asInstanceOf[Option[Int]].get - Await.result(target.ref.get ? Creature.GetMaxSkillPassive(Seq(SkillName.REFL, SkillName.ATHL)), timeout.duration).asInstanceOf[Option[Int]].get))
    } else {
      character ! Creature.Message(StunFailedEvent(ActionFailedReason.NotInSameLocation))
    }
  }

  def stun(target: Target, successes: Int) = {
    if (successes >= 0) {
      val duration = 8 * (successes + 1)
      target.ref.get tell (Creature.StunnedConditionStart(duration seconds), character)
      character ! Creature.Message(StunEvent(target, duration))
    } else {
      character ! Creature.Message(StunFailedEvent(ActionFailedReason.CheckFailure))
    }
    setCD()
  }

  def receive = {
    case TalentStunCommand(ref) => stunAttempt(ref)
  }
}

object Stun extends TalentContext {
  def props(character: ActorRef) = Props(new Stun(character))

  case class TalentStunCommand(ref: String)

  val skill = Athletics(4)

  val action = TalentAction.Stun
  val name = action.toString

  val description = "You can stun target creatures in your area. Stunned targets cannot take any actions. The frequency that you can do so, depends on your athletics skill."
  val trainingDescription = "You can train your arcane skills and get access to the power to stun creatures here! " + requirementsString

  val TCP_stunother = "^stun (\\d+)$".r

  val commandsHelp = ("%-32s" format "stun <target>") + " -->  Stun target creature.\r\n"
}