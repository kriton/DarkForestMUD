package kt.game.domain.tombstones

import akka.actor.Actor
import akka.actor.Props
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.DurationLong
import scala.compat.Platform
import akka.actor.ActorRef
import kt.game.domain.locations.Location
import akka.actor.PoisonPill
import kt.game.utility.Result
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.Creature
import kt.game.domain.items.Object
import kt.game.domain.items.Object
import kt.game.events.ActionFailedEvent
import kt.game.events.NonCombatEvent
import kt.game.domain.items.AllObjects
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.worldbuilder.WorldBuilder
import kt.game.events.TombstoneDigFailedEvent
import kt.game.events.TombstoneDiggingFailedReason
import kt.game.events.TombstoneDigSucceededEvent

class Tombstone private (characterName: String, password: String, minutes: Int, description: String, dateCreated: Long, level: Int, skillset: Skillset, friends: scala.Predef.Set[String], receipesLearned: scala.Predef.Set[String], talents: scala.Predef.Set[String]) extends Actor {
  import Tombstone._
  import context._

  val treasures = ListBuffer[Object]()
  var defiled = false
  var location: ActorRef = null

  val spotDC = 20 - (level / 5)
  var digDC = 10 + (level / 5)

  def delay: FiniteDuration = {
    val timeExisting: Long = (Platform.currentTime - dateCreated) / 1000
    val maxTime: Long = (1 + (level / 10)) * 60L * 60L * 24L * 5L
    Math.max(maxTime - timeExisting, 2) seconds
  }

  system.scheduler.scheduleOnce(delay, self, Destroy())

  def spotted(creature: ActorRef, roll: Int) = {
    val result = Result.criticalSteps(roll - spotDC)
    var description = this.description
    description += (if (defiled) " It looks defiled! (still there is a chance that something may stil be hidden here)" else " It looks untouched!")
    if (result >= 0) {
      creature ! Character.SpottedTombstone(description, self, level, defiled)
    }
  }

  def dig(roll: Int) = {
    val result = Result.criticalSteps(roll - digDC)
    if (result > 0 && treasures.isEmpty) {
      sender ! Creature.Message(TombstoneDigFailedEvent(TombstoneDiggingFailedReason.TombstoneIsEmpty, Some(self)))
    } else if ((result == 0 && treasures.isEmpty) || result < 0) {
      sender ! Creature.Message(TombstoneDigFailedEvent(TombstoneDiggingFailedReason.FoundNothing, Some(self)))
    } else { // if (result >= 0 && !treasures.isEmpty) 
      defiled = true
      val item = treasures remove 0
      sender ! Creature.Message(TombstoneDigSucceededEvent(self, item))
      sender ! Character.PickedUp(item)
      digDC += 2
      parent ! TombstoneGenerator.Dug()
    }
  }

  def defile() = {
    defiled = true
    treasures.clear()
    parent ! TombstoneGenerator.Dug()
    system.scheduler.scheduleOnce(1 minute, self, Destroy())
  }

  def destroy() = {
    location ! Location.RemoveTombstone(self)
    parent ! TombstoneGenerator.LostTombstone(self)
  }

  def takeSnapshot() = {
    sender ! Tombstone.SnapshotT002(
      characterName,
      description,
      dateCreated,
      level,
      treasures.map(_.name).toSeq,
      digDC,
      defiled,
      location.path.name,
      skillset,
      friends,
      receipesLearned,
      talents,
      password,
      minutes)
  }

  def loadFromSnapShot(snapshot: SnapshotT002) = {
    treasures ++= snapshot.treasures.map { AllObjects.map.get(_) }.filter { _ != None }.map { _.get }
    digDC = snapshot.digDC
    defiled = snapshot.defiled
    parent ! WorldBuilder.NewTombstoneTo(self, snapshot.location)
  }

  def receive = {
    case Dig(roll)                  => dig(roll)
    case Spotted(creature, roll)    => spotted(creature, roll)
    case Place(location)            => location ! Location.AddTombstone(self)
    case Destroy()                  => destroy()
    case Defile()                   => defile()
    case Add(items)                 => treasures ++= items;
    case RequestSnapshot()          => takeSnapshot()
    case LoadFromSnapshot(snapshot) => loadFromSnapShot(snapshot)
    case AddedTombstone()           => location = sender
  }
}

object Tombstone {
  def props(characterName: String, password: String, minutes: Int, description: String, dateCreated: Long, level: Int, skillset: Skillset, friends: scala.Predef.Set[String], receipesLearned: scala.Predef.Set[String], talents: scala.Predef.Set[String]) = Props(new Tombstone(characterName, password, minutes, description, dateCreated, level, skillset, friends, receipesLearned, talents))

  def is(actor: ActorRef) = actor.path.elements.toSet.contains(TombstoneGenerator.NAME)

  case class Destroy()
  case class Place(location: ActorRef)
  case class Spotted(creature: ActorRef, roll: Int)
  case class Dig(roll: Int)
  case class Defile()
  case class Add(items: Set[Object])
  case class AddedTombstone()
  case class RequestSnapshot()
  case class LoadFromSnapshot(snapshot: SnapshotT002)

  case class Snapshot_T001(
    characterName: String,
    description: String,
    dateCreated: Long,
    level: Int,
    treasures: Seq[String],
    digDC: Int,
    defiled: Boolean,
    location: String,
    skillset: String,
    friends: scala.Predef.Set[String],
    receipesLearned: scala.Predef.Set[String],
    talents: scala.Predef.Set[String],
    password: String)

  case class SnapshotT002(
    characterName: String,
    description: String,
    dateCreated: Long,
    level: Int,
    treasures: Seq[String],
    digDC: Int,
    defiled: Boolean,
    location: String,
    skillset: String,
    friends: scala.Predef.Set[String],
    receipesLearned: scala.Predef.Set[String],
    talents: scala.Predef.Set[String],
    password: String,
    minutesPlayed: Int)
}