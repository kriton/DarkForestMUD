package kt.game.requests.talents

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.characters.talents.Scry
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.talents.ScryEvents.FailureReason
import kt.game.events.talents.ScryEvents.ScryFailedEvent
import kt.game.utility.Result

class ScryRequestHandler private (character: ActorRef, target: String) extends Actor {
  import ScryRequestHandler._
  import context._

  setReceiveTimeout(300 milliseconds)

  implicit val timeout = Timeout(120 milliseconds)

  character ! Creature.GetSpottedTarget(target)

  def scry(creature: ActorRef) = {
    val roll = Await.result(character ? Creature.Roll(SkillName.PERC), timeout.duration).asInstanceOf[Option[Int]].get
    val result = Result.criticalSteps(roll - Scry.DC)
    if (result >= 0) {
      creature.tell(Creature.StartMonitoring(), character)
      parent ! Scry.ScrySuccess(creature, Scry.duration(result))
    } else {
      character ! Creature.Message(ScryFailedEvent(FailureReason.CheckFailure, Some(creature)))
    }
  }

  def receive = {
    case Some(x: ActorRef) => scry(x)
    case None              => { character ! Scry.ScryNotCompleted(); character ! Creature.Message(ScryFailedEvent(FailureReason.NoLongerSpotted, None)) }
    case ReceiveTimeout    => self ! PoisonPill
  }
}

object ScryRequestHandler {
  def props(character: ActorRef, target: String) = Props(new ScryRequestHandler(character, target))
}