//package kt.game.domain
//import kt.game.domain.creatures.Creature
//import kt.game.domain.creatures.Skillset
//import collection.mutable.Stack
//import akka.actor.ActorSystem
//import org.scalatest._
//import akka.testkit.TestActorRef
//import akka.actor.Props
//import akka.testkit.TestKit
//import akka.testkit.ImplicitSender
//import kt.game.utility.Dice
//import kt.game.utility.Dice
//import kt.game.domain.locations.Location
//import kt.game.domain.locations.Area
//import kt.game.domain.locations.Features
//import kt.game.domain.creatures.Character
//import kt.game.player.Player
//import java.util.concurrent.ArrayBlockingQueue
//import kt.game.domain.creatures.worldbuilder.Builder
//
//class SampleDomainTests(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
//    with WordSpecLike with Matchers with BeforeAndAfterAll {
//
//  def this() = this(ActorSystem("MySystemSpec"))
//
//  override def afterAll {
//    TestKit.shutdownActorSystem(system)
//  }
//
//  "A Player ..." must {
//    "... be having fun" in {
//      val builder = system.actorOf(Builder.props, "builder")
//      val out = new ArrayBlockingQueue[String](10000)
//      val player = TestActorRef(Player.props(out, builder))
//      
//      val creatureRef = TestActorRef(Creature.props("Bogus", Skillset.zero), "Bogus")
//      val creatureRef2 = TestActorRef(Character.props("Mooh", "none", Skillset.create(10, 5, 5, 5, 5, 5, 10, 5)), "Mooh")
//      
//      player ! Player.Control("Mooh", "none")
//      player ! Player.Control("Bogus", "none")
//      
//      val creature = creatureRef.underlyingActor.asInstanceOf[Creature]
//      creature.skillset.athletics.bonus should be(0)
//      creature.wounds should be(0)
//      creatureRef ! Creature.Attacked(29)
//      creature.wounds should be(4)
//
//      println("\n\n")
//
//      val area1 = TestActorRef(Area.props("area1"), "area1")
//      val area2 = TestActorRef(Area.props("area2"), "area2")
//      val f1 = TestActorRef(Location.props("Fountain", "Big Fountain.", 10, 0, Features.None()), "Fountain")
//      val f2 = TestActorRef(Location.props("Rocks", "Dangerous Rocks.", 15, 10, Features.Travel(area2)), "Rocks")
//      val f3 = TestActorRef(Location.props("Tree", "Hude Tree with a trunk.", 10, 5, Features.Investigate(5, "something amazing!", null)), "Tree")
//      area1 ! Area.AddFeature(f1)
//      area1 ! Area.AddFeature(f2)
//      area2 ! Area.AddFeature(f2)
//      area2 ! Area.AddFeature(f3)
//      player ! Creature.Position(area1)
//      player ! Creature.Position(area1)
//      f1 ! Location.Move(creatureRef, 20)
//      player ! Creature.SpotCommand()
//      
//      println("\nsleping...\n")
//      Thread sleep 1000
//      
//      player ! Creature.RangedAttackCommand("1")
//      player ! Creature.MeleeAttackCommand("1")
//      player ! Creature.MoveCommand("1")
//      player ! Creature.MeleeAttackCommand("1")
//      player ! Creature.MoveCommand("2")
//      player ! Creature.MeleeAttackCommand("1")
//      player ! Creature.TravelCommand()
//      player ! Creature.SpotCommand()
//      
//      println("\nsleping...\n")
//      Thread sleep 1000
//      
//      player ! Creature.MoveCommand("2")
//      player ! Creature.UseCommand()
//      player ! Creature.UseCommand()
//      player ! Creature.Status()
//
//      println()
//    }
//  }
//
//}
