package kt.game.events.talents

import kt.game.events.ActionFailedReason
import kt.game.events.NonCombatEvent
import kt.game.domain.creatures.characters.talents.Smite
import kt.game.events.ActionFailedEvent

object SmiteEvents {

  case class SmiteFailedEvent(reason: ActionFailedReason) extends NonCombatEvent with ActionFailedEvent {
    import kt.game.events.ActionFailedReason._
    override def toString = reason match {
      case CanNoLongerSpot   => "Smite Failed! Can no longer spot target creture!"
      case NotInSameLocation => "Smite Failed! Target creature is not close enough!"
      case _                 => s"You need ${Smite.fpCost} fate points to smite!"
    }
  }

}