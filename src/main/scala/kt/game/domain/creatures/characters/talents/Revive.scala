package kt.game.domain.creatures.characters.talents

import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Craft
import kt.game.requests.talents.CleanseRequestHandler
import kt.game.requests.talents.RessurectRequestHandler

class Revive private (val character: ActorRef) extends Actor with Talent {
  import Revive._
  import context._

  val name = Revive.name
  val description = Revive.description
  val help = Revive.commandsHelp

  def setResssurectCD() = character ! Character.SetActionCooldown(action, s => ((360 - s.craft.bonus * 10).max(120) seconds).fromNow)
  def setCleanseCD() = character ! Character.SetActionCooldown(action, s => ((120 - s.craft.bonus * 4).max(40) seconds).fromNow)

  def receive = {
    case TalentRessurectCommand(ref) => actorOf(RessurectRequestHandler.props(character, ref))
    case TalentCleanseCommand(ref)   => actorOf(CleanseRequestHandler.props(character, ref))
    case SetCleanseCD()              => setCleanseCD()
    case SetRessurectCD()            => setResssurectCD()
  }
}

object Revive extends TalentContext {
  def props(character: ActorRef) = Props(new Revive(character))

  case class TalentRessurectCommand(ref: String)
  case class TalentCleanseCommand(ref: String)
  case class SetCleanseCD()
  case class SetRessurectCD()

  val cleanseDC = 22
  def ressurectDC(level: Int, defiled: Boolean) = level / 5 + (if (defiled) 22 else 18)
  
  val ressurectFPCost = 3
  
  val skill = Craft(12)

  val action = TalentAction.Revive
  val name = action.toString

  val description = "You can ressurect creatures from their tombs (costs 4 Fate points) or purge the stunned condition from others."
  val trainingDescription = "You can train your arcane skills and get access to revival powers! " + requirementsString

  val TCP_ressurect = "^ressurect (\\d+)$".r
  val TCP_cleanse = "^cleanse (\\d+)$".r

  val commandsHelp = ("%-32s" format "ressurect <tomb>") + " -->  Ressurect target creature from his tomb.\r\n" +
    ("%-32s" format "cleanse <target>") + " -->  Remove the stunned condition from a target.\r\n"
}