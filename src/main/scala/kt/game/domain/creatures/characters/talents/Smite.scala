package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.Weapons
import kt.game.events.ActionFailedEvent
import kt.game.requests.talents.SmiteRequestHandler
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.ActionFailedEvent

class Smite private (val character: ActorRef) extends Actor with Talent {
  import Smite._
  import context._

  val name = Smite.name
  val description = Smite.description
  val help = Smite.commandsHelp

  implicit val timeout = Timeout(200 milliseconds)

  def setCD() = character ! Character.SetActionCooldown(action, s => ((90 - s.weapons.bonus * 3).max(30) seconds).fromNow)

  def smiteAttempt(target: String) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      setCD()
      context.actorOf(SmiteRequestHandler.props(character, target))
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentSmiteCommand(ref) => smiteAttempt(ref)
    case SmiteFailed()           => character ! Character.SetActionCooldown(action, s => Deadline.now)
    case SmiteSuccess()          => setCD()
  }
}

object Smite extends TalentContext {
  def props(character: ActorRef) = Props(new Smite(character))

  case class TalentSmiteCommand(ref: String)
  case class SmiteSuccess()
  case class SmiteFailed()

  val skill = Weapons(8)

  val action = TalentAction.Smite
  val name = action.toString

  val fpCost = 1
  val attackBonus = 12

  val description = "You can make a very attack melee attack that recieved alarge bonus and canot be intercepted. The frequency that you can do so, depends on your weapons skill and it costs 1 fate point."
  val trainingDescription = "You can train your arcane skills and get access to smiting techniques! " + requirementsString

  val TCP_smite = "^smite (\\d+)$".r

  val commandsHelp =
    ("%-32s" format "smite <target>") + " -->  Smite target creature in your area. Require 1 fate point.\r\n"
}