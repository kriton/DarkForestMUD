package kt.game.requests.gm

import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import kt.game.domain.tombstones.Tombstone
import scala.collection.mutable.ListBuffer
import kt.game.domain.tombstones.TombstoneGenerator
import akka.actor.ReceiveTimeout
import kt.game.events.LoggedEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.GMInfoEvent
import akka.actor.PoisonPill
import java.util.Date
import akka.actor.actorRef2Scala

class GetTombstonesRequestHandler(val tombstoneGenerator: ActorRef, filter: Option[String => Boolean], task: Option[ActorRef => Unit]) extends Actor {
  import GetTombstonesRequestHandler._
  import context._

  val listings = ListBuffer[(ActorRef, Tombstone.SnapshotT002)]()

  context.setReceiveTimeout(100 milliseconds)
  if (filter != None) tombstoneGenerator ! TombstoneGenerator.ListAll()

  def receive = {
    case AllTombstones(listings) => this.listings ++= listings
    case ReceiveTimeout if filter == None && task != None => {
      task get tombstoneGenerator
      parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent("Tombstone Request Sent to tombstone Generator!")))
      self ! PoisonPill
    }
    case ReceiveTimeout if task == None && filter != None => {
      parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent(listings.map(_._2.toPrettyString).filter(s => filter.get(s.replaceAll("\r\n", " "))).mkString("\r\n"))))
      self ! PoisonPill
    }
    case ReceiveTimeout if filter != None => {
      val targetTombstones = listings.map(e => (e._2.toPrettyString, e._1)).filter(s => filter.get(s._1.replaceAll("\r\n", " "))).map(_._2).toSet
      if (targetTombstones.isEmpty) {
        parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent("No tombstones match the selection...")))
      } else {
        parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent("Command sent to " + targetTombstones.map(_.path.name).mkString(", ") + ".")))
        targetTombstones.foreach { task.get }
      }
      self ! PoisonPill
    }
  }
}

object GetTombstonesRequestHandler {
  def props(tombstoneGenerator: ActorRef, filter: Option[String => Boolean], task: Option[ActorRef => Unit]) = Props(new GetTombstonesRequestHandler(tombstoneGenerator, filter, task))

  case class AllTombstones(listings: Seq[(ActorRef, Tombstone.SnapshotT002)])

  implicit class TombStonePrinter(tombstone: Tombstone.SnapshotT002) {
    def toPrettyString = {
      ("%-20s" format ("[" + tombstone.characterName + "]")) + " -> " +
        "[ " + tombstone.location + " ]" + ", " + new Date(tombstone.dateCreated) + ". level: " + tombstone.level +
        ", defiled: " + tombstone.defiled + ",  digDC" + tombstone.digDC + ". Items: " + tombstone.treasures.mkString(", ")
    }
  }
}