package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.Reflexes
import kt.game.domain.creatures.skills.SkillName
import kt.game.requests.talents.TrapRequestHandler
import kt.game.events.ActionFailedEvent
import kt.game.events.CombatEvent
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.events.TrapPlacedEvent
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.ActionFailedEvent

class Trap private (val character: ActorRef) extends Actor with Talent {
  import Trap._
  import context._

  val name = Trap.name
  val description = Trap.description
  val help = Trap.commandsHelp

  implicit val timeout = Timeout(200 milliseconds)

  def setCD() = character ! Character.SetActionCooldown(action, s => ((120 - s.reflexes.bonus * 5).max(20) seconds).fromNow)

  def trapAttempt(location: String, component: Option[String]) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      setCD()
      context.actorOf(TrapRequestHandler.props(character, location, component))
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentTrapCommand(location)                         => trapAttempt(location, None)
    case TalentTrapWithComponentCommand(location, component) => trapAttempt(location, Some(component))
    case TrapFailed()                                        => character ! Character.SetActionCooldown(action, s => Deadline.now)
    case TrapSuccess()                                       => { character ! Creature.Message(TrapPlacedEvent()); setCD() }
  }
}

object Trap extends TalentContext {
  def props(character: ActorRef) = Props(new Trap(character))

  case class TalentTrapCommand(locationRef: String)
  case class TalentTrapWithComponentCommand(locationRef: String, componentRef: String)
  case class TrapSuccess()
  case class TrapFailed()

  val skill = Reflexes(4)
  val DC = 10

  val action = TalentAction.Trap
  val name = action.toString

  val description = "You can place traps between locations that will damage the first creatures that goes through them. The frequency that you can do so, depends on your weapons skill."
  val trainingDescription = "You can train your arcane skills and get to trap creating powers! " + requirementsString

  val TCP_trap = "^trap (\\d+)$".r
  val TCP_trapWithComponent = "^trap (\\d+) with (\\d+)$".r

  val commandsHelp =
    ("%-32s" format "trap <locationKey> ") + " -->  Leave a trap on the way to target location.\r\n"
  ("%-32s" format "trap <locationKey> with <item>") + " -->  Leave a trap on the way to target location using an ingriedient from your inventory to make it stronger (the rarer, the better).\r\n"
}