package kt.game.domain.items
import RawMaterials._
import CraftedMaterials._
import Weapons._
import Suits._
import Trinkets._

case class Receipe private[items] (item: Object, ingridients: Set[Object], craftDC: Int, requirements: Set[Receipe]) {
  AllReceipes.map += item.name -> this
  override def toString = "DC [" + craftDC + "]: " + ingridients.mkString(", ") + " --> " + item + "." + (if (requirements.isEmpty) "" else " Requires: " + requirements.map { receipe => receipe.item.name }.mkString(", ") + ".")
}
object AllReceipes {
  val map: scala.collection.mutable.Map[String, Receipe] = scala.collection.mutable.Map()
}
object Receipes {
  val r_ashLeather = Receipe(ashLeather, Set(cloth, ashwood, ashLeaves), 15, Set())
  val r_elderCrystal = Receipe(elderCrystal, Set(psiCrystal, ruby, jade, amber, bones), 25, Set())
  val r_steel = Receipe(steel, Set(iron, coal), 12, Set())
  val r_leather = Receipe(leather, Set(skin), 10, Set())
  val r_ironwood = Receipe(ironwood, Set(iron, mithral, greenwood), 20, Set())
  val r_lightStone = Receipe(lightStone, Set(psiCrystal, gold), 28, Set())

  val r_artisanHammer = Receipe(artisanHammer, Set(steel, bones), 12, Set())
  val r_greatSword = Receipe(greatSword, Set(steel, iron), 15, Set(r_steel))
  val r_quarterStaff = Receipe(quarterStaff, Set(greenwood), 8, Set())
  val r_druidStaff = Receipe(druidStaff, Set(quarterStaff, ashLeaves, bones, ashLeather, jade), 17, Set(r_quarterStaff))
  val r_magicWand = Receipe(magicWand, Set(ashwood, amber, jade), 23, Set())
  val r_magicSword = Receipe(magicSword, Set(greatSword, ruby), 21, Set())
  val r_pistolPair = Receipe(pistolPair, Set(leather, steel, greenwood, coal, bones), 19, Set())
  val r_handCannon = Receipe(handCannon, Set(steel, mithral, coal, ashwood, bones), 29, Set(r_pistolPair))
  val r_greatCrossbow = Receipe(greatCrossbow, Set(greenwood), 17, Set())
  val r_wizardStaff = Receipe(wizardStaff, Set(quarterStaff, elderCrystal, lightStone), 32, Set(r_elderCrystal, r_lightStone, r_quarterStaff, r_magicWand))
  val r_daggers = Receipe(daggers, Set(steel), 12, Set())
  val r_towerShield = Receipe(towerShield, Set(ironwood, ashLeather), 18, Set(r_ironwood))
  val r_Katana = Receipe(Katana, Set(steel, mithral, amber), 20, Set(r_greatSword))
  val r_magicLute = Receipe(magicLute, Set(greenwood, ashwood, amber, jade), 22, Set(r_quarterStaff))

  val r_chainmail = Receipe(chainmail, Set(steel, leather, bones), 13, Set())
  val r_magicCape = Receipe(magicCape, Set(cloth, leather), 17, Set())
  val r_wardenArmor = Receipe(wardenArmor, Set(magicCape, ironwood, ashLeather), 18, Set(r_magicCape, r_chainmail, r_ashLeather))
  val r_phantomSuit = Receipe(phantomSuit, Set(magicCape, cloth, amber), 25, Set(r_magicCape))
  val r_crystalSkin = Receipe(crystalSkin, Set(elderCrystal, psiCrystal), 25, Set(r_magicWand, r_magicCape))
  val r_thornMail = Receipe(thornMail, Set(chainmail, bones), 19, Set(r_chainmail, r_magicSword))

  val r_elvenBoots = Receipe(elvenBoots, Set(ashLeather, ashLeaves), 18, Set())
  val r_boneMask = Receipe(boneMask, Set(bones, coal, amber, cloth, jade), 17, Set())
  val r_giantsGloves = Receipe(giantsGloves, Set(steel, lightStone, leather), 26, Set(r_steel, r_lightStone))
  val r_magicRing = Receipe(magicRing, Set(ruby, gold, jade, amber), 19, Set())
  val r_orbitStone = Receipe(orbitStone, Set(lightStone, magicRing), 31, Set(r_lightStone))
  val r_demonEyes = Receipe(demonEyes, Set(ruby, skin, lightStone), 27, Set())
  val r_angelWings = Receipe(angelWings, Set(cloth, ashLeaves, lightStone), 27, Set())
  val r_flyingBroomstick = Receipe(flyingBroomstick, Set(ashwood, cloth, bones), 21, Set())
  val r_fearBanner = Receipe(fearBanner, Set(ironwood, leather, cloth, jade), 20, Set(r_leather, r_ironwood))
  val r_crystalMask = Receipe(crystalMask, Set(elderCrystal), 29, Set(r_elderCrystal))
  val r_goblinSpyglass = Receipe( goblinSpyglass, Set(cloth, bones, amber, coal), 16, Set())
}