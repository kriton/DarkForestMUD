package kt.game.domain.creatures

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.collection.mutable.Set
import scala.compat.Platform
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.FiniteDuration
import Creature.AffectedSuccesfully
import Creature.AttackFailed
import Creature.AttackSucceeded
import Creature.Attacked
import Creature.CanMeleeAttackFrom
import Creature.Dead
import Creature.Follow
import Creature.FollowSuccess
import Creature.FolloweeLost
import Creature.FolloweeMovedTo
import Creature.FolloweeTriedToHide
import Creature.Hide
import Creature.InterceptedBy
import Creature.Killed
import Creature.MeleeAttackNotPossible
import Creature.MeleeAttackPossible
import Creature.Message
import Creature.MovedTo
import Creature.NoLongerFollowed
import Creature.NoLongerInterceptedBy
import Creature.NoLongerSpotted
import Creature.Presence
import Creature.Spotted
import Creature.SpottedTargetGotAttacked
import Creature.StealAttempt
import Creature.Stolen
import Creature.StopMonitoring
import Creature.StunnedConditionEnd
import Creature.TargetResisted
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Cancellable
import akka.actor.NotInfluenceReceiveTimeout
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.creatures.skills.Skill
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.SkillName.SkillName
import kt.game.domain.creatures.skills.SkillName.SkillName2String
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.items.Object
import kt.game.domain.locations.Feature
import kt.game.domain.locations.Features
import kt.game.domain.locations.Link
import kt.game.domain.locations.LinkItem
import kt.game.domain.locations.Location
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.events.ActionFailedEvent
import kt.game.events.AffectedByConditionEvent
import kt.game.events.AffectedTargetResistedEvent
import kt.game.events.AffectedTargetSuccessfullyEvent
import kt.game.events.AttackAgainstYouWasInterceptedEvent
import kt.game.events.AttackDealtDamageEvent
import kt.game.events.AttackDefendedEvent
import kt.game.events.AttackImpossibleEvent
import kt.game.events.AttackImpossibleReason
import kt.game.events.AttackMissedEvent
import kt.game.events.AttemptAttackEvent
import kt.game.events.CreatureIsKilled
import kt.game.events.CurrentLocationCreaturesSpottedEvent
import kt.game.events.DamageSufferedEvent
import kt.game.events.DeathPreventionByFateEvent
import kt.game.events.DevelopmentEvent
import kt.game.events.EvadedEnvironmentDamageEvent
import kt.game.events.FollowingAnotherCreatureEvent
import kt.game.events.GameEvent
import kt.game.events.HiddenSuccessfulyEvent
import kt.game.events.KilledViaEnvironmentDamageEvent
import kt.game.events.KilledYourTargetEvent
import kt.game.events.LinkItemSpottedEvent
import kt.game.events.LinkedLocationCreatureSpottedEvent
import kt.game.events.LocationSpottedEvent
import kt.game.events.LoggedEvent
import kt.game.events.MovementFailedEvent
import kt.game.events.MovementFailedReason
import kt.game.events.MovementSuccesfulEvent
import kt.game.events.NoLongerHiddenEvent
import kt.game.events.NoLongerProtectingYourTargetEvent
import kt.game.events.ProtectedByCreatureEvent
import kt.game.events.ProtectingYourTargetEvent
import kt.game.events.RessistedConditionEvent
import kt.game.events.RollEvent
import kt.game.events.StatusEvents.ConditionsStatus
import kt.game.events.StatusEvents.CreatureTypeStatus
import kt.game.events.StatusEvents.FullSkillsetStatus
import kt.game.events.StatusEvents.InventoryStatus
import kt.game.events.StatusEvents.LocationStatus
import kt.game.events.StatusEvents.SimpleSkillStatus
import kt.game.events.StatusEvents.StatusOfEvent
import kt.game.events.StatusEvents.Woundstatus
import kt.game.events.StatusOfOtherEvent
import kt.game.events.StoppedFollowingAnotherCreatureEvent
import kt.game.events.StunnedEvent
import kt.game.events.YouDiedEvent
import kt.game.events.YouInterceptedAnAttackEvent
import kt.game.events.YourAttackWasInterceptedEvent
import kt.game.requests.actions.InterceptRequestHandler
import kt.game.system.FeedbackBuffer
import kt.game.utility.Dice
import kt.game.utility.Result
import kt.game.events.DiscoveryMadeEvent
import kt.game.events.StolenSuccessfullyEvent
import kt.game.events.NoLongerStunnedEvent
import kt.game.events.StillStunnedEvent
import kt.game.events.DisabledLinkItemFailedEvent
import kt.game.events.ActionFailedReason
import kt.game.events.FollowFailedEvent
import kt.game.events.ProtectionFailedEvent
import kt.game.events.StealAttemptFailedEvent
import kt.game.events.AlreadyInControlEvent
import kt.game.events.FolloweeLostEvent
import kt.game.events.TargetHideFailedEvent
import kt.game.events.TargetMovedToOtherLocationEvent
import kt.game.events.TargeLostEvent
import kt.game.events.TargetLostReason
import kt.game.events.StolenFromYouEvent
import kt.game.events.StealAttemptOnYouFailedEvent
import kt.game.events.MonsterStateChangedEvent

sealed trait ControlState
case object InControl extends ControlState
case object Stunned extends ControlState

class Creature protected (val creatureType: String, var skillset: Skillset) extends Actor {
  import Creature._
  import context._

  var DR: Int = 0
  var wounds: Int = 0
  var fate_points_remaining: Int = FATE

  def MAX_WOUNDS: Int = 10 + (skillset.athletics.bonus + skillset.weapons.bonus) / 5
  def STAMINA_BONUS: Int = skillset.ballistics.bonus + skillset.willpower.bonus
  def STAMINA: Int = 20 + STAMINA_BONUS * 2
  def FATE: Int = 3 + (skillset.craft.bonus + skillset.influence.bonus) / 2
  def INITIATIVE: Int = (skillset.perception.bonus + skillset.reflexes.bonus) + abilityModifer("INITIATIVE")

  def globalDelay() = (3000 - INITIATIVE * 40).max(500) milliseconds

  val observedCache: Map[String, Tuple4[ActorRef, Descriptor, Int, ActorRef]] = Map() // locationRef, quickDescription, moveDC, linkToLocationRef
  val spottedCache: Map[String, ActorRef] = Map()
  val spottedByCache: Set[ActorRef] = Set()
  val linkItemsFoundCache: Map[String, (ActorRef, SkillName)] = Map()

  var recentAttackers = Predef.Set[(ActorRef, Boolean, AttackType, Long)]() // success/fail; attack type; when attacked  
  var interceptors = Predef.Set[ActorRef]()
  var intercepting: Option[ActorRef] = None

  val followers: Set[ActorRef] = Set()
  var following: Option[ActorRef] = None

  var currentLocation: ActorRef = null
  var locationDescription: Descriptor = null

  val inventory = ListBuffer[Object]()

  def baseDefense = lowPassive(skillset.athletics).max(lowPassive(skillset.reflexes))

  def isDead = wounds >= MAX_WOUNDS
  def woundedPenalty() = if (wounds >= MAX_WOUNDS / 2.0) wounds + 1 - (MAX_WOUNDS / 2.0).round.toInt else 0

  def location = locationDescription

  def fatiguedPenalty() = 0

  val viewers: Set[ActorRef] = Set()
  var controller: Option[(ActorRef, Option[ActorRef => Unit], Option[(Cancellable, ActorRef => Unit)])] = None
  var controlState: ControlState = InControl

  val buffsAndPenalties = Map[String, Map[String, (Int, Deadline)]]()

  implicit def actorRef2Target(creature: ActorRef) = Target(spottedCache.map(_.swap).get(creature), Some(recognize(creature)), Some(creature))
  implicit def stringRef2Target(creatureRef: String) = if (!creatureRef.isEmpty) {
    val target = spottedCache.get(creatureRef)
    Target(Some(creatureRef), target.map { recognize(_) }, target)
  } else NoneTarget
  implicit def actorRef2RelativeLocation(location: ActorRef) = {
    val locationObserved = observedCache.filter(_._2._1 == location).headOption
    RelativeLocation(locationObserved.map(_._1), locationObserved.map(_._2._2), Some(location))
  }

  def actionCheck(action: Action): Boolean = if (controlState == Stunned) {
    log(LoggedEvent(StillStunnedEvent))
    false
  } else true

  def level = Math.max(skillset.influence.ranks, 0) + Math.max(skillset.craft.ranks, 0) + skillset.athletics.ranks + Math.max(skillset.willpower.ranks, 0) + skillset.perception.ranks +
    Math.max(skillset.reflexes.ranks, 0) + (Math.max(skillset.weapons.ranks, skillset.ballistics.ranks) match {
      case x if x < 0 => x
      case x          => 2 * x
    })

  def lowStatus() = {
    (if (woundedPenalty() > 0 && fatiguedPenalty() > 0)
      "Looks " + (if (woundedPenalty() > 2) "very " else "") + "wounded and " + (if (fatiguedPenalty() > 2) "very " else "") + "tired. "
    else if (woundedPenalty() > 0)
      "Looks " + (if (woundedPenalty() > 2) "very " else "") + "wounded. "
    else if (fatiguedPenalty() > 0)
      "Looks " + (if (fatiguedPenalty() > 2) "very " else "") + "tired. "
    else "") +
      (if (controlState == Stunned) "Creature is stunned! " else "")
  }

  def abilityModifer(ability: String): Int = buffsAndPenalties.get(ability) match {
    case None                 => 0
    case Some(x) if x.isEmpty => { buffsAndPenalties -= ability; 0 }
    case Some(x) => x.foldLeft(0)((r, c) => {
      if (c._2._2.isOverdue()) {
        buffsAndPenalties.get(ability).get -= c._1
        r
      } else r + c._2._1
    })
  }

  def affectedSuccesfully(by: Int, effect: String, target: ActorRef) = log(LoggedEvent(AffectedTargetSuccessfullyEvent(target, effect, by)))

  def targetResisted(effect: String, target: ActorRef) = log(LoggedEvent(AffectedTargetResistedEvent(target, effect)))

  def addAbilityBuffOrPenalty(ability: Option[String], roll: Int, against: Seq[SkillName], by: Int => Int, till: Deadline, effect: String, fromRef: ActorRef) = {
    val success = Result.criticalSteps(roll - against.foldLeft(0) { (max, skill) => Math.max(max, passive(skillset.skill(skill))) })
    if (success >= 0) {
      val value = by(success)
      log(LoggedEvent(AffectedByConditionEvent(fromRef, effect, value)))
      fromRef ! AffectedSuccesfully(value, effect)
      ability match {
        case None => for (x <- kt.game.domain.creatures.skills.SkillName.values) addBuffOrPenalty(x, value, till, effect)
        case x    => addBuffOrPenalty(x.get, value, till, effect)
      }
    } else {
      log(LoggedEvent(RessistedConditionEvent(fromRef, effect)))
      fromRef ! TargetResisted(effect)
    }
  }

  def addBuffOrPenalty(ability: String, by: Int, till: Deadline, effect: String) = buffsAndPenalties.get(ability) match {
    case None                     => buffsAndPenalties += ability -> Map(effect -> (by, till))
    case x if x.get contains name => x.get += effect -> Tuple2(x.get.get(effect).get._1.max(by), Deadline.DeadlineIsOrdered.max(x.get.get(effect).get._2, till))
    case x                        => x.get += effect -> Tuple2(by, till)
  }

  def fullSkillModifer(skill: SkillName): Int = skillset.skill(skill).bonus - woundedPenalty - fatiguedPenalty + abilityModifer(skill)

  def roll(skill: Skill): Int = roll(skill, None)

  def roll(skill: Skill, withBonus: Option[Int]): Int = {
    val roll = Dice d20
    val modifier = skill.bonus - woundedPenalty - fatiguedPenalty + abilityModifer(skill.name) + withBonus.getOrElse(0)
    val finalResult = roll + modifier
    log(LoggedEvent(RollEvent(Seq(roll), modifier, finalResult, skill)))
    finalResult
  }
  def lowPassive(skill: Skill): Int = 5 + skill.bonus - woundedPenalty - fatiguedPenalty + abilityModifer(skill.name)
  def highPassive(skill: Skill): Int = 18 + skill.bonus - woundedPenalty - fatiguedPenalty + abilityModifer(skill.name)
  def passive(skill: Skill): Int = skill.passive - woundedPenalty - fatiguedPenalty + abilityModifer(skill.name)

  def log(event: LoggedEvent) = {}

  def name() = creatureType

  def creatureDied(name: String, killer: ActorRef) = {
    val victim = spottedCache.map(_.swap).get(sender)
    if (victim != None) {
      log(LoggedEvent(CreatureIsKilled(sender, killer)))
      spottedCache -= victim.get
    }
  }

  def death() = {
    if (controller != None && controller.get._2 != None) controller.get._2.get(self)
    controlState = InControl
    log(LoggedEvent(YouDiedEvent()))
    currentLocation ! Location.MoveOut()
    currentLocation = null
    locationDescription = null
    spottedByCache foreach { _ ! Dead(name, sender) }
    spottedByCache.clear()
    context become dead
  }

  def sufferWounds(wounds: Int) = {
    this.wounds += wounds
    if (this.wounds < 0) this.wounds = 0
    if (this.wounds >= MAX_WOUNDS) {
      val fatePointsRequired = wounds * 2
      if (fate_points_remaining >= fatePointsRequired) {
        this.wounds -= wounds
        fate_points_remaining -= fatePointsRequired
        log(LoggedEvent(DeathPreventionByFateEvent(wounds, fatePointsRequired)))
      } else death
    }
  }

  def adjustFatePoints(fp: Int) = {
    this.fate_points_remaining += fp
    if (this.fate_points_remaining < 0) this.fate_points_remaining = 0
    else if (this.fate_points_remaining > FATE) this.fate_points_remaining = FATE
  }

  def sufferedEnvironmentDamageBy(dmg: Int, reason: AttackType, by: Option[ActorRef], evadeSkills: scala.Predef.Map[SkillName, Int]) = {
    if (evadeSkills.find(e => passive(skillset.skill(e._1)) - e._2 >= 0) != None) {
      log(LoggedEvent(DamageSufferedEvent(reason, dmg, NoneTarget)))
      sufferWounds(dmg)
      if (isDead && by != None) {
        by.get ! Message(KilledViaEnvironmentDamageEvent(reason, self))
        by.get ! (Killed(self, level))
      }
    } else {
      log(LoggedEvent(EvadedEnvironmentDamageEvent(reason)))
    }
  }

  def attackedWithInteceptionCheck(attackRoll: Int, from: ActorRef, attackType: AttackType) = {
    if (interceptors.isEmpty) {
      attacked(attackRoll, from, attackType)
    } else {
      actorOf(InterceptRequestHandler.props(self, currentLocation, interceptors, attackRoll, sender, from, attackType))
    }
  }

  def defenseAgainst(attacker: ActorRef, attackType: AttackType) = {
    val creature = spottedCache.map(_ swap).get(attacker)
    baseDefense - (if (creature == None) 5 else 0)
  }

  def attacked(attackRoll: Int, from: ActorRef, attackType: AttackType) = {
    val creature = spottedCache.map(_ swap).get(sender)
    val result = attackRoll - defenseAgainst(sender, attackType)
    updateRecentAttacks(sender, result >= 0, attackType)
    if (result < 0) {
      logAttackDefended(sender, from)
      sender() ! AttackFailed(self)
    } else {
      val damageDone = 1 + Result.criticalSteps(result) - DR;
      sufferWounds(damageDone)
      sender ! AttackSucceeded(self, damageDone)
      spottedByCache filter { _ != sender } foreach { _ ! SpottedTargetGotAttacked(self, sender, damageDone) }
      if (isDead) sender() ! (Killed(self, level))
      logAttackSuffered(damageDone, sender, from, attackType)
    }
    if (creature == None) singleSpotAttempt(sender, highPassive(skillset.perception))
  }

  def updateRecentAttacks(attacker: ActorRef, success: Boolean, attackType: AttackType) = {
    recentAttackers = filterRecentAttacks() + Tuple4(attacker, success, attackType, Platform.currentTime)
  }

  def filterRecentAttacks() = {
    recentAttackers = (recentAttackers filter { _._4 + 20000 > Platform.currentTime })
    recentAttackers
  }

  def logHitted(creature: ActorRef, damageDone: Int) = log(LoggedEvent(AttackDealtDamageEvent(creature, damageDone)))

  def logMissed(creature: ActorRef) = log(LoggedEvent(AttackMissedEvent(creature)))

  def logAttackInterceptedBy(attacker: ActorRef, interceptor: ActorRef) = {
    log(LoggedEvent(AttackAgainstYouWasInterceptedEvent(attacker, interceptor)))
  }

  def logAttackInterceptedFor(attacker: ActorRef, target: ActorRef) = {
    log(LoggedEvent(YouInterceptedAnAttackEvent(attacker, target)))
  }

  def logAttackIntercepted(target: ActorRef, interceptor: ActorRef) = {
    log(LoggedEvent(YourAttackWasInterceptedEvent(target, interceptor)))
  }

  def logAttackDefended(creatureRef: ActorRef, location: ActorRef) = spottedCache.map(_.swap).get(creatureRef) match {
    case None                               =>
    case _ if (location != currentLocation) =>
    case x: Option[String]                  => log(LoggedEvent(AttackDefendedEvent(creatureRef)))
  }

  def logAttackSuffered(damageDone: Int, creatureRef: ActorRef, location: ActorRef, attackType: AttackType) = log(LoggedEvent(DamageSufferedEvent(attackType, damageDone, creatureRef)))

  def canMeleeAttackFrom(attacker: ActorRef, location: ActorRef, attackType: AttackType) = attackType match {
    case AttackType.Flank => sender() ! (if (location == currentLocation && !filterRecentAttacks().filter(e => e._1 != attacker && e._2 && e._3 != AttackType.Ranged).isEmpty) MeleeAttackPossible(self, attackType) else MeleeAttackNotPossible(self, attackType))
    case _                => sender() ! (if (location == currentLocation) MeleeAttackPossible(self, attackType) else MeleeAttackNotPossible(self, attackType))
  }

  def meleeAttack(creature: ActorRef, attackType: AttackType) = {
    log(LoggedEvent(AttemptAttackEvent(attackType, creature)))
    performWeaponsAttack(creature, attackType)
  }

  def performWeaponsAttack(creature: ActorRef, attackType: AttackType) = {
    creature ! Attacked(roll(skillset.weapons, attackType match {
      case AttackType.SpecialAttack(bonus, _) => Some(bonus)
      case AttackType.Flank                   => Some(8)
      case _                                  => None
    }), currentLocation, attackType)
    nolongerhidden()
  }

  def meleeAttackCheck(creatureRef: String, attackType: AttackType) = {
    val creature = spottedCache.get(creatureRef)
    if (creature != None) {
      creature.get ! CanMeleeAttackFrom(self, currentLocation, attackType)
    } else {
      log(LoggedEvent(AttackImpossibleEvent(attackType, creatureRef, AttackImpossibleReason.CanNoLongerSpot)))
    }
  }

  def meleeAttackNotPossible(creature: ActorRef, attackType: AttackType) = attackType match {
    case AttackType.Flank => log(LoggedEvent(AttackImpossibleEvent(attackType, creature, AttackImpossibleReason.NotInPosition)))
    case _                => log(LoggedEvent(AttackImpossibleEvent(attackType, creature, AttackImpossibleReason.NotInSameLocation)))
  }

  def rangedAttack(creatureRef: String) = {
    val creature = spottedCache.get(creatureRef)
    if (creature != None) {
      log(LoggedEvent(AttemptAttackEvent(AttackType.Ranged, creature.get)))
      performRangedAttack(creature.get)
    } else {
      log(LoggedEvent(AttackImpossibleEvent(AttackType.Ranged, creatureRef, AttackImpossibleReason.CanNoLongerSpot)))
    }
  }

  def performRangedAttack(creature: ActorRef) = {
    creature ! Attacked(roll(skillset.ballistics), currentLocation, AttackType.Ranged)
  }

  def killed(creature: ActorRef, level: Int) = {
    log(LoggedEvent(KilledYourTargetEvent(creature, level)))
  }

  def observed(location: ActorRef, linkToLocation: ActorRef, description: Descriptor, feature: Option[(Feature, Boolean)], dcToMoveTo: Int) = {
    val reference = (observedCache.size + 1) + ""
    val moveDifficulty = {
      if (location == currentLocation) -20
      else if (lowPassive(skillset.athletics) >= dcToMoveTo) 0
      else if (passive(skillset.athletics) >= dcToMoveTo) 10
      else if (highPassive(skillset.athletics) < dcToMoveTo) 20
      else 5
    }
    observedCache.put(reference, Tuple4(location, description, dcToMoveTo, linkToLocation))
    log(LoggedEvent(LocationSpottedEvent(location, feature, moveDifficulty)))
  }

  def foundLinkItem(linkItem: ActorRef, locationTo: ActorRef, description: Descriptor, disableSkill: SkillName, disableDC: Int, evadeSkills: scala.Predef.Map[SkillName, Int], creator: Option[ActorRef]) = {
    val to = observedCache.filter(_._2._1 == locationTo).headOption
    if (to != None) {
      val reference = (linkItemsFoundCache.size + 1) + ""
      linkItemsFoundCache += reference -> (linkItem, disableSkill)
      val disableDifficulty = Result.criticalSteps(passive(skillset.skill(disableSkill)) - disableDC)
      val evadeDifficulty = Result.criticalSteps(evadeSkills.tail.foldLeft(passive(skillset.skill(evadeSkills.head._1)) - evadeSkills.head._2) { (r, c) => Math.min(r, passive(skillset.skill(c._1)) - c._2) })
      log(LoggedEvent(LinkItemSpottedEvent(reference, ((creator != None && creator.get == self), creator.map(actorRef2Target(_)).getOrElse(NoneTarget)), description, disableDifficulty, evadeDifficulty, linkItem, locationTo)))
    }
  }

  def disable(ref: String) = {
    val linkItem = linkItemsFoundCache.get(ref)
    if (linkItem != None) {
      linkItem.get._1 ! LinkItem.Disable(self, roll(skillset.skill(linkItem.get._2)))
    } else {
      log(LoggedEvent(DisabledLinkItemFailedEvent(None, ActionFailedReason.CanNoLongerSpot)))
    }
  }

  def position(location: ActorRef) = {
    location ! Location.Position(self)
  }

  def move(locationRef: String, sneak: Boolean = false) = {
    val location = observedCache.get(locationRef)
    if (location != None) {
      if (location.get._1 == currentLocation) log(LoggedEvent(MovementFailedEvent(currentLocation, MovementFailedReason.AlreadyThere, Some(currentLocation))))
      else currentLocation ! Location.MoveAttempt(self, roll(skillset.athletics), location.get._1)
      if (!sneak) nolongerhidden()
    } else {
      log(LoggedEvent(MovementFailedEvent(currentLocation, MovementFailedReason.CanNoLongerObserve, None)))
    }
  }

  def moved(location: ActorRef, descriptors: Descriptor) = {
    if (currentLocation != null && currentLocation != location) {
      log(LoggedEvent(MovementSuccesfulEvent(location, descriptors, currentLocation)))
      currentLocation ! Location.MoveOut()
    }
    currentLocation = location
    locationDescription = descriptors
    spottedByCache foreach { _ ! MovedTo(currentLocation, spotDifficulty) }
    followers foreach { _ ! FolloweeMovedTo(currentLocation) }
    spot()
  }

  def moveFailed() = {
    if (following != None) {
      self ! FolloweeLost()
      following.get ! NoLongerFollowed()
    }
  }

  def follow(creatureRef: String) = {
    val creature = spottedCache.get(creatureRef)
    if (creature != None) {
      creature.get ! Follow(currentLocation)
    } else {
      log(LoggedEvent(FollowFailedEvent(creatureRef, ActionFailedReason.CanNoLongerSpot)))
    }
  }

  def FollowedBy(creature: ActorRef, fromLocation: ActorRef) = {
    followers += creature
    creature ! FollowSuccess()
    if (fromLocation != currentLocation) {
      creature ! Creature.FolloweeMovedTo(currentLocation)
    }
  }

  def followSuccess(creature: ActorRef) = {
    stopFollowing()
    following = Some(creature)
    log(LoggedEvent(FollowingAnotherCreatureEvent(creature)))
  }

  def stopFollowing() = if (following != None) {
    log(LoggedEvent(StoppedFollowingAnotherCreatureEvent(following.get)))
    following.get ! NoLongerFollowed()
    following = None
  }

  def stopFollowingAttempt() = {
    if (following != None) stopFollowing()
    else log(LoggedEvent(FollowFailedEvent("", ActionFailedReason.InvalidState)))
  }

  def noLongerFollowedBy(creature: ActorRef) = followers -= creature

  def followTo(target: ActorRef, location: ActorRef) = {
    if (following != None && target == following.get) {
      if (controlState != Stunned) {
        val moveDC = observedCache.map(e => (e._2._1, e._2._3)).get(location)
        if (location != currentLocation && moveDC != None) {
          currentLocation ! Location.MoveAttempt(self, roll(skillset.athletics), location)
        }
      } else {
        stopFollowing()
      }
    }
  }

  def followeeHides(hideRoll: Int, target: ActorRef) = {
    if (following != None && target == following.get && hideRoll > passive(skillset.perception)) {
      self ! FolloweeLost()
      following.get ! NoLongerFollowed()
    } else {
      log(LoggedEvent(TargetHideFailedEvent(target)))
    }
  }

  def followeeLost() = {
    if (following != None) {
      log(LoggedEvent(FolloweeLostEvent(following.get)))
      following = None
    }
  }

  def protect(creatureRef: String) = {
    val creature = spottedCache.get(creatureRef)
    if (creature != None) {
      if (intercepting != None) intercepting.get ! NoLongerInterceptedBy(self)
      intercepting = creature
      intercepting.get ! InterceptedBy(self)
      log(LoggedEvent(ProtectingYourTargetEvent(creature.get)))
    } else {
      log(LoggedEvent(ProtectionFailedEvent(creatureRef, ActionFailedReason.CanNoLongerSpot)))
    }
  }

  def unprotect() = {
    if (intercepting != None) {
      intercepting.get ! NoLongerInterceptedBy(self)
      log(LoggedEvent(NoLongerProtectingYourTargetEvent(intercepting.get)))
      intercepting = None
    } else {
      log(LoggedEvent(ProtectionFailedEvent("", ActionFailedReason.InvalidState)))
    }
  }

  def spotted(creature: ActorRef, inLocation: ActorRef, description: String, strength: Option[Int], status: Option[String]): Unit = {
    val reference = (spottedCache.size + 1)
    val location = observedCache.filter(_._2._1 == inLocation).headOption
    spottedCache.put(reference.toString, creature)
    if (inLocation == currentLocation) log(LoggedEvent(CurrentLocationCreaturesSpottedEvent(creature, description, status, strength.map(_ - level), SameLocation)))
    else log(LoggedEvent(LinkedLocationCreatureSpottedEvent(creature, description, status, strength.map(_ - level), inLocation)))
  }

  def detailedDescription = ""

  var hiddenDC = 0
  def spotDifficulty = Math.max(lowPassive(skillset.reflexes), hiddenDC)

  def nolongerhidden() = if (hiddenDC > 0) {
    hiddenDC = 0
    log(LoggedEvent(NoLongerHiddenEvent))
  }

  def spottedBy(creature: ActorRef, spotRoll: Int, inLocation: ActorRef) {
    val result = spotRoll - spotDifficulty
    if (result >= 0) {
      spottedByCache.add(creature)
      val statusDescriptionToSend = statusDescriptionAgainst(creature, result)
      var strength: Option[Int] = None
      if (Result.criticalSteps(result) > 0) strength = Some(level)
      creature ! Spotted(self, inLocation, creatureType, strength, Some(statusDescriptionToSend))
    }
  }

  def singleSpotAttempt(creature: ActorRef, roll: Int = lowPassive(skillset.perception)) = if (spottedCache.map(_ swap).get(creature) == None) {
    creature ! Creature.SpottedBy(self, roll, currentLocation)
  }

  def statusDescriptionAgainst(creature: ActorRef, spotResult: Int) = {
    lowStatus + (if (Result.criticalSteps(spotResult) > 0) { detailedDescription } else "")
  }

  def discovered(discovery: String) = {
    log(LoggedEvent(DiscoveryMadeEvent(discovery)))
    sender ! Location.FeatureUsed()
  }

  def spot() = {
    linkItemsFoundCache.clear()
    observedCache.clear()
    spottedCache foreach { _._2 ! NoLongerSpotted() }
    spottedCache.clear()
    currentLocation ! Location.Check(roll(skillset.perception))
  }

  def hide() = {
    val hideRoll = roll(skillset.reflexes)
    hiddenDC = hideRoll
    log(LoggedEvent(HiddenSuccessfulyEvent(hiddenDC)))
    spottedByCache foreach { e => e ! Hide(currentLocation, hideRoll) }
    followers foreach { _ ! FolloweeTriedToHide(hideRoll) }
  }

  def hideFrom(location: ActorRef, hideRoll: Int) = {
    val key = spottedCache.map(_.swap).get(sender).orNull
    if (hideRoll >= passive(skillset.perception)) {
      spottedCache -= key
      log(LoggedEvent(TargeLostEvent(sender, TargetLostReason.Hidden)))
      sender ! NoLongerSpotted()
    } else {
      log(LoggedEvent(TargetHideFailedEvent(sender)))
    }
  }

  def movedTo(location: ActorRef, hideRoll: Int) = {
    val key = spottedCache.map(_.swap).get(sender).orNull
    val newlocation = observedCache.filter(_._2._1 == location).headOption
    if (location != currentLocation && newlocation == None) {
      spottedCache -= key
      log(LoggedEvent(TargeLostEvent(sender, TargetLostReason.MovedAway)))
      sender ! NoLongerSpotted()
    } else if (location != currentLocation) {
      log(LoggedEvent(TargetMovedToOtherLocationEvent(sender, location)))
      currentLocation ! Location.ContinueSpottingSpecificCreature(sender, location, highPassive(skillset.perception))
    }
  }

  def noLongerSpottingCreature(creature: ActorRef, location: ActorRef) = {
    val key = spottedCache.map(_.swap).get(creature)
    if (key != None && location != currentLocation) {
      log(LoggedEvent(TargeLostEvent(creature, TargetLostReason.Unknown)))
      spottedCache -= key.get
      creature ! NoLongerSpotted()
    }
  }

  def noLongerSpotted(): Unit = {
    spottedByCache -= sender
  }

  def search(): Int = {
    linkItemsFoundCache.clear()
    val searchRoll = roll(skillset.perception)
    observedCache.foreach(_._2._4 ! Link.Search(searchRoll))
    searchRoll
  }

  def testPresence(willpower: Int, location: ActorRef, attackType: AttackType) = {
    sender ! Presence(Result.criticalSteps(willpower - passive(skillset.influence)), currentLocation, attackType)
  }

  def testWillpowerForLocation() = sender ! Link.MoveTestResult(self, roll(skillset.willpower))

  def steal(creatureRef: String) = {
    val creature = spottedCache.get(creatureRef)
    if (creature != None) {
      creature.get ! StealAttempt(roll(skillset.reflexes), currentLocation)
    } else {
      log(LoggedEvent(StealAttemptFailedEvent(creatureRef, ActionFailedReason.CanNoLongerSpot)))
    }
  }

  def stealAttempt(roll: Int, from: ActorRef) = {
    if (from == currentLocation) {
      var itemStolen: Option[Object] = None
      val result = Result.criticalSteps(roll - Math.max(passive(skillset.influence), passive(skillset.perception)))
      if (result <= -1) sender ! Message(StealAttemptFailedEvent(self, ActionFailedReason.CanNoLongerSpot))
      if (result < -1) caughtStealing(sender, None)
      if (result >= 0)
        if (inventory.size > 2) {
          itemStolen = Some(inventory.remove(inventory.size - 1))
          sender ! Stolen(itemStolen.get)
          if (result == 0) caughtStealing(sender, itemStolen)
        } else sender ! Message(StealAttemptFailedEvent(self, ActionFailedReason.InvalidState))
    } else {
      sender ! Message(StealAttemptFailedEvent(self, ActionFailedReason.NotInSameLocation))
    }
  }

  def caughtStealing(creature: ActorRef, item: Option[Object]) = if (item != None) {
    log(LoggedEvent(StolenFromYouEvent(creature, item.get)))
  } else log(LoggedEvent(StealAttemptOnYouFailedEvent(creature)))

  def stolen(item: Object) = {
    log(LoggedEvent(StolenSuccessfullyEvent(item)))
    receiveItem(item)
  }

  def consumedItem(item: Object): Boolean = {
    val sizeBefore = inventory.size
    inventory -= item
    sizeBefore > inventory.size
  }

  def receiveItem(item: Object) = {
    inventory += item
    true
  }

  def stunnedStart(duration: FiniteDuration) = {
    log(LoggedEvent(StunnedEvent(sender, duration.toSeconds.toInt)))
    controlState = Stunned
    context.system.scheduler.scheduleOnce(duration, self, StunnedConditionEnd())
  }

  def stunnedEnd() = {
    if (controlState == Stunned) {
      controlState = InControl
      log(LoggedEvent(NoLongerStunnedEvent))
    }
  }

  def regainControl() = {
    if (controlState != InControl) {
      controlState = InControl
      log(LoggedEvent(NoLongerStunnedEvent))
    } else {
      log(LoggedEvent(AlreadyInControlEvent))
    }
  }

  def recognize(creature: ActorRef) = if (Monster.is(creature)) Monster.nameFromRef(creature) else "somebody"

  def recognizeWithKey(creature: ActorRef) = spottedCache.map(_.swap).get(creature) match {
    case None    => recognize(creature)
    case Some(x) => s"<$x> " + recognize(creature)
  }

  def statusForOthers() = creatureStatus +: simpleStatus

  def simpleStatus() = StatusOfEvent(locationStatus, woundStatus, conditionsStatus)

  def status() = simpleStatus :+ simpleSkillStatus

  def creatureStatus() = CreatureTypeStatus(creatureType, None)

  def locationStatus() = LocationStatus(currentLocation, location)

  def woundStatus() = Woundstatus(wounds, MAX_WOUNDS, woundedPenalty)

  def conditionsStatus() = ConditionsStatus((buffsAndPenalties.map(_._2).flatten.filter(_._2._2.hasTimeLeft).map(_._1).toSet ++ (if (controlState == Stunned) Set("Stunned") else Set())).toSeq)

  def simpleSkillStatus() = SimpleSkillStatus(skillset)

  def inventoryStatus() = InventoryStatus(inventory)

  def fullSkillStatus() = FullSkillsetStatus(Seq(
    (skillset.athletics, abilityModifer(SkillName.ATHL)),
    (skillset.ballistics, abilityModifer(SkillName.BALL)),
    (skillset.craft, abilityModifer(SkillName.CRAF)),
    (skillset.influence, abilityModifer(SkillName.INFL)),
    (skillset.perception, abilityModifer(SkillName.PERC)),
    (skillset.reflexes, abilityModifer(SkillName.REFL)),
    (skillset.weapons, abilityModifer(SkillName.WEAP)),
    (skillset.willpower, abilityModifer(SkillName.WILL))))

  def receive = {
    case CanMeleeAttackFrom(attacker, location, attackType)                 => canMeleeAttackFrom(attacker, location, attackType)
    case MeleeAttackPossible(creature, attackType)                          => meleeAttack(creature, attackType)
    case MeleeAttackNotPossible(creature, attackType)                       => meleeAttackNotPossible(creature, attackType)
    case Attacked(value, location, attackType)                              => attackedWithInteceptionCheck(value, location, attackType)
    case AttackedNoIntecept(value, location, attackType)                    => attacked(value, location, attackType)
    case RangedAttackPossible(creature)                                     => performRangedAttack(creature)
    case AttackSucceeded(creature, dmg)                                     => logHitted(creature, dmg)
    case AttackFailed(creature)                                             => logMissed(creature)
    case Killed(creature, level)                                            => killed(creature, level)
    case Moved(location, descriptors)                                       => moved(location, descriptors)
    case MoveFailed()                                                       => moveFailed()
    case Observed(location, locationlink, description, feature, movedc)     => observed(location, locationlink, description, feature, movedc)
    case FoundLinkItem(locationTo, description, dis, disDC, evadeSkills, c) => foundLinkItem(sender, locationTo, description, dis, disDC, evadeSkills, c)
    case Spotted(creature, location, description, strength, status)         => spotted(creature, location, description, strength, status)
    case NoLongerSpotted()                                                  => noLongerSpotted()
    case SpottedBy(creature, spotRoll, inLocation)                          => spottedBy(creature, spotRoll, inLocation)
    case Hide(location, hideRoll)                                           => hideFrom(location, hideRoll)
    case NoLongerSpotting(creature, location)                               => noLongerSpottingCreature(creature, location)
    case MovedTo(location, spotDifficulty)                                  => movedTo(location, spotDifficulty)
    case MovedInYourArea(creature)                                          => singleSpotAttempt(creature)
    case Position(area)                                                     => position(area)
    case Discovered(discovery)                                              => discovered(discovery)
    case TestPresence(willPower, location, attackType)                      => testPresence(willPower, location, attackType)
    case TestWillpowerForLocation()                                         => testWillpowerForLocation()
    case Message(event)                                                     => log(event)
    case Heal(value)                                                        => sufferWounds(-value)
    case StealAttempt(roll, location)                                       => stealAttempt(roll, location)
    case Stolen(item)                                                       => stolen(item)
    case SufferedEnvironmentDamageBy(damage, reason, by, evadeSkills)       => sufferedEnvironmentDamageBy(damage, reason, by, evadeSkills)
    case Dead(creature, by)                                                 => creatureDied(creature, by)
    case MeleeAttackCommand(creature)                                       => { if (actionCheck(Action.Attack)) meleeAttackCheck(creature, AttackType.Melee) }
    case RangedAttackCommand(creature)                                      => { if (actionCheck(Action.Ranged)) rangedAttack(creature) }
    case FlankAttackCommand(creature)                                       => { if (actionCheck(Action.Flank)) meleeAttackCheck(creature, AttackType.Flank) }
    case StealCommand(creature)                                             => { if (actionCheck(Action.Steal)) steal(creature) }
    case SpotCommand()                                                      => { if (actionCheck(Action.Spot)) spot() }
    case SearchCommand()                                                    => { if (actionCheck(Action.Search)) search() }
    case DisableCommand(ref)                                                => { if (actionCheck(Action.Disable)) disable(ref) }
    case HideCommand()                                                      => { if (actionCheck(Action.Hide)) hide() }
    case SneakCommand(location)                                             => { if (actionCheck(Action.Sneak)) move(location, true) }
    case MoveCommand(location)                                              => { if (actionCheck(Action.Move)) move(location) }
    case FollowCommand(creature)                                            => { if (actionCheck(Action.Follow)) follow(creature) }
    case StopFollowingCommand()                                             => { if (actionCheck(Action.Stop)) stopFollowingAttempt() }
    case ProtectStartCommand(creature)                                      => { if (actionCheck(Action.Protect)) protect(creature) }
    case ProtectStopCommand()                                               => { if (actionCheck(Action.Unprotect)) unprotect() }
    case RegainControl()                                                    => regainControl()
    case Follow(location)                                                   => FollowedBy(sender, location)
    case NoLongerFollowed()                                                 => noLongerFollowedBy(sender)
    case FollowSuccess()                                                    => followSuccess(sender)
    case FolloweeMovedTo(location)                                          => followTo(sender, location)
    case FolloweeTriedToHide(hideRoll)                                      => followeeHides(hideRoll: Int, sender)
    case FolloweeLost()                                                     => followeeLost()
    case StatusCommand()                                                    => log(LoggedEvent(status))
    case AffectSkill(skill, roll, against, by, till, name)                  => addAbilityBuffOrPenalty(Some(skill), roll, against, _ => by, till, name, sender)
    case AffectAllSkills(roll, against, by, till, name)                     => addAbilityBuffOrPenalty(None, roll, against, _ => by, till, name, sender)
    case AffectAbility(ability, roll, against, by, till, name)              => addAbilityBuffOrPenalty(Some(ability), roll, against, by, till, name, sender)
    case AffectedSuccesfully(by, name)                                      => affectedSuccesfully(by, name, sender)
    case TargetResisted(name)                                               => targetResisted(name, sender)
    case Reskill(newSkills)                                                 => skillset = newSkills.foldLeft(skillset) { (r, c) => r.assign(c._1, c._2) }
    case SetWounds(value)                                                   => wounds = value
    case SetFatePoints(value)                                               => fate_points_remaining = value
    case AddFatePoints(value)                                               => adjustFatePoints(value)
    case StunnedConditionStart(duration)                                    => stunnedStart(duration)
    case StunnedConditionEnd()                                              => stunnedEnd()
    case StatusRequest(requester, successes)                                => requester ! StatusResponse(self, statusForOthers)
    case StatusResponse(creature, status)                                   => log(LoggedEvent(StatusOfOtherEvent(creature, status)))
    case GetControlState()                                                  => sender ! controlState
    case GetFatePoints()                                                    => sender ! fate_points_remaining
    case GetIntercepting()                                                  => sender ! intercepting
    case GetConsumedItem(item)                                              => sender ! consumedItem(item)
    case GetInventoryItem(ref) if ref.toInt < inventory.size                => sender ! inventory(ref.toInt)
    case GetCurrentLocation()                                               => sender ! Option(currentLocation)
    case GetCurrentLocationWithDescription()                                => sender ! Option(currentLocation, location)
    case GetObservedLocation(ref)                                           => sender ! observedCache.get(ref)
    case GetSpottedTargetAsTarget(ref)                                      => sender ! stringRef2Target(ref)
    case GetSpottedTarget(ref)                                              => sender ! spottedCache.get(ref)
    case GetSpottedTargets(refs) if (refs != None)                          => sender ! Some(spottedCache.filter(refs.get contains _._1).map(_._2).toSeq)
    case GetSpottedTargets(refs)                                            => sender ! Some(spottedCache.map(_._2).toSeq)
    case GetSpottedCreature(creature)                                       => sender ! spottedCache.map(_ swap).get(creature)
    case GetFullSkillModifier(skill)                                        => sender ! Some(fullSkillModifer(skill))
    case GetSkillPassive(skill)                                             => sender ! Some(passive(skillset.skill(skill)))
    case GetMaxSkillPassive(skills)                                         => sender ! Some(skills.foldLeft(0) { (max, skill) => Math.max(max, passive(skillset.skill(skill))) })
    case Roll(skill)                                                        => sender ! Some(roll(skillset.skill(skill)))
    case RollAndGetWithName(skill)                                          => sender ! (skill, (roll(skillset.skill(skill))))
    case x: MovedOutOfYourArea                                              =>
    case x: UsedInYourArea                                                  =>
    case x: SpottedTargetGotAttacked                                        =>
    case MonsterStateChanged(monster, state)                                => log(LoggedEvent(MonsterStateChangedEvent(monster, state)))
    case AddToInventory(item)                                               => receiveItem(item)
    case ClearInventory()                                                   => this.inventory.clear()
    case InterceptedBy(creature)                                            => { interceptors += creature; log(LoggedEvent(ProtectedByCreatureEvent(creature))) }
    case Intercepting(creature)                                             => intercepting = Some(creature)
    case NoLongerInterceptedBy(creature)                                    => interceptors -= creature
    case NoLongerIntercepting()                                             => { intercepting = None; log(LoggedEvent(NoLongerProtectingYourTargetEvent)) }
    case AttackInterceptedBy(attacker, interceptor)                         => logAttackInterceptedBy(attacker, interceptor)
    case AttackInterceptedFor(attacker, target)                             => logAttackInterceptedFor(attacker, target)
    case AttackIntercepted(target, interceptor)                             => logAttackIntercepted(target, interceptor)
    case StartMonitoring()                                                  => { viewers += sender; system.scheduler.scheduleOnce(10 minutes, self, StopMonitoring()) }
    case StopMonitoring()                                                   => viewers -= sender
    case FeedbackBuffer.Feedback(msg)                                       => log(msg)
    case other                                                              => println(self.path.name + " received unhandled message: " + other)
  }

  def dead: Receive = {
    case _ if (sender != self) => //sender ! Dead(name)
    case _                     =>
    // TODO spotted if dead special handling maybe only for monsters
  }
}

trait CCommand
trait ACCommand extends CCommand

object Creature {
  def props(creature_type: String, skillset: Skillset) = Props(new Creature(creature_type, skillset))

  case class CanMeleeAttackFrom(attacker: ActorRef, location: ActorRef, attackType: AttackType) extends NotInfluenceReceiveTimeout
  case class MeleeAttackPossible(creature: ActorRef, attackType: AttackType) extends NotInfluenceReceiveTimeout
  case class RangedAttackPossible(creature: ActorRef) extends NotInfluenceReceiveTimeout
  case class MeleeAttackNotPossible(creature: ActorRef, attackType: AttackType) extends NotInfluenceReceiveTimeout
  case class AttackInterceptedBy(attacker: ActorRef, interceptor: ActorRef) extends NotInfluenceReceiveTimeout
  case class AttackInterceptedFor(attacker: ActorRef, target: ActorRef) extends NotInfluenceReceiveTimeout
  case class AttackIntercepted(target: ActorRef, interceptor: ActorRef) extends NotInfluenceReceiveTimeout
  case class Attacked(attackRoll: Int, location: ActorRef, attackType: AttackType) extends NotInfluenceReceiveTimeout
  case class AttackedNoIntecept(attackRoll: Int, location: ActorRef, attackType: AttackType) extends NotInfluenceReceiveTimeout
  case class AttackSucceeded(target: ActorRef, damage: Int) extends NotInfluenceReceiveTimeout
  case class AttackFailed(target: ActorRef) extends NotInfluenceReceiveTimeout
  case class Killed(creature: ActorRef, level: Int) extends NotInfluenceReceiveTimeout
  case class Moved(location: ActorRef, descriptors: Descriptor) extends NotInfluenceReceiveTimeout
  case class MoveFailed() extends NotInfluenceReceiveTimeout
  case class MovedInYourArea(creature: ActorRef) extends NotInfluenceReceiveTimeout
  case class UsedInYourArea(creature: ActorRef) extends NotInfluenceReceiveTimeout
  case class MovedOutOfYourArea(creature: ActorRef) extends NotInfluenceReceiveTimeout
  case class SpottedTargetGotAttacked(creature: ActorRef, by: ActorRef, damage: Int) extends NotInfluenceReceiveTimeout
  case class Observed(location: ActorRef, linkToLocation: ActorRef, quickDescription: Descriptor, feature: Option[(Feature, Boolean)], dcToMoveTo: Int) extends NotInfluenceReceiveTimeout
  case class FoundLinkItem(locationTo: ActorRef, description: Descriptor, disableSkill: SkillName, disableDC: Int, evadeSkills: scala.Predef.Map[SkillName, Int], creator: Option[ActorRef]) extends NotInfluenceReceiveTimeout
  case class NoLongerSpotted() extends NotInfluenceReceiveTimeout
  case class Spotted(creature: ActorRef, location: ActorRef, description: String, strength: Option[Int], status: Option[String]) extends NotInfluenceReceiveTimeout
  case class SpottedBy(creature: ActorRef, spotRoll: Int, inLocation: ActorRef) extends NotInfluenceReceiveTimeout
  case class NoLongerSpotting(creature: ActorRef, location: ActorRef) extends NotInfluenceReceiveTimeout
  case class Hide(location: ActorRef, hideRoll: Int) extends NotInfluenceReceiveTimeout
  case class MovedTo(location: ActorRef, spotDifficulty: Int) extends NotInfluenceReceiveTimeout
  case class Position(area: ActorRef) extends NotInfluenceReceiveTimeout
  case class Heal(value: Int) extends NotInfluenceReceiveTimeout
  case class Discovered(discovery: String) extends NotInfluenceReceiveTimeout
  case class TestPresence(willPower: Int, location: ActorRef, attackType: AttackType) extends NotInfluenceReceiveTimeout
  case class TestWillpowerForLocation() extends NotInfluenceReceiveTimeout
  case class Presence(successes: Int, location: ActorRef, attackType: AttackType) extends NotInfluenceReceiveTimeout
  case class StealAttempt(roll: Int, fromLocation: ActorRef) extends NotInfluenceReceiveTimeout
  case class Stolen(item: Object) extends NotInfluenceReceiveTimeout
  case class BefriendAttempt(roll: Int, fromLocation: ActorRef) extends NotInfluenceReceiveTimeout
  case class Befriended(name: String) extends NotInfluenceReceiveTimeout
  case class BefriendedBy(name: String) extends NotInfluenceReceiveTimeout
  case class SufferedEnvironmentDamageBy(dmg: Int, reason: AttackType, by: Option[ActorRef], evadeSkills: scala.Predef.Map[SkillName, Int]) extends NotInfluenceReceiveTimeout
  case class Message(event: LoggedEvent) extends NotInfluenceReceiveTimeout
  case class Dead(name: String, by: ActorRef) extends NotInfluenceReceiveTimeout
  case class Reskill(newSkills: Seq[(SkillName, Int)])
  case class Follow(currentLocation: ActorRef)
  case class FollowSuccess()
  case class FolloweeMovedTo(location: ActorRef)
  case class FolloweeTriedToHide(hideRoll: Int)
  case class FolloweeLost()
  case class NoLongerFollowed()
  case class InterceptedBy(creature: ActorRef)
  case class Intercepting(creature: ActorRef)
  case class NoLongerInterceptedBy(creature: ActorRef)
  case class NoLongerIntercepting()
  case class MeleeAttackCommand(creature: String) extends ACCommand
  case class RangedAttackCommand(creature: String) extends ACCommand
  case class FlankAttackCommand(creature: String) extends ACCommand
  case class HideCommand() extends ACCommand
  case class SpotCommand() extends ACCommand
  case class SearchCommand() extends ACCommand
  case class DisableCommand(item: String) extends ACCommand
  case class MoveCommand(location: String) extends ACCommand
  case class StealCommand(creature: String) extends ACCommand
  case class SneakCommand(location: String) extends ACCommand
  case class FollowCommand(creature: String) extends ACCommand
  case class ProtectStartCommand(creature: String) extends ACCommand
  case class ProtectStopCommand() extends ACCommand
  case class StopFollowingCommand() extends ACCommand
  case class StatusCommand() extends NotInfluenceReceiveTimeout with CCommand
  case class RegainControl() extends NotInfluenceReceiveTimeout
  case class SetWounds(value: Int) extends NotInfluenceReceiveTimeout
  case class SetFatePoints(value: Int) extends NotInfluenceReceiveTimeout
  case class AddFatePoints(value: Int) extends NotInfluenceReceiveTimeout
  case class AddToInventory(item: Object) extends NotInfluenceReceiveTimeout
  case class ClearInventory() extends NotInfluenceReceiveTimeout
  case class GetFatePoints() extends NotInfluenceReceiveTimeout
  case class GetControlState() extends NotInfluenceReceiveTimeout
  case class GetConsumedItem(item: Object) extends NotInfluenceReceiveTimeout
  case class GetCurrentLocation() extends NotInfluenceReceiveTimeout
  case class GetCurrentLocationWithDescription() extends NotInfluenceReceiveTimeout
  case class GetObservedLocation(ref: String) extends NotInfluenceReceiveTimeout
  case class GetIntercepting() extends NotInfluenceReceiveTimeout
  case class GetInventoryItem(ref: String) extends NotInfluenceReceiveTimeout
  case class GetSpottedTargetAsTarget(ref: String) extends NotInfluenceReceiveTimeout
  case class GetSpottedTarget(ref: String) extends NotInfluenceReceiveTimeout
  case class GetSpottedTargets(refs: Option[Seq[String]]) extends NotInfluenceReceiveTimeout
  case class GetSpottedCreature(ref: ActorRef) extends NotInfluenceReceiveTimeout
  case class GetFullSkillModifier(skill: SkillName) extends NotInfluenceReceiveTimeout
  case class GetSkillPassive(skill: SkillName) extends NotInfluenceReceiveTimeout
  case class GetMaxSkillPassive(skills: Seq[SkillName]) extends NotInfluenceReceiveTimeout
  case class Roll(skill: SkillName) extends NotInfluenceReceiveTimeout
  case class RollAndGetWithName(skill: SkillName) extends NotInfluenceReceiveTimeout
  case class StunnedConditionStart(duration: FiniteDuration) extends NotInfluenceReceiveTimeout
  case class StunnedConditionEnd() extends NotInfluenceReceiveTimeout
  case class MonsterStateChanged(monster: ActorRef, state: String) extends NotInfluenceReceiveTimeout
  case class StartMonitoring() extends NotInfluenceReceiveTimeout
  case class StopMonitoring() extends NotInfluenceReceiveTimeout
  case class ForceTakeControl(onDeath: ActorRef => Unit) extends NotInfluenceReceiveTimeout
  case class TakeControlFor(duration: FiniteDuration, onEnd: ActorRef => Unit, onDeath: ActorRef => Unit) extends NotInfluenceReceiveTimeout
  case class DropControl() extends NotInfluenceReceiveTimeout
  case class AffectSkill(skill: SkillName, roll: Int, against: Seq[SkillName], by: Int, till: Deadline, name: String) extends NotInfluenceReceiveTimeout
  case class AffectAllSkills(roll: Int, against: Seq[SkillName], by: Int, till: Deadline, name: String) extends NotInfluenceReceiveTimeout
  case class AffectAbility(ability: String, roll: Int, against: Seq[SkillName], by: Int => Int, till: Deadline, name: String) extends NotInfluenceReceiveTimeout
  case class AffectedSuccesfully(by: Int, effect: String) extends NotInfluenceReceiveTimeout
  case class TargetResisted(effect: String) extends NotInfluenceReceiveTimeout
  case class StatusRequest(requester: ActorRef, successes: Int) extends NotInfluenceReceiveTimeout
  case class StatusResponse(creature: ActorRef, status: StatusOfEvent) extends NotInfluenceReceiveTimeout

  def is(actor: ActorRef) = actor.path.elements.toSet.contains(CreatureGenerator.NAME)
}