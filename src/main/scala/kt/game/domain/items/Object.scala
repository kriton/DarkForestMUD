package kt.game.domain.items

import kt.game.domain.creatures.skills.Skillset

sealed abstract class Object(val name: String, val description: String, val rarity: Rarity) extends Serializable {
  AllObjects.map += name -> this
}

sealed trait Slot
case object Weapon extends Slot
case object Suit extends Slot
case object Trinket extends Slot

sealed trait Rarity
case object Exotic extends Rarity
case object Rare extends Rarity
case object Common extends Rarity

sealed abstract class Item(override val name: String, slot: Slot, bonuses: Skillset, override val description: String, rarity: Rarity) extends Object(name, description, rarity) {
  final override def toString = name + " (" + rarity + ") [" + slot + "] grants " + bonuses.toNonZerosString + "."
}

case class WeaponItem private[items] (override val name: String, bonuses: Skillset, override val description: String, override val rarity: Rarity) extends Item(name, Weapon, bonuses, description, rarity)
case class SuitItem private[items] (override val name: String, bonuses: Skillset, override val description: String, override val rarity: Rarity) extends Item(name, Suit, bonuses, description, rarity)
case class TrinketItem private[items] (override val name: String, bonuses: Skillset, override val description: String, override val rarity: Rarity) extends Item(name, Trinket, bonuses, description, rarity)

abstract class Ingridient private[items] (override val name: String, override val rarity: Rarity) extends Object(name, name, rarity) {
  override def toString = name + " (" + rarity + ")"
}

case class RawIngridient private[items] (override val name: String, override val rarity: Rarity) extends Ingridient(name, rarity)
case class CraftedIngridient private[items] (override val name: String, override val rarity: Rarity) extends Ingridient(name, rarity)

object AllObjects {
  val map: scala.collection.mutable.Map[String, Object] = scala.collection.mutable.Map()
}

object RawMaterials {
  val greenwood = RawIngridient("greenwood", Common)
  val ashwood = RawIngridient("ashwood", Rare)
  val skin = RawIngridient("skin", Common)
  val psiCrystal = RawIngridient("psi crystal", Exotic)
  val ashLeaves = RawIngridient("ash leaves", Rare)
  val gold = RawIngridient("gold", Rare)
  val iron = RawIngridient("iron", Common)
  val mithral = RawIngridient("mithral", Rare)
  val coal = RawIngridient("coal", Common)
  val amber = RawIngridient("amber", Common)
  val jade = RawIngridient("jade", Common)
  val cloth = RawIngridient("cloth", Common)
  val bones = RawIngridient("bones", Common)
  val ruby = RawIngridient("ruby", Rare)
}

object CraftedMaterials {
  val leather = CraftedIngridient("leather", Common)
  val steel = CraftedIngridient("steel", Common)
  val elderCrystal = CraftedIngridient("elder crystal", Exotic)
  val ironwood = CraftedIngridient("ironwood", Rare)
  val lightStone = CraftedIngridient("light stone", Exotic)
  val ashLeather = CraftedIngridient("ash leather", Rare)
}

object Weapons {
  val artisanHammer = WeaponItem("Artisan's Hammer", Skillset.fromBonus(0, 0, 3, 0, 0, 0, 0, 0), "A finely crafted hammer", Common)
  val greatSword = WeaponItem("Great Sword", Skillset.fromBonus(0, 0, 0, 1, 0, 0, 2, 0), "A huge sword", Common)
  val druidStaff = WeaponItem("Druid's Staff", Skillset.fromBonus(1, 0, 1, 1, 1, 1, 0, 1), "A staff covered with leaves with a bright green stone on top", Rare)
  val magicWand = WeaponItem("Magic Wand", Skillset.fromBonus(0, 2, 2, 0, 2, 0, 0, 0), "A small wand decorated with a variety of jewels and runes", Rare)
  val quarterStaff = WeaponItem("Staff", Skillset.fromBonus(1, 0, 0, 0, 0, 0, 1, 0), "A simple walking staff", Common)
  val magicSword = WeaponItem("Magic Great Sword", Skillset.fromBonus(0, 0, 0, 1, 1, 0, 4, 0), "A huge sword that glows", Rare)
  val handCannon = WeaponItem("Hand Cannon", Skillset.fromBonus(0, 8, 0, 2, 0, 0, 0, 0), "A huge handheld cannon", Exotic)
  val pistolPair = WeaponItem("Pair of Pistols", Skillset.fromBonus(0, 4, 0, 1, 0, 0, 0, 0), "A pair of finely crafted pistols", Rare)
  val greatCrossbow = WeaponItem("Great Croosbow", Skillset.fromBonus(0, 3, 0, 0, 0, 0, 0, 0), "A big crossbow", Common)
  val wizardStaff = WeaponItem("Wizard's Staff", Skillset.fromBonus(0, 4, 4, 4, 0, 0, 2, 2), "A glowing staff with a number of magic stones orbitting around it", Exotic)
  val daggers = WeaponItem("Several Daggers", Skillset.fromBonus(0, 1, 0, 0, 0, 0, 1, 0), "Several sharp daggers", Common)
  val towerShield = WeaponItem("Tower Shield", Skillset.fromBonus(0, 0, 0, 0, 0, 2, 4, 0), "A huge and heavy shield that takes the shape of the forest", Rare)
  val Katana = WeaponItem("Katana", Skillset.fromBonus(0, 0, 0, 1, 0, 0, 5, 0), "A very sharp katana-shaped sword", Rare)
  val magicLute = WeaponItem("Magic Lute", Skillset.fromBonus(0, 0, 0, 5, 0, 0, 0, 0), "A lute that looks as amazing as it sounds", Rare)
}

object Suits {
  val chainmail = SuitItem("Chainmail", Skillset.fromBonus(0, 0, 0, 2, 0, 0, 1, 0), "A heavy suit of armor made made of steel rings", Common)
  val magicCape = SuitItem("Magic cape", Skillset.fromBonus(0, 0, 0, 0, 0, 5, 0, 0), "A cloak that blends in the forest", Rare)
  val wardenArmor = SuitItem("Warden's Armor", Skillset.fromBonus(3, 0, 0, 0, 2, 0, 0, 2), "A suit of armor made of wood and leaves", Rare)
  val phantomSuit = SuitItem("Phantom Suit", Skillset.fromBonus(2, 0, 0, 0, 2, 5, 0, 3), "A tight dark leather suit", Exotic)
  val crystalSkin = SuitItem("Crysal Skin", Skillset.fromBonus(0, 0, 0, 4, 4, 0, 0, 4), "A suit of armor made entirely out of crystal", Exotic)
  val thornMail = SuitItem("Thorn Mail", Skillset.fromBonus(0, 0, 0, 3, 0, 0, 5, 0), "A fiendish chainmail covered with bone horns all around", Rare)
}

object Trinkets {
  val goblinSpyglass = TrinketItem("Goblin Spyglass", Skillset.fromBonus(0, 0, 0, 0, 3, 0, 0, 0), "A crude but effective spyglass", Common)
  val elvenBoots = TrinketItem("Elven Boots", Skillset.fromBonus(2, 0, 0, 0, 0, 3, 0, 0), "A pair of fine leather boots covered with leaves", Rare)
  val boneMask = TrinketItem("Bone Mask", Skillset.fromBonus(0, 0, 0, 2, 2, 0, 2, 2), "A scary mask made of sharp black bones", Rare)
  val giantsGloves = TrinketItem("Giant's Gloves", Skillset.fromBonus(3, 0, 0, 0, 0, 0, 7, 0), "A pair of huge gloves", Exotic)
  val magicRing = TrinketItem("Magic Ring", Skillset.fromBonus(0, 0, 5, 0, 0, 0, 0, 0), "A simple glowing ring", Rare)
  val orbitStone = TrinketItem("Orbit Stone", Skillset.fromBonus(2, 2, 2, 2, 2, 2, 2, 2), "A very bright stone hovering around", Exotic)
  val demonEyes = TrinketItem("Demon Eyes", Skillset.fromBonus(0, 3, 0, 2, 6, 0, 0, 2), "A pair of red bleeding eyes", Rare)
  val angelWings = TrinketItem("Angel's Wings", Skillset.fromBonus(8, 0, 0, 2, 0, 0, 0, 2), "A pair of feather wings", Exotic)
  val flyingBroomstick = TrinketItem("Flying Broomstick", Skillset.fromBonus(5, 0, 0, 0, 0, 0, 0, 0), "A broomstick", Rare)
  val fearBanner = TrinketItem("Fearsome Banner", Skillset.fromBonus(0, 0, 0, 4, 0, 0, 0, 2), "A fearsome looking banner", Rare)
  val crystalMask = TrinketItem("Crystal Mask", Skillset.fromBonus(0, 0, 4, 3, 0, 0, 0, 7), "A beautiful mask made of blue crystal", Exotic)
}

object ObjectTesting {
  val testWeapon = WeaponItem("Excaliblur", Skillset.fromBonus(0, 0, 4, 0, 0, 4, 20, 4), "The most powerful weapon evah!", Exotic)
  val testWeapon2 = WeaponItem("Icecone", Skillset.zero, "A very rare but useless (or not?) weapon!", Exotic)
  val testSuit = SuitItem("Iron Man Suit", Skillset.fromBonus(10, 9, 0, 7, 8, 0, 6, 0), "A super cool futuristic red suit made by 'StarX Industries'!", Exotic)
  val testTrinket = TrinketItem("The One Ring", Skillset.fromBonus(5, 5, 5, 5, 5, 5, 5, 5), "A precious thing!", Exotic)
}