package kt.game.events.talents

import kt.game.events.NonCombatEvent
import akka.actor.ActorRef
import kt.game.events.talents.HealEvents.HealFailureReason._
import kt.game.events.ActionFailedEvent

object HealEvents {

  case object HealSucceededEvent extends NonCombatEvent {
    override def toString = "You fully heal your target"
  }

  case class HealFailedEvent(reason: HealFailureReason) extends NonCombatEvent with ActionFailedEvent {
    override def toString = reason match {
      case TooFarAway          => "Target creature is too far away to heal!"
      case NotEnoughFatePoints => "You need 2 Fate points to heal!"
      case CheckFailure        => "Your heal attempt failed!"
      case CanNoLongerSpot     => "You can no longet spot wanted creature."
    }
  }

  sealed trait HealFailureReason
  object HealFailureReason {
    case object CanNoLongerSpot extends HealFailureReason
    case object TooFarAway extends HealFailureReason
    case object NotEnoughFatePoints extends HealFailureReason
    case object CheckFailure extends HealFailureReason
  }

}