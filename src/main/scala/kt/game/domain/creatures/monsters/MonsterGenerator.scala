package kt.game.domain.creatures.monsters

import scala.collection.mutable.Set
import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.items.Object

sealed trait MonsterBehaviour
case object Aggressive extends MonsterBehaviour
case object Defensive extends MonsterBehaviour
case object Reflexive extends MonsterBehaviour
case object DefensiveRooter extends MonsterBehaviour
case object DefensiveZerkerRooter extends MonsterBehaviour
case object DefensiveRoamingRooter extends MonsterBehaviour
case object AggressiveStunner extends MonsterBehaviour
case object DefensiveStunner extends MonsterBehaviour
case object ReflexiveStunner extends MonsterBehaviour
case object Roamer extends MonsterBehaviour
case object AggressiveZerker extends MonsterBehaviour
case object DefensiveZerker extends MonsterBehaviour
case object AggressiveZerkerStunner extends MonsterBehaviour
case object DefensiveZerkerStunner extends MonsterBehaviour
case object AggressiveRoamer extends MonsterBehaviour
case object DefensiveRoamer extends MonsterBehaviour
case object ReflexiveRoamer extends MonsterBehaviour
case object AggressiveRoamerStunner extends MonsterBehaviour
case object DefensiveRoamerStunner extends MonsterBehaviour
case object AggressiveRoamingZerker extends MonsterBehaviour
case object AggressiveRoamingZerkerStunner extends MonsterBehaviour
case object Static extends MonsterBehaviour

class MonsterGenerator extends Actor {
  import MonsterGenerator._
  import context._

  val monsters: Set[(ActorRef, String, Skillset, MonsterBehaviour, ActorRef, scala.Predef.Set[Object])] = Set()

  def receive = {
    case ListAll() => monsters.foreach(_._1 ! Monster.ListingRequest(sender))
    case NewMonster(monsterType, skillset, behaviour, initialLocation, inventory) => {
      val monsterId = "%05d".format(monsters.size) + "_" + monsterType.replace(" ", "_")
      val monster = behaviour match {
        case Aggressive                     => context.actorOf(Monster.propsWithAggressive(monsterType, skillset), monsterId)
        case Defensive                      => context.actorOf(Monster.propsWithDefensive(monsterType, skillset), monsterId)
        case Reflexive                      => context.actorOf(Monster.propsWithReflexive(monsterType, skillset), monsterId)
        case DefensiveRooter                => context.actorOf(Monster.propsWithDefensiveRooter(monsterType, skillset), monsterId)
        case DefensiveZerkerRooter          => context.actorOf(Monster.propsWithDefensiveZerkerRooter(monsterType, skillset), monsterId)
        case DefensiveRoamingRooter         => context.actorOf(Monster.propsWithDefensiveRoamingRooter(monsterType, skillset), monsterId)
        case AggressiveStunner              => context.actorOf(Monster.propsWithAggressiveStunner(monsterType, skillset), monsterId)
        case DefensiveStunner               => context.actorOf(Monster.propsWithDefensiveStunner(monsterType, skillset), monsterId)
        case ReflexiveStunner               => context.actorOf(Monster.propsWithReflexiveStunner(monsterType, skillset), monsterId)
        case Roamer                         => context.actorOf(Monster.propsWithRoamer(monsterType, skillset), monsterId)
        case AggressiveZerker               => context.actorOf(Monster.propsWithAggressiveZerker(monsterType, skillset), monsterId)
        case DefensiveZerker                => context.actorOf(Monster.propsWithDefensiveZerker(monsterType, skillset), monsterId)
        case AggressiveZerkerStunner        => context.actorOf(Monster.propsWithAggressiveZerkerStunner(monsterType, skillset), monsterId)
        case DefensiveZerkerStunner         => context.actorOf(Monster.propsWithDefensiveZerkerStunner(monsterType, skillset), monsterId)
        case AggressiveRoamer               => context.actorOf(Monster.propsWithAggressiveRoamer(monsterType, skillset), monsterId)
        case DefensiveRoamer                => context.actorOf(Monster.propsWithDefensiveRoamer(monsterType, skillset), monsterId)
        case ReflexiveRoamer                => context.actorOf(Monster.propsWithReflexiveRoamer(monsterType, skillset), monsterId)
        case AggressiveRoamerStunner        => context.actorOf(Monster.propsWithAggressiveRoamerStunner(monsterType, skillset), monsterId)
        case DefensiveRoamerStunner         => context.actorOf(Monster.propsWithDefensiveRoamerStunner(monsterType, skillset), monsterId)
        case AggressiveRoamingZerker        => context.actorOf(Monster.propsWithAggressiveRoamingZerker(monsterType, skillset), monsterId)
        case AggressiveRoamingZerkerStunner => context.actorOf(Monster.propsWithAggressiveRoamingZerkerStunner(monsterType, skillset), monsterId)
        case Static                         => context.actorOf(Monster.props(monsterType, skillset), monsterId)
      }
      monsters += Tuple6(monster, monsterType, skillset, behaviour, initialLocation, inventory)
      monster ! Creature.Position(initialLocation)
      monster ! Monster.Inventory(inventory)
      system.scheduler.scheduleOnce(3 seconds, monster, Monster.StartSchedules())
    }
  }
}

object MonsterGenerator {
  val NAME = "monsters"
  def props = Props(new MonsterGenerator())

  case class NewMonster(monsterType: String, skillset: Skillset, behaviour: MonsterBehaviour, initialLocation: ActorRef, inventory: scala.Predef.Set[Object])
  case class ListAll()
}