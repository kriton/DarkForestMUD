package kt.game.requests.talents

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import scala.concurrent.duration.DurationInt
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Await
import kt.game.domain.creatures.Creature
import akka.actor.ReceiveTimeout
import kt.game.domain.creatures.characters.talents.Daze
import akka.actor.PoisonPill
import kt.game.domain.creatures.skills.SkillName
import scala.concurrent.duration.FiniteDuration
import kt.game.domain.creatures.AttackType
import scala.concurrent.Future
import kt.game.events.MultiAttackImpossibleEvent
import kt.game.events.AttackImpossibleReason

class DazeRequestHandler private (character: ActorRef, targets: Option[Seq[String]], duration: FiniteDuration) extends Actor {
  import DazeRequestHandler._
  import context._

  setReceiveTimeout(300 milliseconds)

  implicit val timeout = Timeout(120 milliseconds)
  val creatures = Await.result(character ? Creature.GetSpottedTargets(targets), timeout.duration).asInstanceOf[Option[Seq[ActorRef]]]

  if (creatures != None) {
    val roll = Await.result(character ? Creature.Roll(SkillName.WILL), timeout.duration).asInstanceOf[Option[Int]].get
    creatures.get.foreach { creature =>
      creature.tell(Creature.AffectAbility("INITIATIVE", roll, Seq(SkillName.WILL, SkillName.ATHL), Daze.initiativeReduction, duration.fromNow, Daze.effectName), character)
    }
  } else {
    parent ! Daze.DazeFailed()
    character ! Creature.Message(MultiAttackImpossibleEvent(AttackType.NonAttack, AttackImpossibleReason.CanNoLongerSpot))
  }

  def receive = {
    case ReceiveTimeout => self ! PoisonPill
  }
}

object DazeRequestHandler {
  def props(character: ActorRef, targets: Option[Seq[String]], duration: FiniteDuration) = Props(new DazeRequestHandler(character, targets, duration))
}