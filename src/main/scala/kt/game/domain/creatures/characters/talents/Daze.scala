package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.Weapons
import kt.game.events.ActionFailedEvent
import kt.game.requests.talents.DazeRequestHandler
import kt.game.domain.creatures.skills.Influence
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Willpower
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.ActionFailedEvent

class Daze private (val character: ActorRef) extends Actor with Talent {
  import Daze._
  import context._

  val name = Daze.name
  val description = Daze.description
  val help = Daze.commandsHelp

  implicit val timeout = Timeout(200 milliseconds)

  def setCD() = character ! Character.SetActionCooldown(action, s => ((150 - s.willpower.bonus * 3).max(60) seconds).fromNow)

  var skillModifier: Option[Int] = None

  def DazeAttempt(targets: Option[Seq[String]]) {
    if (skillModifier == None) skillModifier = Await.result((character ? Creature.GetFullSkillModifier(SkillName.WILL)).mapTo[Option[Int]], timeout.duration)
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      setCD()
      context.actorOf(DazeRequestHandler.props(character, targets, (15 + 3 * skillModifier.get) seconds))
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentDazeCommandToAll() => DazeAttempt(None)
    case TalentDazeCommand(refs)  => DazeAttempt(Some(refs))
    case DazeFailed()             => character ! Character.SetActionCooldown(action, s => Deadline.now)
    case DazeSuccess()            => setCD()
  }
}

object Daze extends TalentContext {
  def props(character: ActorRef) = Props(new Daze(character))

  case class TalentDazeCommand(refs: Seq[String])
  case class TalentDazeCommandToAll()
  case class DazeSuccess()
  case class DazeFailed()

  val skill = Willpower(12)

  val effectName = "dazed"
  def initiativeReduction(success: Int) = -100 - success * 25

  val action = TalentAction.Daze
  val name = action.toString

  val description = "You can daze creatures, greatly penalizing their initiative. The frequency that you can do so, depends on your willpower skill."
  val trainingDescription = "You can train your arcane skills and get access to the secrets of dazing your foes! " + requirementsString

  val TCP_dazeAll = "^daze$".r
  val TCP_dazeSelected = "^daze (\\d+(?: \\d+)*)$".r

  val commandsHelp =
    ("%-32s" format "Daze <target1> <target2> etc") + " -->  Daze target creature(s), penalizing their initiative.\r\n" +
      ("%-32s" format "Daze") + " -->  Daze each spotted target, penalizing their initiative.\r\n"
}