package kt.game.requests

import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.Character
import kt.game.events.ChatEvent
import kt.game.events.LoggedEvent
import kt.game.player.Player
import kt.game.system.FeedbackBuffer
import kt.game.events.ChatEvents

class ReplyRequestHandler(val player: ActorRef, val text: String) extends Actor {
  import Messages._

  context.setReceiveTimeout(100 milliseconds)
  player ! Character.GetName(self)
  player ! Player.GetLastWhisperer()

  var fromName: String = null
  var to: ActorRef = null

  def receive = {
    case NameOfPlayer(name, from) if (from == player)       => fromName = name
    case Player.LastWhisperer(whisperer)                    => { to = whisperer; to ! Character.GetName(self) }
    case NameOfPlayer(name, whisperer) if (whisperer == to) => { this.player ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.DirectedChatAcknowledgementEvent(fromName, name))) }
    case ReceiveTimeout if to != null                       => { to ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.DirectedChatEvent(fromName, "-", text))); to ! Player.LastWhisperer(player); self ! PoisonPill }
    case ReceiveTimeout                                     => { this.player ! FeedbackBuffer.Feedback(LoggedEvent(ChatEvents.ReplyImpossibleEvent)); self ! PoisonPill }
    case other                                              =>
  }
}

object ReplyRequestHandler {
  def props(player: ActorRef, text: String) = Props(new ReplyRequestHandler(player, text))
}