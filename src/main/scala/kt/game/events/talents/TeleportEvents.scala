package kt.game.events.talents

import kt.game.events.NonCombatEvent
import akka.actor.ActorRef
import kt.game.events.talents.TeleportEvents.FailureReason.InvalidLocation
import kt.game.events.talents.TeleportEvents.FailureReason.CheckFailure
import kt.game.events.talents.TeleportEvents.FailureReason.NoAnchor
import kt.game.domain.creatures.RelativeLocation
import kt.game.events.ActionFailedEvent

object TeleportEvents {

  case class AnchorSetEvent(minutesLeft: Long) extends NonCombatEvent {
    override def toString = s"You succesfully set up an anchor in your current location. It will expire in $minutesLeft minutes."
  }

  case object AnchorFailedEvent extends NonCombatEvent with ActionFailedEvent {
    override def toString = "You failed to set up an anchor to your current location. Any previous anchor is also lost."
  }

  case class TeleportationSucceededEvent(location: ActorRef) extends NonCombatEvent {
    override def toString = "You succesfully teleport yourself!"
  }

  case class TeleportationFailedEvent(reason: FailureReason, location: Option[ActorRef]) extends NonCombatEvent with ActionFailedEvent {
    override def toString = reason match {
      case InvalidLocation => "You selected an invalid location to teleport to."
      case CheckFailure    => "You failed to teleport to your destination!"
      case NoAnchor        => "You have no anchor in place!"
    }
  }

  case class AnchorStatusEvent(location: RelativeLocation, timeLeft: Long) extends NonCombatEvent {
    override def toString = s"""You have an anchor in "${location.description.map { _.toString }.getOrElse("")}" for the next $timeLeft minutes."""
  }

  case object NoAnchorStatusEvent extends NonCombatEvent {
    override def toString = "You have no anchor in place!"
  }

  sealed trait FailureReason
  object FailureReason {
    case object InvalidLocation extends FailureReason
    case object CheckFailure extends FailureReason
    case object NoAnchor extends FailureReason
  }

}