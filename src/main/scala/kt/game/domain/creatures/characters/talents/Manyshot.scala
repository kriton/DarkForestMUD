package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.Ballistics
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import kt.game.events.NonCombatEvent
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.events.CombatEvent
import kt.game.domain.creatures.Target
import kt.game.events.AttemptAttackEvent
import kt.game.domain.creatures.AttackType
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.ActionFailedEvent
import kt.game.events.AttackImpossibleEvent
import kt.game.domain.creatures.NoneTarget
import kt.game.events.AttackImpossibleReason

class Manyshot private (val character: ActorRef) extends Actor with Talent {
  import Manyshot._
  import context._

  val name = Manyshot.name
  val description = Manyshot.description
  val help = Manyshot.commandsHelp

  def setCD() = character ! Character.SetActionCooldown(action, s => ((90 - s.ballistics.bonus * 3).max(30) seconds).fromNow)

  implicit val timeout = Timeout(200 milliseconds)

  def manyshotAttempt(target: String) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      val creature = Await.result(character ? Creature.GetSpottedTargetAsTarget(target), timeout.duration).asInstanceOf[Target]
      if (creature.ref != None) {
        setCD()
        character ! Creature.Message(AttemptAttackEvent(AttackType.SpecialRanged("manyshot"), creature))
        for (i <- 0 until 3) character ! Creature.RangedAttackPossible(creature.ref.get)
      } else {
        character ! Creature.Message(AttackImpossibleEvent(AttackType.SpecialRanged("manyshot"), creature, AttackImpossibleReason.CanNoLongerSpot))
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentManyshotCommand(ref) => manyshotAttempt(ref)
  }
}

object Manyshot extends TalentContext {
  def props(character: ActorRef) = Props(new Manyshot(character))

  case class TalentManyshotCommand(ref: String)

  val skill = Ballistics(8)

  val action = TalentAction.Manyshot
  val name = action.toString

  val description = "You can unleash three ranged attacks as a single action against your target. The frequency that you can do so, depends on your ballistics skill."
  val trainingDescription = "You can train your arcane skills and learn the secret of arcane manyshot attacks! " + requirementsString

  val TCP_manyshot = "^manyshot (\\d+)$".r

  val commandsHelp =
    ("%-32s" format "manyshot <target>") + " -->  Make three ranged attacks against the selected target.\r\n"
}