package kt.game.domain.creatures

sealed trait AttackType

object AttackType {
  case object Melee extends AttackType { override def toString = "melee attack" }
  case object Ranged extends AttackType { override def toString = "ranged attack" }
  case class SpecialRanged(name: String) extends AttackType { override def toString = s"$name attack" }
  case object Flank extends AttackType { override def toString = "flank attack" }
  case class SpecialAttack(bonus: Int, name: String) extends AttackType { override def toString = s"$name attack" }
  case object NonAttack extends AttackType
  case object Trap extends AttackType
} 