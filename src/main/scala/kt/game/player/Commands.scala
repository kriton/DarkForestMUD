package kt.game.player

import akka.actor.ActorRef
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.Creature
import kt.game.events.ActionFailedEvent
import kt.game.system.FeedbackBuffer
import kt.game.events.LoggedEvent
import kt.game.events.NullEvent
import kt.game.events.StatusEvent

object Commands {

  val helpText = ("%-32s" format "spot") + " -->  Inspect your surroundings.\r\n" +
    ("%-32s" format "move <key>") + " -->  Move to location mapped to provided key.\r\n" +
    ("%-32s" format "use/investigate") + " -->  Interact with your current location.\r\n" +
    ("%-32s" format "attack <key>") + " -->  Make a melee attack against creature mapped to provided key.\r\n" +
    ("%-32s" format "ranged <key>") + " -->  Make a ranged attack against creature mapped to provided key.\r\n" +
    ("%-32s" format "befriend <key>") + " -->  Attempt to befriend a creature mapped to provided key.\r\n" +
    ("%-32s" format "hide") + " -->  Hide from creatures that have spotted you.\r\n" +
    ("%-32s" format "sneak <key>") + " -->  Move to location mapped to provided key while staying hidden (assuming you already are).\r\n" +
    ("%-32s" format "parry <key>") + " -->  Gain a temporary defensive bonus against all attacks by creature mapped to provided key. Bonus diminishes during combat.\r\n" +
    ("%-32s" format "parry all") + " -->  Gain a temporary defensive bonus against all simple melee attacks. Bonus diminishes during combat.\r\n" +
    "\r\n" +
    ("%-32s" format "effort <ammount>") + " -->  Put effort on your skill rolls, to increase your chanes of getting a good result. Do: 'effort 0' to stop that\r\n" +
    ("%-32s" format "effort <ammount> on <skill>") + " -->  Put effort on skill rolls for targetted skill (as provided by its 4 level code, all caps). \r\n" +
    "\r\n" +
    ("%-32s" format "level <skill>") + " -->  Level up the provided skill as provided by its 4 level code (all caps). Type status+ to learn more about skills.\r\n" +
    ("%-32s" format "status(+)") + " -->  Shows your status. If you add the option '+' it will show in more detail.\r\n" +
    ("%-32s" format "inventory") + " -->  Lists all your inventory items.\r\n" +
    ("%-32s" format "recipes") + " -->  Lists all recipes you know.\r\n" +
    ("%-32s" format "talents") + " -->  Lists all talents you know.\r\n" +
    ("%-32s" format "skills") + " -->  Lists all your skills with full details.\r\n" +
    "\r\n" +
    ("%-32s" format "rest") + " -->  Perfrom a quick rest and restore half of your fatigue at the cost of a fate point.\r\n" +
    ("%-32s" format "pick <key>") + " -->  Pick item mapped to provided key.\r\n" +
    ("%-32s" format "drop <key>") + " -->  Drop item mapped to provided key.\r\n" +
    ("%-32s" format "craft <key>") + " -->  Craft item with recipe mapped to provided key.\r\n" +
    ("%-32s" format "equip <key>") + " -->  Equip item from the inventory mapped to appropriate key.\r\n" +
    ("%-32s" format "unequip <slot>") + " -->  Remove equipped item from provided slot and put it in your inventory.\r\n" +
    ("%-32s" format "steal <key>") + " -->  Attempt to steal an item from a creature mapped to provided key.\r\n" +
    ("%-32s" format "teach <key1> to <key2>") + " -->  Attempt to teach the recipe mapped by key1 to character mapped to key2.\r\n" +
    ("%-32s" format "search") + " -->  Search for tombstone sites and traps.\r\n" +
    ("%-32s" format "disable <key>") + " -->  Disable a trap mapped to provided key.\r\n" +
    ("%-32s" format "dig <key>") + " -->  Dig discovered tombstone mapped to provided key.\r\n" +
    ("%-32s" format "follow <key>") + " -->  Follow target mapped by provided key around. Whenever he moves, you will automatically try to move with him.\r\n" +
    ("%-32s" format "stop") + " -->  Stop Follow target around.\r\n" +
    "\r\n" +
    ("%-32s" format "protect <key>") + " -->  Try to intercept all attacks against target creature.\r\n" +
    ("%-32s" format "unprotect") + " -->  Stop protecting a creature.\r\n" +
    ("%-32s" format "flank <key>") + " -->  Make flanking attack against target creature. This attack will gain a bonus but will fail if you are not in a flanking position.\r\n" +
    "\r\n" +
    ("%-32s" format "new <name> <password>") + " -->  Create a new character with provided name and password.\r\n" +
    ("%-32s" format "login <name> <password>") + " -->  Control provided character provided the password matches.\r\n" +
    ("%-32s" format "logout") + " -->  Lose control of your current character. Your character will be removed from the world till you login with him again.\r\n" +
    ("%-32s" format "exit") + " -->  Exit the game.\r\n" +
    "\r\n" +
    ("%-32s" format "all/a <message>") + " -->  Send a message to all logged in player characters.\r\n" +
    ("%-32s" format "shout/s <message>") + " -->  Send a message to all player characters in your location.\r\n" +
    ("%-32s" format "whisper/tell/t/w <player> <msg>") + " -->  Send a message to a specific player character.\r\n" +
    ("%-32s" format "reply/r <msg>") + " -->  Send a message to the last character that whispered to you.\r\n" +
    "\r\n" +
    ("%-32s" format "list <name/time/level>") + " -->  Lists all currently logged in players ordered as given.\r\n" +
    ("%-32s" format "halloffame") + " -->  Lists all retired characters sorted by their ranking.\r\n" +
    ("%-32s" format "retire <password>") + " -->  Retire your character and enter the hall of fame. You must be at least level 15 to do so. Once you do this, you can no longer play with this character"

  implicit class CaseInsensitiveRegex(sc: StringContext) {
    def ci = ("(?i)" + sc.parts.mkString).r
  }

  implicit class ZeroStripper(string: String) {
    def stripZeros = string.stripPrefix("0") match {
      case x if x matches "\\d+" => x
      case _                     => "0"
    }
  }

  def sendCommand(connector: ActorRef, player: ActorRef, command: String) = command match {
    case ci"help"                       => player.tell(Character.Help(helpText), connector)
    case ci"spot"                       => player.tell(Creature.SpotCommand(), connector)
    case ci"hide"                       => player.tell(Creature.HideCommand(), connector)
    case ci"status"                     => player.tell(Creature.StatusCommand(), connector)
    case ci"status\+"                   => player.tell(Character.DetailedStatusCommand(), connector)
    case ci"status \+"                  => player.tell(Character.DetailedStatusCommand(), connector)
    case ci"inventory"                  => player.tell(Character.InventoryCommand(), connector)
    case ci"recipes"                    => player.tell(Character.ReceipesKnownCommand(), connector)
    case ci"talents"                    => player.tell(Character.TalentsCommand(), connector)
    case ci"skills"                     => player.tell(Character.SkillsCommand(), connector)
    case ci"use"                        => player.tell(Character.UseCommand(), connector)
    case ci"investigate"                => player.tell(Character.UseCommand(), connector)
    case ci"effort (\d+)$a"             => player.tell(Character.StartAllEffortCommand(a.stripZeros), connector)
    case ci"effort (\d+)$a on (.{4})$b" => player.tell(Character.PutEffortCommand(a.stripZeros, b), connector)
    case ci"move (\d+)$a"               => player.tell(Creature.MoveCommand(a.stripZeros), connector)
    case ci"follow (\d+)$a"             => player.tell(Creature.FollowCommand(a.stripZeros), connector)
    case ci"stop"                       => player.tell(Creature.StopFollowingCommand(), connector)
    case ci"pick (\d+)$a"               => player.tell(Character.PickUpCommand(a.stripZeros), connector)
    case ci"drop (\d+)$a"               => player.tell(Character.DropCommand(a), connector)
    case ci"craft (\d+)$a"              => player.tell(Character.CraftCommand(a), connector)
    case ci"dig (\d+)$a"                => player.tell(Character.DigCommand(a.stripZeros), connector)
    case ci"search"                     => player.tell(Creature.SearchCommand(), connector)
    case ci"disable (\d+)$a"            => player.tell(Creature.DisableCommand(a.stripZeros), connector)
    case ci"equip (\d+)$a"              => player.tell(Character.EquipCommand(a), connector)
    case ci"unequip (.+)$a"             => player.tell(Character.TakeOffCommand(a), connector)
    case ci"attack (\d+)$a"             => player.tell(Creature.MeleeAttackCommand(a.stripZeros), connector)
    case ci"flank (\d+)$a"              => player.tell(Creature.FlankAttackCommand(a.stripZeros), connector)
    case ci"ranged (\d+)$a"             => player.tell(Creature.RangedAttackCommand(a.stripZeros), connector)
    case ci"befriend (\d+)$a"           => player.tell(Character.BefriendCommand(a.stripZeros), connector)
    case ci"steal (\d+)$a"              => player.tell(Creature.StealCommand(a.stripZeros), connector)
    case ci"sneak (\d+)$a"              => player.tell(Creature.SneakCommand(a.stripZeros), connector)
    case ci"parry"                      => player.tell(Character.ParryCommand(), connector)
    case ci"parry (\d+)$a"              => player.tell(Character.TargettedParryCommand(a.stripZeros), connector)
    case ci"teach (\d+)$a to (\d+)$b"   => player.tell(Character.TeachCommand(a, b.stripZeros), connector)
    case ci"protect (\d+)$a"            => player.tell(Creature.ProtectStartCommand(a.stripZeros), connector)
    case ci"unprotect"                  => player.tell(Creature.ProtectStopCommand(), connector)
    case ci"rest"                       => player.tell(Character.QuickRestCommand(), connector)
    case _                              => player.tell(Character.PotentialTalentCommand(command trim), connector);
  }
}