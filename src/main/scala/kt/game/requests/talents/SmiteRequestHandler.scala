package kt.game.requests.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.AttackType
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.characters.talents.Smite
import kt.game.events.talents.SmiteEvents
import kt.game.events.talents.SmiteEvents.SmiteFailedEvent
import kt.game.events.ActionFailedReason

class SmiteRequestHandler private (character: ActorRef, target: String) extends Actor {
  import SmiteRequestHandler._
  import context._

  setReceiveTimeout(200 milliseconds)

  implicit val timeout = Timeout(120 milliseconds)
  val cooldownF = character ? Character.GetActionCooldown(Smite.action)
  val creatureF = character ? Creature.GetSpottedTarget(target)
  val locationF = character ? Creature.GetCurrentLocation()
  val fatepoinF = character ? Creature.GetFatePoints()

  val cooldown = Await.result(cooldownF.mapTo[Option[Deadline]], timeout.duration)
  val creature = Await.result(creatureF.mapTo[Option[ActorRef]], timeout.duration)
  val location = Await.result(locationF.mapTo[Option[ActorRef]], timeout.duration)

  if (creature != None && location != None) {
    if (Await.result(fatepoinF.mapTo[Int], timeout.duration) >= Smite.fpCost) creature.get ! Creature.CanMeleeAttackFrom(character, location.get, AttackType.Melee)
    else {
      parent ! Smite.SmiteFailed()
      character ! Creature.Message(SmiteFailedEvent(ActionFailedReason.InvalidState))
    }
  } else {
    parent ! Smite.SmiteFailed()
    character ! Creature.Message(SmiteFailedEvent(ActionFailedReason.CanNoLongerSpot))
  }

  def smite(target: ActorRef) = {
    character ! Creature.MeleeAttackPossible(target, AttackType.SpecialAttack(Smite.attackBonus, "smite"))
    character ! Creature.AddFatePoints(-Smite.fpCost)
    parent ! Smite.SmiteSuccess()
  }

  def receive = {
    case Creature.MeleeAttackPossible(creature, _) => smite(creature)
    case Creature.MeleeAttackNotPossible(_, _)     => { parent ! Smite.SmiteFailed(); character ! Creature.Message(SmiteFailedEvent(ActionFailedReason.NotInSameLocation)) }
    case ReceiveTimeout                            => self ! PoisonPill
  }
}

object SmiteRequestHandler {
  def props(character: ActorRef, target: String) = Props(new SmiteRequestHandler(character, target))
}