package kt.game.requests.gm

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.characters.CharacterGenerator
import kt.game.domain.creatures.characters.LoggedState
import kt.game.system.FeedbackBuffer
import kt.game.events.GMInfoEvent
import kt.game.events.LoggedEvent


class GetCharsRequestHandler private (val characterGenerator: ActorRef, filter: String => Boolean, task: Option[ActorRef => Unit]) extends Actor {
  import GetCharsRequestHandler._
  import context._

  context.setReceiveTimeout(100 milliseconds)

  characterGenerator ! CharacterGenerator.ListAll()

  val listings = ListBuffer[Listing]()

  def receive = {
    case x: Listing @unchecked => listings += x
    case ReceiveTimeout if task == None => {
      parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent(listings.sorted.map(_.toPrettyString).filter(s => filter(s.replaceAll("\r\n", " "))).mkString("\r\n"))))
      self ! PoisonPill
    }
    case ReceiveTimeout => {
      val targetChars = listings.map(e => (e.toPrettyString, e._1)).filter(s => filter(s._1.replaceAll("\r\n", " "))).map(_._2).toSet
      if (targetChars.isEmpty) {
        parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent("No characters match the selection...")))
      } else {
        parent ! FeedbackBuffer.Feedback(LoggedEvent(GMInfoEvent("Command sent to " + targetChars.map(_.path.name).mkString(", ") + ".")))
        targetChars.foreach { task.get }
      }
      self ! PoisonPill
    }
  }
}

object GetCharsRequestHandler {
  def props(characterGenerator: ActorRef, filter: String => Boolean, task: Option[ActorRef => Unit]) = Props(new GetCharsRequestHandler(characterGenerator, filter, task))

  type Listing = (ActorRef, LoggedState, Character.SnapshotV003, Int)

  implicit class ListingString(listing: Listing) {
    def toPrettyString = {
      ("%-11s" format ("[" + listing._2 + "]")) +
        ("%-16s" format listing._3.characterName) + " -> " +
        ("[ " + listing._3.location + " ]. Level: " + listing._4 + ". XP: " + listing._3.xp + ". Fatigue: " + listing._3.fatigue + ". Fate Points: " + listing._3.fatePoints + ". Wounds: " + listing._3.wounds) + ". Skills: " + listing._3.skillset + "\r\n" +
        ("%29s" format " | ") + "Password: " + listing._3.password + ". Weapon: " + listing._3.weaponEquiped + ". Suit: " + listing._3.suitEquiped + ". Trinket: " + listing._3.trinketEquiped + "\r\n" +
        (if (listing._3.inventory.isEmpty) "" else ("%29s" format " | ") + "Inventory: " + listing._3.inventory.mkString(", ") + "\r\n") +
        (if (listing._3.talents.isEmpty) "" else ("%29s" format " | ") + "Talents: " + listing._3.talents.mkString(", ") + "\r\n") +
        (if (listing._3.receipesLearned.isEmpty) "" else ("%29s" format " | ") + "Recipes: " + listing._3.receipesLearned.mkString(", "))

    }
  }

  val orderingByLoggedStatus: Ordering[Listing] = Ordering.by { e => e._2.toString }
  val orderingByName: Ordering[Listing] = Ordering.by { e => e._3.characterName }
  implicit val finalOrdering: Ordering[Listing] = Ordering.by { e: Listing => (e, e) }(Ordering.Tuple2(orderingByLoggedStatus, orderingByName))
}

