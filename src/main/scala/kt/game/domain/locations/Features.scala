package kt.game.domain.locations

import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.FiniteDuration
import scala.util.Random
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.characters.Character
import kt.game.domain.items.Receipe
import kt.game.utility.Dice
import kt.game.events.ActionFailedEvent
import kt.game.events.FeatureUseFailedEvent
import kt.game.events.GameEvent
import kt.game.events.LoggedEvent

sealed trait Feature {
  def messageOnUse(implicit sender: ActorRef): Any = Creature.Message(LoggedEvent(FeatureUseFailedEvent(onError))(sender))
  def dc(): Int = 0
  def frequency(): FiniteDuration = Random.nextInt(300) + 300 seconds
  val onError = "Tried to use something here but failed..."
  val descriptionWhenAvaliable = "There may be something hidden here, or maybe not"
  val descriptionWhenNotAvaliable = descriptionWhenAvaliable
}

object Features {
  case class None() extends Feature {
    override val onError = "Tried to explore the area for anything interesting, but found nothing..."
  }
  case class Discover() extends Feature {
    override def messageOnUse(implicit sender: ActorRef) = Creature.Discovered("an arcane secret!")
    override def dc() = 3 + Dice.d20 + Dice.d20
    override val onError = "Tried to explore the area for anything interesting, but found nothing..."
  }
  case class Learn(DC: Int, receipe: Receipe) extends Feature {
    override def messageOnUse(implicit sender: ActorRef) = Character.Learned(receipe)
    override def dc() = DC
    override val descriptionWhenAvaliable = "There is something to learn here"
    override val descriptionWhenNotAvaliable = "There may have been something here once to learn, but now it looks ruined"
  }
  case class Heal(ammount: Int) extends Feature {
    override def messageOnUse(implicit sender: ActorRef) = Character.UsedHealingFeature(ammount, "You refresh yourself from the magic spring you uncovered. You feel rejuvenated!")
    override def frequency(): FiniteDuration = Random.nextInt(90) + 30 seconds
    override val descriptionWhenAvaliable = "There is a healing spring here"
    override val descriptionWhenNotAvaliable = "There is dried healing spring here"
  }
  case class Train(description: String, talent: String) extends Feature {
    override def messageOnUse(implicit sender: ActorRef) = Character.Train(talent)
    override def frequency(): FiniteDuration = Random.nextInt(90) + 30 seconds
    override val descriptionWhenAvaliable = description
    override val descriptionWhenNotAvaliable = description + " However place looks to be currently ruined!"
  }
}