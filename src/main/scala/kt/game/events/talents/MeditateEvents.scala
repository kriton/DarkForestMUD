package kt.game.events.talents

import kt.game.events.NonCombatEvent
import akka.actor.ActorRef
import kt.game.events.talents.MeditateEvents.RefocusFailureReason._
import kt.game.events.ActionFailedEvent

object MeditateEvents {

  case object MeditateSucceededEvent extends NonCombatEvent {
    override def toString = "You restored a fate point!"
  }

  case object MeditateFailedEvent extends NonCombatEvent with ActionFailedEvent {
    override def toString = "Your meditate attempt failed!"
  }

  case class RefocusFailedEvent(reason: RefocusFailureReason) extends NonCombatEvent with ActionFailedEvent {
    override def toString = reason match {
      case NotEnoughActionPoints => "You need 1 Fate point to refocus."
      case CheckFailure          => "Your refocus attempt failed."
    }
  }

  sealed trait RefocusFailureReason
  object RefocusFailureReason {
    case object NotEnoughActionPoints extends RefocusFailureReason
    case object CheckFailure extends RefocusFailureReason
  }

}