package kt.game.requests.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.ControlState
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.Stunned
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.characters.talents.Revive
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import akka.actor.ReceiveTimeout
import akka.actor.PoisonPill
import kt.game.domain.tombstones.Tombstone
import kt.game.domain.creatures.characters.CharacterGenerator
import kt.game.events.NonCombatEvent
import kt.game.events.talents.ReviveEvents._
import kt.game.events.ActionOnCoolDownEvent

class RessurectRequestHandler(character: ActorRef, target: String) extends Actor {
  import context._

  setReceiveTimeout(500 milliseconds)

  implicit val timeout = Timeout(200 milliseconds)
  val cooldown = Await.result(character ? Character.GetActionCooldown(Revive.action), timeout.duration).asInstanceOf[Option[Deadline]]
  if (cooldown == None || cooldown.get.isOverdue) {
    if (Await.result(character ? Creature.GetFatePoints(), timeout.duration).asInstanceOf[Int] >= Revive.ressurectFPCost) {
      val tombstone = Await.result(character ? Character.GetSpottedTombstone(target), timeout.duration).asInstanceOf[Option[ActorRef]]
      if (tombstone != None) {
        val tombstoneSnapshot = Await.result(tombstone.get ? Tombstone.RequestSnapshot(), timeout.duration).asInstanceOf[Tombstone.SnapshotT002]
        val roll = Await.result(character ? Creature.Roll(SkillName.CRAF), timeout.duration).asInstanceOf[Option[Int]].get
        parent ! Revive.SetRessurectCD()
        character ! Creature.AddFatePoints(-Revive.ressurectFPCost)
        if (roll - Revive.ressurectDC(tombstoneSnapshot.level, tombstoneSnapshot.defiled) >= 0) {
          character ! CharacterGenerator.ReviveFrom(tombstoneSnapshot)
          tombstone.get ! Tombstone.Defile()
          character ! Creature.Message(RessurectSucceededEvent)
        } else character ! Creature.Message(RessurectFailedEvent(RessurectFailureReason.CheckFailure))
      } else {
        character ! Creature.Message(RessurectFailedEvent(RessurectFailureReason.CanNoLongerSpot))
      }
    } else {
      character ! Creature.Message(RessurectFailedEvent(RessurectFailureReason.NotEnoughFatePoints))
    }
  } else {
    character ! Creature.Message(ActionOnCoolDownEvent(Some(Revive.action), cooldown.get.timeLeft.toSeconds))
  }

  def receive = {
    case ReceiveTimeout => self ! PoisonPill
  }
}

object RessurectRequestHandler {
  def props(character: ActorRef, target: String) = Props(new RessurectRequestHandler(character, target))
}