package kt.game.events

import kt.game.domain.creatures.AttackType
import kt.game.domain.creatures.Target

case class AttemptAttackEvent(attackType: AttackType, target: Target) extends CombatEvent {
  override def toString = s"You attack $target with a $attackType!"
}

case class AttackDealtDamageEvent(target: Target, damage: Int) extends CombatEvent {
  override def toString = s"You hit $target for $damage damage!"
}

case class AttackMissedEvent(target: Target) extends CombatEvent with ActionFailedEvent {
  override def toString = s"You missed the attack against $target!"
}

case class AttackDefendedEvent(attacker: Target) extends CombatEvent {
  override def toString = s"You succesfully defended an attack made by $attacker!"
}

case class AttackAgainstYouWasInterceptedEvent(attacker: Target, interceptor: Target) extends CombatEvent {
  override def toString = s"An attack made by $attacker against you was intercepted by $interceptor!"
}

case class YourAttackWasInterceptedEvent(target: Target, interceptor: Target) extends CombatEvent {
  override def toString = s"Your attack against $target was intercepted by $interceptor!"
}

case class YouInterceptedAnAttackEvent(attacker: Target, target: Target) extends CombatEvent {
  override def toString = s"You intercepted an attack made by $attacker against $target!"
}

case class KilledYourTargetEvent(target: Target, level: Int) extends CombatEvent {
  override def toString = s"You killed $target!"
}

case class YouDiedEvent() extends CombatEvent {
  override def toString = "You are dead!"
}

case class CreatureIsKilled(victim: Target, killer: Target) extends CombatEvent {
  override def toString = s"$victim is dead! Was killed by $killer."
}

case class ProtectingYourTargetEvent(target: Target) extends CombatEvent {
  override def toString = s"You are now protecting $target. You will interecept attacks against him as long as you are in the same location as he is and you can spot his attackers."
}

case object NoLongerProtectingYourTargetEvent extends CombatEvent {
  override def toString = "You are no longer protecting your target."
}

case class ProtectionFailedEvent(target: Target, reason: ActionFailedReason) extends CombatEvent with ActionFailedEvent {
  import ActionFailedReason._
  override def toString = reason match {
    case CanNoLongerSpot => s"You can no longet spot $target"
    case _               => "You were not protecting anybody."
  }
}

case class NoLongerProtectingYourTargetEvent(target: Target) extends CombatEvent {
  override def toString = s"You are no longer protecting $target."
}

case class ProtectedByCreatureEvent(protector: Target) extends CombatEvent {
  override def toString = s"You are now being protected by $protector."
}

case class AffectedTargetSuccessfullyEvent(target: Target, condition: String, strength: Int) extends CombatEvent {
  override def toString = s"You got $target $condition for $strength."
}

case class AffectedTargetResistedEvent(target: Target, condition: String) extends CombatEvent with ActionFailedEvent {
  override def toString = s"$target resisted your action and did not get $condition."
}

case class AffectedByConditionEvent(creature: Target, condition: String, strength: Int) extends CombatEvent {
  override def toString = s"You got $condition for $strength by creature."
}

case class RessistedConditionEvent(creature: Target, condition: String) extends CombatEvent {
  override def toString = s"$creature tried to get you $condition but failed."
}

case class DeathPreventionByFateEvent(wounds: Int, fatePoints: Int) extends CombatEvent {
  override def toString = s"You lost $fatePoints fate points to prevent your death!"
}

case class DamageSufferedEvent(source: AttackType, damage: Int, dealer: Target) extends CombatEvent {
  override def toString = (source, dealer) match {
    case (AttackType.Trap, _)                       => s"You suffered $damage damage from a trap."
    case (AttackType.Ranged, Target(None, None, _)) => s"You suffered $damage damage from a ranged sneak attack!"
    case (_, Target(None, None, _))                 => s"You suffered $damage damage from a melee sneak attack!"
    case _                                          => s"You suffered $damage damage from an attack made by $dealer."
  }
}

case class EvadedEnvironmentDamageEvent(source: AttackType) extends CombatEvent {
  override def toString = s"You succesfully evaded damage from a $source!"
}

case class KilledViaEnvironmentDamageEvent(source: AttackType, victim: Target) extends CombatEvent {
  override def toString = s"Your $source killed $victim!"
}

case class ParrySucceededEvent(bonus: Int) extends CombatEvent {
  override def toString = s"Your parry attempt succeeded. You have a $bonus deflect bonus!"
}

case class ParryFailedEvent(reason: ActionFailedReason) extends CombatEvent with ActionFailedEvent {
  import ActionFailedReason._
  override def toString = reason match {
    case CanNoLongerSpot => "You can no longet spot wanted creature."
    case _               => "Your parry attempt failed!"
  }
}

case class StunEvent(target: Target, duration: Int) extends CombatEvent {
  override def toString = s"You stun $target for $duration seconds!"
}

case class StunnedEvent(attacker: Target, duration: Int) extends CombatEvent {
  override def toString = s"$attacker stunned you for $duration seconds!"
}

case object NoLongerStunnedEvent extends CombatEvent {
  override def toString = "You are no longer stunned!"
}

case object StillStunnedEvent extends CombatEvent with ActionFailedEvent {
  override def toString = "You are stunned!"
}

case class TrapPlacedEvent() extends CombatEvent {
  override def toString = "You succesfully laid down a trap!"
}

case class TargetIsNotFearsomeEvent(target: Target) extends CombatEvent {
  override def toString = s"$target does not looks fearsome! You try to attack him!"
}

case class TargetIsPunnyEvent(target: Target) extends CombatEvent {
  override def toString = s"$target looks punny to you! You can keep attacking him fearlessly!"
}

case class TargetIsFearsomeEvent(target: Target) extends CombatEvent {
  override def toString = s"$target looks fearsome! You cower and do not attack him!"
}

case class LostControlToEvent(controller: Target) extends CombatEvent {
  override def toString = "You lost control of yourself!"
}

case class RegainedControlEvent() extends CombatEvent {
  override def toString = "You regained control of yourself!"
}

case object AlreadyInControlEvent extends CombatEvent with ActionFailedEvent {
  override def toString = "You are already in control of yourself!"
}

case class MonsterStateChangedEvent(monster: Target, inState: String) extends CombatEvent {
  override def toString = s"$monster is now $inState!"
}

case class MultiAttackImpossibleEvent(attackType: AttackType, reason: AttackImpossibleReason) extends CombatEvent with ActionFailedEvent {
  import AttackImpossibleReason._
  override def toString = reason match {
    case CanNoLongerSpot => "You can no longet spot any of the targets."
    case _               => "Action is impossible."
  }
}

case class AttackImpossibleEvent(attackType: AttackType, target: Target, reason: AttackImpossibleReason) extends CombatEvent with ActionFailedEvent {
  import AttackImpossibleReason._
  override def toString = reason match {
    case CanNoLongerSpot   => s"You can no longet spot $target."
    case NotInPosition     => s"You are not in position to attack $target."
    case NotInSameLocation => s"$target is not in the same location as you are. You need to find it and move closer to it."
  }
}

sealed trait AttackImpossibleReason

object AttackImpossibleReason {
  case object CanNoLongerSpot extends AttackImpossibleReason
  case object NotInPosition extends AttackImpossibleReason
  case object NotInSameLocation extends AttackImpossibleReason
}