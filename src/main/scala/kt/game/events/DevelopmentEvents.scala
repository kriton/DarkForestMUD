package kt.game.events

import kt.game.domain.creatures.skills.Skill
import kt.game.domain.creatures.characters.Character
import kt.game.domain.items.Receipe
import kt.game.domain.creatures.characters.talents.Talent
import kt.game.domain.creatures.characters.talents.TalentContext

object DevelopmentEvents {
  case class CanNotLearnTalentEvent(xp: Int, skill: Skill, alreadyKnown: Boolean = false, limitReached: Boolean = false) extends DevelopmentEvent with ActionFailedEvent {
    override def toString = if (alreadyKnown) "You already know selected talent!"
    else if (limitReached) "You already know the maximum number of talents for your level!"
    else s"You can not learn this talent yet! It requires $xp xp and a $skill skill."
  }
  case class LevelUpEvent(level: Int) extends DevelopmentEvent {
    override def toString = s"You are now level $level!"
  }
  case class CannotLevelupSelectedSkillEvent(level: Int) extends DevelopmentEvent with ActionFailedEvent {
    override def toString = s"Cannot level up selected skill. Limit of ${Character.STATLIMIT(level)} for your level has been reached!"
  }
  case object CannotLevelUpEvent extends DevelopmentEvent with ActionFailedEvent {
    override def toString = "You do not have enough experience points to level up!"
  }
  case object InvalidLevelupSkillSelectedEvent extends DevelopmentEvent with ActionFailedEvent {
    override def toString = "You selected an invalid skill to level up!"
  }
  case class XPGainedEvent(xp: Int) extends DevelopmentEvent {
    override def toString = s"You gained $xp xp."
  }
  case object CanLevelUpEvent extends DevelopmentEvent {
    override def toString = "You can now level up!"
  }
  case class XPLostEvent(xp: Int) extends DevelopmentEvent {
    override def toString = s"You lost $xp xp."
  }
  case class MonsterWasTooWeakToGrantXPEvent(level: Int) extends DevelopmentEvent {
    override def toString = s"The monster you defeated was too weak (level $level) to grant you any experience!"
  }
  case class NewRecipeLEarnedEvent(recipe: Receipe) extends DevelopmentEvent {
    override def toString = s"You succesfully learned a new recipe: $recipe!"
  }
  case class RecipeAlreadyKnownEvent(recipe: Receipe) extends DevelopmentEvent with ActionFailedEvent {
    override def toString = s"You would have learned $recipe, but you already know it!"
  }
  case class RecipeNotComprehendedEvent(recipe: Receipe) extends DevelopmentEvent with ActionFailedEvent {
    override def toString = s"You would have learned a new recipe: $recipe, but you cannot comprehend it! You need to find and learn its requirements first!"
  }
  case class NewTalentAcquiredEvent(talent: Option[TalentContext]) extends DevelopmentEvent {
    override def toString = s"You succesfully learned the ${talent.get.name} talent: ${talent.get.description}"
  }
}