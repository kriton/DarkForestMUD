package kt.game.domain.locations.descriptors

sealed trait DescrType
case object Noun extends DescrType
case object Adjective extends DescrType
case object Extension extends DescrType
case object Relation extends DescrType
case object Connector extends DescrType
case object Verb extends DescrType

sealed trait Descriptor {
  private[descriptors] def visibility: Int
  private[descriptors] def seeAgainst(spotRoll: Int): Descriptor = if (spotRoll >= visibility) this else Nothing
  def +(descriptor: Descriptor): Descriptor
  def :+(descriptor: Descriptor): Descriptor
  def !(spotRoll: Int) = seeAgainst(spotRoll)
  def > = this
  def !>(spotRoll: Int) = seeAgainst(spotRoll)
}

object Nothing extends Descriptor {
  def +(descriptor: Descriptor) = descriptor
  def :+(descriptor: Descriptor) = descriptor
  def visibility = Int.MinValue
  override def toString() = "nothing."
}

sealed case class DescriptorSuperSet private[descriptors] (descriptorSets: Seq[DescriptorSet]) extends Descriptor {
  def +(descriptor: Descriptor) = descriptor match {
    case x: DescriptorSuperSet => DescriptorSuperSet(this.descriptorSets ++ x.descriptorSets)
    case x: DescriptorSet      => DescriptorSuperSet(this.descriptorSets :+ x)
    case x: DescriptorItem     => DescriptorSuperSet(this.descriptorSets :+ DescriptorSet(Seq(x)))
    case Nothing               => this
  }
  def :+(descriptor: Descriptor) = this + descriptor
  override def > = this.descriptorSets(0)
  override def !>(spotRoll: Int) = this.descriptorSets(0) seeAgainst spotRoll
  def visibility = descriptorSets(0).visibility
  override def seeAgainst(spotRoll: Int) = if (spotRoll >= visibility) DescriptorSuperSet(descriptorSets.filter { _.seeAgainst(spotRoll) != Nothing }) else Nothing
  override def toString() = descriptorSets.mkString(". ")
}

sealed case class DescriptorSet private[descriptors] (descriptors: Seq[DescriptorItem]) extends Descriptor {
  def +(descriptor: Descriptor) = descriptor match {
    case x: DescriptorSuperSet => DescriptorSuperSet(this +: x.descriptorSets)
    case x: DescriptorSet      => DescriptorSuperSet(Seq(this, x))
    case x: DescriptorItem     => DescriptorSet(this.descriptors :+ x)
    case Nothing               => this
  }
  def :+(descriptor: Descriptor) = descriptor match {
    case x: DescriptorSuperSet => DescriptorSuperSet(this +: x.descriptorSets)
    case x: DescriptorSet      => DescriptorSuperSet(Seq(this, x))
    case x: DescriptorItem     => DescriptorSuperSet(Seq(this, DescriptorSet(Seq(x))))
    case Nothing               => this
  }
  def visibility = descriptors.map(_.visibility).reduce((a, b) => a max b)
  override def toString() = {
    descriptors match {
      case head :: tail => tail.foldLeft((head.value, head.descr))((result, current) => (result._2, current.descr) match {
        case (Adjective, Adjective) if tail.head.descr == Noun => (result._1 + " and " + current.value, current.descr)
        case (Adjective, Adjective)                            => (result._1 + ", " + current.value, current.descr)
        case _                                                 => (result._1 + " " + current.value, current.descr)
      })._1
    }
  }
}

case class DescriptorItem private[descriptors] (descr: DescrType, value: String) extends Descriptor {
  def +(descriptor: Descriptor) = descriptor match {
    case x: DescriptorSuperSet => DescriptorSuperSet(DescriptorSet(Seq(this)) +: x.descriptorSets)
    case x: DescriptorSet      => DescriptorSet(this +: x.descriptors)
    case x: DescriptorItem     => DescriptorSet(Seq(this, x))
    case Nothing               => this
  }
  def :+(descriptor: Descriptor) = descriptor match {
    case x: DescriptorSuperSet => DescriptorSuperSet(DescriptorSet(Seq(this)) +: x.descriptorSets)
    case x: DescriptorSet      => DescriptorSet(this +: x.descriptors)
    case x: DescriptorItem     => DescriptorSuperSet(Seq(DescriptorSet(Seq(this)), DescriptorSet(Seq(x))))
    case Nothing               => this
  }
  override def toString() = value
  def visibility = 0
}

class HiddenDescriptorItem private[descriptors] (override val visibility: Int) extends DescriptorItem(Adjective, "hidden")
class FarDescriptorItem private[descriptors] (override val visibility: Int) extends DescriptorItem(Extension, "")
