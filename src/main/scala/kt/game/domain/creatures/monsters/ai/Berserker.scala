package kt.game.domain.creatures.monsters.ai

import scala.concurrent.duration.DurationInt

import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.monsters.Behaviour
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.SkillName.SkillName2String

trait Berserker extends Monster with Behaviour {
  import context._
  import Berserker._

  def enraged = buffsAndPenalties.map(_._2).flatten.filter(_._2._2.hasTimeLeft).map(_._1).toSet.contains(BERSERK)

  override def detailedDescription = (if (enraged) "Looks to be enraged." else "") + (if (super.detailedDescription != "") " " + super.detailedDescription else "")

  override def sufferWounds(wounds: Int) = {
    super.sufferWounds(wounds)
    if (this.wounds < MAX_WOUNDS && wounds > 0 && woundedPenalty() > 0 && !enraged) {
      goBerserk()
    }
  }

  override def killed(creature: ActorRef, level: Int) = {
    super.killed(creature, level)
    goBerserk()
  }

  def goBerserk() = {
    spottedByCache foreach { _ ! Creature.MonsterStateChanged(self, "enraged") }
    addBuffOrPenalty(SkillName.PERC, 5, (60 seconds).fromNow, BERSERK)
    addBuffOrPenalty(SkillName.WEAP, 5, (60 seconds).fromNow, BERSERK)
    addBuffOrPenalty(SkillName.ATHL, 5, (60 seconds).fromNow, BERSERK)
    addBuffOrPenalty(SkillName.WILL, 5, (60 seconds).fromNow, BERSERK)
    addBuffOrPenalty("INITIATIVE", 25, (60 seconds).fromNow, BERSERK)
  }
}

object Berserker {
  val BERSERK = "Enraged"
}