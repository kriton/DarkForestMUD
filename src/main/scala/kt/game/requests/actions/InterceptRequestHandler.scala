package kt.game.requests.actions

import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.locations.Location
import akka.actor.PoisonPill
import akka.actor.ReceiveTimeout
import kt.game.events.CombatEvent
import kt.game.domain.creatures.AttackType

class InterceptRequestHandler private (creature: ActorRef, in: ActorRef, interceptors: Set[ActorRef], attacker: ActorRef, attackRoll: Int, attackLocation: ActorRef, attackType: AttackType) extends Actor {
  import context._

  interceptors.par.foreach { _ ! Creature.GetIntercepting() }
  context.setReceiveTimeout(100 milliseconds)

  var interceptor: Option[ActorRef] = None

  def intercept() = {
    interceptor = Some(sender)
    creature ! Creature.AttackInterceptedBy(attacker, interceptor.get)
    interceptor.get ! Creature.AttackInterceptedFor(attacker, creature)
    attacker ! Creature.AttackIntercepted(creature, interceptor.get)
    interceptor.get.tell(Creature.AttackedNoIntecept(attackRoll, attackLocation, attackType), attacker)
  }

  def receive = {
    case Some(x: ActorRef) if Creature.is(x) && interceptor == None && x != attacker => sender ! Creature.GetCurrentLocation()
    case Some(x: ActorRef) if Location.is(x) && interceptor == None && x == in       => sender ! Creature.GetSpottedCreature(attacker)
    case Some(x: ActorRef) if Location.is(x) && x != in                              => { creature ! Creature.NoLongerInterceptedBy(sender); sender ! Creature.NoLongerIntercepting() }
    case Some(x: String) if interceptor == None                                      => intercept()
    case ReceiveTimeout                                                              => { self ! PoisonPill; if (interceptor == None) creature.tell(Creature.AttackedNoIntecept(attackRoll, attackLocation, attackType), attacker) }
    case _                                                                           =>
  }
}

object InterceptRequestHandler {
  def props(creature: ActorRef, in: ActorRef, interceptors: Set[ActorRef], attackRoll: Int, attacker: ActorRef, from: ActorRef, attackType: AttackType) = Props(new InterceptRequestHandler(creature, in, interceptors, attacker, attackRoll, from, attackType))
}