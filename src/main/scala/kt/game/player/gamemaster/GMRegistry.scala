package kt.game.player.gamemaster

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import kt.game.player.PlayerRegistry
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import scala.concurrent.duration.DurationInt

class GMRegistry private (password: String, builder: ActorRef, playerRegistry: ActorRef, monsters: ActorRef, characters: ActorRef, tombstones: ActorRef) extends Actor {
  import GMRegistry._

  def receive = {
    case Login(password, out, ip) if (password == this.password) => {
      val gm = context.actorOf(GameMaster.props(out, builder, ip, playerRegistry, monsters, characters, tombstones))
      sender ! gm
      playerRegistry ! PlayerRegistry.RegisterLoggedInPlayer(gm)
    }
    case Login(password, out, ip) if (password != this.password) => {
      sender ! "Wrong password"
    }
    case other => playerRegistry forward other
  }

  override val supervisorStrategy = {
    OneForOneStrategy() {
      case _: Exception => Resume
    }
  }
}

object GMRegistry {
  
  val NAME = "gms"
  
  def props(password: String, builder: ActorRef, playerRegistry: ActorRef, monsters: ActorRef, characters: ActorRef, tombstones: ActorRef) = Props(new GMRegistry(password, builder, playerRegistry, monsters, characters, tombstones))
  case class Login(password: String, out: ActorRef, ip: String)
}