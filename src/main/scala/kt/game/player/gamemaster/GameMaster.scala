package kt.game.player.gamemaster

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.items.AllObjects
import kt.game.domain.worldbuilder.WorldBuilder
import kt.game.player.Player
import kt.game.requests.gm.GetMonstersRequestHandler
import kt.game.events.ActionFailedEvent
import kt.game.system.FeedbackBuffer.Feedback
import kt.game.events.GMInfoEvent
import kt.game.events.LoggedEvent
import kt.game.domain.tombstones.TombstoneGenerator
import kt.game.domain.items.Object
import kt.game.requests.gm.GetTombstonesRequestHandler
import kt.game.requests.gm.GetCharsRequestHandler
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Skillset
import kt.game.domain.tombstones.Tombstone
import kt.game.domain.creatures.characters.CharacterGenerator
import kt.game.domain.creatures.CCommand
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import kt.game.system.FeedbackBuffer.Feedback

class GameMaster(out: ActorRef, builder: ActorRef, ip: String, playerRegistry: ActorRef, monsters: ActorRef, characters: ActorRef, tombstones: ActorRef) extends Player(out, builder, ip, characters) {
  import GameMaster._
  import context._

  implicit val timeout = Timeout(100 milliseconds)

  val controlling = scala.collection.mutable.Set[ActorRef]()

  override def keepAliveCheck(): Boolean = {
    if (super.keepAliveCheck()) {
      controlling foreach { dropControl(_) }
      true
    } else {
      false
    }
  }

  def findItemsCH(p: String => Boolean) = {
    out ! Feedback(LoggedEvent(GMInfoEvent(AllObjects.map.map { e =>
      ("%-15s" format ("[" + e._2.getClass.getSimpleName + "]")) +
        ("%-20s" format e._1) + " -> " + e._2
    }.filter(p).toSeq.sorted.mkString("\r\n"))))
  }

  def findMonstersCH(p: String => Boolean) = {
    context actorOf GetMonstersRequestHandler.props(monsters, p, None)
  }

  def reskillMonstersCH(p: String => Boolean, skills: String) {
    context actorOf GetMonstersRequestHandler.props(monsters, p, Some(reskill(skills)))
  }

  def reskill(skills: String)(creature: ActorRef): Unit = try {
    creature ! Creature.Reskill(skills.trim.split(" ").grouped(2).map(x => SkillName.withName(x(0)) -> x(1).toInt).toSeq)
  } catch {
    case _: Throwable => out ! Feedback(LoggedEvent(GMInfoEvent("Invalid skills selected")))
  }

  def addToMonsterInventoryCH(p: String => Boolean, itemfilter: String => Boolean) {
    try {
      val itemsCollection = AllObjects.map.map { e =>
        (e._2, ("%-15s" format ("[" + e._2.getClass.getSimpleName + "]")) + ("%-20s" format e._1) + " -> " + e._2)
      }.filter { v => itemfilter(v._2) }.toSeq
      context actorOf GetMonstersRequestHandler.props(monsters, p, Some(c => itemsCollection.foreach { i => c ! Creature.AddToInventory(i._1) }))
    } catch {
      case _: Throwable => out ! Feedback(LoggedEvent(GMInfoEvent("Invalid items selected")))
    }
  }

  def clearMonsterInventoryCH(p: String => Boolean) {
    context actorOf GetMonstersRequestHandler.props(monsters, p, Some(_ ! Creature.ClearInventory()))
  }

  def resetMonstersCH(p: String => Boolean) {
    context actorOf GetMonstersRequestHandler.props(monsters, p, Some(_ ! Monster.Respawn()))
  }

  def repositionMonstersCH(p: String => Boolean, newLocation: String) {
    context actorOf GetMonstersRequestHandler.props(monsters, p, Some(reposition(newLocation)))
  }

  def reposition(newLocation: String)(creature: ActorRef): Unit = {
    val future = builder ? WorldBuilder.GetArea(newLocation)
    val location = Await.result(future, timeout.duration).asInstanceOf[Option[ActorRef]]
    if (location != None) creature ! Creature.Position(location.get)
    else self ! Feedback(LoggedEvent(GMInfoEvent("Invalid location!")))
  }

  def findTombstonesAndRunTaskCH(p: String => Boolean, task: Option[ActorRef => Unit]) = {
    context actorOf GetTombstonesRequestHandler.props(tombstones, Some(p), task)
  }

  def createTombstoneCH(name: String, level: String, location: String, items: String) {
    context actorOf GetTombstonesRequestHandler.props(tombstones, None, Some(createNewTombstone(name, level, location, items)))
  }

  def createNewTombstone(name: String, level: String, locationRef: String, items: String)(tombstoneGenerator: ActorRef) {
    val itemsSet: Set[Object] = items.split(",").map { e => AllObjects.map.get(e.trim) }.filter(_ != None).map { _.get }.toSet
    val future = builder ? WorldBuilder.GetArea(locationRef)
    val location = Await.result(future, timeout.duration).asInstanceOf[Option[ActorRef]]
    if (location != None) tombstoneGenerator ! TombstoneGenerator.NewTombstone(name, name, 0, level.toInt, location.get, itemsSet, Skillset.zero, scala.Predef.Set[String](), scala.Predef.Set[String](), scala.Predef.Set[String]())
    else self ! Feedback(LoggedEvent(GMInfoEvent("Invalid location!")))
  }

  def reviveFromTombstone(tombstone: ActorRef) = {
    characters ! CharacterGenerator.ReviveFrom(Await.result(tombstone ? Tombstone.RequestSnapshot(), timeout.duration).asInstanceOf[Tombstone.SnapshotT002])
    tombstone ! Tombstone.Defile()
  }

  def updateWounds(value: String)(creature: ActorRef): Unit = creature ! Creature.SetWounds(value.toInt)
  def updateFatigue(value: String)(creature: ActorRef): Unit = creature ! Character.SetFatigue(value.toInt)
  def updateXP(value: String)(creature: ActorRef): Unit = creature ! Character.SetXP(value.toInt)

  def control(creature: ActorRef): Unit = {
    creature ! Creature.ForceTakeControl(self ! ControlledCreatureDied(_))
    controlling += creature
  }

  def dropControl(creature: ActorRef): Unit = {
    creature ! Creature.DropControl()
    controlling -= creature
  }

  def addToCharacterInventoryCH(p: String => Boolean, itemfilter: String => Boolean) = {
    try {
      val itemsCollection = AllObjects.map.map { e =>
        (e._2, ("%-15s" format ("[" + e._2.getClass.getSimpleName + "]")) + ("%-20s" format e._1) + " -> " + e._2)
      }.filter { v => itemfilter(v._2) }.toSeq
      context actorOf GetCharsRequestHandler.props(characters, p, Some(c => itemsCollection.foreach { i => c ! Creature.AddToInventory(i._1) }))
    } catch {
      case _: Throwable => out ! Feedback(LoggedEvent(GMInfoEvent("Invalid items selected")))
    }
  }

  override val supervisorStrategy = {
    OneForOneStrategy(maxNrOfRetries = 2, withinTimeRange = 1 minute) {
      case _: Exception => Stop
    }
  }

  override def receive = ({
    case ControlledCreatureDied(creature)                     => controlling -= creature
    case x: CCommand if (!controlling.isEmpty)                => controlling foreach { _ ! x }
    case AllItemsCommand()                                    => findItemsCH(p => true)
    case FilteredItemsCommand(filter)                         => findItemsCH(matchFor(filter))
    case AllMonstersCommand()                                 => findMonstersCH(p => true)
    case FilteredMonstersCommand(filter)                      => findMonstersCH(matchFor(filter))
    case RepositionMonsterCommand(filter, location)           => repositionMonstersCH(matchFor(filter), location)
    case ResetMonsterCommand(filter)                          => resetMonstersCH(matchFor(filter))
    case ChangeSkillsOfMonstersCommand(filter, skills)        => reskillMonstersCH(matchFor(filter), skills)
    case AddToInventoryOfMonstersCommand(filter, items)       => addToMonsterInventoryCH(matchFor(filter), _ matches "(?i).*(" + items + "?).*")
    case ClearInventoryOfMonstersCommand(filter)              => clearMonsterInventoryCH(matchFor(filter))
    case MonitorMonsterStartCommand(filter)                   => context actorOf GetMonstersRequestHandler.props(monsters, matchFor(filter), Some(_ ! Creature.StartMonitoring()))
    case MonitorMonsterStopCommand(filter)                    => context actorOf GetMonstersRequestHandler.props(monsters, matchFor(filter), Some(_ ! Creature.StopMonitoring()))
    case ControlMonsterStartCommand(filter)                   => context actorOf GetMonstersRequestHandler.props(monsters, matchFor(filter), Some(control))
    case ControlMonsterStopCommand(filter)                    => context actorOf GetMonstersRequestHandler.props(monsters, matchFor(filter), Some(dropControl))
    case CreateTombstoneCommand(name, level, location, items) => createTombstoneCH(name, level, location, items)
    case ReviveFromTombstoneCommand(filter)                   => findTombstonesAndRunTaskCH(matchFor(filter), Some(reviveFromTombstone))
    case FilteredTombstonesCommand(filter)                    => findTombstonesAndRunTaskCH(matchFor(filter), None)
    case AllTombstonesCommand()                               => findTombstonesAndRunTaskCH(p => true, None)
    case AllCharsCommand()                                    => context actorOf GetCharsRequestHandler.props(characters, p => true, None)
    case FilteredCharsCommand(filter)                         => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), None)
    case SetWoundsToCharsCommand(filter, value)               => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(updateWounds(value)))
    case SetXPToCharsCommand(filter, value)                   => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(updateXP(value)))
    case SetFatigueToCharsCommand(filter, value)              => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(updateFatigue(value)))
    case ChangeSkillsOfCharsCommand(filter, skills)           => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(reskill(skills)))
    case RepositionCharsCommand(filter, location)             => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(reposition(location)))
    case MonitorCharStartCommand(filter)                      => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(_ ! Creature.StartMonitoring()))
    case MonitorCharStopCommand(filter)                       => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(_ ! Creature.StopMonitoring()))
    case ControlCharStartCommand(filter)                      => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(control))
    case ControlCharStopCommand(filter)                       => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(dropControl))
    case AddToInventoryOfCharsCommand(filter, items)          => addToCharacterInventoryCH(matchFor(filter), _ matches "(?i).*(" + items + "?).*")
    case ClearInventoryOfCharsCommand(filter)                 => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(_ ! Creature.ClearInventory()))
    case TrainTalentOnCharsCommand(filter, talent)            => context actorOf GetCharsRequestHandler.props(characters, matchFor(filter), Some(_ ! Character.Train(talent)))
  }: Receive) orElse super.receive

}

object GameMaster {
  def props(out: ActorRef, builder: ActorRef, ip: String, playerRegistry: ActorRef, monsters: ActorRef, characters: ActorRef, tombstones: ActorRef) = Props(new GameMaster(out, builder, ip, playerRegistry, monsters, characters, tombstones))

  def matchFor(filter: String)(command: String) = command matches "(?i).*(" + filter + "?).*"

  case class ControlledCreatureDied(creature: ActorRef)

  case class AllItemsCommand()
  case class FilteredItemsCommand(filterRegex: String)

  case class AllMonstersCommand()
  case class FilteredMonstersCommand(filterRegex: String)
  case class RepositionMonsterCommand(monsterSelection: String, newLocation: String)
  case class ResetMonsterCommand(monsterSelection: String)
  case class ChangeSkillsOfMonstersCommand(monsterSelection: String, skills: String)
  case class AddToInventoryOfMonstersCommand(monsterSelection: String, item: String)
  case class ClearInventoryOfMonstersCommand(monsterSelection: String)
  case class MonitorMonsterStartCommand(monsterSelection: String)
  case class MonitorMonsterStopCommand(monsterSelection: String)
  case class ControlMonsterStartCommand(monsterSelection: String)
  case class ControlMonsterStopCommand(monsterSelection: String)

  case class AllTombstonesCommand()
  case class FilteredTombstonesCommand(filterRegex: String)
  case class ReviveFromTombstoneCommand(filterRegex: String)
  case class CreateTombstoneCommand(name: String, level: String, location: String, items: String)

  case class AllCharsCommand()
  case class FilteredCharsCommand(filterRegex: String)
  case class SetXPToCharsCommand(charSelection: String, value: String)
  case class SetWoundsToCharsCommand(charSelection: String, value: String)
  case class SetFatigueToCharsCommand(charSelection: String, value: String)
  case class ChangeSkillsOfCharsCommand(charSelection: String, skills: String)
  case class RepositionCharsCommand(charSelection: String, newLocation: String)
  case class AddToInventoryOfCharsCommand(charSelection: String, item: String)
  case class ClearInventoryOfCharsCommand(charSelection: String)
  case class MonitorCharStartCommand(charSelection: String)
  case class MonitorCharStopCommand(charSelection: String)
  case class ControlCharStartCommand(charSelection: String)
  case class ControlCharStopCommand(charSelection: String)
  case class TrainTalentOnCharsCommand(charSelection: String, talent: String)
}