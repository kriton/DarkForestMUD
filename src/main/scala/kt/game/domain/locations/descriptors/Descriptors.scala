package kt.game.domain.locations.descriptors

object Descriptors {
  object ForestEdge extends DescriptorItem(Noun, "forest edge")
  object Tree extends DescriptorItem(Noun, "tree")
  object SkullPile extends DescriptorItem(Noun, "pile of skulls")
  object BonesPile extends DescriptorItem(Noun, "pile of bones")
  object BonesAndSkullsPile extends DescriptorItem(Noun, "pile of skulls and bones")
  object Hut extends DescriptorItem(Noun, "hut")
  object Treehome extends DescriptorItem(Noun, "treehome")
  object Workshop extends DescriptorItem(Noun, "workshop")
  object Laboratory extends DescriptorItem(Noun, "laboratoty")
  object Dungeons extends DescriptorItem(Noun, "dungeons")
  object Cell extends DescriptorItem(Noun, "cell")
  object TorturingChamber extends DescriptorItem(Noun, "torturing chamber")
  object Table extends DescriptorItem(Noun, "table")
  object Library extends DescriptorItem(Noun, "library")
  object TreeTrunk extends DescriptorItem(Noun, "tree trunk")
  object Stream extends DescriptorItem(Noun, "stream")
  object Bridge extends DescriptorItem(Noun, "bridge")
  object Mountain extends DescriptorItem(Noun, "mountain")
  object Rock extends DescriptorItem(Noun, "rock")
  object Rocks extends DescriptorItem(Noun, "rocks")
  object Hill extends DescriptorItem(Noun, "hill")
  object Path extends DescriptorItem(Noun, "path")
  object Building extends DescriptorItem(Noun, "building")
  object Stables extends DescriptorItem(Noun, "stables")
  object Basement extends DescriptorItem(Noun, "basement")
  object Level extends DescriptorItem(Noun, "level")
  object Balcony extends DescriptorItem(Noun, "balcony")
  object Cellar extends DescriptorItem(Noun, "cellar")
  object Room extends DescriptorItem(Noun, "room")
  object Tower extends DescriptorItem(Noun, "tower")
  object Castle extends DescriptorItem(Noun, "castle")
  object Obelisk extends DescriptorItem(Noun, "obelisk")
  object Cavern extends DescriptorItem(Noun, "cave")
  object Cliff extends DescriptorItem(Noun, "cliff")
  object CliffSide extends DescriptorItem(Noun, "cliffside")
  object Lake extends DescriptorItem(Noun, "lake")
  object Clearing extends DescriptorItem(Noun, "clearing")
  object Waterfall extends DescriptorItem(Noun, "waterfall")
  object River extends DescriptorItem(Noun, "river")
  object Forest extends DescriptorItem(Noun, "forest")
  object Volcano extends DescriptorItem(Noun, "volcano")
  object Pit extends DescriptorItem(Noun, "pit")
  object Ruins extends DescriptorItem(Noun, "ruins")
  object Vegetation extends DescriptorItem(Noun, "vegetation")
  object MudPool extends DescriptorItem(Noun, "mud pool")
  object AlchemyLab extends DescriptorItem(Noun, "alchemy lab")
  object Chasm extends DescriptorItem(Noun, "chasm")
  object CaveFork extends DescriptorItem(Noun, "cave fork")
  object CaveArea extends DescriptorItem(Noun, "cave area")
  object CavePath extends DescriptorItem(Noun, "cave path")
  object CrystalCavernComplex extends DescriptorItem(Noun, "crystal cave complex")
  object Door extends DescriptorItem(Noun, "door")
  object Steps extends DescriptorItem(Noun, "steps")
  object Light extends DescriptorItem(Noun, "light")
  object Attic extends DescriptorItem(Noun, "attic")
  object Swamp extends DescriptorItem(Noun, "swamp")
  object Jungle extends DescriptorItem(Noun, "jungle")
  object ThroneRoom extends DescriptorItem(Noun, "throne room")
  object Barracks extends DescriptorItem(Noun, "barracks")
  object Armory extends DescriptorItem(Noun, "armory")
  object Observatory extends DescriptorItem(Noun, "observatory")
  object RedWoodForest extends DescriptorItem(Noun, "redwood forest")
  object Clouds extends DescriptorItem(Noun, "clouds")
  object Trap extends DescriptorItem(Noun, "trap")
  object TrapDoor extends DescriptorItem(Noun, "trapdoor")
  object Portal extends DescriptorItem(Noun, "portal")
  object Walls extends DescriptorItem(Noun, "walls")
  object Hall extends DescriptorItem(Noun, "hall")
  object Stairway extends DescriptorItem(Noun, "stairway")

  object Smelly extends DescriptorItem(Adjective, "smelly")
  object Foggy extends DescriptorItem(Adjective, "foggy")
  object Sealed extends DescriptorItem(Adjective, "sealed")
  object Iron extends DescriptorItem(Adjective, "iron")
  object Scary extends DescriptorItem(Adjective, "scary")
  object VeryScary extends DescriptorItem(Adjective, "very scary")
  object Wider extends DescriptorItem(Adjective, "wider")
  object Bigger extends DescriptorItem(Adjective, "bigger")
  object Narrower extends DescriptorItem(Adjective, "narrower")
  object Thinner extends DescriptorItem(Adjective, "thinner")
  object Thicker extends DescriptorItem(Adjective, "thicker")
  object Descending extends DescriptorItem(Adjective, "descending")
  object Ascending extends DescriptorItem(Adjective, "ascending")
  object VerySteep extends DescriptorItem(Adjective, "very steep")
  object Steep extends DescriptorItem(Adjective, "steep")
  object Mountainous extends DescriptorItem(Adjective, "mountainous")
  object Hilly extends DescriptorItem(Adjective, "hilly")
  object Jungly extends DescriptorItem(Adjective, "jungly")
  object Dryer extends DescriptorItem(Adjective, "dryer")
  object Floody extends DescriptorItem(Adjective, "floody")
  object Swampy extends DescriptorItem(Adjective, "swampy")
  object High extends DescriptorItem(Adjective, "high")
  object Rocky extends DescriptorItem(Adjective, "rocky")
  object Wild extends DescriptorItem(Adjective, "wild")
  object OneWay extends DescriptorItem(Adjective, "one-way")
  object VeryTall extends DescriptorItem(Adjective, "very tall")
  object Standing extends DescriptorItem(Adjective, "standing")
  object Tall extends DescriptorItem(Adjective, "tall")
  object Upper extends DescriptorItem(Adjective, "upper")
  object Lower extends DescriptorItem(Adjective, "lower")
  object Inner extends DescriptorItem(Adjective, "inner")
  object Outer extends DescriptorItem(Adjective, "outer")
  object Stone extends DescriptorItem(Adjective, "stone")
  object Wooden extends DescriptorItem(Adjective, "wooden")
  object Ruined extends DescriptorItem(Adjective, "ruined")
  object Fallen extends DescriptorItem(Adjective, "fallen")
  object Elven extends DescriptorItem(Adjective, "elven")
  object Abandoned extends DescriptorItem(Adjective, "abandoned")
  object Ancient extends DescriptorItem(Adjective, "ancient")
  object Gargantuan extends DescriptorItem(Adjective, "gargantuan")
  object Huge extends DescriptorItem(Adjective, "huge")
  object Large extends DescriptorItem(Adjective, "large")
  object Small extends DescriptorItem(Adjective, "small")
  object Dark extends DescriptorItem(Adjective, "dark")
  object Bright extends DescriptorItem(Adjective, "bright")
  object Blueish extends DescriptorItem(Adjective, "blueish")
  object Redish extends DescriptorItem(Adjective, "redish")
  object Green extends DescriptorItem(Adjective, "green")
  object Black extends DescriptorItem(Adjective, "black")
  object White extends DescriptorItem(Adjective, "white")
  object Thick extends DescriptorItem(Adjective, "thick")
  object Wide extends DescriptorItem(Adjective, "wide")
  object Deep extends DescriptorItem(Adjective, "deep")
  object Narrow extends DescriptorItem(Adjective, "narrow")
  object Rough extends DescriptorItem(Adjective, "rough")
  object VeryRough extends DescriptorItem(Adjective, "very rough")
  object Dangerous extends DescriptorItem(Adjective, "dangerous")
  object Beautiful extends DescriptorItem(Adjective, "beautiful")
  object Flowing extends DescriptorItem(Adjective, "flowing")
  object FastFlowing extends DescriptorItem(Adjective, "fast flowing")
  object Overgrown extends DescriptorItem(Adjective, "overgrown")
  object VeryGoodCondition extends DescriptorItem(Adjective, "in very good condition")
  object GreatCondition extends DescriptorItem(Adjective, "in great condition")
  object Homey extends DescriptorItem(Adjective, "homey")
  object Locked extends DescriptorItem(Adjective, "locked")
  object Stuck extends DescriptorItem(Adjective, "stuck")
  object Cold extends DescriptorItem(Adjective, "cold")
  object VeryHot extends DescriptorItem(Adjective, "very hot")
  object Burning extends DescriptorItem(Adjective, "burning")
  object Broken extends DescriptorItem(Adjective, "broken")

  object SomeWhatHidden extends HiddenDescriptorItem(5)
  object Hidden extends HiddenDescriptorItem(10)
  object WellHidden extends HiddenDescriptorItem(15)
  object VeryWellHidden extends HiddenDescriptorItem(20)
  object HiddenALot extends HiddenDescriptorItem(25)
  object HiddenCompletely extends HiddenDescriptorItem(30)

  object TooFarToSeeClearly extends FarDescriptorItem(18)

  object Onwards extends DescriptorItem(Extension, "onwards")
  object Southwards extends DescriptorItem(Extension, "southwards")
  object LeadingSouth extends DescriptorItem(Extension, "leading south")
  object LeadingEast extends DescriptorItem(Extension, "leading east")
  object LeadingWest extends DescriptorItem(Extension, "leading west")
  object LeadingNorth extends DescriptorItem(Extension, "leading north")
  object LeadingSouthEast extends DescriptorItem(Extension, "leading southeast")
  object LeadingNorthEast extends DescriptorItem(Extension, "leading northeast")
  object LeadingSouthWest extends DescriptorItem(Extension, "leading southwest")
  object LeadingNorthWest extends DescriptorItem(Extension, "leading northwest")
  object Side extends DescriptorItem(Extension, "side")
  object OtherSide extends DescriptorItem(Extension, "other side")
  object Upstairs extends DescriptorItem(Extension, "upstairs")
  object Outside extends DescriptorItem(Extension, "outside")
  object Inside extends DescriptorItem(Extension, "inside")
  object Top extends DescriptorItem(Extension, "top")
  object InTheMiddle extends DescriptorItem(Extension, "in the middle")
  object Bottom extends DescriptorItem(Extension, "bottom")
  object Peak extends DescriptorItem(Extension, "peak")
  object Down extends DescriptorItem(Extension, "down")
  object Main extends DescriptorItem(Extension, "main")
  object Area extends DescriptorItem(Extension, "area")
  object ComingOut extends DescriptorItem(Extension, "coming out")
  object Turning extends DescriptorItem(Extension, "turning")
  object AllAround extends DescriptorItem(Extension, "all around")
  object Crafted extends DescriptorItem(Extension, "crafted")
  object GoingUp extends DescriptorItem(Extension, "going up")
  object GoingDown extends DescriptorItem(Extension, "going down")
  object Ahead extends DescriptorItem(Extension, "ahead")
  object ThisWay extends DescriptorItem(Extension, "this way")

  object Gets extends DescriptorItem(Verb, "gets")
  object Continues extends DescriptorItem(Verb, "continues")
  object Looks extends DescriptorItem(Verb, "looks")
  object Feels extends DescriptorItem(Verb, "feels")
  object Grows extends DescriptorItem(Verb, "grows")
  object Starts extends DescriptorItem(Verb, "starts")
  object ToBeA extends DescriptorItem(Verb, "to be a")
  object ReachesThe extends DescriptorItem(Verb, "reaches the")
  object Are extends DescriptorItem(Verb, "are")
  object Is extends DescriptorItem(Verb, "is")

  object IntoA extends DescriptorItem(Relation, "IntoA")
  object Here extends DescriptorItem(Relation, "here")
  object FormingA extends DescriptorItem(Relation, "forming")
  object SankInA extends DescriptorItem(Relation, "sank in")
  object CoveredIn extends DescriptorItem(Relation, "covered in")
  object ByA extends DescriptorItem(Relation, "by a")
  object ByThe extends DescriptorItem(Relation, "by the")
  object OfA extends DescriptorItem(Relation, "of a")
  object OfThe extends DescriptorItem(Relation, "of a")
  object BehindA extends DescriptorItem(Relation, "behind a")
  object BehindThe extends DescriptorItem(Relation, "behind the")
  object OverThe extends DescriptorItem(Relation, "over the")
  object OverA extends DescriptorItem(Relation, "over a")
  object AcrossThe extends DescriptorItem(Relation, "across the")
  object AcrossA extends DescriptorItem(Relation, "across a")
  object InA extends DescriptorItem(Relation, "in a")
  object InAn extends DescriptorItem(Relation, "in an")
  object InThe extends DescriptorItem(Relation, "in the")
  object ThroughThe extends DescriptorItem(Relation, "through the")
  object ThroughA extends DescriptorItem(Relation, "through a")
  object OnA extends DescriptorItem(Relation, "on a")
  object OnThe extends DescriptorItem(Relation, "on the")
  object On extends DescriptorItem(Relation, "on")
  object AtA extends DescriptorItem(Relation, "at a")
  object AtThe extends DescriptorItem(Relation, "at the")
  object ToThe extends DescriptorItem(Relation, "to the")
  object ToA extends DescriptorItem(Relation, "to a")
  object WithA extends DescriptorItem(Relation, "with a")
  object With extends DescriptorItem(Relation, "with")

  object There extends DescriptorItem(Connector, "There")
  object ThereIsA extends DescriptorItem(Connector, "There is a")
  object ThereAlsoIsA extends DescriptorItem(Connector, "There also is a")
  object It extends DescriptorItem(Connector, "It")
  object ItSeemsToGet extends DescriptorItem(Connector, "It seems to get")
  object The extends DescriptorItem(Connector, "The")
  object Place extends DescriptorItem(Connector, "Place")

}

