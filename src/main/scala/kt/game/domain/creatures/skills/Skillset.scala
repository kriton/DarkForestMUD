package kt.game.domain.creatures.skills

object Skillset {
  implicit class BonusToRanks(x: Int) {
    def ranks() = if (x > 0) Math.pow((x + 1.0) * 2.0 / 5.0, 2).ceil.toInt else x
  }
  def fromBonus(athl: Int, ball: Int, craf: Int, infl: Int, perc: Int, refl: Int, weap: Int, will: Int) = create(athl.ranks, ball.ranks, craf.ranks, infl.ranks, perc.ranks, refl.ranks, weap.ranks, will.ranks)
  def zero: Skillset = create(0, 0, 0, 0, 0, 0, 0, 0)
  def create(v: Array[Int]): Skillset = create(v(0), v(1), v(2), v(3), v(4), v(5), v(6), v(7))
  def create(athl: Int, ball: Int, craf: Int, infl: Int, perc: Int, refl: Int, weap: Int, will: Int): Skillset = new Skillset(Athletics(athl), Ballistics(ball), Craft(craf), Influence(infl), Perception(perc), Reflexes(refl), Weapons(weap), Willpower(will))
  def toQuickString(skillset: Skillset) = "(" + skillset.athletics.ranks + ", " + skillset.ballistics.ranks + ", " + skillset.craft.ranks + ", " + skillset.influence.ranks + ", " + skillset.perception.ranks + ", " + skillset.reflexes.ranks + ", " + skillset.weapons.ranks + ", " + skillset.willpower.ranks + ")"
  //  def toQuickBonuseString(skillset: Skillset) = "(" + skillset.athletics.bonus + ", " + skillset.ballistics.bonus + ", " + skillset.craft.bonus + ", " + skillset.influence.bonus + ", " + skillset.perception.bonus + ", " + skillset.reflexes.bonus + ", " + skillset.weapons.bonus + ", " + skillset.willpower.bonus + ")"

  implicit def toPersistableString(skillset: Skillset): String = toQuickString(skillset)
  implicit def fromPersistableString(string: String): Skillset = create(string.stripPrefix("(").stripSuffix(")").split(", ").map { _.toInt })
}

case class Skillset(athletics: Athletics,
                    ballistics: Ballistics,
                    craft: Craft,
                    influence: Influence,
                    perception: Perception,
                    reflexes: Reflexes,
                    weapons: Weapons,
                    willpower: Willpower) {
  import SkillName._

  override def toString = "ATHL( " + athletics.bonus + " ), BALL( " + ballistics.bonus + " ), CRAF( " + craft.bonus + " ), INFL( " + influence.bonus + " ), PERC( " + perception.bonus + " ), REFL( " + reflexes.bonus + " ), WEAP( " + weapons.bonus + " ),  WILL ( " + willpower.bonus + " )"

  def toNonZerosString = {
    val s: StringBuilder = StringBuilder.newBuilder
    if (athletics.bonus != 0) s ++= "ATHL( " + athletics.ranks + " ) "
    if (ballistics.bonus != 0) s ++= "BALL( " + ballistics.ranks + " ) "
    if (craft.bonus != 0) s ++= "CRAF( " + craft.ranks + " ) "
    if (influence.bonus != 0) s ++= "INFL( " + influence.ranks + " ) "
    if (perception.bonus != 0) s ++= "PERC( " + perception.ranks + " ) "
    if (reflexes.bonus != 0) s ++= "REFL( " + reflexes.ranks + " ) "
    if (weapons.bonus != 0) s ++= "WEAP( " + weapons.ranks + " ) "
    if (willpower.bonus != 0) s ++= "WILL( " + willpower.ranks + " ) "
    s.toString()
  }

  def bonusSum() = athletics.bonus + ballistics.bonus + craft.bonus + influence.bonus + perception.bonus + reflexes.bonus + weapons.bonus + willpower.bonus
  def ranksSum() = athletics.ranks + ballistics.ranks + craft.ranks + influence.ranks + perception.ranks + reflexes.ranks + weapons.ranks + willpower.ranks

  def skill(skill: SkillName) = skill match {
    case ATHL => athletics
    case BALL => ballistics
    case CRAF => craft
    case INFL => influence
    case PERC => perception
    case REFL => reflexes
    case WEAP => weapons
    case WILL => willpower
  }

  def increment(skill: SkillName.SkillName): Skillset = {
    import SkillName._
    skill match {
      case ATHL =>
        Skillset(Athletics(athletics.ranks + 1), ballistics, craft, influence, perception, reflexes, weapons, willpower)
      case BALL =>
        Skillset(athletics, Ballistics(ballistics.ranks + 1), craft, influence, perception, reflexes, weapons, willpower)
      case CRAF =>
        Skillset(athletics, ballistics, Craft(craft.ranks + 1), influence, perception, reflexes, weapons, willpower)
      case INFL =>
        Skillset(athletics, ballistics, craft, Influence(influence.ranks + 1), perception, reflexes, weapons, willpower)
      case PERC =>
        Skillset(athletics, ballistics, craft, influence, Perception(perception.ranks + 1), reflexes, weapons, willpower)
      case REFL =>
        Skillset(athletics, ballistics, craft, influence, perception, Reflexes(reflexes.ranks + 1), weapons, willpower)
      case WEAP =>
        Skillset(athletics, ballistics, craft, influence, perception, reflexes, Weapons(weapons.ranks + 1), willpower)
      case WILL =>
        Skillset(athletics, ballistics, craft, influence, perception, reflexes, weapons, Willpower(willpower.ranks + 1))
      case _ => this
    }
  }

  def assign(skill: SkillName.SkillName, newValue: Int): Skillset = {
    import SkillName._
    skill match {
      case ATHL =>
        Skillset(Athletics(newValue), ballistics, craft, influence, perception, reflexes, weapons, willpower)
      case BALL =>
        Skillset(athletics, Ballistics(newValue), craft, influence, perception, reflexes, weapons, willpower)
      case CRAF =>
        Skillset(athletics, ballistics, Craft(newValue), influence, perception, reflexes, weapons, willpower)
      case INFL =>
        Skillset(athletics, ballistics, craft, Influence(newValue), perception, reflexes, weapons, willpower)
      case PERC =>
        Skillset(athletics, ballistics, craft, influence, Perception(newValue), reflexes, weapons, willpower)
      case REFL =>
        Skillset(athletics, ballistics, craft, influence, perception, Reflexes(newValue), weapons, willpower)
      case WEAP =>
        Skillset(athletics, ballistics, craft, influence, perception, reflexes, Weapons(newValue), willpower)
      case WILL =>
        Skillset(athletics, ballistics, craft, influence, perception, reflexes, weapons, Willpower(newValue))
      case _ => this
    }
  }
}

object SkillName extends Enumeration {
  type SkillName = Value
  val ATHL = Value("Athletics")
  val BALL = Value("Ballistics")
  val CRAF = Value("Craft")
  val INFL = Value("Influence")
  val PERC = Value("Perception")
  val REFL = Value("Reflexes")
  val WEAP = Value("Weapons")
  val WILL = Value("Willpower")
  implicit def SkillName2String(skill: SkillName) = skill.toString
}

import SkillName._

abstract class Skill(val name: SkillName) extends Serializable {
  def ranks: Int
  def bonus: Int = if (ranks > 0) {
    (Math.sqrt(ranks) * 5.0 / 2.0).toInt - 1
  } else ranks

  def passive: Int = bonus + 10
}

case class Athletics(ranks: Int) extends Skill(ATHL)
case class Ballistics(ranks: Int) extends Skill(BALL)
case class Craft(ranks: Int) extends Skill(CRAF)
case class Influence(ranks: Int) extends Skill(INFL)
case class Perception(ranks: Int) extends Skill(PERC)
case class Reflexes(ranks: Int) extends Skill(REFL)
case class Weapons(ranks: Int) extends Skill(WEAP)
case class Willpower(ranks: Int) extends Skill(WILL)
