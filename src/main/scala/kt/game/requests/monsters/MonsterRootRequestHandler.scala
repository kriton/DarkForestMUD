package kt.game.requests.talents

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import kt.game.events.CombatEvent
import kt.game.utility.Result
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map

class MonsterRootRequestHandler private (monster: ActorRef, spottedCreatures: Set[ActorRef], currentLocation: ActorRef, roll: Int) extends Actor {
  import MonsterRootRequestHandler._
  import context._

  setReceiveTimeout(200 milliseconds)

  implicit val timeout = Timeout(120 milliseconds)

  val inLocation = ListBuffer[ActorRef]()

  spottedCreatures foreach { _ ! Creature.GetCurrentLocation() }

  def receive = {
    case Some(x: ActorRef) if x == currentLocation => inLocation += sender
    case ReceiveTimeout => {
      inLocation foreach { _.tell(Creature.AffectSkill(SkillName.ATHL, roll, Seq(SkillName.ATHL, SkillName.REFL), -15, (12 seconds).fromNow, "Rooted"), monster) }
      self ! PoisonPill
    }
  }
}

object MonsterRootRequestHandler {
  def props(monster: ActorRef, spottedCreatures: Set[ActorRef], currentLocation: ActorRef, roll: Int) = Props(new MonsterRootRequestHandler(monster, spottedCreatures, currentLocation, roll))
}