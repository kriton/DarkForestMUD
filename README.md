Dark Forest MUD TODOs
=====================


--> PHASE 1

PRIO ONE
- Test the world / play some
- Travel and explore commands; dynamic content.
- All logged should be logged on a proper logger on the log Buffer actor

PRIO TWO
- Monster AIS:
  - Terrorizer: Intimidates all spotted creatures on a high cd and Intimidates aggroed creature on a low cd
  - Protector or On Defender: Protects other monsters in its area.
  - Leader: Calls other monsters it commands (?) to follow him and join fights

PRIO THREE
- in UsedIn-YourArea messages, log a message if creature and feature were spotted
- Serialize everything to json
- Split Connectors from main server. 2 apps.
- Guilds and housing
- Quests ?
- Monster Factions and Reputation
- Guide command

PRIO FOUR
- Game master additions (kick chas & ban chars/ips, free up names)
- More talents:
  - Bladesong (WEAP 3): Use the craft, befriend, hide or ranged attack actions with your weapons skill, albeit with a penalty (maybe -4?)
  - shield (WILL 1): gain a bonus on your passive weapons
  - invisibility (REFL 2): hide with an immense bonus (like 100) and remain hidden after move for a duration. high cooldown.
  - truesight (PERC 3): requires perception. gain a super high bonus on perception and ignore locations with nonvisible creatures, treat them as normal.
  - curse (INFL 2): grant a penalty and misfortune on a selected skill on a crature.
  - Mark (BALL 1): Mark a target. You thereafter always succeed on your spot actions against him and he cannot ever hide from you for the duration.
  - Summon (CRAF 2): Summon a monster to fight for you (2 versions with special AIs, will either defend you or attack your target; or regular defensive or aggresive and befriended to you from the start).
  - Might (ATHL 3): Boost the Athletics, Willpower, Ballistics and Weapons skills of a creature (but not your own).

PRIO FIVE
- Enrich all location/path descriptions
- Advanced steb by step Character Creation with possible char descriptions
- Thief AI (attempts to steal from chars)
- Sneak AI (attempts to hide from chars)
- More Locations!!
- Greatly Improve All text descriptions, including intro



==================================================================================================================================

--> PHASE 2

- GFX


==================================================================================================================================

--> TO THINK ABOUT IF NEEDED

- Refactor: favor immutable var collections over mutable val wherever possible.
- Locations that were spotted recently are easy to spot again
- Weather / Mist
- silent mode. You are not listed under the online listing of players and players cannot whisper to you unless you whisper to them first
- Dockerize
- MUD color and stuff see http://cryosphere.net/mud-protocol.html


