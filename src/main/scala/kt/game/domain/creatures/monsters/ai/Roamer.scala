package kt.game.domain.creatures.monsters.ai

import scala.collection.mutable.Set
import scala.concurrent.duration.DurationInt
import scala.util.Random
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.monsters.Behaviour
import kt.game.domain.creatures.monsters.Monster
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.creatures.MonsterAction

trait Roamer extends Monster with Behaviour {
  import context._

  def roamFrequency = Random.nextInt(20) + 15 seconds

  var allowedLocationsLimit = 5
  val allowedLocations: Set[ActorRef] = Set()

  def updateRoamSchedule() = system.scheduler.scheduleOnce(roamFrequency, self, Roamer.Roam())
  allSchedules += updateRoamSchedule

  override def detailedDescription = "Looks to be roaming around." + (if (super.detailedDescription != "") " " + super.detailedDescription else "")

  def roam() = {
    if (observedCache.size > 0 && allowedLocations.size < allowedLocationsLimit)
      self ! Creature.MoveCommand((Random.nextInt(observedCache.size) + 1).toString())
    else if (observedCache.size > 0) {
      val filteredCache = observedCache.filter(allowedLocations contains _._2._1).toSeq
      if (!filteredCache.isEmpty) self ! Creature.MoveCommand(filteredCache(Random.nextInt(filteredCache.size))._1)
      else spot()
    } else spot()
  }

  override def moved(location: ActorRef, description: Descriptor) = {
    allowedLocations += location
    super.moved(location, description)
  }

  override def reset() = {
    allowedLocations.clear()
    super.reset()
  }

  override def position(location: ActorRef) = {
    allowedLocations.clear()
    super.position(location)
  }

  override def receive = ({
    case Roamer.Roam() => {
      updateRoamSchedule()
      if (actionCheck(MonsterAction.Roam) && aggro.isEmpty) roam()
    }
    case Roamer.UpdateLocationLimit(value) => allowedLocationsLimit = value
  }: Receive) orElse super.receive

}

object Roamer {
  case class Roam()
  case class UpdateLocationLimit(value: Int)
}