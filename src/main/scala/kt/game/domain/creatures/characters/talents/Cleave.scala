package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.Weapons
import kt.game.events.ActionFailedEvent
import kt.game.requests.talents.CleaveRequestHandler
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.events.ActionOnCoolDownEvent
import kt.game.domain.creatures.Action

class Cleave private (val character: ActorRef) extends Actor with Talent {
  import Cleave._
  import context._

  val name = Cleave.name
  val description = Cleave.description
  val help = Cleave.commandsHelp

  implicit val timeout = Timeout(200 milliseconds)

  def setCD() = character ! Character.SetActionCooldown(action, s => ((40 - s.weapons.bonus * 2).max(8) seconds).fromNow)

  def cleaveAttempt(targets: Option[Seq[String]]) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      setCD()
      context.actorOf(CleaveRequestHandler.props(character, targets))
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentCleaveCommandToAll() => cleaveAttempt(None)
    case TalentCleaveCommand(refs)  => cleaveAttempt(Some(refs))
    case CleaveFailed()             => character ! Character.SetActionCooldown(action, s => Deadline.now);
    case CleaveSuccess()            => setCD()
  }
}

object Cleave extends TalentContext {
  def props(character: ActorRef) = Props(new Cleave(character))

  case class TalentCleaveCommand(refs: Seq[String])
  case class TalentCleaveCommandToAll()
  case class CleaveSuccess()
  case class CleaveFailed()

  val skill = Weapons(4)

  val action = TalentAction.Cleave
  val name = action.toString

  val description = "You can attack multiple target creatures in your area at once. The frequency that you can do so, depends on your weapons skill."
  val trainingDescription = "You can train your arcane skills and get access to cleaving techniques! " + requirementsString

  val TCP_cleaveAll = "^cleave$".r
  val TCP_cleaveSelected = "^cleave (\\d+(?: \\d+)*)$".r

  val commandsHelp =
    ("%-32s" format "cleave <target1> <target2> etc") + " -->  Attack target creatures that are in your area.\r\n" +
      ("%-32s" format "cleave") + " -->  Make a melee attack against each spotted target in your area.\r\n"
}