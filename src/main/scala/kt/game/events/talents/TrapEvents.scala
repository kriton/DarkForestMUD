package kt.game.events.talents

import kt.game.events.ActionFailedReason
import kt.game.events.CombatEvent
import kt.game.events.ActionFailedEvent

object TrapEvents {

  case class TrapFailedEvent(reason: ActionFailedReason) extends CombatEvent with ActionFailedEvent {
    import kt.game.events.ActionFailedReason._
    override def toString = reason match {
      case CanNoLongerSpot => "Trap Failed! Can no longer see the locaiton you chose to place the trap!"
      case InvalidState    => "Trap Failed! You failed to move to the location to place the trap!"
      case _               => "Trap placement Failed!"
    }
  }

}