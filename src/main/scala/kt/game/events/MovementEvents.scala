package kt.game.events

import akka.actor.ActorRef
import kt.game.domain.locations.descriptors.Descriptor
import kt.game.domain.creatures.Target

case class MovementSuccesfulEvent(location: ActorRef, description: Descriptor, fromLocation: ActorRef) extends MovementEvent {
  override def toString = "You moved succesfully!"
}

case class MovementFailedEvent(toLocation: ActorRef, reason: MovementFailedReason, fromLocation: Option[ActorRef]) extends MovementEvent with ActionFailedEvent {
  import kt.game.events.MovementFailedReason._
  override def toString = reason match {
    case CanNoLongerObserve => "You cannot find the path to this area any more..."
    case AlreadyThere       => "You are already there..."
    case CheckFailure       => "You failed to move."
    case ScaryLocation      => "Location is too scary for you to enter!"
  }
}

case class FollowingAnotherCreatureEvent(target: Target) extends MovementEvent {
  override def toString = s"You are now following $target"
}

case class FollowFailedEvent(target: Target, reason: ActionFailedReason) extends MovementEvent with ActionFailedEvent {
  import ActionFailedReason._
  override def toString = reason match {
    case CanNoLongerSpot => s"You can no longet spot $target" 
    case _               => "You were not following anybody!"
  }
}

case class StoppedFollowingAnotherCreatureEvent(target: Target) extends MovementEvent {
  override def toString = s"You are no longer following $target"
}

case class HiddenSuccessfulyEvent(hiddenDC: Int) extends MovementEvent {
  override def toString = "You tried to hide from creatures that have seen you. Hopefully you did hide succesfully!"
}

case object NoLongerHiddenEvent extends MovementEvent {
  override def toString = "You are no longer hidden."
}

sealed trait MovementFailedReason

object MovementFailedReason {
  case object CanNoLongerObserve extends MovementFailedReason
  case object AlreadyThere extends MovementFailedReason
  case object CheckFailure extends MovementFailedReason
  case object ScaryLocation extends MovementFailedReason
}