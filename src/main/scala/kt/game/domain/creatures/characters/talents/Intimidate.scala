package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.skills.Weapons
import kt.game.events.ActionFailedEvent
import kt.game.requests.talents.IntimidateRequestHandler
import kt.game.domain.creatures.skills.Influence
import kt.game.domain.creatures.TalentAction
import kt.game.domain.creatures.characters.Character
import kt.game.events.ActionFailedEvent
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.ActionFailedEvent

class Intimidate private (val character: ActorRef) extends Actor with Talent {
  import Intimidate._
  import context._

  val name = Intimidate.name
  val description = Intimidate.description
  val help = Intimidate.commandsHelp

  implicit val timeout = Timeout(200 milliseconds)

  def setCD() = character ! Character.SetActionCooldown(action, s => ((115 - s.influence.bonus * 3).max(45) seconds).fromNow)

  var skillModifier: Option[Int] = None

  def IntimidateAttempt(targets: Option[Seq[String]]) {
    if (skillModifier == None) skillModifier = Await.result(character ? Creature.GetFullSkillModifier(SkillName.INFL), timeout.duration).asInstanceOf[Option[Int]]
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      setCD()
      context.actorOf(IntimidateRequestHandler.props(character, targets, (10 + 2 * skillModifier.get) seconds))
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def receive = {
    case TalentIntimidateCommandToAll() => IntimidateAttempt(None)
    case TalentIntimidateCommand(refs)  => IntimidateAttempt(Some(refs))
    case IntimidateFailed()             => character ! Character.SetActionCooldown(action, s => Deadline.now)
    case IntimidateSuccess()            => setCD()
  }
}

object Intimidate extends TalentContext {
  def props(character: ActorRef) = Props(new Intimidate(character))

  case class TalentIntimidateCommand(refs: Seq[String])
  case class TalentIntimidateCommandToAll()
  case class IntimidateSuccess()
  case class IntimidateFailed()

  val skill = Influence(4)

  val effectName = "intimidated"

  val action = TalentAction.Intimidate
  val name = action.toString

  val description = "You can frighten creatures, penalizing their skills. The frequency that you can do so, depends on your weapons skill."
  val trainingDescription = "You can train your arcane skills and get access to the secrets of instilling terror to your foes! " + requirementsString

  val TCP_intimidateAll = "^intimidate$".r
  val TCP_intimidateSelected = "^intimidate (\\d+(?: \\d+)*)$".r

  val commandsHelp =
    ("%-32s" format "Intimidate <target1> <target2> etc") + " -->  Intimidate target creature(s) that are in your area, penalizing all their skills.\r\n" +
      ("%-32s" format "Intimidate") + " -->  Intimidate each spotted target in your area, penalizing all their skills.\r\n"
}