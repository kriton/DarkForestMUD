package kt.game.domain.creatures.characters.talents

import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Skill
import kt.game.events.ActionFailedEvent
import kt.game.events.DevelopmentEvent
import kt.game.events.LoggedEvent
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import kt.game.domain.creatures.characters.Character
import scala.collection.immutable.ListSet
import kt.game.domain.creatures.Action
import kt.game.domain.creatures.TalentAction
import kt.game.events.DevelopmentEvents.CanNotLearnTalentEvent
import kt.game.events.UnrecognizedCommandEvent

trait Talent {
  def help: String
}

object Talent {
  import kt.game.player.Commands._
  implicit def splitStringToSeq(s: String) = s.split(" ").map(_.trim.stripZeros).toSeq

  implicit class containedTalent(talent: TalentContext) {
    def in(talents: ListSet[(ActorRef, String, String)]) = !talents.filter(_._1.path.name == talent.name).isEmpty
  }

  def parse(command: String, c: Character) = command match {
    case Heal.TCP_healself() if Heal in c.talents                           => c.talents.filter(_._1.path.name == Heal.name).head._1 ! Heal.TalentHealSelfCommand()
    case Heal.TCP_healother(ref) if Heal in c.talents                       => c.talents.filter(_._1.path.name == Heal.name).head._1 ! Heal.TalentHealCommand(ref.stripZeros)
    case Study.TCP_studyother(ref) if Study in c.talents                    => c.talents.filter(_._1.path.name == Study.name).head._1 ! Study.TalentStudyCommand(ref.stripZeros)
    case Stun.TCP_stunother(ref) if Stun in c.talents                       => c.talents.filter(_._1.path.name == Stun.name).head._1 ! Stun.TalentStunCommand(ref.stripZeros)
    case Manyshot.TCP_manyshot(ref) if Manyshot in c.talents                => c.talents.filter(_._1.path.name == Manyshot.name).head._1 ! Manyshot.TalentManyshotCommand(ref.stripZeros)
    case Volley.TCP_volleyAll() if Volley in c.talents                      => c.talents.filter(_._1.path.name == Volley.name).head._1 ! Volley.TalentVolleyCommandToAll()
    case Volley.TCP_volleySelected(refs) if Volley in c.talents             => c.talents.filter(_._1.path.name == Volley.name).head._1 ! Volley.TalentVolleyCommand(refs.trim)
    case Cleave.TCP_cleaveAll() if Cleave in c.talents                      => c.talents.filter(_._1.path.name == Cleave.name).head._1 ! Cleave.TalentCleaveCommandToAll()
    case Cleave.TCP_cleaveSelected(refs) if Cleave in c.talents             => c.talents.filter(_._1.path.name == Cleave.name).head._1 ! Cleave.TalentCleaveCommand(refs.trim)
    case Create.TCP_create(ref) if Create in c.talents                      => c.talents.filter(_._1.path.name == Create.name).head._1 ! Create.TalentCreateCommand(ref.trim)
    case Trap.TCP_trap(ref) if Trap in c.talents                            => c.talents.filter(_._1.path.name == Trap.name).head._1 ! Trap.TalentTrapCommand(ref.stripZeros)
    case Trap.TCP_trapWithComponent(ref1, ref2) if Trap in c.talents        => c.talents.filter(_._1.path.name == Trap.name).head._1 ! Trap.TalentTrapWithComponentCommand(ref1.stripZeros, ref2.stripZeros)
    case Meditate.TCP_meditate() if Meditate in c.talents                   => c.talents.filter(_._1.path.name == Meditate.name).head._1 ! Meditate.TalentMeditateCommand()
    case Meditate.TCP_refocus() if Meditate in c.talents                    => c.talents.filter(_._1.path.name == Meditate.name).head._1 ! Meditate.TalentMeditateReFocusCommand()
    case Intimidate.TCP_intimidateAll() if Intimidate in c.talents          => c.talents.filter(_._1.path.name == Intimidate.name).head._1 ! Intimidate.TalentIntimidateCommandToAll()
    case Intimidate.TCP_intimidateSelected(refs) if Intimidate in c.talents => c.talents.filter(_._1.path.name == Intimidate.name).head._1 ! Intimidate.TalentIntimidateCommand(refs.trim)
    case Revive.TCP_cleanse(ref) if Revive in c.talents                     => c.talents.filter(_._1.path.name == Revive.name).head._1 ! Revive.TalentCleanseCommand(ref.stripZeros)
    case Revive.TCP_ressurect(ref) if Revive in c.talents                   => c.talents.filter(_._1.path.name == Revive.name).head._1 ! Revive.TalentRessurectCommand(ref.stripZeros)
    case Dominate.TCP_dominate(ref) if Dominate in c.talents                => c.talents.filter(_._1.path.name == Dominate.name).head._1 ! Dominate.TalentDominateCommand(ref.stripZeros)
    case Dominate.TCP_thrall(command) if Dominate in c.talents              => c.talents.filter(_._1.path.name == Dominate.name).head._1 ! Dominate.TalentThrallCommand(command)
    case Scry.TCP_scry(ref) if Scry in c.talents                            => c.talents.filter(_._1.path.name == Scry.name).head._1 ! Scry.TalentScryCommand(ref.stripZeros)
    case Scry.TCP_scryStop() if Scry in c.talents                           => c.talents.filter(_._1.path.name == Scry.name).head._1 ! Scry.TalentScryStopCommand()
    case Smite.TCP_smite(ref) if Smite in c.talents                         => c.talents.filter(_._1.path.name == Smite.name).head._1 ! Smite.TalentSmiteCommand(ref.stripZeros)
    case Teleport.TCP_teleport(ref) if Teleport in c.talents                => c.talents.filter(_._1.path.name == Teleport.name).head._1 ! Teleport.TalentTeleportCommand(ref.stripZeros)
    case Teleport.TCP_anchor() if Teleport in c.talents                     => c.talents.filter(_._1.path.name == Teleport.name).head._1 ! Teleport.TalentAnchorCommand()
    case Teleport.TCP_teleportToAnchor() if Teleport in c.talents           => c.talents.filter(_._1.path.name == Teleport.name).head._1 ! Teleport.TalentTeleportToAnchorCommand()
    case Teleport.TCP_anchorInfo() if Teleport in c.talents                 => c.talents.filter(_._1.path.name == Teleport.name).head._1 ! Teleport.TalentAnchorInfoCommand()
    case Daze.TCP_dazeAll() if Daze in c.talents                            => c.talents.filter(_._1.path.name == Daze.name).head._1 ! Daze.TalentDazeCommandToAll()
    case Daze.TCP_dazeSelected(refs) if Daze in c.talents                   => c.talents.filter(_._1.path.name == Daze.name).head._1 ! Daze.TalentDazeCommand(refs.trim)
    case _                                                                  => c.log(LoggedEvent(UnrecognizedCommandEvent)(c.self))
  }

  def fromName(name: String) = name match {
    case Heal.name       => Some(Heal)
    case Scry.name       => Some(Scry)
    case Study.name      => Some(Study)
    case Stun.name       => Some(Stun)
    case Dominate.name   => Some(Dominate)
    case Manyshot.name   => Some(Manyshot)
    case Volley.name     => Some(Volley)
    case Cleave.name     => Some(Cleave)
    case Create.name     => Some(Create)
    case Trap.name       => Some(Trap)
    case Meditate.name   => Some(Meditate)
    case Intimidate.name => Some(Intimidate)
    case Revive.name     => Some(Revive)
    case Smite.name      => Some(Smite)
    case Teleport.name   => Some(Teleport)
    case Daze.name       => Some(Daze)
    case _               => None
  }
}

trait TalentContext {
  def conditions(character: Character) = character.skillset.skill(skill.name).ranks >= skill.ranks && character.XP >= xp

  def xp = 5
  def skill: Skill
  def props(character: ActorRef): Props

  def action: TalentAction
  def name: String

  def description: String
  def requirementsString = "It requires " + xp + "xp and a " + skill + " skill."

  def learn(character: Character) = if (conditions(character)) {
    character acquire (props(character.self), xp, description, name.toString, commandsHelp)
    true
  } else {
    character.log(LoggedEvent(CanNotLearnTalentEvent(xp, skill))(character.self))
    false
  }
  def commandsHelp: String
  def trainingDescription: String
}

