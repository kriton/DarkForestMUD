package kt.game.requests.talents

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import scala.concurrent.duration.DurationInt
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Await
import kt.game.domain.creatures.Creature
import akka.actor.ReceiveTimeout
import kt.game.domain.creatures.characters.talents.Cleave
import akka.actor.PoisonPill
import kt.game.domain.creatures.skills.SkillName
import kt.game.domain.creatures.AttackType
import kt.game.events.MultiAttackImpossibleEvent
import kt.game.events.AttackImpossibleReason

class CleaveRequestHandler private (character: ActorRef, targets: Option[Seq[String]]) extends Actor {
  import CleaveRequestHandler._
  import context._

  setReceiveTimeout(300 milliseconds)

  var roll: Option[Int] = None

  implicit val timeout = Timeout(120 milliseconds)
  val creatures = Await.result(character ? Creature.GetSpottedTargets(targets), timeout.duration).asInstanceOf[Option[Seq[ActorRef]]]
  val location = Await.result(character ? Creature.GetCurrentLocation(), timeout.duration).asInstanceOf[Option[ActorRef]]

  if (creatures != None && location != None) {
    creatures.get.foreach { _ ! Creature.CanMeleeAttackFrom(character, location.get, AttackType.Melee) }
  } else {
    parent ! Cleave.CleaveFailed()
    character ! Creature.Message(MultiAttackImpossibleEvent(AttackType.Melee, AttackImpossibleReason.CanNoLongerSpot))
  }

  def receive = {
    case Creature.MeleeAttackPossible(creature, _) if roll == None => { roll = Await.result(character ? Creature.Roll(SkillName.WEAP), timeout.duration).asInstanceOf[Option[Int]]; creature.tell(Creature.Attacked(roll.get, location.get, AttackType.Melee), character) }
    case Creature.MeleeAttackPossible(creature, _)                 => creature.tell(Creature.Attacked(roll.get, location.get, AttackType.Melee), character)
    case Creature.MeleeAttackNotPossible(_, _)                     =>
    case ReceiveTimeout if roll == None                            => { parent ! Cleave.CleaveFailed(); character ! Creature.Message(MultiAttackImpossibleEvent(AttackType.Melee, AttackImpossibleReason.CanNoLongerSpot)); self ! PoisonPill }
    case ReceiveTimeout                                            => self ! PoisonPill
  }
}

object CleaveRequestHandler {
  def props(character: ActorRef, targets: Option[Seq[String]]) = Props(new CleaveRequestHandler(character, targets))
}