package kt.game.utility

import scala.util.Random

object Dice {
  def d20 = Random.nextInt(20) + 1
  def d20(bonus: Int) = Random.nextInt(20) + 1 + bonus
}