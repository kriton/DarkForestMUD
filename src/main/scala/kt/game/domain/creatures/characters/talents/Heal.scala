package kt.game.domain.creatures.characters.talents

import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.domain.creatures.Creature
import kt.game.domain.creatures.characters.Character
import kt.game.domain.creatures.skills.Athletics
import kt.game.domain.creatures.skills.SkillName
import kt.game.events.ActionFailedEvent
import kt.game.events.NonCombatEvent
import kt.game.domain.creatures.TalentAction
import kt.game.events.talents.HealEvents.HealSucceededEvent
import kt.game.events.talents.HealEvents.HealFailedEvent
import kt.game.events.talents.HealEvents.HealFailureReason
import kt.game.events.ActionOnCoolDownEvent
import kt.game.events.LoggedEvent

class Heal private (val character: ActorRef) extends Actor with Talent {
  import Heal._

  val name = Heal.name
  val description = Heal.description
  val help = Heal.commandsHelp

  def setCD() = character ! Character.SetActionCooldown(action, s => ((300 - s.athletics.bonus * 10).max(120) seconds).fromNow)

  implicit val timeout = Timeout(200 milliseconds)

  def healAttempt(target: Option[String]) {
    val cooldown = Await.result(character ? Character.GetActionCooldown(action), timeout.duration).asInstanceOf[Option[Deadline]]
    if (cooldown == None || cooldown.get.isOverdue) {
      if (target == None) {
        healCheck(None, None, None)
      } else {
        val creature = Await.result(character ? Creature.GetSpottedTarget(target.get), timeout.duration).asInstanceOf[Option[ActorRef]]
        if (creature != None) {
          val targetLocation = Await.result(creature.get ? Creature.GetCurrentLocation(), timeout.duration).asInstanceOf[Option[ActorRef]]
          val charLocation = Await.result(character ? Creature.GetCurrentLocation(), timeout.duration).asInstanceOf[Option[ActorRef]]
          healCheck(creature, targetLocation, charLocation)
        } else {
          character ! Creature.Message(HealFailedEvent(HealFailureReason.CanNoLongerSpot))
        }
      }
    } else {
      character ! Creature.Message(ActionOnCoolDownEvent(Some(action), cooldown.get.timeLeft.toSeconds))
    }
  }

  def healCheck(target: Option[ActorRef], targetLocation: Option[ActorRef], charLocation: Option[ActorRef]) = {
    if (target == None) {
      healSuccess(character)
    } else if (targetLocation != None && targetLocation.get == charLocation.get) {
      healSuccess(target.get)
    } else {
      character ! Creature.Message(HealFailedEvent(HealFailureReason.TooFarAway))
    }
  }

  def healSuccess(target: ActorRef) = {
    if (Await.result(character ? Creature.GetFatePoints(), timeout.duration).asInstanceOf[Int] >= 2) {
      character ! Creature.AddFatePoints(-2)
      if (Await.result(character ? Creature.Roll(SkillName.ATHL), timeout.duration).asInstanceOf[Option[Int]].get >= DC) {
        target tell (Creature.Heal(100), character)
        character ! Creature.Message(HealSucceededEvent)
        setCD()
      } else {
        character ! Creature.Message(HealFailedEvent(HealFailureReason.CheckFailure))
      }
    } else {
      character ! Creature.Message(HealFailedEvent(HealFailureReason.NotEnoughFatePoints))
    }
  }

  def receive = {
    case TalentHealCommand(ref)  => healAttempt(Some(ref))
    case TalentHealSelfCommand() => healAttempt(None)
  }
}

object Heal extends TalentContext {
  def props(character: ActorRef) = Props(new Heal(character))

  case class TalentHealCommand(ref: String)
  case class TalentHealSelfCommand()

  val DC = 20

  val skill = Athletics(8)

  val action = TalentAction.Heal
  val name = action.toString

  val description = "You can heal yourself or other creatures. The frequency that you can do, depends on your craft skill and a single use requires 2 fate points."
  val trainingDescription = "You can train your arcane skills and get access to healing powers here! " + requirementsString

  val TCP_healself = "^healself$".r
  val TCP_healother = "^heal (\\d+)$".r

  val commandsHelp =
    ("%-32s" format "healself") + " -->  Heal yourself to maximum help.\r\n" +
      ("%-32s" format "heal <target>") + " -->  Heal target creature.\r\n"
}