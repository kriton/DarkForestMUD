package kt.game.system

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStream
import java.net.Socket
import java.util.Date
import java.util.concurrent.ArrayBlockingQueue
import scala.concurrent.Await
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.DurationInt
import Connection.Initialize
import Connection.Input
import Connection.KeepAlive
import Connection.Selfdestruct
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.NotInfluenceReceiveTimeout
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import kt.game.player.Commands
import kt.game.player.Player
import kt.game.player.PlayerRegistry
import kt.game.requests.ListRequestHandler
import kt.game.requests.ReplyRequestHandler
import kt.game.requests.ShoutRequestHandler
import kt.game.requests.TellAllRequestHandler
import kt.game.requests.WhisperRequestHandler
import kt.game.domain.creatures.characters.Character
import kt.game.events.LoggedEvent
import kt.game.events.NullEvent
import kt.game.events.StatusEvent
import kt.game.events.CombatEvent
import kt.game.events.RollEvent
import kt.game.events.InfoEvent
import kt.game.events.InfoEvents.NowExitingEvent

class Connection(playerRegistry: ActorRef, builder: ActorRef, clientSocket: Socket) extends Actor {
  import Connection._
  import context._

  setReceiveTimeout(5 minutes)

  var player: ActorRef = null
  val msgQueue = new ArrayBlockingQueue[LoggedEvent](40)

  var in: BufferedReader = null
  var out: OutputStream = null
  var feedbackBuffer: ActorRef = null
  self ! Initialize(clientSocket)

  def startUpMessage = welcome

  def print(message: String): Unit = out.print(message);

  def initialize(in: BufferedReader, out: OutputStream) = {
    this.in = in
    this.out = out
    out.print("Hit ENTER to continue ...\r\n")
    in.readLine();
    out.print(startUpMessage)
    feedbackBuffer = context.actorOf(FeedbackBuffer.props(250 millisecond), "feedbackBuffer")
    if (newPlayer()) {
      player ! Player.KeepAlive()
      system.scheduler.scheduleOnce(1 second, self, Player.KeepAlive())
      this.out.print("\r\n\r\n")
      this.out.write(PROMPT)
      this.out.flush()
      system.scheduler.scheduleOnce(100 milliseconds, self, Input())
    }
  }

  def newPlayer(): Boolean = {
    implicit val timeout = Timeout(5 seconds)
    val future = playerRegistry ? PlayerRegistry.LogInPlayer(feedbackBuffer, builder, clientSocket.getRemoteSocketAddress.toString)
    player = Await.result(future, timeout.duration).asInstanceOf[ActorRef]
    true
  }

  def startReadingInput(): Unit = {
    this.out.write(PROMPT)
    system.scheduler.scheduleOnce(100 milliseconds, self, Input())
  }

  var ratelimiter: Deadline = Deadline.now

  def filterInput(input: String) = new String(input.getBytes.filter { b => 127 > b && b > 31 }.dropWhile { x => x < 65 || x > 122 || (x > 90 && x < 97) }).trim

  def readInput() {
    if (ratelimiter.isOverdue && in.ready()) {
      val line = in.readLine()
      val fiteredLine = filterInput(line)
      println("cmd: (" + new Date() + ") { " + self.path.name + " } --> [" + fiteredLine + "]")
      if (fiteredLine != "") ratelimiter = (250 milliseconds).fromNow
      self ! KeepAlive()
      handle(fiteredLine)
    }
    system.scheduler.scheduleOnce(100 milliseconds, self, Input())
  }

  def handle(input: String) = input match {
    case ""                           => player ! FeedbackBuffer.Feedback(LoggedEvent(NullEvent))
    case "exit"                       => predestroy()
    case listPattern(ordering)        => system.actorOf(ListRequestHandler.props(player, playerRegistry, ordering))
    case tellAllPattern(text)         => system.actorOf(TellAllRequestHandler.props(player, playerRegistry, text))
    case tellInAreaPattern(text)      => system.actorOf(ShoutRequestHandler.props(player, playerRegistry, text))
    case whisperPattern(target, text) => system.actorOf(WhisperRequestHandler.props(player, playerRegistry, text, target))
    case replyPattern(text)           => system.actorOf(ReplyRequestHandler.props(player, text))
    case newCharPattern(a, b)         => player ! Player.CreateNewCharacter(a, b)
    case loginCharPattern(a, b)       => player ! Player.Control(a, b)
    case logoutCharPattern()          => player ! Player.Exit()
    case retirePattern(a)             => player ! Player.RetireCommand(a)
    case levelUpCharPattern(a)        => player ! Character.LevelUpCommand(a)
    case hallOfFamePattern()          => player ! Player.HallOfFameCommand()
    case _                            => Commands.sendCommand(self, player, input)
  }

  def readFeedBack(feedback: Seq[LoggedEvent]) = {
    out.print("\r\n")
    val groupedMsgs = feedback.sorted.groupBy { e => e.event.priority }
    for (b <- groupedMsgs.toSeq.sortBy(_._1)) {
      for (x <- b._2) out.print(x.event.toString + "\r\n")
      out.print("\r\n")
    }
    out.write(PROMPT)
    out.flush()
  }

  def predestroy() = {
    if (player != null) {
      player ! Player.Exit()
      system.scheduler.scheduleOnce(100 milliseconds, player, FeedbackBuffer.Feedback(LoggedEvent(NowExitingEvent)))
    }
    system.scheduler.scheduleOnce(1000 milliseconds, self, Selfdestruct())
  }

  def destroy() = {
    in.close()
    out.close()
    self ! PoisonPill
  }

  def receive = {
    case Initialize(socket)  => initialize(socket.getInputStream(), socket.getOutputStream())
    case Feedback(feedback)  => readFeedBack(feedback)
    case Selfdestruct()      => destroy()
    case ReceiveTimeout      => predestroy()
    case Input()             => readInput()
    case KeepAlive()         =>
    case x: Player.KeepAlive => { system.scheduler.scheduleOnce(1 second, self, Player.KeepAlive()); player forward x }
  }
}

object Connection {
  implicit class OutConversion(out: OutputStream) {
    def print(arg: String) = {
      out.write(arg.getBytes)
    }
  }
  implicit def inputStreamWrapper(in: InputStream) = new BufferedReader(new InputStreamReader(in))
  implicit val logOrder = LoggedEvent.orderingByLex

  val PROMPT: Array[Byte] = Array.concat(":::>>".getBytes, Array(0xff.toByte, 0xF9.toByte))
  val commandPatternWithText = "(.*?) (.*)".r
  val tellAllPattern = "(?:all|a) (.*)".r
  val tellInAreaPattern = "(?:shout|s) (.*)".r
  val whisperPattern = "(?:whisper|tell|w|t) (.*?) (.*)".r
  val replyPattern = "(?:reply|r) (.*)".r
  val listPattern = "list (name|level|time)".r
  val newCharPattern = "(?i)new (.*?) (.*)".r
  val loginCharPattern = "(?i)login (.*?) (.*)".r
  val logoutCharPattern = "(?i)logout".r
  val levelUpCharPattern = "(?i)level (.*{4})".r
  val retirePattern = "(?i)retire (.+)".r
  val hallOfFamePattern = "(?i)halloffame".r

  val welcome = "\r\n\r\n     ============ Welcome to Kriton's Dark Forest! ============ \r\n\r\n" +
    "Young Hero, gather your Strength and courage and enter the Dark Forest.\r\n" +
    "Gain knowledge of the forest's arcane secrets and kill the strange beasts that haunt it!\r\n" +
    "Discover the crafting recipes and find the ingredients to craft legendary weapons!\r\n" +
    "When you feel powerfull enough, retire from the Forest and enter its Hall Of Fame!\r\n\r\n" +
    "     ========================================================== \r\n\r\n" +
    "--> To create a new character type: new <character's name> <password>\r\n\r\n" +
    "--> To login with an existing character type: login <character's name> <password>\r\n\r\n" +
    "--> If you get bored, to exit the game type: exit\r\n\r\n" +
    "--> For the full list of game commands, type: help\r\n\r\n"

  def props(playerRegistry: ActorRef, builder: ActorRef, clientSocket: Socket) = Props(new Connection(playerRegistry, builder, clientSocket))
  case class Initialize(socket: Socket)
  case class Selfdestruct()
  case class KeepAlive()
  case class Feedback(messages: Seq[LoggedEvent]) extends NotInfluenceReceiveTimeout
  case class Input() extends NotInfluenceReceiveTimeout
}
